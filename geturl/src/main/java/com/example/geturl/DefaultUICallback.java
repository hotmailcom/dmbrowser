package com.example.geturl;

import android.view.View;

import java.util.List;

public class DefaultUICallback implements  com.example.geturl.SniffingUICallback {

    @Override
    public void onSniffingStart(View webView, String url) {
    }

    @Override
    public void onSniffingFinish(View webView, String url) {
    }

    @Override
    public void onSniffingSuccess(View webView, String url, List< com.example.geturl.SniffingVideo> videos) {
    }

    @Override
    public void onSniffingError(View webView, String url, int errorCode) {
    }

}

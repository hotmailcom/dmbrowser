package com.example.geturl;

import android.view.View;

public class DefaultFilter implements  com.example.geturl.SniffingFilter {

    @Override
    public  com.example.geturl.SniffingVideo onFilter(View webView, String url) {
        for (String type : DEFAULT_TYPE) {
            if (url.contains(type)) {
                if(url.contains("=")){
                    String[] split = url.split("=");
                    if(split[1].startsWith("http") && split[1].lastIndexOf(type) == split[1].length() - type.length()){
                        url = split[1];
                    }
                }
                Object[] content =  com.example.geturl.Util.getContent(url);
                int len = (int) content[0];
                String ctp = (String) content[1];
                if(ctp.toLowerCase().contains("video") || ctp.toLowerCase().contains("mpegurl")){
                    return new com.example.geturl.SniffingVideo(url, type, len, ctp);
                }
            }
        }
        return null;
    }

}
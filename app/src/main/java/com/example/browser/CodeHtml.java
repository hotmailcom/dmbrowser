package com.example.browser; /**
 * 网页操作相关类
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author winddack
 *
 */
//https://blog.csdn.net/qq_41251963/article/details/82215721
public class CodeHtml {
    static String GetHtml(String url, String encoding) {
        StringBuffer buffer = new StringBuffer();
        InputStreamReader isr = null;

        try {
            // 建立网络连接
            URL urlObj = new URL(url);
            // 打开网络连接
            URLConnection uc = urlObj.openConnection();
            /*
             * io 流
             * 从服务器下载源码到本地
             * */
            isr = new InputStreamReader(uc.getInputStream(), encoding);//建立文件的输入流
            BufferedReader reader = new BufferedReader(isr);//缓冲

            String line = null;
            while ((line = reader.readLine()) != null) {

                buffer.append(line + "\n");

            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != isr) isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return buffer.toString();

    }
}


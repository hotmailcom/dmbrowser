package com.example.browser;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.graphics.Bitmap;

import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;


import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;


import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.example.browser.happylook.PicView;
import com.example.browser.util.BitmapUtil;
import com.example.browser.zxing.android.CaptureActivity;
import com.example.browser.zxing.decode.RGBLuminanceSource;
import com.example.geturl.SniffingUICallback;
import com.example.geturl.SniffingVideo;
import com.example.geturl.web.SniffingUtil;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;

import java.io.IOException;
import java.io.InputStream;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;



public class webview extends BaseActivity1 implements SniffingUICallback {
    private ProgressDialog dialog;
    private ProgressBar pb;
    private LinearLayout back,backweb;
    private LinearLayout go,historyDB,buttom,search_view;

    private String design;
    private String htmlData;

    private ArrayList arrayList=new ArrayList();
    private boolean if_exist;
    private String get_title,get_url;//暂存从数据库得到的title和url
    private int get_id; //暂存从数据库得到的id
    private String titler, url;//按值查找详细信息
    private LinearLayout open_list;
    Dialog source_dialog;
    ListView lv;
    private ImageView home_search;
    private Button search_bt;

    private ImageView back_img;
    private ImageView go_img;
    private LinearLayout list_home,ll_top;
    private final String SEARCH="搜索",TRANSLATE="翻译";
    private TextView close;
    private EditText txtTitle;
    private String  prefix=null,keyword;
    int id; //按值查找id
    //监听某个activity是否在运行
    public static webview instance= null;
    //在activity关闭另外一个activity http://blog.sina.com.cn/s/blog_6e334dc701018m2v.html

    //识别二维码
    private Bitmap scanBitmap;
    private ProgressDialog mProgress;

    private static final String FILENAME="YX";
    private MyDatabaseHelper dbHelper;
    private static final String TABLENAME = "bookmarkDB";

    private MyWebView mWebView;
    private FrameLayout frameLayout,top;
    private IntentFilter intentFilter;
    private NetworkChangeReceiver networkChangeReceiver;
    private TextView add_text,error_reload;
    private RelativeLayout errorView,view_top;
    private Boolean errorvs=false;

    //https://www.jianshu.com/p/ec23fe80d5a3
    //视图View
    private View mCustomView;
    EditText et_key;
    TextView tv_position;
    Button btn_pre;
    Button btn_next;
    String nightCode;
    SharedPreferences share;
    //WebView文件上传
    private static final int REQUEST_CODE_CHOOSE = 23;
    private ValueCallback<Uri[]> uploadMessage;
    //二维码 https://www.cnblogs.com/xch-yang/p/9418493.html
    private ImageView home_qr;
    private static final String DECODED_CONTENT_KEY = "codedContent";
    private static final String DECODED_BITMAP_KEY = "codedBitmap";
    private static final int REQUEST_CODE_SCAN = 3;

    private FrameLayout mFrameLayout;

    private long exitTime = 0;
    private String com_vipsp="dmbrowser";
    private LinearLayout txt_app;
    private TextView codeView;
    private Button txt_close;
    private ImageView home;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.wedview);
        dialog = new ProgressDialog(this);
        dialog.setTitle("正在处理....");
        getWindow().setBackgroundDrawable(null);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);// 显示状态栏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 透明状态栏
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // 透明导航栏
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        home=(ImageView)findViewById(R.id.home);
        txt_close=(Button)findViewById(R.id.txt_close);
        codeView=(TextView)findViewById(R.id.codeView);
        txt_app=(LinearLayout)findViewById(R.id.txt_app) ;
        txtTitle = (EditText) findViewById(R.id.txtTitle);
        list_home = (LinearLayout) findViewById(R.id.list_home);
//获取主页输入框内容
        Intent it=super.getIntent();

        String searchText=it.getStringExtra("searchText");
        com_vipsp=it.getStringExtra("vipspcom");
//监听某个activity是否在运行
        instance = this;
        //在activity关闭另外一个activity http://blog.sina.com.cn/s/blog_6e334dc701018m2v.html

//加载进度
        pb = (ProgressBar) findViewById(R.id.pb);
        pb.setMax(100);

        back_img = (ImageView) findViewById(R.id.back_img);
        go_img = (ImageView) findViewById(R.id.go_img);

        top = findViewById(R.id.top);
        buttom = (LinearLayout)findViewById(R.id.buttom);
        back = (LinearLayout)findViewById(R.id.back);
        search_view = (LinearLayout)findViewById(R.id.search_view);
        go = (LinearLayout)findViewById(R.id.go);
        historyDB=(LinearLayout)findViewById(R.id.historyDB);
        share= getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
        close = (TextView) findViewById(R.id.close);
        et_key = (EditText) findViewById(R.id.et_key);
        tv_position = (TextView) findViewById(R.id.tv_position);
        btn_pre = (Button) findViewById(R.id.btn_pre);
        btn_next = (Button) findViewById(R.id.btn_next);
        ll_top=(LinearLayout)findViewById(R.id.ll_top) ;
        errorView=(RelativeLayout)findViewById(R.id.error);
        view_top=(RelativeLayout)findViewById(R.id.view_top);
        error_reload=(TextView)findViewById(R.id.error_reload);
        home_search=(ImageView) findViewById(R.id.home_search);
        search_bt = (Button)findViewById(R.id.search_bt) ;
        open_list = (LinearLayout) findViewById(R.id.open_list);
        home_qr =(ImageView) findViewById(R.id.home_qr);




//夜间模式
        InputStream is = webview.this.getResources().openRawResource(R.raw.night);
        byte[] buffer = new byte[0];
        try {
            buffer = new byte[is.available()];
            is.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        nightCode = Base64.encodeToString(buffer, Base64.NO_WRAP);



        String searchhistory = share.getString("searchurl", "https://www.baidu.com/s?wd=");
        if(searchhistory.equals("https://www.baidu.com/s?wd=")){
            webview.this.home_search.setImageResource(R.drawable.baidu);
        }else if(searchhistory.equals("https://www.sogou.com/web?query=")){
            webview.this.home_search.setImageResource(R.drawable.sougou);
        }else if(searchhistory.equals("http://www.google.com.tw/search?q=")){
            webview.this.home_search.setImageResource(R.drawable.chrome);
        }else if(searchhistory.equals("http://cn.bing.com/search?q=")){
            webview.this.home_search.setImageResource(R.drawable.bing);
        }else if(searchhistory.equals("https://quark.sm.cn/s?q=")){
            webview.this.home_search.setImageResource(R.drawable.quak);
        }else {
            webview.this.home_search.setImageResource(R.drawable.iconyx);
        }
        //原文链接：https://blog.csdn.net/qq_43319748/article/details/106641138,问题：动态改变布局
        SoftKeyBoardListener.setListener(this, new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
            @Override
            public void keyBoardShow(int height) {
                txtTitle.setText(mWebView.getUrl());
                txtTitle.selectAll();
            }
            @Override
            public void keyBoardHide(int height) {
                txtTitle.setText(mWebView.getUrl());
                String content=txtTitle.getText().toString();
                txtTitle.setSelection(content.length());//光标定位到后面
            }
        });

//动态创建webview
        frameLayout = findViewById(R.id.web_frame);
        mWebView = new MyWebView(this);
        initView(searchText);
        initListener();
        initEvent();
        resourcedialog();//资源
// //https://www.jianshu.com/p/f61849073bad 网络提醒
        intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        networkChangeReceiver = new NetworkChangeReceiver();
        registerReceiver(networkChangeReceiver, intentFilter);

//Android-应用被作为第三方浏览器打开 https://blog.csdn.net/jingbin_/article/details/88349746
        // 处理 作为三方浏览器打开传过来的值



    }



    private void night_pattem(){
        if(share.getString("night", "夜间模式关闭").equals("夜间模式开启")){
            buttom.setBackgroundColor(Color.rgb(		105,105,105));
            top.setBackgroundColor(Color.rgb(0,0,0));
            mWebView.setBackgroundColor(Color.rgb(0,0,0));
            search_view.setBackgroundColor(Color.rgb(		105,105,105));
            mWebView.loadUrl("javascript:(function() {" + "var parent = document.getElementsByTagName('head').item(0);" + "var style = document.createElement('style');" + "style.type = 'text/css';" + "style.innerHTML = window.atob('" + nightCode + "');" + "parent.appendChild(style)" + "})();");
        }else {
            buttom.setBackgroundColor(Color.rgb(245,245,245));

            mWebView.setBackgroundColor(Color.rgb(255,255,255));
            search_view.setBackgroundColor(Color.rgb(245,245,245));
            top.setBackgroundColor(Color.rgb(192,192,192));
        }
    }
    private void night(){
        if(share.getString("night", "夜间模式关闭").equals("夜间模式开启")){
            buttom.setBackgroundColor(Color.rgb(		105,105,105));
            top.setBackgroundColor(Color.rgb(0,0,0));
            mWebView.setBackgroundColor(Color.rgb(0,0,0));
            search_view.setBackgroundColor(Color.rgb(		105,105,105));
        }else {
            buttom.setBackgroundColor(Color.rgb(245,245,245));
            mWebView.setBackgroundColor(Color.rgb(255,255,255));
            search_view.setBackgroundColor(Color.rgb(245,245,245));
            top.setBackgroundColor(Color.rgb(192,192,192));
        }
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    private void initView(String url) {
        frameLayout.addView(mWebView);
        night();
        mWebView.setMenuText(SEARCH,"无痕复制");
        mWebView.setActionSelectListener(new MyWebView.ActionSelectListener() {
            @Override
            public void onClick(String value, String title) {
                Log.d("value",value);
                //Toast.makeText(getApplicationContext(), "title is:" + title + " value is：" + value, Toast.LENGTH_LONG).show();
                switch (value) {
                    case TRANSLATE:
                        Toast.makeText(webview.this, "成功", Toast.LENGTH_SHORT).show();
                        break;
                    case SEARCH:
                        Intent intent = new Intent(webview.this, webview.class);//没有任何参数（意图），只是用来传递数据
                        intent.putExtra("searchText", share.getString("searchurl", "https://www.baidu.com/s?wd=") + title);
                        startActivity(intent);
                        break;
                    case "无痕复制":
                        cv(title);
                        break;
                }
            }
        });


        WebSettings webSettings = mWebView.getSettings();
        //保存密码功能 出处：https://zhuanlan.zhihu.com/p/190219649
        //webSettings.setSavePassword(true);
        //优化加载图片 出处：https://zhuanlan.zhihu.com/p/190219649
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            //设置网页在加载的时候暂时不加载图片
            mWebView.getSettings().setLoadsImagesAutomatically(true);
        } else {
            mWebView.getSettings().setLoadsImagesAutomatically(false);
        }

        webSettings.setDefaultTextEncodingName("UTF-8");//设置字符编码
        //listview,webview中滚动拖动到顶部或者底部时的阴影(滑动到项部或底部不固定).setOverScrollMode(View.OVER_SCROLL_NEVER);
        mWebView.setOverScrollMode(View.OVER_SCROLL_NEVER);
//        //取消滚动条
//        this.setScrollBarStyle(SCROLLBARS_OUTSIDE_OVERLAY);
        // 设置WebView属性，能够执行Javascript脚本
        webSettings.setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(new JS(), "android");



        //触摸焦点起作用
        mWebView.requestFocus();

//支持获取手势焦点，输入用户名、密码或其他
        mWebView.requestFocusFromTouch();
//不显示滚动条
        //mWebView.setScrollBarStyle(0);

//密码与cookie
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
            webSettings.setSavePassword(true);
            webSettings.setSaveFormData(true);


        //外部JavaScript攻击 https://www.jianshu.com/p/3e0136c9e748/
        webSettings.setAllowFileAccess(false);
        webSettings.setAllowFileAccessFromFileURLs(false);
        webSettings.setAllowUniversalAccessFromFileURLs(false);


        webSettings.setNeedInitialFocus(true); //当webview调用requestFocus时为webview设置节点

        // 让JavaScript可以自动打开windows
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);//支持通过JS打开新窗口

        // webSettings.supportMultipleWindows();  //多窗口

        //设置缓存模式
        webSettings.setAppCacheEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setDomStorageEnabled(true);

        // 开启DOM storage API 功能
        webSettings.setDomStorageEnabled(true);
        //获取位置
        webSettings.setGeolocationEnabled(true);

        // 开启database storage API功能
        webSettings.setDatabaseEnabled(true);
        //String cacheDirPath = getCacheDir().getAbsolutePath();
        String cacheDirPath = getCacheDir().getAbsolutePath();
        //设置数据库缓存路径
        mWebView.getSettings().setGeolocationDatabasePath(cacheDirPath);
        //设置  Application Caches 缓存目录
        mWebView.getSettings().setAppCachePath(cacheDirPath);
        //开启 Application Caches 功能
        webSettings.setAppCacheEnabled(true);

        //网页缩放
        //但是在部分系统版本中，如果在缩放按钮消失前退出了Activity，就可能引起应用崩溃。
        webSettings.setDisplayZoomControls(false);//隐藏原生的缩放控件

        webSettings.setSupportZoom(true);//支持缩放，默认为true。是下面那个的前提。
        webSettings.setBuiltInZoomControls(true);// 设置WebView可触摸放大缩小 //设置内置的缩放控件。若上面是false，则该WebView不可缩放，这个不管设置什么都不能缩放。
        webSettings.setUseWideViewPort(true);
/*缩放开关，设置此属性，仅支持双击缩放，不支持触摸缩放
      mWebView.getSettings().setSupportZoom(true);
设置是否可缩放，会出现缩放工具（若为true则上面的设值也默认为true）
      mWebView.getSettings().setBuiltInZoomControls(true);
隐藏缩放工具
     mWebView.getSettings().setDisplayZoomControls(false);
*/

//设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小

        // 支持内容重新布局,一共有四种方式
        // 默认的是NARROW_COLUMNS
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        // 设置默认字体大小
        webSettings.setTextZoom(Integer.parseInt(share.getString("textsize", "100")));

//WebView硬件加速导致页面渲染闪烁出处：https://zhuanlan.zhihu.com/p/190219649
//关闭硬件加速
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
//开启硬件加速
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
// 自动播放视频 https://blog.csdn.net/connor__ak/article/details/81086085
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        //解决一些图片加载问题
        //出处：https://zhuanlan.zhihu.com/p/190219649
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(webSettings.getMixedContentMode());
        }
        webSettings.setBlockNetworkImage(false);



        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (mCustomView == null) {//https://www.jianshu.com/p/b9de6714482c,监听全屏
                    delete_all();
                }
                pb.setVisibility(View.VISIBLE);
                if(share.getString("prefix","自动前缀【关闭】").equals("自动前缀【开启】")) {
                    prefixquery();
                }

                night_pattem();//夜间模式
                js_domain_name(getDomainName(mWebView.getUrl()));
                js_all();
                mWebView.getSettings().getAllowContentAccess();
                mWebView.getSettings().setUserAgentString(share.getString("UA", "Mozilla/5.0 (Linux; U; Android 11; zh-cn; KB2000 Build/RP1A.201005.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/40.7.25.1"));

                SharedPreferences.Editor edit=share.edit();
                edit.putString("historyWeb",mWebView.getUrl());
                edit.commit();
                jsad(getDomainName(mWebView.getUrl()));

            }
            @Override
            public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {
                //https://blog.csdn.net/toast_tips/article/details/61916377
                if(!url.startsWith("http"))
                {

                    Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                    boolean isInstall = getPackageManager().queryIntentActivities(intent,PackageManager.MATCH_DEFAULT_ONLY).size()>0;
                    if(isInstall&&share.getString("openApp", "允许网站打开app【关闭】").equals("允许网站打开app【开启】"))
                    {
                        startActivity(intent);
                    }

                        return true;


                }else {
                    return false;
                }




            }
            @Override
            public void onPageFinished(android.webkit.WebView view, String url) {
                super.onPageFinished(view, url);
                if(com_vipsp!=null){
                    mWebView.loadUrl("javascript:"+"/*\n" +
                            " * @name: 视频解析\n" +
                            " * @Author: 就像wo\n" +
                            " * @version: 3.0\n" +
                            " * @description: 视频解析，免费看VIP视频\n" +
                            " * @include: *\n" +
                            " * @createTime: 2019-12-22\n" +
                            " * @updateTime: 2019-12-24\n" +
                            "*/\n" +
                            "(function(){\n" +
                            "    var videoParseKey=encodeURIComponent(\"就像wo:视频解析\");\n" +
                            "    if(window.videoParseKey==true){\n" +
                            "        return 0;\n" +
                            "    }\n" +
                            "    window.videoParseKey=true;\n" +
                            "    var videoParseUrl=[\n" +
                            "        [\""+share.getString("解析接口1", "https://z1.m1907.cn/?jx=")+"\","+"\"m1907\"],\n" +
                            "        [\""+share.getString("解析接口2", "https://jx.618g.com/?url=")+"\","+"\"618\"],\n" +
                            "        [\""+share.getString("解析接口3", "https://www.1717yun.com/jx/ty.php?url=")+"\","+"\"1717云\"],\n" +
                            "        [\""+share.getString("解析接口4", "https://video.isyour.love/player/getplayer?url=")+"\","+"\"云曦解析1\"],\n" +
                            "        [\""+share.getString("解析接口5", "https://okjx.cc/?url=")+"\","+"\"ok解析\"],\n" +
                            "        [\""+share.getString("解析接口6", "https://jx.xmflv.com/?url=")+"\","+"\"云曦解析2\"],\n" +
                            "        [\""+share.getString("解析接口7", "https://cdn.yangju.vip/k/?url=")+"\","+"\"云曦解析3\"],\n" +
                            "        [\""+share.getString("解析接口8", "https://www.administratorw.com/video.php?url=")+"\","+"\"云曦解析4\"],\n" +

                            "    ];\n" +
                            "    var videoParseCode=\"dmFyJTIwbWVkaWFQYXJzZURvY0JvZHk9KGRvY3VtZW50LmJvZHk9PW51bGw/ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50OmRvY3VtZW50LmJvZHkpO3ZhciUyMG1lZGlhUGFyc2VTaXplPSU3QiUyMnBoMnBjJTIyOmlubmVyV2lkdGgvMzYwLCUyMmZvbnQlMjI6MjQsJTIyYnRuJTIyOjEyJTdEO3ZhciUyMHZpZGVvUGFyc2VCdG49ZG9jdW1lbnQuY3JlYXRlRWxlbWVudCglMjJkaXYlMjIpO3ZpZGVvUGFyc2VCdG4uc3R5bGU9JTIycG9zaXRpb246Zml4ZWQ7ei1pbmRleDo5OTk5OTk5OTk5O3JpZ2h0OjIwJTI1O2JvdHRvbToxNSUyNTtib3gtc2l6aW5nOmJvcmRlci1ib3g7b3BhY2l0eToxO2JhY2tncm91bmQtc2l6ZTphdXRvJTIwMTAwJTI1O2JhY2tncm91bmQtcmVwZWF0Om5vLXJlcGVhdDtiYWNrZ3JvdW5kLXBvc2l0aW9uOmNlbnRlciUyMjt2aWRlb1BhcnNlQnRuLnN0eWxlLmJhY2tncm91bmRDb2xvcj0lMjJyZ2JhKDg4LDE0MSwxMzQsMC44KSUyMjt2aWRlb1BhcnNlQnRuLnN0eWxlLmJhY2tncm91bmRJbWFnZT0lMjJ1cmwoJTVDJTIyaHR0cHM6Ly9zczAuYmRzdGF0aWMuY29tLzcwY0Z1SFNoX1ExWW54R2twb1dLMUhGNmhoeS9pdC91PTQxMzIxMjA1OTAsMzg3NDM4MjIwNCZmbT0yNiZncD0wLmpwZyU1QyUyMiklMjI7dmlkZW9QYXJzZUJ0bi5zdHlsZS5ib3hTaGFkb3c9JTIyMHB4JTIwMHB4JTIwMTBweCUyMDVweCUyMHJnYmEoODgsMTQxLDEzNCwxKSUyMjt2aWRlb1BhcnNlQnRuLnN0eWxlLndpZHRoPW1lZGlhUGFyc2VTaXplLmJ0bislMjIlMjUlMjI7dmlkZW9QYXJzZUJ0bi5zdHlsZS5oZWlnaHQ9KGlubmVyV2lkdGgvaW5uZXJIZWlnaHQqbWVkaWFQYXJzZVNpemUuYnRuKSslMjIlMjUlMjI7dmlkZW9QYXJzZUJ0bi5zdHlsZS5ib3JkZXJSYWRpdXM9JTIyMTAwJTI1JTIyO3ZpZGVvUGFyc2VCdG4uYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMix2aWRlb1BhcnNlQ2xpY2spO3dpbmRvdy5hZGRFdmVudExpc3RlbmVyKCUyMnJlc2l6ZSUyMixmdW5jdGlvbigpJTdCdmlkZW9QYXJzZUJ0bi5zdHlsZS5oZWlnaHQ9aW5uZXJXaWR0aC9pbm5lckhlaWdodCptZWRpYVBhcnNlU2l6ZS5idG4rJTIyJTI1JTIyOyU3RCk7dmFyJTIwdmlkZW9QYXJzZUhpZFRpbWVJRDt2YXIlMjB2aWRlb1BhcnNlSGlkVGltZT0yMDt2YXIlMjB2aWRlb1BhcnNlQnRuT3BhY2l0eT0wLjE7aWYoZG9jdW1lbnQucmVhZHlTdGF0ZT09JTIyY29tcGxldGUlMjIpJTdCbWVkaWFTZWFyY2hWaWRlbygpOyU3RGVsc2UlN0J3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciglMjJsb2FkJTIyLGZ1bmN0aW9uKCklN0JtZWRpYVNlYXJjaFZpZGVvKCk7JTdEKTslN0RmdW5jdGlvbiUyMG1lZGlhU2VhcmNoVmlkZW8oKSU3QnZhciUyMHZpZGVvcz1kb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSglMjJWSURFTyUyMik7aWYodmlkZW9zLmxlbmd0aCUzRTApJTdCbWVkaWFQYXJzZURvY0JvZHkuYXBwZW5kQ2hpbGQodmlkZW9QYXJzZUJ0bik7JTdEZWxzZSU3QnNldFRpbWVvdXQobWVkaWFTZWFyY2hWaWRlbywxMDAwKTslN0QlN0RmdW5jdGlvbiUyMHZpZGVvUGFyc2VDbGljaygpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7aWYodmlkZW9QYXJzZUJ0bi5zdHlsZS5vcGFjaXR5PT12aWRlb1BhcnNlQnRuT3BhY2l0eSklN0J2aWRlb1BhcnNlQnRuLnN0eWxlLm9wYWNpdHk9MTt2aWRlb1BhcnNlSGlkVGltZUlEPXNldFRpbWVvdXQoZnVuY3Rpb24oKSU3QnZpZGVvUGFyc2VCdG4uc3R5bGUub3BhY2l0eT12aWRlb1BhcnNlQnRuT3BhY2l0eTslN0QsdmlkZW9QYXJzZUhpZFRpbWUqMTAwMCk7cmV0dXJuJTIwMDslN0RpZihtZWRpYVBhcnNlRG9jQm9keS5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbih2aWRlb1BhcnNlQmdkKSUyNTI9PTApJTdCbWVkaWFQYXJzZShmYWxzZSk7JTdEZWxzZSU3Qm1lZGlhUGFyc2UodHJ1ZSk7JTdEJTdEdmFyJTIwdmlkZW9QYXJzZUJnZD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMmRpdiUyMik7dmFyJTIwdmlkZW9QYXJzZURpdj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMmRpdiUyMik7dmFyJTIwdmlkZW9QYXJzZVVybERpdj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMmRpdiUyMik7dmlkZW9QYXJzZUJnZC5zdHlsZT0lMjJwb3NpdGlvbjpmaXhlZDt6LWluZGV4Ojk5OTk5OTk5OTt0b3A6MHB4O2xlZnQ6MHB4O2JveC1zaXppbmc6Ym9yZGVyLWJveDt3aWR0aDoxMDAlMjU7aGVpZ2h0OjEwMCUyNTtvdmVyZmxvdzphdXRvJTIyO3ZhciUyMHZpZGVvUGFyc2VTY3JvbGxUb3A9MDt2aWRlb1BhcnNlQmdkLmFkZEV2ZW50TGlzdGVuZXIoJTIyY2xpY2slMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO21lZGlhUGFyc2UoZmFsc2UpOyU3RCk7dmlkZW9QYXJzZURpdi5zdHlsZT0lMjJwb3NpdGlvbjpyZWxhdGl2ZTt0b3A6NjUlMjU7bGVmdDoxMCUyNTtib3gtc2l6aW5nOmluaGVyaXQ7d2lkdGg6ODAlMjU7JTIyO3ZpZGVvUGFyc2VEaXYuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7JTdEKTt2aWRlb1BhcnNlVXJsRGl2LnN0eWxlPSUyMmJveC1zaXppbmc6aW5oZXJpdDt3aWR0aDoxMDAlMjU7cGFkZGluZzo1JTI1O21hcmdpbjowcHg7Zm9udC13ZWlnaHQ6Ym9sZDt0ZXh0LWFsaWduOmNlbnRlcjtjb2xvcjojOWJhZmNjO2JhY2tncm91bmQtY29sb3I6IzI2MmYzZDtib3gtc2hhZG93OjBweCUyMDBweCUyMDEwcHglMjAjMDAwMDAwOyUyMjt2aWRlb1BhcnNlVXJsRGl2LnN0eWxlLmJvcmRlclJhZGl1cz1pbm5lcldpZHRoKjUvMTAwKyUyMnB4JTIyO3ZpZGVvUGFyc2VVcmxEaXYuc3R5bGUuZm9udFNpemU9bWVkaWFQYXJzZVNpemUucGgycGMqbWVkaWFQYXJzZVNpemUuZm9udCslMjJweCUyMjtmb3IobGV0JTIwaT0wO2klM0N2aWRlb1BhcnNlVXJsLmxlbmd0aDtpKyspJTdCbWFrVmlkZW9VcmxEaXYoaSk7JTdEdmlkZW9QYXJzZUJnZC5hcHBlbmRDaGlsZCh2aWRlb1BhcnNlRGl2KTtmdW5jdGlvbiUyMG1lZGlhUGFyc2UoYm9vbCklN0JpZihib29sKSU3Qm1lZGlhUGFyc2VEb2NCb2R5LmFwcGVuZENoaWxkKHZpZGVvUGFyc2VCZ2QpO3ZpZGVvUGFyc2VCZ2Quc2Nyb2xsQnkoMCx2aWRlb1BhcnNlU2Nyb2xsVG9wKTslN0RlbHNlJTdCdmlkZW9QYXJzZVNjcm9sbFRvcD12aWRlb1BhcnNlQmdkLnNjcm9sbFRvcDttZWRpYVBhcnNlRG9jQm9keS5yZW1vdmVDaGlsZCh2aWRlb1BhcnNlQmdkKTslN0QlN0RmdW5jdGlvbiUyMG1ha1ZpZGVvVXJsRGl2KGlkKSU3QmlmKHZpZGVvUGFyc2VVcmwlNUJpZCU1RC5sZW5ndGg9PTIpJTdCbGV0JTIwdmRpdj12aWRlb1BhcnNlVXJsRGl2LmNsb25lTm9kZSgpO3ZkaXYudGl0bGU9dmlkZW9QYXJzZVVybCU1QmlkJTVEJTVCMCU1RDt2ZGl2LmlubmVySFRNTD12aWRlb1BhcnNlVXJsJTVCaWQlNUQlNUIxJTVEO3ZkaXYuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCd2luZG93Lm9wZW4odGhpcy50aXRsZStsb2NhdGlvbi5ocmVmKTslN0QpO3ZpZGVvUGFyc2VEaXYuYXBwZW5kQ2hpbGQodmRpdik7JTdEJTdE\";\n" +
                            "    var h5PlayerCode=\"KGZ1bmN0aW9uKCklN0J2YXIlMjBoNVBsYXllcktleT1lbmNvZGVVUklDb21wb25lbnQoJTIyJUU1JUIwJUIxJUU1JTgzJThGd286JUU4JUE3JTg2JUU5JUEyJTkxJUU2JTkyJUFEJUU2JTk0JUJFJUU1JTk5JUE4JTIyKTtpZih3aW5kb3cuaDVQbGF5ZXJLZXk9PXRydWUpJTdCcmV0dXJuJTIwMDslN0R3aW5kb3cuaDVQbGF5ZXJLZXk9dHJ1ZTt2YXIlMjBoNUNvbnRhaW5lcj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkRJViUyMik7dmFyJTIwaDVDb250cm9sc0JnZD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkRJViUyMik7dmFyJTIwaDVDb250cm9sc1RvcDt2YXIlMjBoNUJ1ZmZlckJhcj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkNBTlZBUyUyMik7dmFyJTIwaDVQcm9ncmVzc0Jhcjt2YXIlMjBoNVZpZGVvVGl0bGU7dmFyJTIwaDVQbGF5PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJTIyQ0FOVkFTJTIyKTt2YXIlMjBoNUNvbnRyb2xzPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJTIyRElWJTIyKTt2YXIlMjBoNUN1cnJlbnRUaW1lPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJTIyQlVUVE9OJTIyKTt2YXIlMjBoNUR1cmF0aW9uO3ZhciUyMGg1RXh0cmFWaWV3O3ZhciUyMGg1RnVsbHNjcmVlbj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkJVVFRPTiUyMik7dmFyJTIwaDVWb2x1bWVJbmM7dmFyJTIwaDVWb2x1bWVEZWM7aDVDb250YWluZXIuc3R5bGU9JTIycG9zaXRpb246YWJzb2x1dGU7ei1pbmRleDo5OTk5OTk5OTk5O2JveC1zaXppbmc6Ym9yZGVyLWJveDslMjI7aDVDb250cm9sc0JnZC5zdHlsZT0lMjJwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjBweDt0b3A6MHB4O3otaW5kZXg6OTk5OTk5OTk5OTtib3gtc2l6aW5nOmluaGVyaXQ7d2lkdGg6MTAwJTI1O2hlaWdodDoxMDAlMjU7JTIyO2g1QnVmZmVyQmFyLnN0eWxlPSUyMnBvc2l0aW9uOmFic29sdXRlO2xlZnQ6MHB4O3RvcDowcHg7ei1pbmRleDoxO2JveC1zaXppbmc6Ym9yZGVyLWJveDt3aWR0aDoxMDAlMjU7aGVpZ2h0OjEwJTI1OyUyMjtoNUJ1ZmZlckJhci53aWR0aD0zNjA7aDVCdWZmZXJCYXIuaGVpZ2h0PTMwO2Z1bmN0aW9uJTIwaDVQbGF5ZXJHZXRCdWZmZXIob2JqKSU3QnZhciUyMGN0eD1vYmouYnVmZmVyQmFyLmdldENvbnRleHQoJTIyMmQlMjIpO2N0eC5jbGVhclJlY3QoMCwwLDM2MCwzMCk7dmFyJTIwcmF0aW89MzYwL29iai52aWRlby5kdXJhdGlvbjt2YXIlMjByYW5nZT1vYmoudmlkZW8uYnVmZmVyZWQ7Y3R4LmJlZ2luUGF0aCgpO2Zvcih2YXIlMjBpPTA7aSUzQ3JhbmdlLmxlbmd0aDtpKyspJTdCY3R4Lm1vdmVUbyhNYXRoLmZsb29yKHJhbmdlLnN0YXJ0KGkpKnJhdGlvKSwxNSk7Y3R4LmxpbmVUbyhNYXRoLmNlaWwocmFuZ2UuZW5kKGkpKnJhdGlvKSwxNSk7JTdEY3R4LnN0cm9rZSgpOyU3RGg1UHJvZ3Jlc3NCYXI9aDVCdWZmZXJCYXIuY2xvbmVOb2RlKCk7aDVQcm9ncmVzc0Jhci5zdHlsZS56SW5kZXg9JTIyMiUyMjtmdW5jdGlvbiUyMGg1UGxheWVyR2V0UHJvZ3Jlc3Mob2JqLHRpbWUpJTdCdmFyJTIwY3R4PW9iai5wcm9ncmVzc0Jhci5nZXRDb250ZXh0KCUyMjJkJTIyKTtjdHguY2xlYXJSZWN0KDAsMCwzNjAsMzApO3ZhciUyMHJhdGlvPTM2MC9vYmoudmlkZW8uZHVyYXRpb247Y3R4LmJlZ2luUGF0aCgpO2N0eC5tb3ZlVG8oMCwxNSk7Y3R4LmxpbmVUbyhNYXRoLmZsb29yKHRpbWUqcmF0aW8pLDE1KTtjdHguc3Ryb2tlKCk7JTdEaDVQbGF5LnN0eWxlPSUyMnBvc2l0aW9uOmFic29sdXRlOyUyMjtoNVBsYXkud2lkdGg9NjAwO2g1UGxheS5oZWlnaHQ9NjAwO2Z1bmN0aW9uJTIwaDVQbGF5ZXJHZXRQbGF5KGN0eCklN0JjdHguZmlsbFN0eWxlPSUyMmJsYWNrJTIyO3ZhciUyMHk9MzAwLU1hdGguc3FydCgzKSo1MDtjdHguYmVnaW5QYXRoKCk7Y3R4Lm1vdmVUbyg0MDAsMzAwKTtjdHgubGluZVRvKDI1MCx5KTtjdHgubGluZVRvKDI1MCw2MDAteSk7Y3R4LmNsb3NlUGF0aCgpO2N0eC5maWxsKCk7JTdEZnVuY3Rpb24lMjBoNVBsYXllckdldFBhdXNlKGN0eCklN0JjdHguZmlsbFN0eWxlPSUyMmJsYWNrJTIyO2N0eC5maWxsUmVjdCgyMjUsMjEwLDUwLDE4MCk7Y3R4LmZpbGxSZWN0KDMyNSwyMTAsNTAsMTgwKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyR2V0V2FpdGluZyhvYmopJTdCdmFyJTIwY3R4PW9iai5wbGF5LmdldENvbnRleHQoJTIyMmQlMjIpO2N0eC5yZXN0b3JlKCk7Y3R4LnJvdGF0ZShNYXRoLlBJLzI0KTtjdHguY2xlYXJSZWN0KC0zMDAsLTMwMCw2MDAsNjAwKTtjdHguYmVnaW5QYXRoKCk7Y3R4LmFyYygwLDAsMjUwLDAsTWF0aC5QSS8yKTtjdHguc3Ryb2tlKCk7Y3R4LnNhdmUoKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyUGxheUNsaWNrKG9iaiklN0J2YXIlMjBjdHg9b2JqLnBsYXkuZ2V0Q29udGV4dCglMjIyZCUyMik7Y3R4LmZpbGxTdHlsZT0lMjIjZmZmZmZmY2MlMjI7Y3R4LnJlc2V0VHJhbnNmb3JtKCk7Y3R4LmNsZWFyUmVjdCgwLDAsNjAwLDYwMCk7Y3R4LmJlZ2luUGF0aCgpO2N0eC5hcmMoMzAwLDMwMCwzMDAsMCwyKk1hdGguUEkpO2N0eC5maWxsKCk7aWYoIW9iai52aWRlby5wYXVzZWQpJTdCaDVQbGF5ZXJHZXRQYXVzZShjdHgpOyU3RGVsc2UlN0JoNVBsYXllckdldFBsYXkoY3R4KTslN0QlN0RoNUNvbnRyb2xzLnN0eWxlPSUyMnBvc2l0aW9uOmFic29sdXRlO2xlZnQ6MHB4O2JvdHRvbTowcHg7ei1pbmRleDo5OTk5OTk5OTk5O2JveC1zaXppbmc6aW5oZXJpdDtib3gtc2hhZG93OjBweCUyMDBweCUyMDEwcHglMjAjMDAwMDAwO3dpZHRoOjEwMCUyNTtwYWRkaW5nOjBweCUyMDIlMjU7bWFyZ2luOjBweDtiYWNrZ3JvdW5kLWNvbG9yOmJsYWNrOyUyMjtoNUNvbnRyb2xzVG9wPWg1Q29udHJvbHMuY2xvbmVOb2RlKCk7aDVDb250cm9sc1RvcC5zdHlsZS50b3A9JTIyMHB4JTIyO2g1Q29udHJvbHNUb3Auc3R5bGUucGFkZGluZz0lMjIwcHglMjI7aDVDdXJyZW50VGltZS5zdHlsZT0lMjJwb3NpdGlvbjpyZWxhdGl2ZTt6LWluZGV4OjA7ZmxvYXQ6bGVmdDtib3gtc2l6aW5nOmluaGVyaXQ7d2lkdGg6MTUlMjU7aGVpZ2h0OjEwMCUyNTtib3JkZXItc3R5bGU6bm9uZTttYXJnaW4tcmlnaHQ6MSUyNTtiYWNrZ3JvdW5kLWNvbG9yOiMwMDAwMDAwMDtvdmVyZmxvdzphdXRvO3RleHQtYWxpZ246Y2VudGVyO3doaXRlLXNwYWNlOm5vd3JhcDtmb250LXdlaWdodDpib2xkO2NvbG9yOndoaXRlOyUyMjtoNVZpZGVvVGl0bGU9aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtoNVZpZGVvVGl0bGUuc3R5bGUud2lkdGg9JTIyMTAwJTI1JTIyO2g1VmlkZW9UaXRsZS5zdHlsZS56SW5kZXg9JTIyMiUyMjtoNUR1cmF0aW9uPWg1Q3VycmVudFRpbWUuY2xvbmVOb2RlKCk7aDVFeHRyYVZpZXc9aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtoNUZ1bGxzY3JlZW49aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtoNUZ1bGxzY3JlZW4uc3R5bGUubWFyZ2luUmlnaHQ9JTIyMHB4JTIyO2g1RnVsbHNjcmVlbi5zdHlsZS5tYXJnaW5MZWZ0PSUyMjElMjUlMjI7aDVGdWxsc2NyZWVuLnN0eWxlLmNzc0Zsb2F0PSUyMnJpZ2h0JTIyO2g1RnVsbHNjcmVlbi5pbm5lckhUTUw9JTIyJUU1JTg1JUE4JUU1JUIxJThGJTIyO2Z1bmN0aW9uJTIwaDVQbGF5ZXJGdWxsc2NyZWVuQ2xpY2soY29udGFpbmVyKSU3QmlmKGRvY3VtZW50LmZ1bGxzY3JlZW5FbGVtZW50PT1udWxsKSU3QmNvbnRhaW5lci5yZXF1ZXN0RnVsbHNjcmVlbigpOyU3RGVsc2UlN0Jkb2N1bWVudC5leGl0RnVsbHNjcmVlbigpOyU3RCU3RGg1Vm9sdW1lSW5jPWg1RnVsbHNjcmVlbi5jbG9uZU5vZGUoKTtoNVZvbHVtZUluYy5pbm5lckhUTUw9JTIyJUU5JTlGJUIzJUU5JTg3JThGKyUyMjtoNVZvbHVtZURlYz1oNUZ1bGxzY3JlZW4uY2xvbmVOb2RlKCk7aDVWb2x1bWVEZWMuaW5uZXJIVE1MPSUyMiVFOSU5RiVCMyVFOSU4NyU4Ri0lMjI7dmFyJTIwaDVQbGF5ZXJWaWRlb0Fycj0lNUIlNUQ7dmFyJTIwaDVQbGF5ZXJJZnJhbWVBcnI9JTVCJTVEO3ZhciUyMGg1UGxheWVyVGltZUlEPSU1QiU1RDtpZihkb2N1bWVudC5yZWFkeVN0YXRlPT0lMjJjb21wbGV0ZSUyMiklN0JoNVBsYXllcihkb2N1bWVudCwwKTslN0RlbHNlJTdCd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJTIybG9hZCUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXIoZG9jdW1lbnQsMCk7JTdEKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyQ2xhc3MoKSU3QnZhciUyMG9iaj1uZXclMjBPYmplY3Q7b2JqLmhhdmVDb250cm9scztvYmoucGFyZW50WkluZGV4O29iai5wYXJlbnRQb3NpdGlvbjtvYmoudmlkZW9XaWR0aDtvYmoudmlkZW9IZWlnaHQ7b2JqLmZvcndhcmRUaW1lPTA7b2JqLmZvcndhcmRWb2x1bWU9MTtvYmouZm9yd2FyZFJhdGU9MTtvYmoudmlkZW9SYXRpbz0wO29iai5zdGF0ZT0lMjJwYXVzZWQlMjI7b2JqLmhpZGRlblRpbWVJRDtvYmoud2FpdFRpbWVJRDtvYmoucGFyZW50O29iai52aWRlbztvYmouY29udGFpbmVyPWg1Q29udGFpbmVyLmNsb25lTm9kZSgpO29iai5jb250cm9sc0JnZD1oNUNvbnRyb2xzQmdkLmNsb25lTm9kZSgpO29iai5jb250cm9sc1RvcD1oNUNvbnRyb2xzVG9wLmNsb25lTm9kZSgpO29iai5idWZmZXJCYXI9aDVCdWZmZXJCYXIuY2xvbmVOb2RlKCk7b2JqLnByb2dyZXNzQmFyPWg1UHJvZ3Jlc3NCYXIuY2xvbmVOb2RlKCk7b2JqLnZpZGVvVGl0bGU9aDVWaWRlb1RpdGxlLmNsb25lTm9kZSgpO29iai5wbGF5PWg1UGxheS5jbG9uZU5vZGUoKTtvYmouY29udHJvbHM9aDVDb250cm9scy5jbG9uZU5vZGUoKTtvYmouY3VycmVudFRpbWU9aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtvYmouZHVyYXRpb249aDVEdXJhdGlvbi5jbG9uZU5vZGUoKTtvYmouZXh0cmFWaWV3PWg1RXh0cmFWaWV3LmNsb25lTm9kZSgpO29iai5mdWxsc2NyZWVuPWg1RnVsbHNjcmVlbi5jbG9uZU5vZGUodHJ1ZSk7b2JqLnZvbHVtZUluYz1oNVZvbHVtZUluYy5jbG9uZU5vZGUodHJ1ZSk7b2JqLnZvbHVtZURlYz1oNVZvbHVtZURlYy5jbG9uZU5vZGUodHJ1ZSk7b2JqLm9mZnNldFg9MDtvYmoub2Zmc2V0WT0wO29iai53aWR0aD0wO29iai5oZWlnaHQ9MTtvYmouc2l6ZUZ1bmM9ZnVuY3Rpb24oKSU3Qm9iai5wYXJlbnRQb3NpdGlvbj13aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShvYmoucGFyZW50LG51bGwpLnBvc2l0aW9uO29iai5wYXJlbnRaSW5kZXg9d2luZG93LmdldENvbXB1dGVkU3R5bGUob2JqLnBhcmVudCxudWxsKS56SW5kZXg7b2JqLnZpZGVvV2lkdGg9d2luZG93LmdldENvbXB1dGVkU3R5bGUob2JqLnZpZGVvLG51bGwpLndpZHRoO29iai52aWRlb0hlaWdodD13aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShvYmoucGFyZW50LG51bGwpLmhlaWdodDtpZihvYmoucGFyZW50UG9zaXRpb249PSUyMnN0YXRpYyUyMiklN0JvYmoucGFyZW50LnN0eWxlLnBvc2l0aW9uPSUyMnJlbGF0aXZlJTIyOyU3RG9iai5wYXJlbnQuc3R5bGUuekluZGV4PSUyMjk5OTk5OTk5OTklMjI7b2JqLnZpZGVvLnN0eWxlLndpZHRoPSUyMjEwMCUyNSUyMjtvYmoudmlkZW8uc3R5bGUuaGVpZ2h0PSUyMjEwMCUyNSUyMjtvYmoudmlkZW9SYXRpbz1vYmoudmlkZW8udmlkZW9IZWlnaHQvb2JqLnZpZGVvLnZpZGVvV2lkdGg7dmFyJTIwJTIwYWRkSGVpZ2h0PW9iai5yZXNpemVGdW5jKCk7dmFyJTIwaGVpZ2h0PShvYmouaGVpZ2h0K2FkZEhlaWdodCklM0VvYmoudmlkZW8ub2Zmc2V0SGVpZ2h0P29iai52aWRlby5vZmZzZXRIZWlnaHQ6b2JqLmhlaWdodCthZGRIZWlnaHQ7b2JqLmNvbnRhaW5lci5zdHlsZS5sZWZ0PShvYmoudmlkZW8ub2Zmc2V0TGVmdCsob2JqLnZpZGVvLm9mZnNldFdpZHRoLW9iai53aWR0aCkvMikvb2JqLnBhcmVudC5jbGllbnRXaWR0aCoxMDArJTIyJTI1JTIyO29iai5jb250YWluZXIuc3R5bGUudG9wPShvYmoudmlkZW8ub2Zmc2V0VG9wKyhvYmoudmlkZW8ub2Zmc2V0SGVpZ2h0LWhlaWdodCkvMikvb2JqLnBhcmVudC5jbGllbnRIZWlnaHQqMTAwKyUyMiUyNSUyMjtvYmouY29udGFpbmVyLnN0eWxlLndpZHRoPW9iai53aWR0aC9vYmoucGFyZW50LmNsaWVudFdpZHRoKjEwMCslMjIlMjUlMjI7b2JqLmNvbnRhaW5lci5zdHlsZS5oZWlnaHQ9aGVpZ2h0L29iai5wYXJlbnQuY2xpZW50SGVpZ2h0KjEwMCslMjIlMjUlMjI7b2JqLnBsYXkuc3R5bGUudG9wPShoZWlnaHQtYWRkSGVpZ2h0KS8yKyUyMnB4JTIyO29iai5wbGF5LnN0eWxlLmxlZnQ9KG9iai53aWR0aC1hZGRIZWlnaHQpLzIrJTIycHglMjI7Zm9yKHZhciUyMGVsZT1vYmoudmlkZW87ZWxlIT1udWxsO2VsZT1lbGUub2Zmc2V0UGFyZW50KSU3Qm9iai5vZmZzZXRYKz1lbGUub2Zmc2V0TGVmdCtlbGUuY2xpZW50TGVmdDtvYmoub2Zmc2V0WSs9ZWxlLm9mZnNldFRvcCtlbGUuY2xpZW50VG9wOyU3RCU3RDtvYmouZm9udFJlc2l6ZUVsZT0lNUJvYmoudmlkZW9UaXRsZSxvYmouY3VycmVudFRpbWUsb2JqLmR1cmF0aW9uLG9iai5leHRyYVZpZXcsb2JqLmZ1bGxzY3JlZW4sb2JqLnZvbHVtZUluYyxvYmoudm9sdW1lRGVjJTVEO29iai5yZXNpemVGdW5jPWZ1bmN0aW9uKCklN0JpZihvYmoudmlkZW8ub2Zmc2V0SGVpZ2h0L29iai52aWRlby5vZmZzZXRXaWR0aCUzQ29iai52aWRlb1JhdGlvKSU3Qm9iai5oZWlnaHQ9b2JqLnZpZGVvLm9mZnNldEhlaWdodDtvYmoud2lkdGg9b2JqLmhlaWdodC9vYmoudmlkZW9SYXRpbzslN0RlbHNlJTdCb2JqLndpZHRoPW9iai52aWRlby5vZmZzZXRXaWR0aDtvYmouaGVpZ2h0PW9iai52aWRlb1JhdGlvKm9iai53aWR0aDslN0R2YXIlMjBoZWlnaHQ9b2JqLnZpZGVvUmF0aW8lM0UxP29iai53aWR0aDpvYmouaGVpZ2h0O29iai5jb250cm9sc1RvcC5zdHlsZS5oZWlnaHQ9aGVpZ2h0KjMvMjArJTIycHglMjI7b2JqLnBsYXkuc3R5bGUud2lkdGg9aGVpZ2h0KjMvMTArJTIycHglMjI7b2JqLnBsYXkuc3R5bGUuaGVpZ2h0PW9iai5wbGF5LnN0eWxlLndpZHRoO29iai5wbGF5LnN0eWxlLnRvcD0ob2JqLnZpZGVvLm9mZnNldEhlaWdodC1oZWlnaHQqMy8xMCkvMislMjJweCUyMjtvYmoucGxheS5zdHlsZS5sZWZ0PShvYmoudmlkZW8ub2Zmc2V0V2lkdGgtaGVpZ2h0KjMvMTApLzIrJTIycHglMjI7b2JqLmNvbnRyb2xzLnN0eWxlLmhlaWdodD1vYmouY29udHJvbHNUb3Auc3R5bGUuaGVpZ2h0O2Zvcih2YXIlMjBpPTA7aSUzQ29iai5mb250UmVzaXplRWxlLmxlbmd0aDtpKyspJTdCb2JqLmZvbnRSZXNpemVFbGUlNUJpJTVELnN0eWxlLmZvbnRTaXplPShvYmoudmlkZW9SYXRpbyUzRTE/aGVpZ2h0LzM2OmhlaWdodC8yMCkrJTIycHglMjI7JTdEcmV0dXJuJTIwaGVpZ2h0KjMvMTA7JTdEO29iai52YWx1ZUZ1bmM9ZnVuY3Rpb24oZG9jKSU3Qm9iai52aWRlb1RpdGxlLmlubmVySFRNTD0oZG9jLnRpdGxlPT0lMjIlMjIlN0MlN0NoNVBsYXllclZpZGVvQXJyLmxlbmd0aCUzRTEpP29iai52aWRlby5jdXJyZW50U3JjOmRvYy50aXRsZTtvYmouY3VycmVudFRpbWUuaW5uZXJIVE1MPWg1UGxheWVyRm9ybWF0VGltZShvYmoudmlkZW8uY3VycmVudFRpbWUpO29iai5kdXJhdGlvbi5pbm5lckhUTUw9aXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKT9oNVBsYXllckZvcm1hdFRpbWUob2JqLnZpZGVvLmR1cmF0aW9uKTolMjJMSVZFJTIyO3ZhciUyMGN0eD1vYmouYnVmZmVyQmFyLmdldENvbnRleHQoJTIyMmQlMjIpO2N0eC5zdHJva2VTdHlsZT0lMjIjODg4ODg4JTIyO2N0eC5saW5lV2lkdGg9MzA7Y3R4PW9iai5wcm9ncmVzc0Jhci5nZXRDb250ZXh0KCUyMjJkJTIyKTtjdHguc3Ryb2tlU3R5bGU9JTIyd2hpdGUlMjI7Y3R4LmxpbmVXaWR0aD0zMDtjdHg9b2JqLnBsYXkuZ2V0Q29udGV4dCglMjIyZCUyMik7Y3R4LnRyYW5zbGF0ZSgzMDAsMzAwKTt2YXIlMjBncmFkaWVudD1jdHguY3JlYXRlTGluZWFyR3JhZGllbnQoMCwwLDAsMzAwKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMCwlMjJ3aGl0ZSUyMik7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDAuMSwlMjJyZWQlMjIpO2dyYWRpZW50LmFkZENvbG9yU3RvcCgwLjIsJTIyb3JhbmdlJTIyKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMC4zLCUyMnllbGxvdyUyMik7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDAuNCwlMjJncmVlbiUyMik7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDAuNiwlMjJjeWFuJTIyKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMC43LCUyMmJsdWUlMjIpO2dyYWRpZW50LmFkZENvbG9yU3RvcCgwLjksJTIycHVycGxlJTIyKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMSwlMjJibGFjayUyMik7Y3R4LmZpbGxTdHlsZT0lMjIjZmZmZmZmY2MlMjI7Y3R4LnN0cm9rZVN0eWxlPWdyYWRpZW50O2N0eC5saW5lV2lkdGg9MTAwO2N0eC5saW5lQ2FwPSUyMnJvdW5kJTIyO2N0eC5zYXZlKCk7JTdEO29iai5maXRUb2dldGhlcj1mdW5jdGlvbihkb2MpJTdCb2JqLnNpemVGdW5jKCk7b2JqLnZhbHVlRnVuYyhkb2MpO29iai5wYXJlbnQuaW5zZXJ0QmVmb3JlKG9iai5jb250YWluZXIsb2JqLnZpZGVvKTtvYmouY29udGFpbmVyLmFwcGVuZENoaWxkKG9iai52aWRlbyk7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai5idWZmZXJCYXIpO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmoucHJvZ3Jlc3NCYXIpO29iai5jb250cm9sc1RvcC5hcHBlbmRDaGlsZChvYmoudmlkZW9UaXRsZSk7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai5jdXJyZW50VGltZSk7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai5kdXJhdGlvbik7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai5leHRyYVZpZXcpO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmouZnVsbHNjcmVlbik7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai52b2x1bWVJbmMpO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmoudm9sdW1lRGVjKTtvYmouY29udHJvbHNCZ2QuYXBwZW5kQ2hpbGQob2JqLmNvbnRyb2xzVG9wKTtvYmouY29udHJvbHNCZ2QuYXBwZW5kQ2hpbGQob2JqLnBsYXkpO29iai5jb250cm9sc0JnZC5hcHBlbmRDaGlsZChvYmouY29udHJvbHMpO29iai5jb250YWluZXIuYXBwZW5kQ2hpbGQob2JqLmNvbnRyb2xzQmdkKTtoNVBsYXllclBsYXlDbGljayhvYmopOyU3RDtyZXR1cm4lMjBvYmo7JTdEZnVuY3Rpb24lMjBoNVBsYXllclJlbW92ZUNvbnRyb2xzKG9iaixib29sKSU3QmNsZWFyVGltZW91dChvYmouaGlkZGVuVGltZUlEKTtpZihib29sKSU3Qm9iai5jb250cm9sc0JnZC5yZW1vdmVDaGlsZChvYmouY29udHJvbHNUb3ApO29iai5jb250cm9sc0JnZC5yZW1vdmVDaGlsZChvYmoucGxheSk7b2JqLmNvbnRyb2xzQmdkLnJlbW92ZUNoaWxkKG9iai5jb250cm9scyk7JTdEZWxzZSU3Qm9iai5jb250cm9sc0JnZC5hcHBlbmRDaGlsZChvYmouY29udHJvbHNUb3ApO29iai5jb250cm9sc0JnZC5hcHBlbmRDaGlsZChvYmoucGxheSk7b2JqLmNvbnRyb2xzQmdkLmFwcGVuZENoaWxkKG9iai5jb250cm9scyk7JTdEJTdEZnVuY3Rpb24lMjBoNVBsYXllckhpZGRlbihvYmopJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7aDVQbGF5ZXJSZW1vdmVDb250cm9scyhvYmosZmFsc2UpO29iai5oaWRkZW5UaW1lSUQ9c2V0VGltZW91dChmdW5jdGlvbigpJTdCaDVQbGF5ZXJSZW1vdmVDb250cm9scyhvYmosdHJ1ZSk7JTdELDEwMDAwKTslN0R2YXIlMjBoNVBsYXllclNoYWtlPSU3QiUyMmNhbmFibGUlMjI6dHJ1ZSwlMjJmdW5jJTIyOmZ1bmN0aW9uKCklN0JpZihoNVBsYXllclNoYWtlLmNhbmFibGUpJTdCaWYoZXZlbnQuYWNjZWxlcmF0aW9uLnglM0UzMCU3QyU3Q2V2ZW50LmFjY2VsZXJhdGlvbi55JTNFMzAlN0MlN0NldmVudC5hY2NlbGVyYXRpb24ueiUzRTMwKSU3Qmg1UGxheWVyU2hha2UuY2FuYWJsZT1mYWxzZTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCklN0JoNVBsYXllclNoYWtlLmNhbmFibGU9dHJ1ZTslN0QsMTAwMCk7aWYoaDVQbGF5ZXJWaWRlb0Fyci5sZW5ndGglM0UwJTdDJTdDaDVQbGF5ZXJUaW1lSUQubGVuZ3RoJTNFMCklN0JoNVBsYXllclJlbW92ZSgpOyU3RGVsc2UlN0JoNVBsYXllcihkb2N1bWVudCwwKTslN0QlN0QlN0QlN0QlN0Q7ZnVuY3Rpb24lMjBoNVBsYXllclJlbW92ZSgpJTdCZm9yKHZhciUyMGk9MDtpJTNDaDVQbGF5ZXJUaW1lSUQubGVuZ3RoO2krKyklN0JjbGVhclRpbWVvdXQoaDVQbGF5ZXJUaW1lSUQlNUJpJTVEKTslN0Rmb3IodmFyJTIwaT0wO2klM0NoNVBsYXllclZpZGVvQXJyLmxlbmd0aDtpKyspJTdCdmFyJTIwb2JqPWg1UGxheWVyVmlkZW9BcnIlNUJpJTVEO29iai5wYXJlbnQuaW5zZXJ0QmVmb3JlKG9iai52aWRlbyxvYmouY29udGFpbmVyKTtvYmoucGFyZW50LnJlbW92ZUNoaWxkKG9iai5jb250YWluZXIpO29iai5wYXJlbnQuc3R5bGUucG9zaXRpb249b2JqLnBhcmVudFBvc2l0aW9uO29iai5wYXJlbnQuc3R5bGUuekluZGV4PW9iai5wYXJlbnRaSW5kZXg7b2JqLnZpZGVvLnN0eWxlLndpZHRoPW9iai52aWRlb1dpZHRoO29iai52aWRlby5zdHlsZS5oZWlnaHQ9b2JqLnZpZGVvSGVpZ2h0O29iai52aWRlby5jb250cm9scz1vYmouaGF2ZUNvbnRyb2xzOyU3RGg1UGxheWVyVmlkZW9BcnI9JTVCJTVEO2g1UGxheWVySWZyYW1lQXJyPSU1QiU1RDtoNVBsYXllclRpbWVJRD0lNUIlNUQ7JTdEdmFyJTIwaDVQbGF5ZXJSZXNpemU9JTdCJTIydGltZXIlMjI6MCwlMjJmdW5jJTIyOmZ1bmN0aW9uKCklN0JjbGVhclRpbWVvdXQodGhpcy50aW1lcik7dGhpcy50aW1lcj1zZXRUaW1lb3V0KGZ1bmN0aW9uKCklN0Jmb3IodmFyJTIwaT0wO2klM0NoNVBsYXllclZpZGVvQXJyLmxlbmd0aDtpKyspJTdCaDVQbGF5ZXJWaWRlb0FyciU1QmklNUQucmVzaXplRnVuYyh0cnVlKTslN0QlN0QsMzAwKTslN0QlN0Q7d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJTIyZGV2aWNlbW90aW9uJTIyLGg1UGxheWVyU2hha2UuZnVuYyk7d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJTIycmVzaXplJTIyLGg1UGxheWVyUmVzaXplLmZ1bmMpO2Z1bmN0aW9uJTIwaDVQbGF5ZXIoZG9jLGRlcHRoKSU3QnZhciUyMHZpZGVvcz1kb2MuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJTIyVklERU8lMjIpO3ZhciUyMGlmcmFzPWRvYy5nZXRFbGVtZW50c0J5VGFnTmFtZSglMjJJRlJBTUUlMjIpO3ZhciUyMGhhdmVIaWRkZW49dmlkZW9zLmxlbmd0aCUzRTA/ZmFsc2U6dHJ1ZTtmb3IodmFyJTIwaT0wO2klM0N2aWRlb3MubGVuZ3RoO2krKyklN0J2YXIlMjBib29sPXRydWU7Zm9yKHZhciUyMGo9MDtqJTNDaDVQbGF5ZXJWaWRlb0Fyci5sZW5ndGg7aisrKSU3QmlmKHZpZGVvcyU1QmklNUQuaXNTYW1lTm9kZShoNVBsYXllclZpZGVvQXJyJTVCaiU1RC52aWRlbykpJTdCYm9vbD1mYWxzZTticmVhazslN0QlN0RpZihib29sKSU3QmlmKCFpc0g1UGxheWVySGlkZGVuKHZpZGVvcyU1QmklNUQpJiZ2aWRlb3MlNUJpJTVELnJlYWR5U3RhdGUlM0UwKSU3QmlmKHZpZGVvcyU1QmklNUQudmlkZW9XaWR0aCp2aWRlb3MlNUJpJTVELnZpZGVvSGVpZ2h0PT0wKSU3QnZpZGVvcyU1QmklNUQucHJlbG9hZD0lMjJtZXRhZGF0YSUyMjtoYXZlSGlkZGVuPXRydWU7JTdEZWxzZSU3QmFkZEg1UGxheWVyKHZpZGVvcyU1QmklNUQsZG9jKTslN0QlN0RlbHNlJTdCaGF2ZUhpZGRlbj10cnVlOyU3RCU3RCU3RGZvcih2YXIlMjBpPTA7aSUzQ2lmcmFzLmxlbmd0aDtpKyspJTdCdmFyJTIwYm9vbD10cnVlO2Zvcih2YXIlMjBqPTA7aiUzQ2g1UGxheWVySWZyYW1lQXJyLmxlbmd0aDtqKyspJTdCaWYoaWZyYXMlNUJpJTVELmlzU2FtZU5vZGUoaDVQbGF5ZXJJZnJhbWVBcnIlNUJqJTVEKSklN0Jib29sPWZhbHNlO2JyZWFrOyU3RCU3RGlmKGJvb2wpJTdCaWYoIWlzSDVQbGF5ZXJIaWRkZW4oaWZyYXMlNUJpJTVEKSklN0JpZnJhRG9jPWlmcmFzJTVCaSU1RC5jb250ZW50RG9jdW1lbnQ7aWYoaWZyYURvYyE9bnVsbCklN0JoNVBsYXllcklmcmFtZUFyciU1Qmg1UGxheWVySWZyYW1lQXJyLmxlbmd0aCU1RD1pZnJhcyU1QmklNUQ7aWYoaWZyYURvYy5yZWFkeVN0YXRlPT0lMjJjb21wbGV0ZSUyMiklN0JoNVBsYXllcihpZnJhRG9jLGRlcHRoKzEpOyU3RGVsc2UlN0JpZnJhcyU1QmklNUQuYWRkRXZlbnRMaXN0ZW5lciglMjJsb2FkJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllcihpZnJhRG9jLGRlcHRoKzEpOyU3RCk7JTdEJTdEJTdEZWxzZSU3QmhhdmVIaWRkZW49dHJ1ZTslN0QlN0QlN0RpZihoYXZlSGlkZGVuKSU3Qmg1UGxheWVyVGltZUlEJTVCZGVwdGglNUQ9c2V0VGltZW91dChoNVBsYXllciwxMDAwLGRvYyxkZXB0aCk7JTdEJTdEZnVuY3Rpb24lMjBhZGRINVBsYXllcih2aWRlbyxkb2MpJTdCdmFyJTIwb2JqPWg1UGxheWVyQ2xhc3MoKTtvYmouaGF2ZUNvbnRyb2xzPXZpZGVvLmNvbnRyb2xzO3ZpZGVvLmNvbnRyb2xzPWZhbHNlO29iai52aWRlbz12aWRlbztvYmoucGFyZW50PXZpZGVvLnBhcmVudE5vZGU7b2JqLmZpdFRvZ2V0aGVyKGRvYyk7aDVQbGF5ZXJWaWRlb0FyciU1Qmg1UGxheWVyVmlkZW9BcnIubGVuZ3RoJTVEPW9iajtvYmouY29udHJvbHNCZ2QuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7aWYob2JqLnZpZGVvLnBhdXNlZCklN0JvYmoudmlkZW8ucGxheSgpOyU3RGVsc2UlN0JpZihvYmouY29udHJvbHNCZ2QuY29tcGFyZURvY3VtZW50UG9zaXRpb24ob2JqLnBsYXkpJTI1Mj09MCklN0JoNVBsYXllclJlbW92ZUNvbnRyb2xzKG9iaix0cnVlKTslN0RlbHNlJTdCaDVQbGF5ZXJIaWRkZW4ob2JqKTslN0QlN0QlN0QpO29iai5jb250cm9sc0JnZC5hZGRFdmVudExpc3RlbmVyKCUyMmRibGNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTtoNVBsYXllckhpZGRlbihvYmopO2g1UGxheWVyRnVsbHNjcmVlbkNsaWNrKG9iai5jb250YWluZXIpOyU3RCk7b2JqLmNvbnRyb2xzQmdkLmFkZEV2ZW50TGlzdGVuZXIoJTIydG91Y2hzdGFydCUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7aDVQbGF5ZXJTbGlkZVN0YXJ0KCk7JTdEKTtvYmouY29udHJvbHNCZ2QuYWRkRXZlbnRMaXN0ZW5lciglMjJ0b3VjaG1vdmUlMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpOyU3RCk7b2JqLmNvbnRyb2xzQmdkLmFkZEV2ZW50TGlzdGVuZXIoJTIydG91Y2hlbmQlMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2g1UGxheWVyU2xpZGVFbmQob2JqKTslN0QpO29iai5jb250cm9sc1RvcC5hZGRFdmVudExpc3RlbmVyKCUyMmNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9sc1RvcC5hZGRFdmVudExpc3RlbmVyKCUyMmRibGNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9sc1RvcC5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoc3RhcnQlMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2NsZWFyVGltZW91dChvYmouaGlkZGVuVGltZUlEKTslN0QpO29iai5jb250cm9sc1RvcC5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoZW5kJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopOyU3RCk7b2JqLnZpZGVvVGl0bGUuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCaWYoaXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKSklN0JvYmoudmlkZW8uY3VycmVudFRpbWU9ZXZlbnQub2Zmc2V0WC90aGlzLm9mZnNldFdpZHRoKm9iai52aWRlby5kdXJhdGlvbjslN0QlN0QpO29iai5wbGF5LmFkZEV2ZW50TGlzdGVuZXIoJTIyY2xpY2slMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2lmKG9iai52aWRlby5wYXVzZWQpJTdCb2JqLnZpZGVvLnBsYXkoKTslN0RlbHNlJTdCb2JqLnZpZGVvLnBhdXNlKCk7JTdEJTdEKTtvYmoucGxheS5hZGRFdmVudExpc3RlbmVyKCUyMmRibGNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9scy5hZGRFdmVudExpc3RlbmVyKCUyMmNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9scy5hZGRFdmVudExpc3RlbmVyKCUyMmRibGNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9scy5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoc3RhcnQlMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2NsZWFyVGltZW91dChvYmouaGlkZGVuVGltZUlEKTtpZihpc0Zpbml0ZShvYmoudmlkZW8uZHVyYXRpb24pKSU3Qmg1UGxheWVyU2xpZGVTdGFydCgpOyU3RCU3RCk7b2JqLmNvbnRyb2xzLmFkZEV2ZW50TGlzdGVuZXIoJTIydG91Y2htb3ZlJTIyLGZ1bmN0aW9uKCklN0JpZihpc0Zpbml0ZShvYmoudmlkZW8uZHVyYXRpb24pKSU3Qm9iai52aWRlby5wYXVzZSgpO2g1UGxheWVyU2xpZGVDb250cm9sc01vdmUob2JqKTslN0QlN0QpO29iai5jb250cm9scy5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoZW5kJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopO2lmKGlzRmluaXRlKG9iai52aWRlby5kdXJhdGlvbikpJTdCaDVQbGF5ZXJTbGlkZUNvbnRyb2xzRW5kKG9iaik7JTdEJTdEKTtvYmoucHJvZ3Jlc3NCYXIuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCaWYoaXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKSklN0JvYmoudmlkZW8uY3VycmVudFRpbWU9ZXZlbnQub2Zmc2V0WC90aGlzLm9mZnNldFdpZHRoKm9iai52aWRlby5kdXJhdGlvbjslN0QlN0QpO29iai5jdXJyZW50VGltZS5hZGRFdmVudExpc3RlbmVyKCUyMmNsaWNrJTIyLGZ1bmN0aW9uKCklN0JvYmoudmlkZW8uY3VycmVudFRpbWU9MDslN0QpO29iai5kdXJhdGlvbi5hZGRFdmVudExpc3RlbmVyKCUyMmNsaWNrJTIyLGZ1bmN0aW9uKCklN0J2YXIlMjBhZGRUaW1lPW9iai52aWRlby5jdXJyZW50VGltZSsob2JqLnZpZGVvLmR1cmF0aW9uJTNDNjE/MTpvYmoudmlkZW8uZHVyYXRpb24vMTApO29iai52aWRlby5jdXJyZW50VGltZT0oYWRkVGltZSUzQ29iai52aWRlby5kdXJhdGlvbj9hZGRUaW1lOm9iai52aWRlby5kdXJhdGlvbi0xKTslN0QpO29iai5leHRyYVZpZXcuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCb2JqLnZpZGVvLmN1cnJlbnRUaW1lPW9iai52aWRlby5kdXJhdGlvbi0xOyU3RCk7b2JqLmZ1bGxzY3JlZW4uYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXJGdWxsc2NyZWVuQ2xpY2sob2JqLmNvbnRhaW5lcik7JTdEKTtkb2MuYWRkRXZlbnRMaXN0ZW5lciglMjJmdWxsc2NyZWVuY2hhbmdlJTIyLGZ1bmN0aW9uKCklN0JpZihkb2MuZnVsbHNjcmVlbkVsZW1lbnQ9PW51bGwpJTdCb2JqLmZ1bGxzY3JlZW4uaW5uZXJIVE1MPSUyMiVFNSU4NSVBOCVFNSVCMSU4RiUyMjslN0RlbHNlJTIwaWYob2JqLmNvbnRhaW5lci5pc1NhbWVOb2RlKGRvYy5mdWxsc2NyZWVuRWxlbWVudCkpJTdCb2JqLmZ1bGxzY3JlZW4uaW5uZXJIVE1MPSUyMiVFOSU4MCU4MCVFNSU4NyVCQSUyMjslN0QlN0QpO29iai52b2x1bWVJbmMuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCb2JqLnZpZGVvLnZvbHVtZT0ob2JqLnZpZGVvLnZvbHVtZSUzQzE/cGFyc2VJbnQob2JqLnZpZGVvLnZvbHVtZSoxMCsxKS8xMDoxKTslN0QpO29iai52b2x1bWVEZWMuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCb2JqLnZpZGVvLnZvbHVtZT0ob2JqLnZpZGVvLnZvbHVtZSUzRTA/TWF0aC5jZWlsKG9iai52aWRlby52b2x1bWUqMTAtMSkvMTA6MCk7JTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJjYW5wbGF5JTIyLGZ1bmN0aW9uKCklN0JpZih0aGlzLnBhdXNlZCYmKG9iai5zdGF0ZT09JTIyc2Vla2luZyUyMiU3QyU3Q29iai5zdGF0ZT09JTIyd2FpdGluZyUyMikpJTdCdGhpcy5wbGF5KCk7JTdEJTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJkdXJhdGlvbmNoYW5nZSUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXJIaWRkZW4ob2JqKTtvYmouZHVyYXRpb24uaW5uZXJIVE1MPWlzRmluaXRlKHRoaXMuZHVyYXRpb24pP2g1UGxheWVyRm9ybWF0VGltZSh0aGlzLmR1cmF0aW9uKTolMjJMSVZFJTIyOyU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIydGltZXVwZGF0ZSUyMixmdW5jdGlvbigpJTdCb2JqLmZvcndhcmRUaW1lPXRoaXMuY3VycmVudFRpbWU7b2JqLmN1cnJlbnRUaW1lLmlubmVySFRNTD1oNVBsYXllckZvcm1hdFRpbWUodGhpcy5jdXJyZW50VGltZSk7aWYoaXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKSklN0JoNVBsYXllckdldFByb2dyZXNzKG9iaixvYmoudmlkZW8uY3VycmVudFRpbWUpOyU3RCU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIydm9sdW1lY2hhbmdlJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopO29iai5mb3J3YXJkVm9sdW1lPW9iai52aWRlby52b2x1bWU7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU5JTlGJUIzJUU5JTg3JThGOiUyMitwYXJzZUludCh0aGlzLnZvbHVtZSoxMDApKyUyMiUyNSUyMjslN0QpO29iai52aWRlby5hZGRFdmVudExpc3RlbmVyKCUyMnJhdGVjaGFuZ2UlMjIsZnVuY3Rpb24oKSU3Qmg1UGxheWVySGlkZGVuKG9iaik7b2JqLmZvcndhcmRSYXRlPW9iai52aWRlby5wbGF5YmFja1JhdGU7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU1JTgwJThEJUU5JTgwJTlGOiUyMitwYXJzZUludCh0aGlzLnBsYXliYWNrUmF0ZSoxMDApKyUyMiUyNSUyMjslN0QpO29iai52aWRlby5hZGRFdmVudExpc3RlbmVyKCUyMnByb2dyZXNzJTIyLGZ1bmN0aW9uKCklN0JpZihpc0Zpbml0ZShvYmoudmlkZW8uZHVyYXRpb24pKSU3Qmg1UGxheWVyR2V0QnVmZmVyKG9iaik7JTdEJTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJzZWVraW5nJTIyLGZ1bmN0aW9uKCklN0JvYmouc3RhdGU9JTIyc2Vla2luZyUyMjt2YXIlMjBzdWJUaW1lPW9iai52aWRlby5jdXJyZW50VGltZS1vYmouZm9yd2FyZFRpbWU7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9KHN1YlRpbWUlM0UwPyUyMiVFNSVCRiVBQiVFOCVCRiU5QjolMjI6JTIyJUU1JUJGJUFCJUU5JTgwJTgwOiUyMikrTWF0aC5hYnMocGFyc2VJbnQoc3ViVGltZSkpKyUyMnMlMjI7JTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJ3YWl0aW5nJTIyLGZ1bmN0aW9uKCklN0JpZihvYmouc3RhdGUhPSUyMnNlZWtpbmclMjIpJTdCb2JqLnN0YXRlPSUyMndhaXRpbmclMjI7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU1JThBJUEwJUU4JUJEJUJEJUU0JUI4JUFELi4uJTIyOyU3RG9iai52aWRlby5wYXVzZSgpOyU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIyZW5kZWQlMjIsZnVuY3Rpb24oKSU3Qm9iai52aWRlby5wYXVzZSgpOyU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIycGF1c2UlMjIsZnVuY3Rpb24oKSU3Qmg1UGxheWVyUmVtb3ZlQ29udHJvbHMob2JqLGZhbHNlKTtpZihvYmouc3RhdGUhPSUyMndhaXRpbmclMjImJm9iai5zdGF0ZSE9JTIyc2Vla2luZyUyMiklN0JvYmouc3RhdGU9JTIycGF1c2VkJTIyO2g1UGxheWVyUGxheUNsaWNrKG9iaik7JTdEZWxzZSU3QmNsZWFyVGltZW91dChvYmoud2FpdFRpbWVJRCk7b2JqLndhaXRUaW1lSUQ9c2V0SW50ZXJ2YWwoaDVQbGF5ZXJHZXRXYWl0aW5nLDQwLG9iaik7JTdEJTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJwbGF5aW5nJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopO2lmKG9iai5zdGF0ZT09JTIyd2FpdGluZyUyMiU3QyU3Q29iai5zdGF0ZT09JTIyc2Vla2luZyUyMiklN0JpZihvYmouc3RhdGU9PSUyMndhaXRpbmclMjIpJTdCb2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJTIyOyU3RGNsZWFyVGltZW91dChvYmoud2FpdFRpbWVJRCk7JTdEb2JqLnN0YXRlPSUyMnBsYXlpbmclMjI7aDVQbGF5ZXJQbGF5Q2xpY2sob2JqKTslN0QpOyU3RHZhciUyMGg1UGxheWVyU2xpZGVTdGFydFg7dmFyJTIwaDVQbGF5ZXJTbGlkZVN0YXJ0WTtmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVTdGFydCgpJTdCaDVQbGF5ZXJTbGlkZVN0YXJ0WD1ldmVudC50YXJnZXRUb3VjaGVzJTVCMCU1RC5jbGllbnRYO2g1UGxheWVyU2xpZGVTdGFydFk9ZXZlbnQudGFyZ2V0VG91Y2hlcyU1QjAlNUQuY2xpZW50WTslN0RmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVDb250cm9sc01vdmUob2JqKSU3QnZhciUyMHNsaWRlTW92ZVg9ZXZlbnQudGFyZ2V0VG91Y2hlcyU1QjAlNUQuY2xpZW50WDt2YXIlMjBkaXN0YW5jZT1zbGlkZU1vdmVYLWg1UGxheWVyU2xpZGVTdGFydFg7dmFyJTIwYWRkVGltZT1kaXN0YW5jZS9vYmouY29udHJvbHMub2Zmc2V0V2lkdGgqb2JqLnZpZGVvLmR1cmF0aW9uK29iai52aWRlby5jdXJyZW50VGltZTthZGRUaW1lPShhZGRUaW1lJTNDMCU3QyU3Q2FkZFRpbWUlM0VvYmoudmlkZW8uZHVyYXRpb24pPyhhZGRUaW1lJTNDMD8wOm9iai52aWRlby5kdXJhdGlvbi0xKTphZGRUaW1lO2g1UGxheWVyR2V0UHJvZ3Jlc3Mob2JqLGFkZFRpbWUpO29iai5leHRyYVZpZXcuaW5uZXJIVE1MPWg1UGxheWVyRm9ybWF0VGltZShhZGRUaW1lKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVDb250cm9sc0VuZChvYmopJTdCdmFyJTIwc2xpZGVFbmRYPWV2ZW50LmNoYW5nZWRUb3VjaGVzJTVCMCU1RC5jbGllbnRYO3ZhciUyMGRpc3RhbmNlPXNsaWRlRW5kWC1oNVBsYXllclNsaWRlU3RhcnRYO2lmKCFkaXN0YW5jZSUzRTApJTdCcmV0dXJuJTIwMDslN0R2YXIlMjBhZGRUaW1lPW9iai52aWRlby5jdXJyZW50VGltZStkaXN0YW5jZS9vYmouY29udHJvbHMub2Zmc2V0V2lkdGgqb2JqLnZpZGVvLmR1cmF0aW9uO2FkZFRpbWU9KGFkZFRpbWUlM0MwJTdDJTdDYWRkVGltZSUzRW9iai52aWRlby5kdXJhdGlvbik/KGFkZFRpbWUlM0MwPzA6b2JqLnZpZGVvLmR1cmF0aW9uLTEpOmFkZFRpbWU7b2JqLnZpZGVvLnBsYXkoKTtvYmoudmlkZW8uY3VycmVudFRpbWU9YWRkVGltZTslN0RmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVFbmQob2JqKSU3QnZhciUyMHNsaWRlRW5kWD1ldmVudC5jaGFuZ2VkVG91Y2hlcyU1QjAlNUQuY2xpZW50WDt2YXIlMjBzbGlkZUVuZFk9ZXZlbnQuY2hhbmdlZFRvdWNoZXMlNUIwJTVELmNsaWVudFk7dmFyJTIwZGlzdGFuY2VYPXNsaWRlRW5kWC1oNVBsYXllclNsaWRlU3RhcnRYO3ZhciUyMGRpc3RhbmNlWT1zbGlkZUVuZFktaDVQbGF5ZXJTbGlkZVN0YXJ0WTt2YXIlMjBwb3NYPWg1UGxheWVyU2xpZGVTdGFydFgtb2JqLm9mZnNldFg7dmFyJTIwZGlzdGFuY2U9TWF0aC5zcXJ0KE1hdGgucG93KGRpc3RhbmNlWCwyKStNYXRoLnBvdyhkaXN0YW5jZVksMikpO3ZhciUyMHBhcm09TWF0aC5hYnMoZGlzdGFuY2VZL2Rpc3RhbmNlWCk7aWYoZGlzdGFuY2U9PTApJTdCcmV0dXJuJTIwMDslN0RpZihwYXJtJTNDMC4zNiklN0J2YXIlMjBkaXN0YW5jZT1kaXN0YW5jZS9vYmoud2lkdGg7ZGlzdGFuY2U9ZGlzdGFuY2VYJTNFMD9kaXN0YW5jZTotMSpkaXN0YW5jZTtoNVBsYXllclByb2dyZXNzQ29udHJvbChvYmosZGlzdGFuY2UpOyU3RGVsc2UlMjBpZihkaXN0YW5jZVg9PTAlN0MlN0NwYXJtJTNFMi43NSklN0J2YXIlMjBkaXN0YW5jZT1kaXN0YW5jZS9vYmouaGVpZ2h0O2Rpc3RhbmNlPWRpc3RhbmNlWSUzRTA/LTEqZGlzdGFuY2U6ZGlzdGFuY2U7aWYocG9zWC9vYmoud2lkdGglM0UwLjUpJTdCaDVQbGF5ZXJWb2x1bWVDb250cm9sKG9iaixkaXN0YW5jZSk7JTdEZWxzZSU3Qmg1UGxheWVyUmF0ZUNvbnRyb2wob2JqLGRpc3RhbmNlJTNFMD8xOi0xKTslN0QlN0QlN0RmdW5jdGlvbiUyMGg1UGxheWVyUmF0ZUNvbnRyb2wob2JqLGRpcmVjdGlvbiklN0JpZihvYmoudmlkZW8ucGxheWJhY2tSYXRlIT11bmRlZmluZWQpJTdCdmFyJTIwYWRkUmF0ZT1vYmouZm9yd2FyZFJhdGUrZGlyZWN0aW9uLzQ7b2JqLnZpZGVvLnBsYXliYWNrUmF0ZT0oYWRkUmF0ZSUzQzAuMjUlN0MlN0NhZGRSYXRlJTNFNCk/KGFkZFJhdGUlM0MwLjI1PzAuMjU6NCk6YWRkUmF0ZTslN0RlbHNlJTdCb2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU4JUE3JTg2JUU5JUEyJTkxJUU0JUI4JThEJUU2JTk0JUFGJUU2JThDJTgxJUU1JTgwJThEJUU5JTgwJTlGJUU2JTkyJUFEJUU2JTk0JUJFJUVGJUJDJTgxJTIyOyU3RCU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJWb2x1bWVDb250cm9sKG9iaixkaXN0YW5jZSklN0J2YXIlMjBzdGVwPU1hdGgucm91bmQoZGlzdGFuY2UqNTApO2lmKChvYmouZm9yd2FyZFZvbHVtZStzdGVwLzEwMCklM0UxKSU3Qm9iai52aWRlby52b2x1bWU9MTslN0RlbHNlJTIwaWYoKG9iai5mb3J3YXJkVm9sdW1lK3N0ZXAvMTAwKSUzQzApJTdCb2JqLnZpZGVvLnZvbHVtZT0wOyU3RGVsc2UlN0JvYmoudmlkZW8udm9sdW1lPW9iai5mb3J3YXJkVm9sdW1lK3N0ZXAvMTAwOyU3RCU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJQcm9ncmVzc0NvbnRyb2wob2JqLGRpc3RhbmNlKSU3QnZhciUyMHN0ZXA9TWF0aC5yb3VuZChkaXN0YW5jZSo1MCk7b2JqLnZpZGVvLmN1cnJlbnRUaW1lPW9iai5mb3J3YXJkVGltZSs2MCo3KnN0ZXAvMTAwOyU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJGb3JtYXRUaW1lKHRpbWUpJTdCaWYoaXNOYU4odGltZSkpJTdCdmFyJTIwYXJyPXRpbWUuc3BsaXQoJTIyOiUyMik7dmFyJTIwbnVtPTA7Zm9yKHZhciUyMGk9MDtpJTNDYXJyLmxlbmd0aDtpKyspJTdCbnVtPW51bSo2MCtwYXJzZUludChhcnIlNUJpJTVEKTslN0RyZXR1cm4lMjBudW07JTdEZWxzZSU3QnZhciUyMHN0cj0lMjIlMjI7dmFyJTIwbnVtPXBhcnNlSW50KHRpbWUpO2Zvcih2YXIlMjBpPTA7aSUzQzIlN0MlN0NudW0lM0UwO2krKyklN0J2YXIlMjBtb2Q9bnVtJTI1NjA7bnVtPShudW0tbW9kKS82MDtzdHI9KG1vZCUzQzEwPyUyMjAlMjIrbW9kOm1vZCkrKGk9PTA/JTIyJTIyOiUyMjolMjIpK3N0cjslN0RyZXR1cm4lMjBzdHI7JTdEJTdEZnVuY3Rpb24lMjBpc0g1UGxheWVySGlkZGVuKGVsZSklN0JpZihlbGUuc2Nyb2xsV2lkdGghPTAmJmVsZS5zY3JvbGxIZWlnaHQhPTApJTdCcmV0dXJuJTIwZmFsc2U7JTdEcmV0dXJuJTIwdHJ1ZTslN0QlN0QpKCk7\";\n" +
                            "    eval(decodeURI(window.atob(videoParseCode)));\n" +
                            "    eval(decodeURI(window.atob(h5PlayerCode)));\n" +
                            "})();");
                }
                if(errorvs){
                    errorView.setVisibility(View.VISIBLE);
                }else {
                    errorView.setVisibility(View.GONE);
                    frameLayout.setVisibility(View.VISIBLE);
                }

                mWebView.loadUrl("javascript:window.android.showSource('<head>'+" +
                        "document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                view.loadUrl("javascript:"+"document.addEventListener('touchstart', function (e) {\n" +
                        "\tvar parentic;\n" +
                        "\tvar parentii;\n" +
                        "\tvar ii=0;\n" +
                        "\t\tvar element = event.srcElement || event.target;\n" +
                        "\t\tif(element.className==''&&element.id==''){\n" +
                        "\t\t\tparentii=element;\n" +
                        "\t\t\twhile(parentii.id==''&&ii<10){\n" +
                        "\t\t\t\tparentii=parentii.parentElement;\n" +
                        "\t\t\t\tif(parentii.id!=''){\n" +
                        "\t\t\t\t\twindow.android.get('这是个id'+parentii.id);\n" +
                        "                    return;\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t\tif(parentii.className!=''){\n" +
                        "\t\t\t\t\twindow.android.get('这是个class'+parentii.className);\n" +
                        "\t\t\t\t\treturn;\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t\tii++; \n" +
                        "\t\t\t}\n" +
                        "\n" +
                        "\t\t\t}else if(element.className!=''&&element.id==''){\n" +
                        "\t\t\t\twindow.android.get('这是个class'+element.className);\n" +
                        "\t\t\t}else if(element.className==''&&element.id!=''){\n" +
                        "\t\t\t\twindow.android.get('这是个id'+element.id);\n" +
                        "\t\t\t}else if(element.className!=''&&element.id!=''){\n" +
                        "\t\t\t\twindow.android.get('这是个id'+element.id);\t\n" +
                        "\t\t\t}\n" +
                        "\t\t});");

            }
            // 将要加载资源(url)
            @Override
            public void onLoadResource(android.webkit.WebView view, String url) {
                super.onLoadResource(view, url);
                if(share.getString("getvideo","自动嗅探视频【关闭】").equals("自动嗅探视频【开启】")){
                    if(url.contains(".m3u8")||url.contains(".mp4")||url.contains(".3gp")||url.contains(".wmv")
                            ||url.contains(".avi")
                            ||url.endsWith(".rm")||url.contains(".rm?")||url.contains(".rm.") ||url.contains(".rm=")||url.contains(".rm/")
                            ||url.contains(".mkv")||url.contains(".flv")
                            ||url.contains(".mov")||url.contains(".mpeg")||url.contains(".rmvb")||url.contains(".swf")
                            ||url.contains(".mpg")||url.contains(".f4v")){
//                    AlertDialog dialog;
//                    dialog = new AlertDialog.Builder(webview.this).setTitle("嗅探到视频！")
//                            .setIcon(R.drawable.micon)
//                            .setMessage("打开播放")
//                            .setPositiveButton("打开", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                                    startActivity(intent);
//                                }
//                            })
//                            .setNegativeButton("取消",null)
//
//                            .create();
//                    dialog.show();
                        resourceselect(url);
                        queryinfo();

                    }
                }

            }
            /**
             * 在加载资源时通知主机应用程序发生SSL错误
             * 作用：处理https请求
             * @param view   view
             * @param handler  handler
             * @param error   error
             */
            @Override
            public void onReceivedSslError(android.webkit.WebView view, SslErrorHandler handler, SslError error) {
//super.onReceivedSslError(view, handler, error);注意一定要去除这行代码，否则设置无效。
                // handler.cancel();// Android默认的处理方式
                handler.proceed();// 接受所有网站的证书
                // handleMessage(Message msg);// 进行其他处理

            }

            @Override
            public void onReceivedError(android.webkit.WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                view.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
                frameLayout.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                errorvs=true;
            }


            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(android.webkit.WebView view, String url) {
//                if (!url.contains(adselect(getDomainName(url)))) {
//                    return super.shouldInterceptRequest ( view , url ) ; //正常加载
//                }else{
//                    return new WebResourceResponse( null , null , null ) ; //含有广告资源屏蔽请求
//                }
                return super.shouldInterceptRequest ( view , url ) ; //正常加载
            }
        });

        //辅助 WebView 处理 Javascript 的对话框,网站图标,网站标题等等https://blog.csdn.net/weixin_40438421/article/details/85700109
        mWebView.setWebChromeClient(new WebChromeClient() {


            @Override
            public void onProgressChanged(android.webkit.WebView view, int newProgress) {
                super.onProgressChanged(view,newProgress);

                // TODO 自动生成的方法存根
                if (newProgress == 100) {
                    pb.setVisibility(View.GONE);//加载完网页进度条消失
                    js_domain_name(getDomainName(url));
                    js_all();
                    history(mWebView.getTitle(),mWebView.getUrl());
                    jsad(getDomainName(mWebView.getUrl()));
                    if(!mWebView.getSettings().getLoadsImagesAutomatically()) {
                        mWebView.getSettings().setLoadsImagesAutomatically(true);
                    }
                    //可以做加载图https://www.jianshu.com/p/860e5658b54bhttps://www.jianshu.com/p/860e5658b54b
                } else {
                    pb.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                    pb.setProgress(newProgress);//设置进度值

                }

                night_pattem();//夜间模式

            }
            //获取Web页中的标题
            @Override
            public void onReceivedTitle(android.webkit.WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (!TextUtils.isEmpty(title)) {
                    txtTitle.setText(title);
                    txtTitle.setTextColor(Color.rgb(0,0,0));
                }
            }


            //WebView全屏播放https://www.jianshu.com/p/ec23fe80d5a3

            // 一个回调接口使用的主机应用程序通知当前页面的自定义视图已被撤职
            private CustomViewCallback mCustomViewCallback;
            //网页进入全屏模式监听
            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView(view, callback);
                mFrameLayout = findViewById(R.id.frameLayout);
                if (mCustomView != null) {
                    callback.onCustomViewHidden();
                    return;
                }
                //赋值，关闭时需要调用
                mCustomView = view;
                // 将video放到FrameLayout中
                mFrameLayout.addView(mCustomView);
                //  退出全屏模式时释放需要调用，这里先赋值
                mCustomViewCallback = callback;
                // 设置webView隐藏
                mWebView.setVisibility(View.GONE);
                //切换至横屏
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }

            //网页退出全屏模式监听
            @Override
            public void onHideCustomView() {
                //显示竖屏时候的webview
                mWebView.setVisibility(View.VISIBLE);
                if (mCustomView == null) {
                    return;
                }
                //隐藏
                mCustomView.setVisibility(View.GONE);
                //从当前视图中移除
                mFrameLayout.removeView(mCustomView);
                //释放自定义视图
                mCustomViewCallback.onCustomViewHidden();
                mCustomView = null;

                //切换至竖屏
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                super.onHideCustomView();
            }

            //WebView文件上传 https://www.jianshu.com/p/2ad472027410
            @Override
            public boolean onShowFileChooser(android.webkit.WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                uploadMessage=filePathCallback;
                //网页文件上传回调
                //这里打开图库
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "Image Chooser"), REQUEST_CODE_CHOOSE);
                return true;
            }
        });

        //https://www.jianshu.com/p/6e38e1ef203a
        mWebView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                // TODO: 2017-5-6 处理下载事件

                AlertDialog dialog;
                dialog = new AlertDialog.Builder(webview.this).setTitle("下载操作")
                        .setIcon(R.drawable.iconyx)
                        .setMessage("是否下载")
                        .setPositiveButton("系统下载器下载", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                downloadBySystem(url,contentDisposition,mimeType);
                            }
                        })
                        .setNegativeButton("取消",null)
                        .setNeutralButton("浏览器下载", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                downloadByBrowser(url);
                            }
                        })
                        .create();
                dialog.show();
            }
        });
        if(com_vipsp==null){
            if (url.startsWith("http")) {
            mWebView.loadUrl(url);
        } else {
            mWebView.loadUrl(share.getString("searchurl", "https://www.baidu.com/s?wd=") + url);
        }
        }else {mWebView.loadUrl(com_vipsp);}


        //https://www.jianshu.com/p/377706b9e00b
        mWebView.setOnScrollChangeListener(new MyWebView.OnScrollChangeListener() {
            @Override
            public void onPageEnd(int l, int t, int oldl, int oldt) {

            }

            @Override
            public void onPageTop(int l, int t, int oldl, int oldt) {

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {

                if(share.getString("vtb", "滑动时隐藏上下部分【关闭】").equals("滑动时隐藏上下部分【开启】")){
                    if(oldt-t>100){
                        view_top.setVisibility(View.VISIBLE);
                        buttom.setVisibility(View.VISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                        getWindow().setStatusBarColor(getResources().getColor(R.color.white));//设置状态栏颜色
                        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏图标和文字颜色为暗色


                    }else if (oldt-t<100){
                        view_top.setVisibility(View.GONE);
                        buttom.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

                    }
                }
            }
        });

    }


    /**
     * 初始化事件
     */
    private void initEvent(){
        //关闭
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_top.setVisibility(View.GONE);
            }
        });
        //上一个
        btn_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebView.findNext(false);
            }
        });
        //下一个
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebView.findNext(true);
            }
        });
        //输入文本自动查询
        et_key.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void afterTextChanged(Editable key) {
                //搜索点击
                mWebView.findAllAsync(key.toString());
                mWebView.setFindListener(new android.webkit.WebView.FindListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onFindResultReceived(int position, int all, boolean b) {
                        tv_position.setText("(位置："+(position+1)+"/"+all+")");
                    }
                });
            }
        });
    }


    private void exitDialog(){
        AlertDialog dialog;
        dialog = new AlertDialog.Builder(this).setTitle("退出提示")
                .setIcon(R.drawable.iconyx)
                .setMessage("是否退出")
                .setPositiveButton("关闭程序", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        exit();
                    }
                })
                .setNegativeButton("取消",null)
                .setNeutralButton("关闭当前页面", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();//与System.exit(0)区别 https://www.jianshu.com/p/d2e6def08f88
                    }
                })
                .create();
        dialog.show();
    }
    private void resourcedialog(){
        source_dialog = new Dialog(webview.this, R.style.MyDialogActivityStyle);
        View list_view = View.inflate(webview.this, R.layout.dialogresource, null);
        source_dialog.setContentView(list_view);
        source_dialog.setCanceledOnTouchOutside(true);
        Window window =source_dialog.getWindow();//得到对话框的窗口．
        WindowManager.LayoutParams wl = window.getAttributes();
        //https://snakey.blog.csdn.net/article/details/53693446?spm=1001.2101.3001.6650.2&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-2.no_search_link&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-2.no_search_link
        // 将对话框的大小按屏幕大小的百分比设置
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
//                wl.x = x;//设置对话框的位置．0为中间
//                wl.y = y;
        wl.width = (int)(display.getWidth() * 1); //设置宽度
        wl.height = (int)(display.getHeight() * 0.4);
        wl.alpha = 1f;// 设置对话框的透明度,1f不透明
        window.setGravity(Gravity.BOTTOM);//底部出现//设置显示在中间
        window.setAttributes(wl);

        //点击函数：
        lv=list_view.findViewById(R.id.resource_list);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (if_exist) {
                    Intent intent = new Intent(webview.this, webview.class);//没有任何参数（意图），只是用来传递数据
                    intent.putExtra("searchText", query_by_id(position));
                    startActivity(intent);
                }
            }
        });

    }
    private void initListener() {
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new AlertDialog.Builder(webview.this)	// 创建Dialog
                        .setTitle("返回首页")		// 设置标题
                        .setMessage("退出浏览页面并且直接返回首页！！")	// 设置组件
                        .setNegativeButton("确定退出", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setPositiveButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .create();// 创建对话框
                dialog.show();		// 显示对话框
            }
        });
        txt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_app.setVisibility(View.GONE);
            }
        });

        home_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //动态权限申请
                if (ContextCompat.checkSelfPermission(webview.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(webview.this, new String[]{Manifest.permission.CAMERA}, 1);
                } else {
                    //扫码
                    goScan();
                }
            }
        });
        open_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                source_dialog.show();
            }
        });
        //监听编辑框
        txtTitle.addTextChangedListener(new EditChangedListener());
        home_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(webview.this,MainActivity2.class);
                startActivityForResult(intent,2);
            }
        });
        search_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 先隐藏键盘
//                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
//                        .hideSoftInputFromWindow(Main4Activity.this.getCurrentFocus()
//                                .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//进行搜索操作的方法，在该方法中可以加入mEditSearchUser的非空判断
                if (txtTitle.getText().toString().startsWith("http")) {
                    mWebView.loadUrl(txtTitle.getText().toString());
                } else {
                    mWebView.loadUrl(share.getString("searchurl", "https://www.baidu.com/s?wd=") + txtTitle.getText().toString());
                }
            }
        });

////接收来自主页传来的搜索引擎，更改对应的搜索图标并将传递到webview的搜索引擎更改
//        if(searchpic.equals("https://www.baidu.com/s?wd=")){
//            Main4Activity.this.home_search.setImageResource(R.drawable.baidu);
//            surl = "https://www.baidu.com/s?wd=";
//        }else if (searchpic.equals("https://www.sogou.com/web?query=")){
//            Main4Activity.this.home_search.setImageResource(R.drawable.sougou);
//            surl = "https://www.sogou.com/web?query=";
//        }else if (searchpic.equals("https://www.google.com/search?q=")){
//            Main4Activity.this.home_search.setImageResource(R.drawable.chrome);
//            surl = "https://www.google.com/search?q=";
//        }else  if(searchpic.equals("http://cn.bing.com/search?q=")){
//            Main4Activity.this.home_search.setImageResource(R.drawable.bing);
//            surl = "http://cn.bing.com/search?q=";
//        }

        //键盘回车搜索
        txtTitle.setOnKeyListener(new View.OnKeyListener() {
            @Override

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
// 先隐藏键盘
                    ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                            .hideSoftInputFromWindow(webview.this.getCurrentFocus()
                                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//进行搜索操作的方法，在该方法中可以加入mEditSearchUser的非空判断
                    mWebView.loadUrl(share.getString("searchurl", "https://www.baidu.com/s?wd=")+txtTitle.getText().toString());
                }
                return false;
            }
        });
        error_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorvs=false;
                mWebView.goBack();
            }
        });
        historyDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.setClass(webview.this,bookmark.class);
                startActivityForResult(intent,1);
            }
        });

        //网页前进后退并替换图标
        back.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                if (mWebView.canGoBack()) {
                    mWebView.goBack();
                } else {
                    if((System.currentTimeMillis()-exitTime) > 2000){
                        Toast.makeText(webview.this, "已经回到第一页，再按一次退出", Toast.LENGTH_SHORT).show();
                        exitTime = System.currentTimeMillis();
                    } else {
                        finish();
                    }
                }


            }
        });

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mWebView.canGoForward()) {
                    mWebView.goForward();
                } else {
                    Toast.makeText(webview.this, "已经去到网页最后一页", Toast.LENGTH_SHORT).show();

                }

            }
        });
//打开浮窗
        list_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor edit=share.edit();
                //弹出对话框
                final Dialog dialog = new Dialog(webview.this, R.style.MyDialogActivityStyle);
                View list_view = View.inflate(webview.this, R.layout.activity_list3, null);
                dialog.setContentView(list_view);
                dialog.setCanceledOnTouchOutside(true);
                Window window = dialog.getWindow();//得到对话框的窗口．
                WindowManager.LayoutParams wl = window.getAttributes();

                //https://snakey.blog.csdn.net/article/details/53693446?spm=1001.2101.3001.6650.2&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-2.no_search_link&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-2.no_search_link
                // 将对话框的大小按屏幕大小的百分比设置

                WindowManager windowManager = getWindowManager();
                Display display = windowManager.getDefaultDisplay();
//                wl.x = x;//设置对话框的位置．0为中间
//                wl.y = y;
                wl.width = (int)(display.getWidth() * 1); //设置宽度
                wl.height = (int)(display.getHeight() * 0.4);
                wl.alpha = 1f;// 设置对话框的透明度,1f不透明
                window.setGravity(Gravity.BOTTOM);//底部出现//设置显示在中间
                window.setAttributes(wl);

                add_text=list_view.findViewById(R.id.add_text);
                TextView nighttxt=list_view.findViewById(R.id.nighttxt);
                LinearLayout list_b=list_view.findViewById(R.id.list_b);
                if (share.getString("night", "夜间模式关闭").equals("夜间模式关闭")){
                    nighttxt.setText("夜间模式");
                    list_b.setBackgroundResource(R.drawable.view);

                }else {
                    nighttxt.setText("日间模式");
                    list_b.setBackgroundResource(R.drawable.night_view);
                }
                if(select1(mWebView.getUrl())==1){
                    add_text.setText("取消书签");
                }
                list_view.findViewById(R.id.txt_open).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showNetText();
                        dialog.dismiss();
                    }
                });
                list_view.findViewById(R.id.code).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        showNetPageData();
                        dialog.dismiss();
                    }
                });

                list_view.findViewById(R.id.night).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if(share.getString("night", "夜间模式关闭").equals("夜间模式关闭")){
                            buttom.setBackgroundColor(Color.rgb(	105,105,105));
                            top.setBackgroundColor(Color.rgb(0,0,0));
                            mWebView.setBackgroundColor(Color.rgb(0,0,0));
                            search_view.setBackgroundColor(Color.rgb(		105,105,105));
                            mWebView.loadUrl("javascript:(function() {" + "var parent = document.getElementsByTagName('head').item(0);" + "var style = document.createElement('style');" + "style.type = 'text/css';" + "style.innerHTML = window.atob('" + nightCode + "');" + "parent.appendChild(style)" + "})();");
                            edit.putString("night","夜间模式开启");
                            edit.commit();
                        }else {
                            buttom.setBackgroundColor(Color.rgb(	245,245,245));
                            top.setBackgroundColor(Color.rgb(192,192,192));
                            mWebView.setBackgroundColor(Color.rgb(0,0,0));
                            search_view.setBackgroundColor(Color.rgb(	245,245,245));
                            edit.putString("night","夜间模式关闭");
                            edit.commit();
                            mWebView.reload();
                        }
                        dialog.dismiss();
                    }
                });
                list_view.findViewById(R.id.clean).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Dialog dialogc = new AlertDialog.Builder(webview.this)	// 创建Dialog
                                .setIcon(R.drawable.iconyx)	// 设置显示图片
                                .setTitle("缓存清理")		// 设置标题
                                .setMessage("这个操作会清理网页缓存、网页表单数据、和网页账号密码缓存，如果出现切换账号出现问题可进行清理")	// 设置组件
                                .setNegativeButton("确认清理", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mWebView.clearCache(true);
                                        mWebView.clearHistory();
                                        mWebView.clearFormData();
                                        CookieManager.getInstance().removeSessionCookie();//过期移除
                                        Toast.makeText(webview.this, "清理成功", Toast.LENGTH_SHORT).show();

                                    }
                                })
                                .setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .create();// 创建对话框
                        dialogc.show();		// 显示对话框
                        dialog.dismiss();
                    }
                });
                list_view.findViewById(R.id.ad).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent();
                        intent.setClass(webview.this,yad.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                list_view.findViewById(R.id.reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mWebView.loadUrl("javascript:window.location.reload(true)");
                        dialog.dismiss();
                    }
                });
                list_view.findViewById(R.id.ua).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(webview.this)
                                .setItems(new String[]{"移动端UA","iphoneUA","PC端UA","塞班UA(对去广告有奇效)","贴吧、知乎去看帖限制UA","简单搜索UA(去掉百度搜索出现的推荐广告)","QQ浏览器UA(可直接下载微云文件)","微信UA(可以访问微信才可以访问的链接)","百度网盘UA"}, (dialog1, which1) -> {

                                    switch (which1) {
                                        case 0:
                                            edit.putString("UA","Mozilla/5.0 (Linux; U; Android 11; zh-cn; KB2000 Build/RP1A.201005.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 HeyTapBrowser/40.7.25.1");
                                            edit.commit();

                                            mWebView.reload();
                                            break;
                                        case 1:
                                            edit.putString("UA","Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5");
                                            edit.commit();

                                            mWebView.reload();
                                            break;
                                        case 2:
                                            edit.putString("UA","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36");
                                            edit.commit();

                                            mWebView.reload();
                                            break;
                                        case 3:
                                            edit.putString("UA","Mozilla/5.0 (Symbian/3; Series60/5.2 NokiaN8-00/012.002; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/533.4 (KHTML, like Gecko) NokiaBrowser/7.3.0 Mobile Safari/533.4 3gpp-gba");
                                            edit.commit();

                                            mWebView.reload();
                                            break;
                                        case 4:
                                            edit.putString("UA","UCWEB/2.0 (MIDP-2.0; U; Adr 9.0.0) UCBrowser U2/1.0.0 Gecko/63.0 Firefox/63.0 iPhone/7.1 SearchCraft/2.8.2 baiduboxapp/3.2.5.10 BingWeb/9.1 ALiSearchApp/2.4");
                                            edit.commit();

                                            mWebView.reload();
                                            break;
                                        case 5:
                                            edit.putString("UA","Mozilla/5.0 (Linux; Android 7.0; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/48.0.2564.116 Mobile Safari/537.36 T7/10.3 SearchCraft/2.6.2 (Baidu; P1 7.0)");
                                            edit.commit();

                                            mWebView.reload();
                                            break;
                                        case 6:
                                            edit.putString("UA","MQQBrowser/26 Mozilla/5.0 (Linux; U; Android 2.3.7; zh-cn; MB200 Build/GRJ22; CyanogenMod-7) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1");
                                            edit.commit();

                                            mWebView.reload();
                                            break;
                                        case 7:
                                            edit.putString("UA","Mozilla/5.0 (Linux; Android 6.0; NEM-AL10 Build/HONORNEM-AL10; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.132 MQQBrowser/6.2 TBS/043906 Mobile Safari/537.36 MicroMessenger/6.6.1.1220(0x26060133) NetType/WIFI Language/zh_CN");
                                            edit.commit();

                                            mWebView.reload();
                                            break;
                                        case 8:
                                            edit.putString("UA","netdisk;5.5.1;PC;PC-Windows;6.2.9200;WindowsBaiduYunGuanJia");
                                            edit.commit();

                                            mWebView.reload();
                                            break;
                                    }

                                })
                                .show();
                    }
                });


                list_view.findViewById(R.id.end).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        exitDialog();
                        dialog.dismiss();
                    }
                });
                list_view.findViewById(R.id.history).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent();
                        intent.setClass(webview.this,bookmark.class);
                        startActivityForResult(intent,1);
                        dialog.dismiss();
                    }
                });
                list_view.findViewById(R.id.addweb).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        select(mWebView.getUrl());
                        dialog.dismiss();
                    }
                });
                list_view.findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //分享网页链接
                        Intent share_intent = new Intent();
                        share_intent.setAction(Intent.ACTION_SEND);//设置分享行为
                        share_intent.setType("text/plain");//设置分享内容的类型
                        share_intent.putExtra(Intent.EXTRA_SUBJECT, "share");//添加分享内容标题
                        share_intent.putExtra(Intent.EXTRA_TEXT,  mWebView.getUrl());//添加分享内容
                        //创建分享的Dialog
                        share_intent = Intent.createChooser(share_intent, "share");
                        startActivity(share_intent);
                        dialog.dismiss();
                    }
                });
                list_view.findViewById(R.id.tool).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        list_view.findViewById(R.id.main1).setVisibility(View.GONE);
                        list_view.findViewById(R.id.main2).setVisibility(View.GONE);
                        list_view.findViewById(R.id.tool1).setVisibility(View.VISIBLE);
                        list_view.findViewById(R.id.tool2).setVisibility(View.VISIBLE);

                    }
                });
                list_view.findViewById(R.id.closetool).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        list_view.findViewById(R.id.tool1).setVisibility(View.GONE);
                        list_view.findViewById(R.id.tool2).setVisibility(View.GONE);
                        list_view.findViewById(R.id.main1).setVisibility(View.VISIBLE);
                        list_view.findViewById(R.id.main2).setVisibility(View.VISIBLE);


                    }
                });
                list_view.findViewById(R.id.xiutan).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SniffingUtil.get().activity(webview.this).referer("https://github.com/wizos/Sniffing").callback(webview.this).url(mWebView.getUrl()).start();
                        dialog.dismiss();

                    }
                });

                list_view.findViewById(R.id.setting).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent it = new Intent(webview.this, setting.class);
                        startActivity(it);
                        dialog.dismiss();

                    }
                });
                list_view.findViewById(R.id.js).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent4=new Intent();
                        intent4.setClass(webview.this,jsstate.class);
                        startActivity(intent4);
                        dialog.dismiss();
                    }

                });

                list_view.findViewById(R.id.yncz).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ll_top.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                    }

                });
                list_view.findViewById(R.id.jsgl).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent3=new Intent();
                        intent3.setClass(webview.this,jstool.class);
                        startActivity(intent3);
                        dialog.dismiss();
                    }

                });
                list_view.findViewById(R.id.cv).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent2 = new Intent(webview.this,dialog_cv.class);//没有任何参数（意图），只是用来传递数据
                        startActivity(intent2);
                        dialog.dismiss();
                    }

                });
                list_view.findViewById(R.id.prefix).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent();
                        intent.setClass(webview.this,prefix.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }

                });
                list_view.findViewById(R.id.wyfy).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(webview.this)
                                .setItems(new String[]{"谷歌翻译", "谷歌翻译需要梯子","彩云翻译"}, (dialog, which) -> {

                                    switch (which) {
                                        case 0:
                                            mWebView.loadUrl("javascript:"+"(function(){\n" +
                                                    "\n" +
                                                    "void((function () {\n" +
                                                    "        var script = document.createElement('script');\n" +
                                                    "        script.src = '//cdn.jsdelivr.net/gh/damengzhu/js1@main/element.js';\n" +
                                                    "        document.getElementsByTagName('head')[0].appendChild(script);\n" +
                                                    "\n" +
                                                    "        var google_translate_element = document.createElement('div');\n" +
                                                    "        google_translate_element.id = 'google_translate_element';\n" +
                                                    "        google_translate_element.style = 'position:fixed; bottom:10px; right:10px; cursor:pointer;';\n" +
                                                    "        document.documentElement.appendChild(google_translate_element);\n" +
                                                    "\n" +
                                                    "        script = document.createElement('script');\n" +
                                                    "        script.innerHTML = \"function googleTranslateElementInit() {\" +\n" +
                                                    "            \"new google.translate.TranslateElement({\" +\n" +
                                                    "            \"layout: google.translate.TranslateElement.InlineLayout.SIMPLE,\" +\n" +
                                                    "            \"multilanguagePage: true,\" +\n" +
                                                    "            \"pageLanguage: 'auto',\" +\n" +
                                                    "            \"includedLanguages: 'zh-CN,zh-TW,en,ko,ja'\" +\n" +
                                                    "            \"}, 'google_translate_element');}\";\n" +
                                                    "        document.getElementsByTagName('head')[0].appendChild(script);\n" +
                                                    "})());\n" +
                                                    "\n" +
                                                    "})();");
                                            break;
                                        case 1:
                                            mWebView.loadUrl("javascript:"+"/*\n" +
                                                    " * @name: 谷歌原生网页翻译\n" +
                                                    " * @Author: 谷花泰\n" +
                                                    " * @version: 2.0\n" +
                                                    " * @description: 调用谷歌翻译接口翻译整个网站\n" +
                                                    " * @include: *\n" +
                                                    " * @createTime: 2019-10-12 09:11:58\n" +
                                                    " * @updateTime   : 2020-03-16 00:51:04\n" +
                                                    " */\n" +
                                                    "(function () {\n" +
                                                    "  /* 判断是否该执行 */\n" +
                                                    "  /* 网址黑名单制，遇到这些域名不执行 */\n" +
                                                    "  const blackList = ['zhihu.com', 'twitter.com', 'wenku.baidu.com', 'wk.baidu.com'];\n" +
                                                    "\n" +
                                                    "  const hostname = window.location.hostname;\n" +
                                                    "  const key = encodeURIComponent('谷花泰:谷歌原生网页翻译:执行判断');\n" +
                                                    "  const isBlack = blackList.some(keyword => {\n" +
                                                    "    if (hostname.match(keyword)) {\n" +
                                                    "      return true;\n" +
                                                    "    };\n" +
                                                    "    return false;\n" +
                                                    "  });\n" +
                                                    "\n" +
                                                    "  if (isBlack || window[key]) {\n" +
                                                    "    return;\n" +
                                                    "  };\n" +
                                                    "  window[key] = true;\n" +
                                                    "\n" +
                                                    "  /* 开始执行代码 */\n" +
                                                    "  const config = {\n" +
                                                    "    initName: 'googleTranslate',\n" +
                                                    "    initElm: 'google_translate_elm',\n" +
                                                    "    /*多语言页面支持*/\n" +
                                                    "    multilanguagePage: true,\n" +
                                                    "    /* 哪种需要转 */\n" +
                                                    "    pageLanguage: 'auto',\n" +
                                                    "    /* 能转成啥 */\n" +
                                                    "    includedLanguages: 'zh-CN,zh-TW,en',\n" +
                                                    "    /* 隐藏工具bar */\n" +
                                                    "    autoDisplay: false\n" +
                                                    "  };\n" +
                                                    "\n" +
                                                    "  /* 注入谷歌翻译脚本 */\n" +
                                                    "  const script = document.createElement('script');\n" +
                                                    "  script.src = `//translate.google.cn/translate_a/element.js?&cb=${config.initName}`;\n" +
                                                    "  script.async = true;\n" +
                                                    "  document.head.appendChild(script);\n" +
                                                    "\n" +
                                                    "  /* 创建谷歌翻译右下角的语言选择框容器 */\n" +
                                                    "  const div = document.createElement('div');\n" +
                                                    "  div.id = config.initElm;\n" +
                                                    "  div.setAttribute('style', `\n" +
                                                    "    position: fixed;\n" +
                                                    "    bottom: 5vw;\n" +
                                                    "    right: 5vw;\n" +
                                                    "    z-index: 999999999;\n" +
                                                    "  `);\n" +
                                                    "  document.documentElement.appendChild(div);\n" +
                                                    "\n" +
                                                    "  /* 一些样式 */\n" +
                                                    "  const style = document.createElement('style');\n" +
                                                    "  style.innerHTML = `\n" +
                                                    "    body {\n" +
                                                    "      position: relative;\n" +
                                                    "      top: 0 !important;\n" +
                                                    "      left: 0 !important;    \n" +
                                                    "    }\n" +
                                                    "    .skiptranslate iframe{\n" +
                                                    "      width: 0px !important;\n" +
                                                    "      height: 0px !important;\n" +
                                                    "      overflow: hidden;\n" +
                                                    "      display: none !important;\n" +
                                                    "    }\n" +
                                                    "    .goog-te-gadget-simple {\n" +
                                                    "      border: 1px solid #ececec !important;\n" +
                                                    "      border-radius: 4px;\n" +
                                                    "      padding: 8px;\n" +
                                                    "      line-height: 26px;\n" +
                                                    "      text-align: center;\n" +
                                                    "    }\n" +
                                                    "    .goog-te-gadget-simple a,.goog-te-gadget-simple a:link,.goog-te-gadget-simple a:visited,.goog-te-gadget-simple a:hover,.goog-te-gadget-simple a:active{\n" +
                                                    "      text-decoration: none;\n" +
                                                    "      color:inherit;\n" +
                                                    "    }\n" +
                                                    "  `;\n" +
                                                    "  document.head.appendChild(style);\n" +
                                                    "\n" +
                                                    "  /* 谷歌翻译实例化函数 */\n" +
                                                    "  window[config.initName] = () => {\n" +
                                                    "    new window.google.translate.TranslateElement({\n" +
                                                    "      layout: google.translate.TranslateElement.InlineLayout.SIMPLE,\n" +
                                                    "      multilanguagePage: config.multilanguagePage,\n" +
                                                    "      pageLanguage: config.pageLanguage,\n" +
                                                    "      includedLanguages: config.includedLanguages,\n" +
                                                    "      autoDisplay: config.autoDisplay\n" +
                                                    "    }, config.initElm);\n" +
                                                    "  };\n" +
                                                    "\n" +
                                                    "  /* 监听dom函数 */\n" +
                                                    "  function observe({ targetNode, config = {}, callback = () => { } }) {\n" +
                                                    "    if (!targetNode) {\n" +
                                                    "      return;\n" +
                                                    "    };\n" +
                                                    "    config = Object.assign({\n" +
                                                    "      attributes: true,\n" +
                                                    "      childList: true,\n" +
                                                    "      subtree: true\n" +
                                                    "    }, config);\n" +
                                                    "    const observer = new MutationObserver(callback);\n" +
                                                    "    observer.observe(targetNode, config);\n" +
                                                    "  };\n" +
                                                    "\n" +
                                                    "  /* 监听右下角的语言选择框 */\n" +
                                                    "  const initElm = document.querySelector(`#${config.initElm}`);\n" +
                                                    "  observe({\n" +
                                                    "    targetNode: initElm,\n" +
                                                    "    config: {\n" +
                                                    "      attributes: false\n" +
                                                    "    },\n" +
                                                    "    callback(mutationList, observer) {\n" +
                                                    "      /* 遍历节点 */\n" +
                                                    "      mutationList.forEach(mutation => {\n" +
                                                    "        Array.from(mutation.addedNodes, node => {\n" +
                                                    "          /* 清除 google image */\n" +
                                                    "          const googleImages = document.querySelectorAll(`#goog-gt-tt img, #${config.initElm} img`);\n" +
                                                    "          Array.from(googleImages, img => {\n" +
                                                    "            img.parentNode.removeChild(img);\n" +
                                                    "          });\n" +
                                                    "\n" +
                                                    "          /* 添加关闭按钮 */\n" +
                                                    "          if (node.className === 'goog-te-menu-value') {\n" +
                                                    "            const btnContent = document.querySelector('.goog-te-gadget-simple > span > a');\n" +
                                                    "            const closeBtn = document.createElement('span');\n" +
                                                    "            closeBtn.innerText = '关闭 | ';\n" +
                                                    "            closeBtn.addEventListener('click', (e) => {\n" +
                                                    "              initElm.parentNode.removeChild(initElm);\n" +
                                                    "              e.stopPropagation();\n" +
                                                    "            }, true);\n" +
                                                    "            btnContent.parentNode.insertBefore(closeBtn, btnContent);\n" +
                                                    "          };\n" +
                                                    "\n" +
                                                    "        });\n" +
                                                    "        /* 结束监听 */\n" +
                                                    "        observer.disconnect();\n" +
                                                    "      });\n" +
                                                    "    }\n" +
                                                    "  });\n" +
                                                    "})();\n");
                                            break;
                                        case 2:
                                            mWebView.loadUrl("javascript:"+"!function(){if(!document.getElementById(\"gzfy\")){var zfyan=document.createElement(\"span\");zfyan.id=\"gzfy\";zfyan.innerHTML=\"彩\";zfyan.style.cssText=\"display:none;text-align:center !important;font-size:22px;width:32px;height:32px;line-height:28px;text-align:center;float:right;position:fixed;right:15px;top:75%;color:#fff;opacity:0.6;background:#008080;cursor:pointer;position:fixed !important;z-index:9999999999 !important;box-shadow:0px 1px 1px #000;border-radius:50%\";zfyan.addEventListener(\"click\",function(){zfy()});document.body.appendChild(zfyan);}var zfyhdy1,zfyhdy2;document.addEventListener(\"touchstart\",function(e){zfyhdy1=e.changedTouches[0].clientY});document.addEventListener(\"touchmove\",function(e){zfyhdy2=e.changedTouches[0].clientY;document.getElementById(\"gzfy\").style.display=zfyhdy2-zfyhdy1>0?\"block\":\"none\"});function zfy(){var zfyfy=document.getElementById(\"gzfy\");zfyfy.parentNode.removeChild(zfyfy);var cyfy=document.createElement(\"script\");cyfy.type=\"text/javascript\";cyfy.charset=\"UTF-8\";cyfy.src=(\"https:\"==document.location.protocol?\" https://\":\"http://\")+\"caiyunapp.com/dest/trs.js\";document.body.appendChild(cyfy); }}();");
                                            break;
                                    }
                                })
                                .show();
                    }

                });
                list_view.findViewById(R.id.wywz).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Dialog textSizedialog = new Dialog(webview.this, R.style.MyDialogActivityStyle);
                        View textSizeview = View.inflate(webview.this, R.layout.dialog_textsize, null);
                        textSizedialog.setContentView(textSizeview);
                        textSizedialog.setCanceledOnTouchOutside(true);
                        Window window = textSizedialog.getWindow();//得到对话框的窗口．
                        WindowManager.LayoutParams wl = window.getAttributes();
                        WindowManager windowManager = getWindowManager();
                        Display display = windowManager.getDefaultDisplay();
//                wl.x = x;//设置对话框的位置．0为中间
//                wl.y = y;
                        wl.width = (int)(display.getWidth() * 1); //设置宽度
                        wl.height = (int)(display.getHeight() * 0.65);
                        wl.alpha = 1f;// 设置对话框的透明度,1f不透明
                        window.setGravity(Gravity.CENTER);//底部出现//设置显示在中间
                        window.setAttributes(wl);

                        //显示详细信息
                        WebSettings webSettings = mWebView.getSettings();
                        textSizeview.findViewById(R.id.bt1).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                edit.putString("textsize","100");
                                edit.commit();
                                webSettings.setTextZoom(Integer.parseInt(share.getString("textsize", "100")));
                                mWebView.reload();
                                textSizedialog.dismiss();
                            }
                        });

                        textSizeview.findViewById(R.id.bt2).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                edit.putString("textsize","125");
                                edit.commit();
                                mWebView.reload();
                                textSizedialog.dismiss();
                            }
                        });
                        textSizeview.findViewById(R.id.bt3).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                edit.putString("textsize","150");
                                edit.commit();

                                webSettings.setTextZoom(Integer.parseInt(share.getString("textsize", "100")));
                                mWebView.reload();
                                textSizedialog.dismiss();
                            }
                        });
                        textSizeview.findViewById(R.id.bt4).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                edit.putString("textsize","175");
                                edit.commit();
                                webSettings.setTextZoom(Integer.parseInt(share.getString("textsize", "100")));
                                mWebView.reload();
                                textSizedialog.dismiss();
                            }
                        });
                        textSizeview.findViewById(R.id.bt5).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                edit.putString("textsize","200");
                                edit.commit();
                                webSettings.setTextZoom(Integer.parseInt(share.getString("textsize", "100")));
                                mWebView.reload();
                                textSizedialog.dismiss();
                            }
                        });
                        textSizeview.findViewById(R.id.bt6).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                edit.putString("textsize","50");
                                edit.commit();
                                webSettings.setTextZoom(Integer.parseInt(share.getString("textsize", "100")));
                                mWebView.reload();
                                textSizedialog.dismiss();
                            }
                        });
                        textSizeview.findViewById(R.id.bt7).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                edit.putString("textsize","75");
                                edit.commit();
                                mWebView.reload();
                                textSizedialog.dismiss();
                            }
                        });
                        textSizedialog.show();

                    }

                });
                dialog.show();

            }
        });
        mWebView.setLongClickable(true);//禁用长时间单击
//网页长按事件
        mWebView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                WebView.HitTestResult result =mWebView.getHitTestResult();
                String imgurl = result.getExtra();

                int type = result.getType();//https://www.cnblogs.com/classloader/p/5302784.html
                switch (type) {
                    case WebView.HitTestResult.EDIT_TEXT_TYPE: // 选中的文字类型
                    case WebView.HitTestResult.PHONE_TYPE: // 处理拨号
                    case WebView.HitTestResult.UNKNOWN_TYPE: //未知
                    case WebView.HitTestResult.EMAIL_TYPE: // 处理Email
                    case WebView.HitTestResult.GEO_TYPE: // &emsp;地图类型
                        new AlertDialog.Builder(webview.this)
                                .setIcon(R.drawable.iconyx)
                                .setItems(new String[]{"标识为广告(当前有效)","标识广告(总是生效)"}, (dialog, which) -> {

                                    switch (which) {
                                        case 0:
                                           addDesign();
                                            break;
                                        case 1:
                                            addDesignDB();
                                            break;
                                    }
                                })
                                .show();
                        break;
                    case WebView.HitTestResult.SRC_ANCHOR_TYPE: // 超链接
                        String urlzz=null;
                        if(imgurl.length()>31){
                            urlzz=imgurl.substring(0,30);
                        }else urlzz=imgurl;
                        new AlertDialog.Builder(webview.this)
                                .setIcon(R.drawable.iconyx)
                                .setItems(new String[]{"复制链接", "分享链接","转到   "+urlzz+"......","标识为广告(当前有效)","标识广告(总是生效)"}, (dialog, which) -> {
                                    switch (which) {
                                        case 0:
                                            // 将ClipData内容放到系统剪贴板里。
                                            ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                            ClipData mClipData = ClipData.newPlainText("Label",imgurl);
                                            cm.setPrimaryClip(mClipData);
                                            Toast.makeText(webview.this, "已复制", Toast.LENGTH_SHORT).show();
                                            break;
                                        case 1:
                                            //分享网页链接
                                            Intent share_intent = new Intent();
                                            share_intent.setAction(Intent.ACTION_SEND);//设置分享行为
                                            share_intent.setType("text/plain");//设置分享内容的类型
                                            share_intent.putExtra(Intent.EXTRA_SUBJECT, "share");//添加分享内容标题
                                            share_intent.putExtra(Intent.EXTRA_TEXT,imgurl);//添加分享内容
                                            //创建分享的Dialog
                                            share_intent = Intent.createChooser(share_intent, "share");
                                            startActivity(share_intent);
                                            break;
                                        case 2:
                                            Intent intent = new Intent(webview.this, webview.class);//没有任何参数（意图），只是用来传递数据
                                            intent.putExtra("searchText",imgurl);
                                            startActivity(intent);
                                            break;
                                        case 3:
                                           addDesign();
                                            break;
                                        case 4:
                                            addDesignDB();
                                            break;
                                    }
                                })
                                .show();

                        break;
                    case WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE: // 带有链接的图片类型
                    case WebView.HitTestResult.IMAGE_TYPE: // 处理长按图片的菜单项
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int displayheight = displayMetrics.heightPixels;//高度
                        int displayWidth = displayMetrics.widthPixels;//宽度

                        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
                        String newPath="/dmbrowser/downloadpic";
                        String qrPath="/dmbrowser/qrpic";
                        File f = new File(root,newPath);
                        File qr = new File(root,qrPath);
                        new AlertDialog.Builder(webview.this)
                                .setIcon(R.drawable.iconyx)
                                .setItems(new String[]{"保存图片到本地", "分享图片","复制链接地址","搜图","识别二维码","标识为广告(当前有效)","标识广告(总是生效)"}, (dialog, which) -> {

                                    switch (which) {
                                        case 0:
//保存图片https://www.jianshu.com/p/4f457a124d67
                                            if (!f.exists()) {
                                                f.mkdirs();

                                            }else {
                                                //文件名为时间
                                                long timeStamp = System.currentTimeMillis();
                                                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
                                                String sd = sdf.format(new Date(timeStamp));
                                                String fileName = sd + ".png";
                                                File mFile = new File(root+newPath,fileName);
                                                //https://www.jianshu.com/p/4f457a124d67
                                                DownloadImage downloadImage = new DownloadImage(imgurl, getApplicationContext(),0,0, mFile, new DownloadImage.ImagedownLoadCallBack() {
                                                    @Override
                                                    public void onDownLoadSuccess(Bitmap bitmap) {
                                                        Toast.makeText(webview.this, "下载完成,图片保存在"+root+newPath, Toast.LENGTH_SHORT).show();
                                                        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                                        Uri uri = Uri.fromFile(mFile);
                                                        intent.setData(uri);
                                                        sendBroadcast(intent);
                                                    }
                                                    @Override
                                                    public void onDownLoadFailed() {
                                                        Toast.makeText(webview.this, "下载失败", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                                new Thread(downloadImage).start();//异步下载

                                            }


                                            break;
                                        case 1:
                                            //分享图片
                                            Intent share_intent = new Intent();
                                            share_intent.setAction(Intent.ACTION_SEND);//设置分享行为
                                            share_intent.setType("text/plain");//设置分享内容的类型
                                            share_intent.putExtra(Intent.EXTRA_SUBJECT, "分享图片");//添加分享内容标题
                                            share_intent.putExtra(Intent.EXTRA_TEXT,imgurl);//添加分享内容
                                            //创建分享的Dialog
                                            share_intent = Intent.createChooser(share_intent, "云曦分享");
                                            startActivity(share_intent);

                                            break;
                                        case 2:
                                            ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                            ClipData mClipData = ClipData.newPlainText("Label",imgurl);
                                            cm.setPrimaryClip(mClipData);
                                            Toast.makeText(webview.this, "已复制", Toast.LENGTH_SHORT).show();
                                            break;

                                        case 3:
                                            Intent intent = new Intent(webview.this, webview.class);//没有任何参数（意图），只是用来传递数据
                                            intent.putExtra("searchText","http://pic.sogou.com/pic/ris_searchList.jsp?statref=search_download&v=5&ul=1&keyword="+imgurl);
                                            startActivity(intent);
                                            break;
                                        case 4:
                                            if (!qr.exists()) {
                                                qr.mkdirs();

                                            }else {
                                                //文件名为时间

                                                String fileName ="qr.png";
                                                File mFile = new File(root+qrPath,fileName);
                                                //https://www.jianshu.com/p/4f457a124d67
                                                DownloadImage downloadImage = new DownloadImage(imgurl, getApplicationContext(),displayWidth, displayheight, mFile, new DownloadImage.ImagedownLoadCallBack() {
                                                    @Override
                                                    public void onDownLoadSuccess(Bitmap bitmap) {
                                                        handleAlbumPic(mFile);
                                                    }
                                                    @Override
                                                    public void onDownLoadFailed() {
                                                        Toast.makeText(webview.this, "识别", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                                new Thread(downloadImage).start();//异步下载

                                            }
                                            break;
                                        case 5:
                                           addDesign();
                                            break;
                                        case 6:
                                            addDesignDB();
                                            break;
                                    }
                                })
                                .show();

                        break;
                }
//长按事件还可以复制黏贴返回false
                return false;
            }
        });

    }
    //查询数据库里的字段
    public void jsad(String doMainName) {
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sql ="SELECT * FROM adDB WHERE url='"+doMainName+"'";
        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        if(result.moveToFirst())//如果结果集的第一行有数据
        {
            String adtable=result.getString(1);
            refusead(adtable);
            result.close();
            db.close();

        }
        else {
            result.close();
            db.close();

        }
    }
    public void refusead(String adtable) {
        int count=0;
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";
        //String sql = "select * from " + TABLENAME + " where name like '%"+ username + "%'";
        Cursor result = db.rawQuery("select * from "+adtable, null);
        if (result != null && result.getCount() > 0){
            while (result.moveToNext())//moveToNext()与moveToFirst() https://www.cnblogs.com/zhaoleigege/p/5074963.html
            {
                count=count+1;
                String idcalss=result.getString(1);
                if(idcalss.startsWith("这是个class")){

                mWebView.loadUrl("javascript:(function() { " + "var x=document.getElementsByClassName("+"'"+idcalss.substring(8)+"'"+").length;\n" +
                        "for(var i=0;i<x;i++){\n" +
                        "document.getElementsByClassName("+"'"+idcalss.substring(8)+"'"+")[i].style.display='none';\n" +
                        "}})()");
            }else if(idcalss.startsWith("这是个id")){

                mWebView.loadUrl("javascript:(function() { " + "document.getElementById("+"'"+idcalss.substring(5)+"'"+").style.display='none';})()");
            }else{

            }
                if (count==result.getCount())
                    break;
            }
        }
        result.close();
        db.close();

    }
    //查询数据库里的字段
    public void selectad(String doMainName,String idclass) {
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String sql ="SELECT * FROM adDB WHERE url='"+doMainName+"'";
        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        if(result.moveToFirst())//如果结果集的第一行有数据
        {
            String adtable=result.getString(1);
            addidclass(adtable,idclass);
            result.close();
            db.close();

        }
        else {
            SharedPreferences share=getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
            SharedPreferences.Editor edit=share.edit();
            String tablead ="adtable";
            int count = share.getInt("adTABLENAME",0)+1;
            String sql2="create table if not exists'"+tablead+count+"'(id integer primary key autoincrement," +
                    "idclass text)";
            edit.putInt("adTABLENAME",count);
            edit.commit();
            db.execSQL(sql2);
            db.close();
            addDomain(tablead+count,doMainName,mWebView.getTitle(),idclass);
            result.close();
            db.close();

        }
    }
    //文件夹表添加数据
    public void addDomain(String adtable,String doMainName,String title,String idclass){
        //第二个参数是数据库名
        MyDatabaseHelper  dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("adtable",adtable);
        values.put("title", title);
        values.put("url", doMainName);
        db.insert("adDB",null,values);
        addidclass(adtable,idclass);

    }
    public void addidclass(String adtable,String idclass){
        //第二个参数是数据库名
        MyDatabaseHelper  dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        if(idclass!=""){
            values.put("idclass",idclass);
            db.insert(adtable,null,values);
            Toast.makeText(this,"已屏蔽",Toast.LENGTH_SHORT).show();
        }


    }

    /**
     * 处理选择的图片
     * @param qrPath
     */
    private void handleAlbumPic(File qrPath) {//https://blog.csdn.net/qq_27061049/article/details/115521167
        //获取选中图片的路径
        final Uri uri =Uri.fromFile(qrPath);//路径与uri互相转换 https://www.jianshu.com/p/33bc363290e9
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("正在扫描...");
        mProgress.setCancelable(false);
        mProgress.show();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Result result = scanningImage(uri);
                mProgress.dismiss();
                if (result != null) {
                    Toast.makeText(webview.this,String.valueOf(result), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(webview.this, "识别失败", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    /**
     * 扫描二维码图片的方法
     * @param uri
     * @return
     */
    public Result scanningImage(Uri uri) {//https://blog.csdn.net/qq_27061049/article/details/115521167
        if (uri == null) {
            return null;
        }
        Hashtable<DecodeHintType, String> hints = new Hashtable<>();
        hints.put(DecodeHintType.CHARACTER_SET, "UTF8"); //设置二维码内容的编码

        scanBitmap = BitmapUtil.decodeUri(this, uri, 500, 500);
        RGBLuminanceSource source = new RGBLuminanceSource(scanBitmap);
        BinaryBitmap bitmap1 = new BinaryBitmap(new HybridBinarizer(source));
        QRCodeReader reader = new QRCodeReader();
        try {
            return reader.decode(bitmap1, hints);
        } catch (NotFoundException e) {
            e.printStackTrace();
        } catch (ChecksumException e) {
            e.printStackTrace();
        } catch (FormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 跳转到扫码界面扫码
     */
    private void goScan(){//https://www.cnblogs.com/xch-yang/p/9418493.html
        Intent intent = new Intent(webview.this, CaptureActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SCAN);
    }

    //WebView 支持文件下载的几种方式 https://www.jianshu.com/p/6e38e1ef203a
    private void downloadByBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
    private void downloadBySystem(String url, String contentDisposition, String mimeType) {
        // 指定下载地址
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        // 允许媒体扫描，根据下载的文件类型被加入相册、音乐等媒体库
        request.allowScanningByMediaScanner();
        // 设置通知的显示类型，下载进行时和完成后显示通知
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        // 设置通知栏的标题，如果不设置，默认使用文件名
//        request.setTitle("This is title");
        // 设置通知栏的描述
//        request.setDescription("This is description");
        // 允许在计费流量下下载
        request.setAllowedOverMetered(false);
        // 允许该记录在下载管理界面可见
        request.setVisibleInDownloadsUi(false);
        // 允许漫游时下载
        request.setAllowedOverRoaming(true);
        // 允许下载的网路类型
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        // 设置下载文件保存的路径和文件名
        String fileName  = URLUtil.guessFileName(url, contentDisposition, mimeType);

        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
//        另外可选一下方法，自定义下载路径
//        request.setDestinationUri()
//        request.setDestinationInExternalFilesDir()
        final DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        // 添加一个下载任务
        long downloadId = downloadManager.enqueue(request);

    }
//复制数据库https://blog.csdn.net/wuhongqi0012/article/details/17222053
    public void addDesignDB(){
        if (TextUtils.isEmpty(design)) {
            Toast.makeText(webview.this, "请再重新操作一次", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(webview.this,design, Toast.LENGTH_SHORT).show();
            if(design.startsWith("这是个class")){
                mWebView.loadUrl("javascript:(function() { " + "var x=document.getElementsByClassName("+"'"+design.substring(8)+"'"+").length;\n" +
                        "for(var i=0;i<x;i++){\n" +
                        "document.getElementsByClassName("+"'"+design.substring(8)+"'"+")[i].style.display='none';\n" +
                        "}})()");
            }else if(design.startsWith("这是个id")){

                mWebView.loadUrl("javascript:(function() { " + "document.getElementById("+"'"+design.substring(5)+"'"+").style.display='none';})()");
            }
            selectad(getDomainName(mWebView.getUrl()),design);
        }
    }
    public void addDesign(){
        if (TextUtils.isEmpty(design)) {
            Toast.makeText(webview.this, "请再重新操作一次", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(webview.this,design, Toast.LENGTH_SHORT).show();
            if(design.startsWith("这是个class")){
                mWebView.loadUrl("javascript:(function() { " + "var x=document.getElementsByClassName("+"'"+design.substring(8)+"'"+").length;\n" +
                        "for(var i=0;i<x;i++){\n" +
                        "document.getElementsByClassName("+"'"+design.substring(8)+"'"+")[i].style.display='none';\n" +
                        "}})()");
            }else if(design.startsWith("这是个id")){

                mWebView.loadUrl("javascript:(function() { " + "document.getElementById("+"'"+design.substring(5)+"'"+").style.display='none';})()");
            }
        }
    }
    public void prefixquery() {
        dbHelper = new MyDatabaseHelper(this, "dmbrowser.db", null, 1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from addprefixDB", null);

        while (cursor.moveToNext()) {
            keyword = cursor.getString(2);
            prefix = cursor.getString(1);
            id = cursor.getInt(0);
            //找到id相等的就返回url
            if (mWebView.getUrl().contains(keyword)) {
                //mWebView.stopLoading();
                AlertDialog dialog;
                dialog = new AlertDialog.Builder(this).setTitle("浏览器打开提示")
                        .setIcon(R.drawable.iconyx)
                        .setMessage("已获取到该视频")
                        .setPositiveButton("播放", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                downloadByBrowser(prefix+mWebView.getUrl());
                            }
                        })
                        .setNegativeButton("取消",null)
                        .setNeutralButton("下载", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ////https://github.com/wizos/Sniffing

                            }
                        })
                        .create();
                dialog.show();

                break;
            }

        }
        cursor.close();
        db.close();

    }


    //查询数据库里的字段
    public int select(String url) {
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";
        //String sql = "select * from " + TABLENAME + " where name like '%"+ username + "%'";
        String sql ="SELECT * FROM " + TABLENAME + " WHERE url='"+url+"'";

        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        if(result.moveToFirst())//如果结果集的第一行有数据
        {
            //https://codingdict.com/questions/117721
            // Toast.makeText(WebView.this, "已存在书签中,名称为———"+result.getString(1), Toast.LENGTH_LONG).show();
            delete(result.getInt(0));
            result.close();
            db.close();
            return 1;
        }
        else {
            addinfo(mWebView.getTitle(),mWebView.getUrl());
            result.close();
            db.close();
            return 0;
        }
    }
    //查询数据库里的字段
    public void js_domain_name(String url) {
        int count=0;
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";
        //String sql = "select * from " + TABLENAME + " where name like '%"+ username + "%'";
        String sql ="SELECT * FROM jstoolDB WHERE url like '%"+ url + "%'";

        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        if (result != null && result.getCount() > 0){
            while (result.moveToNext())//moveToNext()与moveToFirst() https://www.cnblogs.com/zhaoleigege/p/5074963.html
            {
                count=count+1;
                String js=result.getString(3);
                String state=result.getString(4);
                if(state.equals("已启用")){
                    mWebView.loadUrl("javascript:"+js);
                }else if(state.equals("已停用")){

                }else {
                    result.close();
                    db.close();
                }
                if (count==result.getCount())
                    break;
            }
        }
        result.close();
        db.close();

    }
    public void js_all() {
        int count=0;
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";
        //String sql = "select * from " + TABLENAME + " where name like '%"+ username + "%'";
        String sql ="SELECT * FROM jstoolDB WHERE url='*'";
        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        //Toast.makeText(this,String.valueOf(result.getCount()), Toast.LENGTH_SHORT).show();
        if (result != null && result.getCount() > 0){
            while (result.moveToNext())//moveToNext()与moveToFirst() https://www.cnblogs.com/zhaoleigege/p/5074963.html
            {
                count=count+1;
                String js=result.getString(3);
                String state=result.getString(4);
                if(state.equals("已启用")){
                    mWebView.loadUrl("javascript:"+js);
                }else if(state.equals("已停用")){

                }else {
                    result.close();
                    db.close();
                }
                if (count==result.getCount())
                    break;


            }
        }

        result.close();
        db.close();
    }
    //查询数据库里的字段
    public void resourceselect(String url) {
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";
        //String sql = "select * from " + TABLENAME + " where name like '%"+ username + "%'";
        String sql ="SELECT * FROM resourceDB WHERE url='"+url+"'";

        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        if(result.moveToFirst())//如果结果集的第一行有数据
        {
            //https://codingdict.com/questions/117721
            // Toast.makeText(WebView.this, "已存在书签中,名称为———"+result.getString(1), Toast.LENGTH_LONG).show();
            result.close();
            db.close();
        }
        else {
            addresource(mWebView.getTitle(),url);
            result.close();
            db.close();
        }
    }
    //数据库添加数据
    public void addresource(String title,String url){
        //第二个参数是数据库名
        MyDatabaseHelper  dbHelper = new MyDatabaseHelper(webview.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("url", url);
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("resourceDB",null,values);
        //Toast.makeText(webview.this,"添加成功",Toast.LENGTH_SHORT).show();
        arrayList.clear();
    }
    //按值查找：
    public String query_by_id(int position){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from resourceDB", null);
        while (cursor.moveToNext()) {
            url = cursor.getString(2);
            titler=cursor.getString(1);
            id=cursor.getInt(0);
            //找到id相等的就返回url
            if (arrayList.get(position).equals(id))
                break;
        }
        cursor.close();
        db.close();
        return url;
    }
    //全部删除
    public void delete_all(){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from resourceDB", null);
        while (cursor.moveToNext()) {
            id=cursor.getInt(0);
            db.delete("resourceDB","id=?",new String[] {String.valueOf(id)});
        }
        cursor.close();
        db.close();


    }
    //数据库逆序查询函数：
    public void queryinfo(){
        if(share.getString("getvideo","自动嗅探视频【关闭】").equals("自动嗅探视频【开启】")){
            final ArrayList<HashMap<String, Object>> listItem = new ArrayList <HashMap<String,Object>>();/*在数组中存放数据*/
            //第二个参数是数据库名
            arrayList.clear();
            dbHelper = new MyDatabaseHelper(webview.this,"dmbrowser.db",null,1);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            //Cursor cursor = db.rawQuery("select * from bookmarkDB", null);
            //查询语句也可以这样写
            Cursor cursor = db.query("resourceDB", null, null, null, null, null, "id desc");
            if (cursor != null && cursor.getCount() > 0) {
                open_list.setVisibility(View.VISIBLE);
                if_exist=true;
                while(cursor.moveToNext()) {
                    get_id=cursor.getInt(0);//得到int型的自增变量
                    get_title = cursor.getString(1);
                    get_url = cursor.getString(2);
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map.put("title", get_title);
                    map.put("url", get_url);
                    arrayList.add(get_id);
                    listItem.add(map);
                    //new String  数据来源， new int 数据到哪去
                    SimpleAdapter mSimpleAdapter = new SimpleAdapter(webview.this,listItem,R.layout.bookmarkzujian,
                            new String[] {"title","url"},
                            new int[] {R.id.title,R.id.url});
                    lv.setAdapter(mSimpleAdapter);//为ListView绑定适配器
                }
            }
            else {
                open_list.setVisibility(View.GONE);
            }

            cursor.close();
            db.close();
        }

    }

    public int select1(String url) {
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";
        //String sql = "select * from " + TABLENAME + " where name like '%"+ username + "%'";
        String sql ="SELECT * FROM " + TABLENAME + " WHERE url='"+url+"'";

        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        if(result.moveToFirst())//如果结果集的第一行有数据
        {
            //https://codingdict.com/questions/117721
            // Toast.makeText(WebView.this, "已存在书签中,名称为———"+result.getString(1), Toast.LENGTH_LONG).show();

            result.close();
            db.close();
            return 1;
        }
        else {

            result.close();
            db.close();
            return 0;
        }

    }

    //
    public void delete(int id) {
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String sql = "delete from " + TABLENAME + " where id=" + id;
        db.execSQL(sql);
        db.close();
        Toast.makeText(this, "已取消书签", Toast.LENGTH_SHORT).show();
    }
    //数据库添加数据(书签表)
    public void addinfo(String title,String url){
        //第二个参数是数据库名
        MyDatabaseHelper  dbHelper = new MyDatabaseHelper(webview.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("url", url);
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("bookmarkDB",null,values);
        Toast.makeText(webview.this,"添加成功",Toast.LENGTH_SHORT).show();
    }
    //数据库添加数据(历史表)
    public void history(String title,String url){
        //第二个参数是数据库名
        MyDatabaseHelper  dbHelper = new MyDatabaseHelper(webview.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("url", url);
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("historyDB",null,values);

    }
    //剪贴板
    public void cv(String title){
        //第二个参数是数据库名
        MyDatabaseHelper  dbHelper = new MyDatabaseHelper(webview.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("copy", title);
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("cvDB",null,values);
        Toast.makeText(webview.this, "已复制到本软件剪贴板", Toast.LENGTH_SHORT).show();
    }
    //
//    public String adselect(String url) {
//        dbHelper = new MyDatabaseHelper(this,"yunxibhxd.db",null,1);
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";
//        //String sql = "select * from " + TABLENAME + " where name like '%"+ username + "%'";
//        String sql ="SELECT * FROM " + "adDB" + " WHERE url='"+url+"'";
//
//        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
//        if(result.moveToFirst())//如果结果集的第一行有数据
//        {
//            //https://codingdict.com/questions/117721
//            // Toast.makeText(WebView.this, "已存在书签中,名称为———"+result.getString(1), Toast.LENGTH_LONG).show();
//            String host=result.getString(2);
//            result.close();
//            db.close();
//            return host;
//        }
//        else {
//
//            result.close();
//            db.close();
//            return "fgvhjbjjdgcfg";
//        }
//    }

    //获取域名
    //https://blog.csdn.net/ziele_008/article/details/106275703
    public static String getDomainName(String url) {
        String host = "";
        try {
            URL Url = new URL(url);
            host = Url.getHost();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return host;
    }


    @Override
    public void onBackPressed() {
         if (mCustomView != null) {//https://www.jianshu.com/p/b9de6714482c,监听全屏

            }else if (mWebView.canGoBack())
            {
                mWebView.goBack(); //goBack()表示返回WebView的上一页面
            }else if(!mWebView.canGoBack())
            {
                if((System.currentTimeMillis()-exitTime) > 2000){
                    Toast.makeText(getApplicationContext(), "再按一次退出", Toast.LENGTH_SHORT).show();
                    exitTime = System.currentTimeMillis();
                } else {
                    finish();
                }
            }

    }

    //https://www.cnblogs.com/HDK2016/p/8695052.html
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_BACK) ) {

            if (mCustomView != null) {//https://www.jianshu.com/p/b9de6714482c,监听全屏

            }else if (mWebView.canGoBack())
            {
                mWebView.goBack(); //goBack()表示返回WebView的上一页面
            }else if(!mWebView.canGoBack())
            {
                if((System.currentTimeMillis()-exitTime) > 2000){
                    Toast.makeText(getApplicationContext(), "再按一次退出", Toast.LENGTH_SHORT).show();
                    exitTime = System.currentTimeMillis();
                } else {
                    finish();
                }
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        String searchhistory = share.getString("searchurl", "https://www.baidu.com/s?wd=");

        if (requestCode==1){
            if(resultCode==1){
//接收书签返回url
                String  dataStringExtra2= data.getStringExtra("2_data_return");
                mWebView.loadUrl(dataStringExtra2);
            }
        } else if (requestCode == REQUEST_CODE_CHOOSE) {
            //图片选择后返回图标，通过uploadMessage将图片传给网页
            if (uploadMessage != null) {
                onActivityResultAboveL(resultCode, data);
            }
        }      else  if (requestCode==2){
            if(resultCode==2){
                if(searchhistory.equals("https://www.baidu.com/s?wd=")){
                    webview.this.home_search.setImageResource(R.drawable.baidu);

                }else if(searchhistory.equals("https://www.sogou.com/web?query=")){
                    webview.this.home_search.setImageResource(R.drawable.sougou);

                }else if(searchhistory.equals("http://www.google.com.tw/search?q=")){
                    webview.this.home_search.setImageResource(R.drawable.chrome);

                }else if(searchhistory.equals("http://cn.bing.com/search?q=")){
                    webview.this.home_search.setImageResource(R.drawable.bing);

                }else if(searchhistory.equals("https://quark.sm.cn/s?q=")){
                    webview.this.home_search.setImageResource(R.drawable.quak);
                }else {
                    webview.this.home_search.setImageResource(R.drawable.iconyx);

                }
//                surl=data.getStringExtra("burl");
//
//                //设置搜索图标
//                if(surl.equals("https://www.baidu.com/s?wd=")){
//                    Main4Activity.this.home_search.setImageResource(R.drawable.baidu);
//                    edit.putString("yh","百度");
//                    edit.commit();
//                }else if (surl.equals("https://www.sogou.com/web?query=")){
//                    Main4Activity.this.home_search.setImageResource(R.drawable.sougou);
//
//                    edit.putString("yh","搜狗");
//                    edit.commit();
//                }else if (surl.equals("https://www.google.com/search?q=")){
//                    Main4Activity.this.home_search.setImageResource(R.drawable.chrome);
//
//                    edit.putString("yh","谷歌");
//                    edit.commit();
//                }else  if(surl.equals("http://cn.bing.com/search?q=")){
//                    Main4Activity.this.home_search.setImageResource(R.drawable.bing);
//                    Intent intent = new Intent();
//                    intent.putExtra("burl","http://cn.bing.com/search?q=");
//                    setResult(3,intent);
//                }
            }
        }    else  if (requestCode==3){
            if(resultCode==3){
                mWebView.reload();
            }
        }
        // 扫描二维码/条码回传 https://www.cnblogs.com/xch-yang/p/9418493.html
        if (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                String content = data.getStringExtra(DECODED_CONTENT_KEY);
                Intent it = new Intent(webview.this, webview.class);
                //传递搜索框内容
                it.putExtra("searchText",content);
//获取搜素引擎返回的数据再传递
                startActivity(it);
                //返回的BitMap图像
                Bitmap bitmap = data.getParcelableExtra(DECODED_BITMAP_KEY);
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            //https://www.cnblogs.com/xch-yang/p/9418493.html
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //扫码
                    goScan();
                } else {
                    Toast.makeText(this, "你拒绝了权限申请，无法打开相机扫码哟！", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
        }
    }
    //https://www.jianshu.com/p/ec23fe80d5a3
    //屏幕旋转监听
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        switch (config.orientation) {
            //竖屏方向
            case Configuration.ORIENTATION_LANDSCAPE:
                //设置全屏
                buttom.setVisibility(View.GONE);
                view_top.setVisibility(View.GONE);
                top.setBackgroundColor(Color.rgb(0,0,0));
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                //https://blog.csdn.net/weixin_45882303/article/details/121155068
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//// 隐藏状态栏

                break;
            //横屏方向
            case Configuration.ORIENTATION_PORTRAIT:
                //关闭全屏
                buttom.setVisibility(View.VISIBLE);
                view_top.setVisibility(View.VISIBLE);
                if(share.getString("night", "夜间模式关闭").equals("夜间模式关闭")){
                    top.setBackgroundColor(Color.rgb(0,0,0));
                }else {
                    top.setBackgroundColor(Color.rgb(192,192,192));
                }
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);// 显示状态栏
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

                //https://www.cnblogs.com/classloader/p/5304986.html
                getWindow().setStatusBarColor(getResources().getColor(R.color.white));//设置状态栏颜色
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏图标和文字颜色为暗色

                break;
        }
    }
    //WebView文件上传 https://www.jianshu.com/p/2ad472027410
    //处理网页回调
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void onActivityResultAboveL(int resultCode, Intent intent) {
        Uri[] results = null;
        if (resultCode == Activity.RESULT_OK) {
            if (intent != null) {
                String dataString = intent.getDataString();
                ClipData clipData = intent.getClipData();
                if (clipData != null) {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                if (dataString != null)
                    results = new Uri[]{Uri.parse(dataString)};
            }
        }
        uploadMessage.onReceiveValue(results);
        uploadMessage = null;
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mWebView != null) {
            mWebView.onPause();
            mWebView.pauseTimers();
        }

    }

    //在onStop里面设置setJavaScriptEnabled(false);
    @Override
    protected void onStop() {
        super.onStop();
        if (mWebView != null) {
            mWebView.getSettings().setJavaScriptEnabled(false);//Android后台无法释放js导致发热耗电
        }
    }
    //在onResume里面设置setJavaScriptEnabled(true)。出处：https://zhuanlan.zhihu.com/p/190219649

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onResume() {
        super.onResume();
        if (mWebView != null) {
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.resumeTimers();
            mWebView.onResume();
        }



    }


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onRestart() {
        super.onRestart();
        if (mWebView != null) {
            mWebView.getSettings().setJavaScriptEnabled(true);
        }

    }
    //WebView音频播放销毁后还有声音 出处：https://zhuanlan.zhihu.com/p/190219649
    @Override
    protected void onDestroy() {
        try {
            //有音频播放的web页面的销毁逻辑
            //在关闭了Activity时，如果Webview的音乐或视频，还在播放。就必须销毁Webview
            //但是注意：webview调用destory时,webview仍绑定在Activity上
            //这是由于自定义webview构建时传入了该Activity的context对象
            //因此需要先从父容器中移除webview,然后再销毁webview:
            if (mWebView != null) {
                ViewGroup parent = (ViewGroup) mWebView.getParent();
                if (parent != null) {
                    parent.removeView(mWebView);
                }
                mWebView.removeAllViews();


                mWebView.destroy();
                mWebView = null;
            }
        } catch (Exception e) {

        }
        // //https://www.jianshu.com/p/f61849073bad
        unregisterReceiver(networkChangeReceiver);




        super.onDestroy();
    }
    class NetworkChangeReceiver extends BroadcastReceiver {

        //https://www.jianshu.com/p/f61849073bad
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectionManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable()) {
                switch (networkInfo.getType()) {
                    case 0:
                        Toast.makeText(context, "正在使用移动网络，请注意流量哦！", Toast.LENGTH_SHORT).show();
                        errorvs=false;
                        mWebView.reload();

                        break;
                    case 1:
                        Toast.makeText(context, "正在使用wifi上网", Toast.LENGTH_SHORT).show();
                        errorvs=false;
                        mWebView.reload();
                        break;
                    default:
                        break;
                }
            } else {
                Toast.makeText(context, "当前无网络连接", Toast.LENGTH_SHORT).show();
                errorvs=false;
                error_reload.setText("无网络连接");

            }
        }
    }
    //监听编辑框--https://www.jb51.net/article/135392.htm
    //https://blog.csdn.net/ycwol/article/details/46594275
    class EditChangedListener implements TextWatcher {
        public void afterTextChanged(Editable arg0) {

// 文字改变后出发事件
            String content = txtTitle.getText().toString();
            //若输入框内容不为空按钮可点击，字体为蓝色
            if (!content.isEmpty()) {
                //.setEnabled .setClickable---https://blog.csdn.net/qq_22637473/article/details/54863252
                search_bt.setClickable(true);
                search_bt.setEnabled(true);
                search_bt.setBackgroundResource(R.drawable.cansearch);
            } else {
                search_bt.setClickable(true);
                search_bt.setEnabled(true);
                search_bt.setBackgroundResource(R.drawable.searchbtclick);
            }

        }
        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
// TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
// TODO Auto-generated method stub

        }
    };


    @Override
    public void onSniffingStart(View webView, String url) {
        dialog.show();
    }

    @Override
    public void onSniffingFinish(View webView, String url) {
        dialog.dismiss();
    }

    @Override
    public void onSniffingSuccess(View webView, String url, List<SniffingVideo> videos) {
        //方法SniffingVideo.class里修改
        if (videos.toString().contains("[")&&videos.toString().contains("]")){
            //截取字符串
            String str =videos.toString().substring(1,videos.toString().length()-1);
            Intent intent= new Intent();
            intent.setData(Uri.parse(str));
            intent.setAction(Intent.ACTION_VIEW);
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setIcon(R.drawable.iconyx)
                    .setTitle("嗅探到视频资源啦")//设置对话框的标题
                    //设置对话框的按钮
                    .setMessage("打开")
                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("外置浏览器打开", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    }).create();
            dialog.show();

        }


    }
    @Override
    public void onSniffingError(View webView, String url, int errorCode) {
        Toast.makeText(this,"解析失败...",Toast.LENGTH_SHORT).show();
    }

    final class JS{//https://blog.csdn.net/meijuanyou/article/details/54973556
        @JavascriptInterface
        public void get(String p){
            design=p;
        }
        @JavascriptInterface
        public void showSource(String html) {
            Log.e("html源码打印", html);
            htmlData = html;
        }
    }


    public void showNetPageData() {
        Intent intent = new Intent(this, PicView.class);
        if (TextUtils.isEmpty(htmlData)) {
            Toast.makeText(this, "数据获取失败,请退出或者刷新页面重试！", Toast.LENGTH_SHORT).show();
            return;
        }
        Log.e("html源码打印",htmlData);
       intent.putExtra("htmlData", htmlData);
       startActivity(intent);
    }
    public void showNetText() {
        if (TextUtils.isEmpty(htmlData)) {
            Toast.makeText(this, "数据获取失败,请退出或者刷新页面重试！", Toast.LENGTH_SHORT).show();
            return;
        }
        Document document = Jsoup.parse(htmlData);
        String textCode=document.body().text();
        codeView.setText(textCode);
        txt_app.setVisibility(View.VISIBLE);
    }
}

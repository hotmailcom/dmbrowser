package com.example.browser;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;

//https://blog.csdn.net/true100/article/details/79499337
public class CustomSQLTools {
    //数据库存储路径
    private String filePath= Environment.getExternalStorageDirectory().getAbsolutePath() + "/dmbrowser/dmbrowser.db";
    private SQLiteDatabase database;

    public SQLiteDatabase openDatabase(Context context) {
        File jhPath = new File(filePath);
        //查看数据库文件是否存在
        if (jhPath.exists()) {
            //存在则直接返回打开的数据库
            return SQLiteDatabase.openOrCreateDatabase(jhPath, null);
        } else {
            Toast.makeText(context, "数据库文件不存在", Toast.LENGTH_SHORT).show();
            return null;
        }
    }
}
package com.example.browser;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dhs on 2018/7/27.
 */

public class jstool extends Activity {
    private ListView lv;
    private MyDatabaseHelper dbHelper;


    private static final String TABLENAME = "jstoolDB";
    private static final String FILENAME="YX";
    private String get_title,get_url,get_state;//暂存从数据库得到的title和url
    private int get_id; //暂存从数据库得到的id
    private String titler, url,js;//按值查找详细信息
    int id; //按值查找id
    private TextView text_title, text_url,text_state,text_js;//书签详细信息edittext
    private ArrayList arrayList=new ArrayList();

    private boolean if_exist;

    private Button addjs;
    private Button res;

    SharedPreferences share;

    private EditText search_jstool;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jstool);


        lv=(ListView)findViewById(R.id.bookmark_list);
        addjs=(Button)findViewById(R.id.addjs);
        res=(Button)findViewById(R.id.res);
        search_jstool=(EditText)findViewById(R.id.search_bookmark) ;
        //监听编辑框
        search_jstool.addTextChangedListener(new EditChangedListener());
        queryinfo();
        onclick();

        share=getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
        if(share.getString("jstool", "首次打开").equals("首次打开")){
            delete_all();
            res();

        }


        AlertDialog dialog = new AlertDialog.Builder(this)
                .setIcon(R.drawable.iconyx)
                .setTitle("操作提示")//设置对话框的标题
                //设置对话框的按钮
                .setMessage("第一：该页面直接作用在网页上，启用代表网页会执行这个功能，停用代表不执行；默认执行视频播放器，可以自己选择停用\n" +
                        "第二:里面所有脚本都可以修改，有脚本名称、脚本生效域名、脚本内容三部分，其中脚本名称你可以随意修改，域名部分呢，就是遇到到网页域名在其中的才会执行。例如https://www.baidu.com,它的域名是www.baidu.com,你只想在百度执行这个功能呢，你就可以修改域名部分为www.baidu.com,域名部分可以写多个域名进去进去。域名部分填写*号代表所有网页都会执行该脚本。\n" +
                        "第三：点击右上角即可初始化，可以在脚本错误修改时但是不知道怎么修复时使用")
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        dialog.show();
    }

    //数据库添加数据(书签表)
    public void ydms(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "阅读模式，启用之后网页会右下角出现按钮");
        values.put("url", "*");
        values.put("js","if(document.getElementById(\"cutsread\"));else{var e=function(){for(var e=new Array(\"div\",\"span\",\"p\",\"li\",\"em\",\"ul\",\"ol\"),t=0;t<7;t++){var n=document.getElementsByTagName(e[t]);if(n&&n.length>0)for(var t=0;t<n.length;t++){var a=n[t],i=a.innerText;if(i){try{i=i.replace(/\\r\\n/g,\"\"),i=i.replace(/\\n/g,\"\")}catch(e){}var l=i.length;l>0&&l<50&&(a.style.display=\"none\")}}}if((n=document.getElementsByTagName(\"img\"))&&n.length>0)for(var t=0;t<n.length;t++)n[t].style.display=\"none\";if((n=document.getElementsByTagName(\"input\"))&&n.length>0)for(var t=0;t<n.length;t++)n[t].style.display=\"none\";if((n=document.getElementsByTagName(\"a\"))&&n.length>0)for(var t=0;t<n.length;t++)n[t].style.display=\"none\";for(var e=new Array(\"div\",\"img\",\"span\",\"p\",\"a\",\"li\"),t=0;t<6;t++){var r=document.getElementsByTagName(e[t]);if(r&&r.length>0)for(var s=0;s<r.length;s++){var g=r[s],o=g.getAttribute(\"id\"),d=g.getAttribute(\"class\");d&&\"A\"==g.tagName&&-1!=d.indexOf(\" \")&&g.getAttribute(\"onclick\")&&g.getAttribute(\"target\")&&g.href&&(g.style.display=\"none\"),o&&d&&o==d&&d.length>10&&\"LI\"==g.tagName&&(g.style.display=\"none\"),o&&(o.length>30&&-1==o.indexOf(\"-\")&&(g.style.display=\"none\"),/^[0-9]*$/.test(o)&&o.length>1&&(g.style.display=\"none\"),o.length>7&&/[0-9]/.test(o)&&/[a-z]/i.test(o)&&g.getAttribute(\"style\")&&(g.style.display=\"none\")),o&&d&&(-1!=d.indexOf(\" \")&&-1!=d.indexOf(o)&&g.getAttribute(\"style\")&&(g.style.display=\"none\"),o==d&&/[0-9]/.test(o)&&/[a-z]/i.test(o)&&(g.style.display=\"none\"))}}var y=document.getElementsByTagName(\"iframe\");if(y&&y.length>0)for(var t=0;t<y.length;t++){var c=y[t],d=c.getAttribute(\"height\")+\"\";if(c.getAttribute(\"height\")&&-1==d.indexOf(\"%\")){var f=parseInt(c.height);f>0&&f<100&&(c.style.display=\"none\")}}};c2=\"\";var t=document.createElement(\"span\");t.id=\"cutsread\",t.style.cssText=\"display:block!important;text-align:center;color:#fff;font-size:4vw;opacity:0.7;background:#6a6a6a;cursor:pointer;position:fixed;left:90%;top:90%;width:5vw;height:7vw;right:0px;z-index:9999\";t.innerHTML=unescape(\"阅\".replace(/\\\\u/g,\"%u\")),t.addEventListener(\"click\",function(){t.style.display=\"none\";try{e()}catch(e){}},!1),document.body.appendChild(t)}");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
        Toast.makeText(jstool.this,"添加成功",Toast.LENGTH_SHORT).show();
        queryinfo();

    }
    //数据库添加数据(书签表)
    public void shuan(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "阅读模式-双按钮，网页会右下角出现按钮");
        values.put("url", "*");
        values.put("js","var totn=document.createElement(\"div\");totn.innerHTML=\"﹀\";totn.setAttribute(\"style\",\"font-size:3vw !important;width:13vw !important;height:13vw !important;line-height:13vw !important;text-align:center !important;background-color:rgba(0,0,0,0) !important;box-shadow:0px 0px 1px rgba(0,0,0,1) !important;position:fixed !important;bottom:39vh !important;right:5vw !important;z-index:99999 !important;border-radius:100% !important;\");totn.onclick=function (){window.scrollBy(0,500);};document.getElementsByTagName(\"html\").item(0).appendChild(totn);var totna=document.createElement(\"div\");totna.innerHTML=\"︿\";totna.setAttribute(\"style\",\"font-size:3vw !important;width:13vw !important;height:13vw !important;line-height:13vw !important;text-align:center !important;background-color:rgba(0,0,0,0) !important;box-shadow:0px 0px 1px rgba(0,0,0,1) !important;position:fixed !important;bottom:50vh !important;right:5vw !important;z-index:99999 !important;border-radius:100% !important;\");totna.onclick=function (){window.scrollBy(0,-500);};document.getElementsByTagName(\"html\").item(0).appendChild(totna);");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void dan(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "阅读模式-单按钮，网页会右下角出现按钮");
        values.put("url", "*");
        values.put("js","var totn=document.createElement(\"div\");totn.innerHTML=\"﹀\";totn.setAttribute(\"style\",\"font-size:3vw !important;width:13vw !important;height:13vw !important;line-height:13vw !important;text-align:center !important;background-color:rgba(0,0,0,0) !important;box-shadow:0px 0px 1px rgba(0,0,0,1) !important;position:fixed !important;bottom:50vh !important;right:5vw !important;z-index:99999 !important;border-radius:100% !important;\");totn.onclick=function (){window.scrollBy(0,500);};document.getElementsByTagName(\"html\").item(0).appendChild(totn);");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void topw(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "回到顶部，网页会出现一键回到顶部");
        values.put("url", "*");
        values.put("js","if(document.getElementById('toTopLikeKuAn')){\n" +
                "\n" +
                "}else{\n" +
                "function toTopLikeKuAn() {\n" +
                "  var toTopBtn = document.createElement(\"div\");\n" +
                "  toTopBtn.id = \"toTopBtn\";\n" +
                "  toTopBtn.setAttribute(\"style\",\"font-size:4vw !important;width:10vw !important;height:10vw !important;line-height:10vw !important;text-align:center !important;background:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAQAAABIkb+zAAABD0lEQVR42u3bMRLCMAwFUbz3v7MpKKACkvhLzsyqcCntO4DHfNx7eAgQIECAAAECBAgQIECAAAECBAgQIOBzxhzzxoBXfJ5AMr+CQDY/TyCdnyaQz88SqMhPEqjJzxGoyk8RyOXPMUeeQC7//SYJJPMrCGTz8wTS+WkC+fwsgYr8JIGa/ByBqvwUgbr8DIHK/ASB2vz1BKrzVxOoz19LoCN/JYGe/HUEuvJXEejLX0OgM38Fgd786wS6868S6M+/RmCH/CsE9sg/T2CX/LME9sk/R2Cn/DME/llSl3/8Or+X1OYfvc6vJfX5x67zfUlP/pHrwx8cAgQIECBAgAABAgQIECBAgAABAgQIECCgfJ7DZZC9LC8fDAAAAABJRU5ErkJggg==') no-repeat center center !important;background-size:3vw 3vw !important;background-color:rgba(250,250,250,0.9) !important;box-shadow:0px 1px 1px rgba(0,0,0,0.4);color:#000 !important;position:fixed !important;bottom:5vh !important;right:45vw !important;z-index:99999999999999999 !important;border-radius:100% !important;display:none;\");\n" +
                "  document.body.appendChild(toTopBtn);\n" +
                "};\n" +
                "function isScrollToTop() {\n" +
                " var toTopTimer;\n" +
                " var theBody = document.getElementsByTagName('body')[0];\n" +
                " var topTopBtn = document.getElementById('toTopBtn');\n" +
                " document.ontouchstart = function (e) {\n" +
                "  if (toTopTimer) {\n" +
                "   clearTimeout(toTopTimer);\n" +
                "  };\n" +
                "  mystartY = e.changedTouches[0].clientY;\n" +
                " };\n" +
                " document.ontouchmove = function (e) {\n" +
                "  myendY = e.changedTouches[0].clientY;\n" +
                "  var myY = myendY - mystartY;\n" +
                "  if (myY > 0){\n" +
                "   toTopBtn.style.opacity = \"1\";\n" +
                "   toTopBtn.style.display = \"block\";\n" +
                "   toTopBtn.style.transform = \"rotateZ(0deg)\";\n" +
                "   toTopBtn.style.boxShadow = \"0px 1px 1px rgba(0,0,0,0.4)\";\n" +
                "   toTopBtn.onclick = function () {\n" +
                "    window.scrollTo(0,0);\n" +
                "    this.style.display = \"none\";\n" +
                "    toTopBtn.removeEventListener('click',this,false);\n" +
                "   };\n" +
                "  } else if (myY < 0) {\n" +
                "   toTopBtn.style.opacity = \"1\";\n" +
                "   toTopBtn.style.display = \"block\";\n" +
                "   toTopBtn.style.transform = \"rotateZ(180deg)\";\n" +
                "   toTopBtn.style.boxShadow = \"0px -1px 1px rgba(0,0,0,0.4)\";\n" +
                "   toTopBtn.onclick = function () {\n" +
                "    window.scrollTo(0, document.body.scrollHeight); \n" +
                "    this.style.display = \"none\";\n" +
                "    toTopBtn.removeEventListener('click',this,false);\n" +
                "   };\n" +
                "  }else {\n" +
                "   toTopBtn.style.display = \"none\";\n" +
                "  };\n" +
                " };\n" +
                " document.ontouchend = function (e) {\n" +
                "  toTopTimer = setTimeout(function () {\n" +
                "   toTopBtn.style.transitionProperty=\"opacity,background-color\";\n" +
                "   toTopBtn.style.transitionDuration=\"500ms\";\n" +
                "   toTopBtn.style.transitionTimingFunction = \"linear\";\n" +
                "   toTopBtn.style.opacity = \"0\";\n" +
                "   toTopBtn.style.backgroundColor = \"rgba(200,200,200,1)\";\n" +
                "   setTimeout(function() {\n" +
                "    toTopBtn.style.display = \"none\";\n" +
                "    toTopBtn.style.backgroundColor = \"rgba(250,250,250,0.9)\";\n" +
                "   },500);\n" +
                "  },3000);\n" +
                " };\n" +
                "};\n" +
                "var isHaveToTopBtn;\n" +
                "isHaveToTopBtn = document.getElementById('toTopBtn');\n" +
                "if (!isHaveToTopBtn) {\n" +
                " toTopLikeKuAn();\n" +
                " isScrollToTop();\n" +
                "};\n" +
                "\n" +
                "var pans= document.createElement('b');  \n" +
                "pans.id='toTopLikeKuAn';\n" +
                "document.body.appendChild(pans);\n" +
                "\n" +
                "}\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void jcfz(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "解除网页复制限制，例如百度文库这些不能复制文字等");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 网页复制限制解除\n" +
                " * @Author: 油猴 Cat73\n" +
                " * @version: 1.0\n" +
                " * @description: 通杀大部分网站，可以解除禁止复制、剪切、选择文本的限制。\n" +
                " * @include: *\n" +
                " * @createTime: 2019-10-04 01:47:08\n" +
                " * @updateTime: 2019-10-09 14:10:01\n" +
                " */\n" +
                "(function () {\n" +
                "  /* 判断是否该执行 */\n" +
                "  const key = encodeURIComponent('Cat73:网页限制解除:执行判断');\n" +
                "  if (window[key]) {\n" +
                "    return;\n" +
                "  };\n" +
                "  window[key] = true;\n" +
                "\n" +
                "  /* 开始执行代码 */\n" +
                "  const script = document.createElement('script');\n" +
                "  script.setAttribute('src', 'https://greasyfork.org/scripts/14146-%E7%BD%91%E9%A1%B5%E9%99%90%E5%88%B6%E8%A7%A3%E9%99%A4/code/%E7%BD%91%E9%A1%B5%E9%99%90%E5%88%B6%E8%A7%A3%E9%99%A4.user.js');\n" +
                "  document.body.append(script);\n" +
                "})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void zdpj(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "自动拼接下一页，百度等搜索之后不用点击下一页浏览");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 自动拼接下一页\n" +
                " * @Author: 谷花泰\n" +
                " * @version: 1.0\n" +
                " * @description: 已适配百度、谷歌、搜狗、神马、必应\n" +
                " * @include: *\n" +
                " * @createTime: 2019-11-16 23:10:18\n" +
                " * @updateTime: 2019-11-17 07:33:04\n" +
                " */\n" +
                "(function () {\n" +
                "  /* 执行判断 */\n" +
                "  const key = encodeURIComponent('谷花泰:自动拼接下一页:执行判断');\n" +
                "  if (window[key]) {\n" +
                "    return;\n" +
                "  };\n" +
                "  window[key] = true;\n" +
                "\n" +
                "  class AutoNextPage {\n" +
                "    /**\n" +
                "     * 初始化配置\n" +
                "     */\n" +
                "    constructor(_configs) {\n" +
                "      const that = this;\n" +
                "      this.configs = _configs;\n" +
                "      /* 最终配置  */\n" +
                "      this.config = {};\n" +
                "\n" +
                "      /* 是否执行代码 */\n" +
                "      const canLoad = this.configs.some(config => {\n" +
                "        if (that.siteInList(config.sites)) {\n" +
                "          that.config = config;\n" +
                "        };\n" +
                "        return that.siteInList(config.sites);\n" +
                "      });\n" +
                "      if (!canLoad) {\n" +
                "        return;\n" +
                "      };\n" +
                "\n" +
                "      /* 添加样式 */\n" +
                "      this.addStyle(this.config.style || '');\n" +
                "\n" +
                "      /* 保存获取链接的方法 */\n" +
                "      this._getNextPageUrl = this.config.getNextPageUrl || this.getNextPageUrl;\n" +
                "\n" +
                "      /* 获取下一页方法 */\n" +
                "      this._getNextPage = this.config.getNextPage || null;\n" +
                "\n" +
                "      /* 请求状态判断 */\n" +
                "      this.isRequesting = false;\n" +
                "\n" +
                "      /* 上一次的文档高度 */\n" +
                "      this.lastScrollHeight = top.document.documentElement.scrollHeight;\n" +
                "      this.init();\n" +
                "    };\n" +
                "    /**\n" +
                "     * 监听滚动\n" +
                "     */\n" +
                "    init() {\n" +
                "      const html = top.document.documentElement;\n" +
                "\n" +
                "      /* 刚进来直接加载下一页 */\n" +
                "      this.loadNextPage();\n" +
                "      window.addEventListener('scroll', this.debounce(() => {\n" +
                "        if (html.scrollTop >= this.lastScrollHeight) {\n" +
                "          this.loadNextPage();\n" +
                "        };\n" +
                "      }, 10));\n" +
                "    };\n" +
                "    /**\n" +
                "     * 判断网站是否在名单内，支持正则\n" +
                "     */\n" +
                "    siteInList(sites) {\n" +
                "      const hostname = window.location.hostname;\n" +
                "      const result = sites.some(site => {\n" +
                "        if (hostname.match(site)) {\n" +
                "          return true;\n" +
                "        }\n" +
                "        return false;\n" +
                "      });\n" +
                "      return result;\n" +
                "    };\n" +
                "    /**\n" +
                "     * 默认获取下一页链接的方法\n" +
                "     */\n" +
                "    getNextPageUrl() {\n" +
                "      const nextPageNodes = document.querySelectorAll('.se-page-controller a');\n" +
                "      const nextPageNode = nextPageNodes[nextPageNodes.length - 1];\n" +
                "      return nextPageNode && nextPageNode.getAttribute('href') || '';\n" +
                "    };\n" +
                "    /**\n" +
                "     * 根据 url 获取已渲染的 dom 的 html 代码\n" +
                "     */\n" +
                "    getFrameHtml(url) {\n" +
                "      return new Promise((resolve, reject) => {\n" +
                "        const iframe = top.document.createElement('iframe');\n" +
                "        iframe.src = url;\n" +
                "        iframe.setAttribute('style', `\n" +
                "          position:fixed; \n" +
                "          width:0; \n" +
                "          height:0; \n" +
                "          overflow:hidden; \n" +
                "          opacity:0;\n" +
                "        `);\n" +
                "        top.document.body.appendChild(iframe);\n" +
                "        const framesLength = top.frames.length;\n" +
                "        iframe.onload = () => {\n" +
                "          const lastWindow = top.frames[framesLength - 1] || top;\n" +
                "          const html = lastWindow.document.body.innerHTML;\n" +
                "          resolve(html);\n" +
                "          this.isRequesting = false;\n" +
                "          top.document.body.removeChild(iframe);\n" +
                "        };\n" +
                "      });\n" +
                "    };\n" +
                "    /**\n" +
                "     * 添加样式\n" +
                "     */\n" +
                "    addStyle(css) {\n" +
                "      const style = document.querySelector('style') || document.createElement('style');\n" +
                "      style.innerHTML += css;\n" +
                "    };\n" +
                "    /**\n" +
                "     * 防抖函数\n" +
                "     */\n" +
                "    debounce(fn, delay) {\n" +
                "      let timer = null;\n" +
                "      return function () {\n" +
                "        clearTimeout(timer);\n" +
                "        timer = setTimeout(() => {\n" +
                "          fn.apply(this, arguments);\n" +
                "        }, delay);\n" +
                "      };\n" +
                "    };\n" +
                "    /**\n" +
                "     * 加载下一页\n" +
                "     */\n" +
                "    loadNextPage() {\n" +
                "      /* 点击类 */\n" +
                "      if (this._getNextPage) {\n" +
                "        this.lastScrollHeight = top.document.documentElement.scrollHeight;\n" +
                "        this._getNextPage();\n" +
                "        console.log('点击类：加载下一页');\n" +
                "        return;\n" +
                "      };\n" +
                "\n" +
                "      /* 链接类 */\n" +
                "      const nextPageUrl = this._getNextPageUrl();\n" +
                "      if (this.isRequesting) {\n" +
                "        return;\n" +
                "      };\n" +
                "      console.log('链接类：加载下一页', nextPageUrl);\n" +
                "      this.isRequesting = true;\n" +
                "      this.getFrameHtml(nextPageUrl).then(html => {\n" +
                "        this.lastScrollHeight = top.document.documentElement.scrollHeight;\n" +
                "        top.document.body.innerHTML += `\n" +
                "          <div class=\"via-next-page\">\n" +
                "            ${html}\n" +
                "          </div>\n" +
                "        `;\n" +
                "      });\n" +
                "    };\n" +
                "  };\n" +
                "\n" +
                "  try {\n" +
                "    const configs = [\n" +
                "      {\n" +
                "        sites: ['m.baidu.com', 'www.baidu.com'],\n" +
                "        style: `\n" +
                "          .se-page-ft,\n" +
                "          .se-page-copyright.se-copyright-zbios {\n" +
                "            display: none !important;\n" +
                "          }\n" +
                "        `,\n" +
                "        getNextPageUrl() {\n" +
                "          const nextPageNodes = document.querySelectorAll('.se-page-controller a');\n" +
                "          const nextPageNode = nextPageNodes[nextPageNodes.length - 1];\n" +
                "          let nextPageLink = nextPageNode && nextPageNode.getAttribute('href') || '';\n" +
                "          let reg = /(https*?:\\/\\/\\w+\\.\\w+?\\.com)([\\w\\W]*)/i;\n" +
                "          nextPageLink = nextPageLink.replace(reg, (res, $1, $2) => {\n" +
                "            return top.location.origin + $2;\n" +
                "          });\n" +
                "          return nextPageLink;\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        sites: ['www.google.com'],\n" +
                "        style: `\n" +
                "          #sfooter {\n" +
                "            display: none !important;\n" +
                "          }\n" +
                "        `,\n" +
                "        getNextPageUrl() {\n" +
                "          const moreResultNodes = document.querySelectorAll('a[aria-label=\"更多结果\"]');\n" +
                "          const moreResultNode = moreResultNodes[moreResultNodes.length - 1];\n" +
                "          const moreResultLink = moreResultNode && moreResultNode.getAttribute('href') || '';\n" +
                "\n" +
                "          const nextPageNodes = document.querySelectorAll('a[aria-label=\"下一页\"]');\n" +
                "          const nextPageNode = nextPageNodes[nextPageNodes.length - 1];\n" +
                "          const nextPageLink = nextPageNode && nextPageNode.getAttribute('href') || '';\n" +
                "\n" +
                "          return nextPageLink || moreResultLink;\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        sites: ['m.sogou.com', 'wap.sogou.com'],\n" +
                "        style: ``,\n" +
                "        getNextPage() {\n" +
                "          const nextPageNodes = document.querySelectorAll('a.nextpage');\n" +
                "          const nextPageNode = nextPageNodes[nextPageNodes.length - 1];\n" +
                "          nextPageNode && nextPageNode.click();\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        sites: ['m.sm.cn', 'quark.sm.cn'],\n" +
                "        style: ``,\n" +
                "        getNextPage() {\n" +
                "          const nextPageNodes = document.querySelectorAll('span.p-next');\n" +
                "          const nextPageNode = nextPageNodes[nextPageNodes.length - 1];\n" +
                "          nextPageNode && nextPageNode.click();\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        sites: ['cn.bing.com'],\n" +
                "        style: `\n" +
                "          .b_footer,\n" +
                "          .b_msg {\n" +
                "            display: none !important;\n" +
                "          }\n" +
                "        `,\n" +
                "        getNextPageUrl() {\n" +
                "          const nextPageNodes = document.querySelectorAll('[title=\"下一页\"]');\n" +
                "          const nextPageNode = nextPageNodes[nextPageNodes.length - 1];\n" +
                "          let nextPageLink = nextPageNode && nextPageNode.getAttribute('href') || '';\n" +
                "          let reg = /(https*?:\\/\\/\\w+\\.\\w+?\\.com)([\\w\\W]*)/i;\n" +
                "          nextPageLink = nextPageLink.replace(reg, (res, $1, $2) => {\n" +
                "            return top.location.origin + $2;\n" +
                "          });\n" +
                "          return nextPageLink;\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        sites: ['m.so.com'],\n" +
                "        style: ``,\n" +
                "        getNextPage() {\n" +
                "          const nextPageNodes = document.querySelectorAll('.load-more');\n" +
                "          const nextPageNode = nextPageNodes[nextPageNodes.length - 1];\n" +
                "          nextPageNode && nextPageNode.click();\n" +
                "        }\n" +
                "      }\n" +
                "    ];\n" +
                "\n" +
                "    if (document.readyState === 'complete') {\n" +
                "      new AutoNextPage(configs);\n" +
                "      return;\n" +
                "    };\n" +
                "    window.addEventListener('load', () => {\n" +
                "      new AutoNextPage(configs);\n" +
                "    });\n" +
                "  } catch (err) {\n" +
                "    console.log(':自动拼接下一页：', err);\n" +
                "  };\n" +
                "})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void suofan(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "网页强制缩放，启用之后可以两手指缩放网页");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 强制缩放\n" +
                " * @Author: 谷花泰\n" +
                " * @version: 1.0\n" +
                " * @description: 让所有网页都可以缩放\n" +
                " * @include: *\n" +
                " * @createTime: 2019-10-04 01:47:08\n" +
                " * @updateTime: 2019-10-09 14:46:02\n" +
                " */\n" +
                "(function () {\n" +
                "  /* 判断是否该执行 */\n" +
                "  const key = encodeURIComponent('谷花泰:强制缩放:执行判断');\n" +
                "\n" +
                "  if (window[key]) {\n" +
                "    return;\n" +
                "  };\n" +
                "\n" +
                "  window[key] = true;\n" +
                "\n" +
                "  /* 开始执行代码 */\n" +
                "  const meta = document.createElement('meta');\n" +
                "  meta.setAttribute('name', 'viewport');\n" +
                "  meta.setAttribute('content', 'width=device-width, initial-scale=1, user-scalable=yes');\n" +
                "  document.head.appendChild(meta);\n" +
                "})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void xlsx(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "网页下拉刷新");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 下拉刷新\n" +
                " * @Author: Sky\n" +
                " * @version: 1.7\n" +
                " * @description: 位于页面顶端时下拉刷新\n" +
                " * @include: *\n" +
                " * @createTime: 2020-3-6 01:00\n" +
                " * @updateTime: 2020-10-9 21:10\n" +
                " */\n" +
                "(function(){const\n" +
                "/*等号后的数表示最小触发下拉距离(px)*/\n" +
                " min_dY = 300,\n" +
                "/*－－－－以下勿改－－－－*/\n" +
                "key=encodeURIComponent('下拉刷新:执行判断');if(window[key]){return;}try{window[key]=true;let strtX,strtY=0,rchTp,onePt=false;document.addEventListener('touchstart',function(e){if(onePt){rchTp=false;}else{onePt=true;rchTp=(document.body.scrollTop||document.documentElement.scrollTop)<50;strtX=e.touches[0].screenX;strtY=e.touches[0].screenY;}},{'passive':true});document.addEventListener('touchend',function(e){if(rchTp){const dY=Math.floor(e.changedTouches[0].screenY-strtY);if(dY>min_dY&&Math.abs(e.changedTouches[0].screenX-strtX)<(0.4*dY)){location.reload();}rchTp=false;}onePt=false;},{'passive':true,'capture':true});}catch(err){console.log('下拉刷新：',err);}})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void tranlslate(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "划词翻译，文字长按之后会出现去翻译功能");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 划词菜单(搜索+翻译+转到)\n" +
                " * @Author: 酷安@达蒙山\n" +
                " * @version: 200418.22\n" +
                " * @description: 划词搜索、划词翻译、网址跳转\n" +
                " * @include: *\n" +
                " */\n" +
                "\n" +
                "!function() {\n" +
                "\n" +
                "  /*搜索引擎和翻译接口，请按相同格式修改*/\n" +
                "  var ssyq = [\n" +
                "\n" +
                "  {\n" +
                "    name: \"百度搜索\",\n" +
                "    url: \"https://www.baidu.com/s?wd=\"\n" +
                "  },\n" +
                "  {\n" +
                "    name: \"夸克搜索\",\n" +
                "    url: \"https://quark.sm.cn/s?q=\"\n" +
                "  },\n" +
                "  {\n" +
                "    name: \"百度翻译\",\n" +
                "    url: \"https://fanyi.baidu.com/?tpltype=sigma#zd/zd/\"\n" +
                "  },\n" +
                "  {\n" +
                "    name: \"夸克翻译\",\n" +
                "    url: \"https://quark.sm.cn/api/rest?method=quark_fanyi.dlpage&schema=v2&format=html&entry=fanyi_jinshan#zd/zd/\"\n" +
                "  }\n" +
                "\n" +
                "  ],\n" +
                "  hcTimer,\n" +
                "  ljurl,\n" +
                "  text;\n" +
                "\n" +
                "  function hccdyc() {\n" +
                "    clearTimeout(hcTimer);\n" +
                "    hcTimer = setTimeout(hccd, 750);\n" +
                "    if (document.getElementById(\"zdan\")) {\n" +
                "      document.getElementById(\"zdan\").parentNode.removeChild(document.getElementById(\"zdan\"));\n" +
                "    }\n" +
                "  }\n" +
                "  function hccd() {\n" +
                "    text = window.getSelection().toString().trim();\n" +
                "\n" +
                "    text ? (document.getElementById(\"hckj\").style.display = \"block\", zdcd()) : document.getElementById(\"hckj\").style.display = \"none\";\n" +
                "\n" +
                "  }\n" +
                "\n" +
                "  function tzurl(a, b) {\n" +
                "    b = b || text;\n" +
                "    ljurl = a + b;\n" +
                "    window.open(ljurl);\n" +
                "  }\n" +
                "\n" +
                "  function zdcd() {\n" +
                "    var zdurl = text.match(/(https?:\\/\\/(\\w[\\w-]*\\.)+[A-Za-z]{2,4}(?!\\w)(:\\d+)?(\\/([\\x21-\\x7e]*[\\w\\/=])?)?|(\\w[\\w-]*\\.)+(com|cn|org|net|info|tv|cc|gov|edu)(?!\\w)(:\\d+)?(\\/([\\x21-\\x7e]*[\\w\\/=])?)?)/i)[0];\n" +
                "\n" +
                "    if (zdurl) {\n" +
                "      var tzlj = document.createElement(\"span\");\n" +
                "      tzlj.id = \"zdan\";\n" +
                "      tzlj.innerHTML = \"\\u8f6c\\u5230\";\n" +
                "      tzlj.addEventListener(\"click\",\n" +
                "      function() {\n" +
                "\n" +
                "        zdurl.indexOf(\"http\") < 0 ? tzurl(\"https://\", zdurl) : tzurl(\"\", zdurl);\n" +
                "\n" +
                "      });\n" +
                "      document.getElementById(\"hckj\").appendChild(tzlj);\n" +
                "    }\n" +
                "  }\n" +
                "\n" +
                "  if (!document.getElementById(\"cdkj\")) {\n" +
                "    var cddiv = document.createElement(\"div\");\n" +
                "    cddiv.id = \"cdkj\";\n" +
                "    cddiv.style.cssText = \"display:block!important;width:100%;position:fixed;bottom:0px;z-index:9999999999;text-align:center;margin:4px auto;padding:4px;-webkit-tap-highlight-color:rgba(0,0,0,0);\";\n" +
                "    document.body.appendChild(cddiv);\n" +
                "    var cdstyle = document.createElement(\"style\");\n" +
                "    cdstyle.type = \"text/css\";\n" +
                "    cdstyle.innerHTML = \"#cdkj span{display:inline-block;background:#6a6a6a;color:#fff;font-size:20px;line-height:24px;margin:4px;padding:4px;border:1px solid #c5c5c5;border-radius:5px;}\";\n" +
                "    document.body.appendChild(cdstyle);\n" +
                "  }\n" +
                "  var hcdiv = document.createElement(\"div\");\n" +
                "  hcdiv.id = \"hckj\";\n" +
                "  hcdiv.style.cssText = \"display:none\";\n" +
                "  document.getElementById(\"cdkj\").appendChild(hcdiv);\n" +
                "\n" +
                "  for (var i = 0; i < ssyq.length; i++) {\n" +
                "    var jksp = document.createElement(\"span\");\n" +
                "    jksp.innerHTML = ssyq[i].name;\n" +
                "    jksp.setAttribute(\"jkdz\", ssyq[i].url);\n" +
                "    jksp.onclick = function() {\n" +
                "      tzurl(this.getAttribute(\"jkdz\"));\n" +
                "    };\n" +
                "    document.getElementById(\"hckj\").appendChild(jksp);\n" +
                "  }\n" +
                "\n" +
                "  document.addEventListener(\"selectionchange\", hccdyc);\n" +
                "\n" +
                "} ();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void kkmh(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "夸克引擎美化");
        values.put("url", "*");
        values.put("js","(function() {\n" +
                "  var whiteList = ['quark.sm.cn'];\n" +
                "  if (whiteList.indexOf(window.location.hostname) < 0) {\n" +
                "    return;\n" +
                "  };\n" +
                "document.getElementById(\"header\").innerHTML='<style>#header{display:none!important}</style>';\n" +
                "document.getElementById(\"pager\").click();\n" +
                "})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void erweima(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "将网页转换为二维码，网页右下角会出现图标，点击一下即可生成");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 网址二维码\n" +
                " * @Author: Sky\n" +
                " * @version: 1.2.1\n" +
                " * @description: 将网址转为二维码图片\n" +
                " * @include: *\n" +
                " * @createTime: 2020-7-8 13:30\n" +
                " * @updateTime: 2020-11-3 23:10\n" +
                " */\n" +
                "(function(){const\n" +
                "/* 等号后的数可供修改\n" +
                "   ↓ \"二维码\"按钮 ↓ */\n" +
                "lenToR = 3, /*到右边距离(%)*/\n" +
                "lenToB = 10, /*到底部距离(%)*/\n" +
                "/*－－－－以下勿改－－－－*/\n" +
                "key=encodeURIComponent('网址二维码:执行判断');if(window[key]){return;}try{window[key]=true;var QRCode;!function(){function a(a){var b,d,e,f;for(this.mode=c.MODE_8BIT_BYTE,this.data=a,this.parsedData=[],b=0,d=this.data.length;d>b;b++)e=[],f=this.data.charCodeAt(b),f>65536?(e[0]=240|(1835008&f)>>>18,e[1]=128|(258048&f)>>>12,e[2]=128|(4032&f)>>>6,e[3]=128|63&f):f>2048?(e[0]=224|(61440&f)>>>12,e[1]=128|(4032&f)>>>6,e[2]=128|63&f):f>128?(e[0]=192|(1984&f)>>>6,e[1]=128|63&f):e[0]=f,this.parsedData.push(e);this.parsedData=Array.prototype.concat.apply([],this.parsedData),this.parsedData.length!=this.data.length&&(this.parsedData.unshift(191),this.parsedData.unshift(187),this.parsedData.unshift(239))}function b(a,b){this.typeNumber=a,this.errorCorrectLevel=b,this.modules=null,this.moduleCount=0,this.dataCache=null,this.dataList=[]}function i(a,b){var c,d;if(void 0==a.length)throw new Error(a.length+\"/\"+b);for(c=0;c<a.length&&0==a[c];)c++;for(this.num=new Array(a.length-c+b),d=0;d<a.length-c;d++)this.num[d]=a[d+c]}function j(a,b){this.totalCount=a,this.dataCount=b}function k(){this.buffer=[],this.length=0}function n(a,b){var f,g,h,c=1,e=o(a);for(f=0,g=l.length;g>=f;f++){switch(h=0,b){case d.L:h=l[f][0];break;case d.M:h=l[f][1];break;case d.Q:h=l[f][2];break;case d.H:h=l[f][3]}if(h>=e)break;c++}if(c>l.length)throw new Error(\"Data too long\");return c}function o(a){var b=encodeURI(a).toString().replace(/\\%[0-9a-fA-F]{2}/g,\"a\");return b.length+(b.length!=a?3:0)}var c,d,e,f,g,h,l,m;for(a.prototype={getLength:function(){return this.parsedData.length},write:function(a){for(var b=0,c=this.parsedData.length;c>b;b++)a.put(this.parsedData[b],8)}},b.prototype={addData:function(b){var c=new a(b);this.dataList.push(c),this.dataCache=null},isDark:function(a,b){if(0>a||this.moduleCount<=a||0>b||this.moduleCount<=b)throw new Error(a+\",\"+b);return this.modules[a][b]},getModuleCount:function(){return this.moduleCount},make:function(){this.makeImpl(!1,this.getBestMaskPattern())},makeImpl:function(a,c){var d,e;for(this.moduleCount=4*this.typeNumber+17,this.modules=new Array(this.moduleCount),d=0;d<this.moduleCount;d++)for(this.modules[d]=new Array(this.moduleCount),e=0;e<this.moduleCount;e++)this.modules[d][e]=null;this.setupPositionProbePattern(0,0),this.setupPositionProbePattern(this.moduleCount-7,0),this.setupPositionProbePattern(0,this.moduleCount-7),this.setupPositionAdjustPattern(),this.setupTimingPattern(),this.setupTypeInfo(a,c),this.typeNumber>=7&&this.setupTypeNumber(a),null==this.dataCache&&(this.dataCache=b.createData(this.typeNumber,this.errorCorrectLevel,this.dataList)),this.mapData(this.dataCache,c)},setupPositionProbePattern:function(a,b){var c,d;for(c=-1;7>=c;c++)if(!(-1>=a+c||this.moduleCount<=a+c))for(d=-1;7>=d;d++)-1>=b+d||this.moduleCount<=b+d||(this.modules[a+c][b+d]=c>=0&&6>=c&&(0==d||6==d)||d>=0&&6>=d&&(0==c||6==c)||c>=2&&4>=c&&d>=2&&4>=d?!0:!1)},getBestMaskPattern:function(){var c,d,a=0,b=0;for(c=0;8>c;c++)this.makeImpl(!0,c),d=f.getLostPoint(this),(0==c||a>d)&&(a=d,b=c);return b},createMovieClip:function(a,b,c){var f,g,h,i,j,d=a.createEmptyMovieClip(b,c),e=1;for(this.make(),f=0;f<this.modules.length;f++)for(g=f*e,h=0;h<this.modules[f].length;h++)i=h*e,j=this.modules[f][h],j&&(d.beginFill(0,100),d.moveTo(i,g),d.lineTo(i+e,g),d.lineTo(i+e,g+e),d.lineTo(i,g+e),d.endFill());return d},setupTimingPattern:function(){var a,b;for(a=8;a<this.moduleCount-8;a++)null==this.modules[a][6]&&(this.modules[a][6]=0==a%2);for(b=8;b<this.moduleCount-8;b++)null==this.modules[6][b]&&(this.modules[6][b]=0==b%2)},setupPositionAdjustPattern:function(){var b,c,d,e,g,h,a=f.getPatternPosition(this.typeNumber);for(b=0;b<a.length;b++)for(c=0;c<a.length;c++)if(d=a[b],e=a[c],null==this.modules[d][e])for(g=-2;2>=g;g++)for(h=-2;2>=h;h++)this.modules[d+g][e+h]=-2==g||2==g||-2==h||2==h||0==g&&0==h?!0:!1},setupTypeNumber:function(a){var c,d,b=f.getBCHTypeNumber(this.typeNumber);for(c=0;18>c;c++)d=!a&&1==(1&b>>c),this.modules[Math.floor(c/3)][c%3+this.moduleCount-8-3]=d;for(c=0;18>c;c++)d=!a&&1==(1&b>>c),this.modules[c%3+this.moduleCount-8-3][Math.floor(c/3)]=d},setupTypeInfo:function(a,b){var e,g,c=this.errorCorrectLevel<<3|b,d=f.getBCHTypeInfo(c);for(e=0;15>e;e++)g=!a&&1==(1&d>>e),6>e?this.modules[e][8]=g:8>e?this.modules[e+1][8]=g:this.modules[this.moduleCount-15+e][8]=g;for(e=0;15>e;e++)g=!a&&1==(1&d>>e),8>e?this.modules[8][this.moduleCount-e-1]=g:9>e?this.modules[8][15-e-1+1]=g:this.modules[8][15-e-1]=g;this.modules[this.moduleCount-8][8]=!a},mapData:function(a,b){var h,i,j,k,c=-1,d=this.moduleCount-1,e=7,g=0;for(h=this.moduleCount-1;h>0;h-=2)for(6==h&&h--;;){for(i=0;2>i;i++)null==this.modules[d][h-i]&&(j=!1,g<a.length&&(j=1==(1&a[g]>>>e)),k=f.getMask(b,d,h-i),k&&(j=!j),this.modules[d][h-i]=j,e--,-1==e&&(g++,e=7));if(d+=c,0>d||this.moduleCount<=d){d-=c,c=-c;break}}}},b.PAD0=236,b.PAD1=17,b.createData=function(a,c,d){var h,i,l,e=j.getRSBlocks(a,c),g=new k;for(h=0;h<d.length;h++)i=d[h],g.put(i.mode,4),g.put(i.getLength(),f.getLengthInBits(i.mode,a)),i.write(g);for(l=0,h=0;h<e.length;h++)l+=e[h].dataCount;if(g.getLengthInBits()>8*l)throw new Error(\"code length overflow. (\"+g.getLengthInBits()+\">\"+8*l+\")\");for(g.getLengthInBits()+4<=8*l&&g.put(0,4);0!=g.getLengthInBits()%8;)g.putBit(!1);for(;;){if(g.getLengthInBits()>=8*l)break;if(g.put(b.PAD0,8),g.getLengthInBits()>=8*l)break;g.put(b.PAD1,8)}return b.createBytes(g,e)},b.createBytes=function(a,b){var j,k,l,m,n,o,p,q,r,s,t,c=0,d=0,e=0,g=new Array(b.length),h=new Array(b.length);for(j=0;j<b.length;j++){for(k=b[j].dataCount,l=b[j].totalCount-k,d=Math.max(d,k),e=Math.max(e,l),g[j]=new Array(k),m=0;m<g[j].length;m++)g[j][m]=255&a.buffer[m+c];for(c+=k,n=f.getErrorCorrectPolynomial(l),o=new i(g[j],n.getLength()-1),p=o.mod(n),h[j]=new Array(n.getLength()-1),m=0;m<h[j].length;m++)q=m+p.getLength()-h[j].length,h[j][m]=q>=0?p.get(q):0}for(r=0,m=0;m<b.length;m++)r+=b[m].totalCount;for(s=new Array(r),t=0,m=0;d>m;m++)for(j=0;j<b.length;j++)m<g[j].length&&(s[t++]=g[j][m]);for(m=0;e>m;m++)for(j=0;j<b.length;j++)m<h[j].length&&(s[t++]=h[j][m]);return s},c={MODE_NUMBER:1,MODE_ALPHA_NUM:2,MODE_8BIT_BYTE:4,MODE_KANJI:8},d={L:1,M:0,Q:3,H:2},e={PATTERN000:0,PATTERN001:1,PATTERN010:2,PATTERN011:3,PATTERN100:4,PATTERN101:5,PATTERN110:6,PATTERN111:7},f={PATTERN_POSITION_TABLE:[[],[6,18],[6,22],[6,26],[6,30],[6,34],[6,22,38],[6,24,42],[6,26,46],[6,28,50],[6,30,54],[6,32,58],[6,34,62],[6,26,46,66],[6,26,48,70],[6,26,50,74],[6,30,54,78],[6,30,56,82],[6,30,58,86],[6,34,62,90],[6,28,50,72,94],[6,26,50,74,98],[6,30,54,78,102],[6,28,54,80,106],[6,32,58,84,110],[6,30,58,86,114],[6,34,62,90,118],[6,26,50,74,98,122],[6,30,54,78,102,126],[6,26,52,78,104,130],[6,30,56,82,108,134],[6,34,60,86,112,138],[6,30,58,86,114,142],[6,34,62,90,118,146],[6,30,54,78,102,126,150],[6,24,50,76,102,128,154],[6,28,54,80,106,132,158],[6,32,58,84,110,136,162],[6,26,54,82,110,138,166],[6,30,58,86,114,142,170]],G15:1335,G18:7973,G15_MASK:21522,getBCHTypeInfo:function(a){for(var b=a<<10;f.getBCHDigit(b)-f.getBCHDigit(f.G15)>=0;)b^=f.G15<<f.getBCHDigit(b)-f.getBCHDigit(f.G15);return(a<<10|b)^f.G15_MASK},getBCHTypeNumber:function(a){for(var b=a<<12;f.getBCHDigit(b)-f.getBCHDigit(f.G18)>=0;)b^=f.G18<<f.getBCHDigit(b)-f.getBCHDigit(f.G18);return a<<12|b},getBCHDigit:function(a){for(var b=0;0!=a;)b++,a>>>=1;return b},getPatternPosition:function(a){return f.PATTERN_POSITION_TABLE[a-1]},getMask:function(a,b,c){switch(a){case e.PATTERN000:return 0==(b+c)%2;case e.PATTERN001:return 0==b%2;case e.PATTERN010:return 0==c%3;case e.PATTERN011:return 0==(b+c)%3;case e.PATTERN100:return 0==(Math.floor(b/2)+Math.floor(c/3))%2;case e.PATTERN101:return 0==b*c%2+b*c%3;case e.PATTERN110:return 0==(b*c%2+b*c%3)%2;case e.PATTERN111:return 0==(b*c%3+(b+c)%2)%2;default:throw new Error(\"bad maskPattern:\"+a)}},getErrorCorrectPolynomial:function(a){var c,b=new i([1],0);for(c=0;a>c;c++)b=b.multiply(new i([1,g.gexp(c)],0));return b},getLengthInBits:function(a,b){if(b>=1&&10>b)switch(a){case c.MODE_NUMBER:return 10;case c.MODE_ALPHA_NUM:return 9;case c.MODE_8BIT_BYTE:return 8;case c.MODE_KANJI:return 8;default:throw new Error(\"mode:\"+a)}else if(27>b)switch(a){case c.MODE_NUMBER:return 12;case c.MODE_ALPHA_NUM:return 11;case c.MODE_8BIT_BYTE:return 16;case c.MODE_KANJI:return 10;default:throw new Error(\"mode:\"+a)}else{if(!(41>b))throw new Error(\"type:\"+b);switch(a){case c.MODE_NUMBER:return 14;case c.MODE_ALPHA_NUM:return 13;case c.MODE_8BIT_BYTE:return 16;case c.MODE_KANJI:return 12;default:throw new Error(\"mode:\"+a)}}},getLostPoint:function(a){var d,e,f,g,h,i,j,k,l,b=a.getModuleCount(),c=0;for(d=0;b>d;d++)for(e=0;b>e;e++){for(f=0,g=a.isDark(d,e),h=-1;1>=h;h++)if(!(0>d+h||d+h>=b))for(i=-1;1>=i;i++)0>e+i||e+i>=b||(0!=h||0!=i)&&g==a.isDark(d+h,e+i)&&f++;f>5&&(c+=3+f-5)}for(d=0;b-1>d;d++)for(e=0;b-1>e;e++)j=0,a.isDark(d,e)&&j++,a.isDark(d+1,e)&&j++,a.isDark(d,e+1)&&j++,a.isDark(d+1,e+1)&&j++,(0==j||4==j)&&(c+=3);for(d=0;b>d;d++)for(e=0;b-6>e;e++)a.isDark(d,e)&&!a.isDark(d,e+1)&&a.isDark(d,e+2)&&a.isDark(d,e+3)&&a.isDark(d,e+4)&&!a.isDark(d,e+5)&&a.isDark(d,e+6)&&(c+=40);for(e=0;b>e;e++)for(d=0;b-6>d;d++)a.isDark(d,e)&&!a.isDark(d+1,e)&&a.isDark(d+2,e)&&a.isDark(d+3,e)&&a.isDark(d+4,e)&&!a.isDark(d+5,e)&&a.isDark(d+6,e)&&(c+=40);for(k=0,e=0;b>e;e++)for(d=0;b>d;d++)a.isDark(d,e)&&k++;return l=Math.abs(100*k/b/b-50)/5,c+=10*l}},g={glog:function(a){if(1>a)throw new Error(\"glog(\"+a+\")\");return g.LOG_TABLE[a]},gexp:function(a){for(;0>a;)a+=255;for(;a>=256;)a-=255;return g.EXP_TABLE[a]},EXP_TABLE:new Array(256),LOG_TABLE:new Array(256)},h=0;8>h;h++)g.EXP_TABLE[h]=1<<h;for(h=8;256>h;h++)g.EXP_TABLE[h]=g.EXP_TABLE[h-4]^g.EXP_TABLE[h-5]^g.EXP_TABLE[h-6]^g.EXP_TABLE[h-8];for(h=0;255>h;h++)g.LOG_TABLE[g.EXP_TABLE[h]]=h;i.prototype={get:function(a){return this.num[a]},getLength:function(){return this.num.length},multiply:function(a){var c,d,b=new Array(this.getLength()+a.getLength()-1);for(c=0;c<this.getLength();c++)for(d=0;d<a.getLength();d++)b[c+d]^=g.gexp(g.glog(this.get(c))+g.glog(a.get(d)));return new i(b,0)},mod:function(a){var b,c,d;if(this.getLength()-a.getLength()<0)return this;for(b=g.glog(this.get(0))-g.glog(a.get(0)),c=new Array(this.getLength()),d=0;d<this.getLength();d++)c[d]=this.get(d);for(d=0;d<a.getLength();d++)c[d]^=g.gexp(g.glog(a.get(d))+b);return new i(c,0).mod(a)}},j.RS_BLOCK_TABLE=[[1,26,19],[1,26,16],[1,26,13],[1,26,9],[1,44,34],[1,44,28],[1,44,22],[1,44,16],[1,70,55],[1,70,44],[2,35,17],[2,35,13],[1,100,80],[2,50,32],[2,50,24],[4,25,9],[1,134,108],[2,67,43],[2,33,15,2,34,16],[2,33,11,2,34,12],[2,86,68],[4,43,27],[4,43,19],[4,43,15],[2,98,78],[4,49,31],[2,32,14,4,33,15],[4,39,13,1,40,14],[2,121,97],[2,60,38,2,61,39],[4,40,18,2,41,19],[4,40,14,2,41,15],[2,146,116],[3,58,36,2,59,37],[4,36,16,4,37,17],[4,36,12,4,37,13],[2,86,68,2,87,69],[4,69,43,1,70,44],[6,43,19,2,44,20],[6,43,15,2,44,16],[4,101,81],[1,80,50,4,81,51],[4,50,22,4,51,23],[3,36,12,8,37,13],[2,116,92,2,117,93],[6,58,36,2,59,37],[4,46,20,6,47,21],[7,42,14,4,43,15],[4,133,107],[8,59,37,1,60,38],[8,44,20,4,45,21],[12,33,11,4,34,12],[3,145,115,1,146,116],[4,64,40,5,65,41],[11,36,16,5,37,17],[11,36,12,5,37,13],[5,109,87,1,110,88],[5,65,41,5,66,42],[5,54,24,7,55,25],[11,36,12],[5,122,98,1,123,99],[7,73,45,3,74,46],[15,43,19,2,44,20],[3,45,15,13,46,16],[1,135,107,5,136,108],[10,74,46,1,75,47],[1,50,22,15,51,23],[2,42,14,17,43,15],[5,150,120,1,151,121],[9,69,43,4,70,44],[17,50,22,1,51,23],[2,42,14,19,43,15],[3,141,113,4,142,114],[3,70,44,11,71,45],[17,47,21,4,48,22],[9,39,13,16,40,14],[3,135,107,5,136,108],[3,67,41,13,68,42],[15,54,24,5,55,25],[15,43,15,10,44,16],[4,144,116,4,145,117],[17,68,42],[17,50,22,6,51,23],[19,46,16,6,47,17],[2,139,111,7,140,112],[17,74,46],[7,54,24,16,55,25],[34,37,13],[4,151,121,5,152,122],[4,75,47,14,76,48],[11,54,24,14,55,25],[16,45,15,14,46,16],[6,147,117,4,148,118],[6,73,45,14,74,46],[11,54,24,16,55,25],[30,46,16,2,47,17],[8,132,106,4,133,107],[8,75,47,13,76,48],[7,54,24,22,55,25],[22,45,15,13,46,16],[10,142,114,2,143,115],[19,74,46,4,75,47],[28,50,22,6,51,23],[33,46,16,4,47,17],[8,152,122,4,153,123],[22,73,45,3,74,46],[8,53,23,26,54,24],[12,45,15,28,46,16],[3,147,117,10,148,118],[3,73,45,23,74,46],[4,54,24,31,55,25],[11,45,15,31,46,16],[7,146,116,7,147,117],[21,73,45,7,74,46],[1,53,23,37,54,24],[19,45,15,26,46,16],[5,145,115,10,146,116],[19,75,47,10,76,48],[15,54,24,25,55,25],[23,45,15,25,46,16],[13,145,115,3,146,116],[2,74,46,29,75,47],[42,54,24,1,55,25],[23,45,15,28,46,16],[17,145,115],[10,74,46,23,75,47],[10,54,24,35,55,25],[19,45,15,35,46,16],[17,145,115,1,146,116],[14,74,46,21,75,47],[29,54,24,19,55,25],[11,45,15,46,46,16],[13,145,115,6,146,116],[14,74,46,23,75,47],[44,54,24,7,55,25],[59,46,16,1,47,17],[12,151,121,7,152,122],[12,75,47,26,76,48],[39,54,24,14,55,25],[22,45,15,41,46,16],[6,151,121,14,152,122],[6,75,47,34,76,48],[46,54,24,10,55,25],[2,45,15,64,46,16],[17,152,122,4,153,123],[29,74,46,14,75,47],[49,54,24,10,55,25],[24,45,15,46,46,16],[4,152,122,18,153,123],[13,74,46,32,75,47],[48,54,24,14,55,25],[42,45,15,32,46,16],[20,147,117,4,148,118],[40,75,47,7,76,48],[43,54,24,22,55,25],[10,45,15,67,46,16],[19,148,118,6,149,119],[18,75,47,31,76,48],[34,54,24,34,55,25],[20,45,15,61,46,16]],j.getRSBlocks=function(a,b){var d,e,f,g,h,i,k,c=j.getRsBlockTable(a,b);if(void 0==c)throw new Error(\"bad rs block @ typeNumber:\"+a+\"/errorCorrectLevel:\"+b);for(d=c.length/3,e=[],f=0;d>f;f++)for(g=c[3*f+0],h=c[3*f+1],i=c[3*f+2],k=0;g>k;k++)e.push(new j(h,i));return e},j.getRsBlockTable=function(a,b){switch(b){case d.L:return j.RS_BLOCK_TABLE[4*(a-1)+0];case d.M:return j.RS_BLOCK_TABLE[4*(a-1)+1];case d.Q:return j.RS_BLOCK_TABLE[4*(a-1)+2];case d.H:return j.RS_BLOCK_TABLE[4*(a-1)+3];default:return void 0}},k.prototype={get:function(a){var b=Math.floor(a/8);return 1==(1&this.buffer[b]>>>7-a%8)},put:function(a,b){for(var c=0;b>c;c++)this.putBit(1==(1&a>>>b-c-1))},getLengthInBits:function(){return this.length},putBit:function(a){var b=Math.floor(this.length/8);this.buffer.length<=b&&this.buffer.push(0),a&&(this.buffer[b]|=128>>>this.length%8),this.length++}},l=[[17,14,11,7],[32,26,20,14],[53,42,32,24],[78,62,46,34],[106,84,60,44],[134,106,74,58],[154,122,86,64],[192,152,108,84],[230,180,130,98],[271,213,151,119],[321,251,177,137],[367,287,203,155],[425,331,241,177],[458,362,258,194],[520,412,292,220],[586,450,322,250],[644,504,364,280],[718,560,394,310],[792,624,442,338],[858,666,482,382],[929,711,509,403],[1003,779,565,439],[1091,857,611,461],[1171,911,661,511],[1273,997,715,535],[1367,1059,751,593],[1465,1125,805,625],[1528,1190,868,658],[1628,1264,908,698],[1732,1370,982,742],[1840,1452,1030,790],[1952,1538,1112,842],[2068,1628,1168,898],[2188,1722,1228,958],[2303,1809,1283,983],[2431,1911,1351,1051],[2563,1989,1423,1093],[2699,2099,1499,1139],[2809,2213,1579,1219],[2953,2331,1663,1273]],m=function(){function a(){this._elImage.src=this._elCanvas.toDataURL(\"image/png\"),this._elImage.style.display=\"block\",this._elCanvas.style.display=\"none\"}function b(a){var b=this;b._fSuccess=a,b._fSuccess.call(b)}var c=function(a,b){this._bIsPainted=!1,this._htOption=b,this._elCanvas=document.createElement(\"canvas\"),this._elCanvas.width=b.width,this._elCanvas.height=b.height,a.appendChild(this._elCanvas),this._el=a,this._oContext=this._elCanvas.getContext(\"2d\"),this._bIsPainted=!1,this._elImage=document.createElement(\"img\"),this._elImage.style.display=\"none\",this._el.appendChild(this._elImage)};return c.prototype.draw=function(a){var j,k,l,m,n,b=this._elImage,c=this._oContext,d=this._htOption,e=a.getModuleCount(),f=d.width/e,g=d.height/e,h=Math.round(f),i=Math.round(g);for(b.style.display=\"none\",this.clear(),j=0;e>j;j++)for(k=0;e>k;k++)l=a.isDark(j,k),m=k*f,n=j*g,c.strokeStyle=l?d.colorDark:d.colorLight,c.lineWidth=1,c.fillStyle=l?d.colorDark:d.colorLight,c.fillRect(m,n,f,g),c.strokeRect(Math.floor(m)+.5,Math.floor(n)+.5,h,i),c.strokeRect(Math.ceil(m)-.5,Math.ceil(n)-.5,h,i);this._bIsPainted=!0},c.prototype.makeImage=function(){this._bIsPainted&&b.call(this,a)},c.prototype.isPainted=function(){return this._bIsPainted},c.prototype.clear=function(){this._oContext.clearRect(0,0,this._elCanvas.width,this._elCanvas.height),this._bIsPainted=!1},c.prototype.round=function(a){return a?Math.floor(1e3*a)/1e3:a},c}(),QRCode=function(a,b){if(this._htOption={width:256,height:256,typeNumber:4,colorDark:\"#000\",colorLight:\"#fff\",correctLevel:d.L},\"string\"==typeof b&&(b={text:b}),b)for(var c in b)this._htOption[c]=b[c];this._el=a,this._oQRCode=null,this._oDrawing=new m(this._el,this._htOption),this._htOption.text&&this.makeCode(this._htOption.text)},QRCode.prototype.makeCode=function(a){this._oQRCode=new b(n(a,this._htOption.correctLevel),this._htOption.correctLevel),this._oQRCode.addData(a),this._oQRCode.make(),this._oDrawing.draw(this._oQRCode),this.makeImage()},QRCode.prototype.makeImage=function(){\"function\"==typeof this._oDrawing.makeImage&&this._oDrawing.makeImage()},QRCode.prototype.clear=function(){this._oDrawing.clear()},QRCode.CorrectLevel=d}();const qr=document.createElement(\"div\"),open=document.createElement(\"div\"),f=document.createElement('link'),css=`position:fixed!important;right:${lenToR}%;bottom:${lenToB}%;z-index:999999;padding:7px;`;f.rel='stylesheet';f.href=\"data:text/css,@font-face{font-family:QCicon;src:url('data:font/woff2;base64,d09GMgABAAAAAAJkAAoAAAAABcwAAAIYAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAABmAAgkIKgniCOQsGAAE2AiQDCAQgBYJWByAb0gTIHgXunoYBQWIYlA6DbVkOTuLhv7Fv983MnzWz5njyppnEIa40mmizUMmkSCNEMkkPxuU9mT7CTiASJ9ZlsDs0MMGlLzRBTY7D+y2wUOPInsPEmric/pj2gPv8wd/kwfaiabvoAwsz7KJ1Hk7vEsjCF7bUTfGo4iMVFSmCtebyH2P79+0FJT3EaZC+pBd8SD8aP+1FC0hqBoDoBsDdJ4EW3J4jziPL/8s9zuu+IyAPg2mBjIxxAQ5lgXHFNIoiOI////4/cAkgKvDHEijoxinwAlBA0lnQrNJPQPGwkSZhKOjMQ8TFN7kF6Xn/JpG8MbiNQK0P7bgDSO3cjqzbuiVlcWogRamAg3ku2qA1iOJWHdgC4UbuV/rpN+el8wJjV4t5cc6Czi5E4MW1WFzMdGNMuHMhFAljr4GznHOQX7LXzbWJd8h8ntxnBF6MXF4QWytMvX46XebULHgk82hq5evLP5SKHyvJn1eUEt4HEAj6frOrQzv8hSehAX6cutH9te7/+iLPo1kCwWUuSrcsWwjWKWZwwYICyTNU/K3tPgoQKgIkVRUga9UuQFENCtAwaVyAqtqXRSFAaHdpgAm9KWT97gtFu6dCw6Hzzardt3u+92artWyNUi3ms6RNnu3HdOO7UiqeoUCClwhxPn1NA2M/245N2/p4RtvYRB+uD0y74cVTAlr6psBxs+wU766cvQQKsHtPAAAA')}\";document.head.append(f);new QRCode(qr,location.href);qr.style=css+'background:#ccc;display:none';document.body.appendChild(qr);open.style=css+'font-family:QCicon!important;font-style:normal;font-weight:400;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;line-height:1;font-size:5vmin;padding:5px;background:#111;color:#eee;user-select:none;';open.innerText='码';open.onclick=function(){open.style.display='none';qr.style.display='block';};document.body.appendChild(open);document.addEventListener('click',function(e){if(e.target!=open){qr.style.display='none';open.style.display='block';}},{'passive':true,'capture':true});}catch(err){console.log('网址二维码：',err);}})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void arltlj(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "当网页某弹窗弹出次数达到此值的倍数时，询问是否拦截");
        values.put("url", "*");
        values.put("js"," /*\n" +
                " * @name: 网页弹窗拦截\n" +
                " * @Author: undefined303（残月un）\n" +
                " * @version: 1.0.0\n" +
                " * @include: *\n" +
                " */\n" +
                "(function () {\n" +
                "   'use strict';\n" +
                "const\n" +
                "CONFIRM_COUNT = 5;/*当某弹窗弹出次数达到此值的倍数时，询问是否拦截*/\n" +
                "const originalConfirm =window.confirm;\n" +
                "[\"alert\", \"confirm\", \"prompt\"].forEach((method) => {\n" +
                "let count = 0;\n" +
                "let count_time = 1;\n" +
                "let ignore = false;\n" +
                "const fn = window[method] ;\n" +
                "window[method]=function(){\n" +
                "if(ignore) {\n" +
                "return undefined;\n" +
                "}\n" +
                "const result = fn((arguments.length==0)?\"\":String(arguments[0]));\n" +
                "count++;\n" +
                "if (count == CONFIRM_COUNT*count_time) {\n" +
                "ignore = originalConfirm(\n" +
                "`当前网页已弹出`+method+`弹窗${count}次，是否忽略后续该弹窗`);\n" +
                "count_time++;\n" +
                "}\n" +
                "return result;\n" +
                "};\n" +
                "});\n" +
                "})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void kbn(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "网页在wifi情况下右边显示一个看板娘");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 看板娘\n" +
                " * @Author: Sky\n" +
                " * @version: 2.0\n" +
                " * @description: 愿你每天好心情\n" +
                " * @include: *\n" +
                " * @createTime: 2020-5-9 21:00\n" +
                " * @updateTime: 2022-2-8 23:55\n" +
                " */\n" +
                "(function(){const w = window,\n" +
                "/*以下参数可修改，=null表示恢复默认值*/\n" +
                "onlyWifi=true; /*仅在Wifi环境运行*/\n" +
                "w.kbn_setting0=6; /*人物ID*/\n" +
                "w.kbn_setting1=19; /*衣服ID*/\n" +
                "w.kbn_setting2=true; /*是否显示关闭按钮*/\n" +
                "w.kbn_setting3='180x170'; /*看板娘大小*/\n" +
                "w.kbn_setting4='right:10'; /*停靠侧:到侧边距离*/\n" +
                "w.kbn_setting5='160x50'; /*提示框大小*/\n" +
                "w.kbn_setting6='14px'; /*提示框字体大小*/\n" +
                "w.kbn_setting7='-13px'; /*提示框Y轴偏移*/\n" +
                "w.kbn_setting8='18px'; /*工具栏图标大小*/\n" +
                "w.kbn_setting9='36px'; /*工具栏行高*/\n" +
                "w.kbn_setting10=null; /*一言API可选'fghrsh.net', 'hitokoto.cn', 'jinrishici.com'(古诗词)*/\n" +
                "/*－－－－以下勿改－－－－*/\n" +
                "const key=encodeURIComponent('看板娘:执行判断');if(w[key]||(!(onlyWifi==false)&&navigator.connection.type!='wifi')){return;}try{w[key]=true;const lib=document.createElement('script');lib.src='https://cdn.jsdelivr.net/gh/IlysvlVEizbr/Live2D@2.0/kbn.js';lib.defer=true;document.body.append(lib);}catch(err){console.log('看板娘：',err);}})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void spts(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "视频倍速调试，启用之后网页视频播放时会出现调试按钮");
        values.put("url", "*");
        values.put("js","/*H5视频调速器，作者：Mr.NullNull*/\n" +
                "(function () {\n" +
                "    'use strict';\n" +
                " \n" +
                "    var IntPbr = 1;\n" +
                " \n" +
                "    const IntPbrMax = 16;\n" +
                "    const IntPbrMin = 0.1;\n" +
                "    const IntPbrStep = 0.25;\n" +
                " \n" +
                "    function Main() {\n" +
                "        if (document.querySelector(\"myPbrMain\")) return;\n" +
                " \n" +
                "        var myCss = document.createElement(\"style\");\n" +
                "        myCss.innerHTML = `\n" +
                "            div#myPbrMain {\n" +
                "                padding: 0;\n" +
                "                margin: 0;\n" +
                "                width: 1px;\n" +
                "                position: fixed;\n" +
                "                bottom: 28vw;\n" +
                "                right: 5vw;\n" +
                "                z-index: 2147483647;\n" +
                "            }\n" +
                " \n" +
                "            div#myPbrMain>* {\n" +
                "                float: right;\n" +
                "            }\n" +
                " \n" +
                "            div#myPbrMain>div.myPbrBtns {\n" +
                "                width: 1px;\n" +
                "                margin-bottom: 2.2vw;\n" +
                "            }\n" +
                " \n" +
                "            div.myPbrBtns {\n" +
                "                opacity: 0;\n" +
                "            }\n" +
                " \n" +
                "            div.myPbrBtn {\n" +
                "                font: 4vw/1 '微软雅黑';\n" +
                "                float: right;\n" +
                "                height: 4vw;\n" +
                "                padding: 2vw;\n" +
                "                margin-bottom: 0.8vw;\n" +
                "                border-radius: 20vw;\n" +
                "                color: #eee;\n" +
                "                background-color: rgba(0, 0, 0, 0.65);\n" +
                "            }\n" +
                " \n" +
                "            div#myPbrMain>div.myPbrBtn {\n" +
                "                width: auto;\n" +
                "            }\n" +
                " \n" +
                "            div#myPbrMain * {\n" +
                "                box-sizing: content-box;\n" +
                "                word-break: normal;\n" +
                "            }\n" +
                " \n" +
                "            div.show {\n" +
                "                animation: shower 0.3s;\n" +
                "                opacity: 1;\n" +
                "                display: block;\n" +
                "            }\n" +
                " \n" +
                "            div.hidden {\n" +
                "                animation: hiddener 0.3s;\n" +
                "                opacity: 0;\n" +
                "                display: none;\n" +
                "            }\n" +
                "\n" +
                "\n" +
                "            @keyframes shower {\n" +
                "                from {\n" +
                "                    opacity: 0;\n" +
                "                }\n" +
                "                to {\n" +
                "                    opacity: 1;\n" +
                "                }\n" +
                "            }\n" +
                " \n" +
                "            @keyframes hiddener {\n" +
                "                from {\n" +
                "                    opacity: 1;\n" +
                "                }\n" +
                "                to {\n" +
                "                    opacity: 0;\n" +
                "                }\n" +
                "            }\n" +
                "        `;\n" +
                "        document.head.appendChild(myCss);\n" +
                " \n" +
                "        var mainDivTop = document.createElement(\"div\");\n" +
                "        mainDivTop.id = \"myPbrMain\";\n" +
                "        mainDivTop.innerHTML = `\n" +
                "        <div class=\"myPbrBtns hidden\">\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_800\">x8.00</div>\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_300\">x3.00</div>\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_200\">x2.00</div>\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_150\">x1.50</div>\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_125\">x1.25</div>\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_100\">x1.00</div>\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_075\">x0.75</div>\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_050\">x0.50</div>\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_Add\">+${IntPbrStep.toFixed(2)}</div>\n" +
                "            <div class=\"myPbrBtn\" id=\"myPbrBtn_Cut\">-${IntPbrStep.toFixed(2)}</div>\n" +
                "        </div>\n" +
                "        <div class=\"myPbrBtn\" id=\"myPbrBtn_Main\">x1.XX</div>\n" +
                "        `;\n" +
                "        document.body.appendChild(mainDivTop);\n" +
                " \n" +
                "        var mainBtn = mainDivTop.querySelector(\"#myPbrBtn_Main\");\n" +
                "        var mainDiv = mainDivTop.querySelector(\".myPbrBtns\");\n" +
                " \n" +
                " \n" +
                "        setMainBtnTxt();\n" +
                " \n" +
                " \n" +
                "        mainBtn.onclick = function () {\n" +
                "            if (mainDiv.className == \"myPbrBtns hidden\") {\n" +
                "                setMainDivShow();\n" +
                "            } else {\n" +
                "                setMainDivNone();\n" +
                "            }\n" +
                "        };\n" +
                " \n" +
                "        mainDiv.querySelector(\"#myPbrBtn_800\").onclick = function () {\n" +
                "            IntPbr = 8;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "            setMainDivNone();\n" +
                "        };\n" +
                "        mainDiv.querySelector(\"#myPbrBtn_300\").onclick = function () {\n" +
                "            IntPbr = 3;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "            setMainDivNone();\n" +
                "        };\n" +
                "        mainDiv.querySelector(\"#myPbrBtn_200\").onclick = function () {\n" +
                "            IntPbr = 2.00;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "            setMainDivNone();\n" +
                "        };\n" +
                "        mainDiv.querySelector(\"#myPbrBtn_150\").onclick = function () {\n" +
                "            IntPbr = 1.50;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "            setMainDivNone();\n" +
                "        };\n" +
                "        mainDiv.querySelector(\"#myPbrBtn_125\").onclick = function () {\n" +
                "            IntPbr = 1.25;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "            setMainDivNone();\n" +
                "        };\n" +
                "        mainDiv.querySelector(\"#myPbrBtn_100\").onclick = function () {\n" +
                "            IntPbr = 1.00;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "            setMainDivNone();\n" +
                "        };\n" +
                "        mainDiv.querySelector(\"#myPbrBtn_075\").onclick = function () {\n" +
                "            IntPbr = 0.75;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "            setMainDivNone();\n" +
                "        };\n" +
                "        mainDiv.querySelector(\"#myPbrBtn_050\").onclick = function () {\n" +
                "            IntPbr = 0.50;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "            setMainDivNone();\n" +
                "        };\n" +
                " \n" +
                "        mainDiv.querySelector(\"#myPbrBtn_Add\").onclick = function () {\n" +
                "            IntPbr += IntPbrStep;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "        };\n" +
                "        mainDiv.querySelector(\"#myPbrBtn_Cut\").onclick = function () {\n" +
                "            IntPbr -= IntPbrStep;\n" +
                "            setVideoPBR();\n" +
                "            setMainBtnTxt();\n" +
                "        };\n" +
                " \n" +
                " \n" +
                "        function setVideoPBR() {\n" +
                "            if (IntPbr > IntPbrMax) {\n" +
                "                IntPbr = IntPbrMax;\n" +
                "            }\n" +
                "            if (IntPbr < IntPbrMin) {\n" +
                "                IntPbr = IntPbrMin;\n" +
                "            }\n" +
                " \n" +
                "            var tmps = document.querySelectorAll(\"video\");\n" +
                "            for (let i = 0; i < tmps.length; i++) {\n" +
                "                const element = tmps[i];\n" +
                "                element.playbackRate = IntPbr;\n" +
                "            }\n" +
                "            console.log(\"PBR = \" + IntPbr);\n" +
                "        }\n" +
                " \n" +
                "        function setMainBtnTxt() {\n" +
                "   mainBtn.innerHTML = \"x\" + IntPbr.toFixed(2);\n" +
                "        }\n" +
                " \n" +
                "        function setMainDivShow() {\n" +
                "            mainDiv.className = \"myPbrBtns show\";\n" +
                "        }\n" +
                " \n" +
                "        function setMainDivNone() {\n" +
                "            mainDiv.className = \"myPbrBtns hidden\";\n" +
                "        }\n" +
                "    };\n" +
                " \n" +
                "    function Padding(num, length) {\n" +
                "        return (Array(length).join(\"0\") + num).slice(-length);\n" +
                "    }\n" +
                " \n" +
                " \n" +
                "    var sli = setInterval(() => {\n" +
                "        if (document.querySelector(\"video\")) {\n" +
                "            Main();\n" +
                "            clearInterval(sli);\n" +
                "        }\n" +
                "    }, 1000);\n" +
                "    setTimeout(() => {\n" +
                "        clearInterval(sli);\n" +
                "    }, 10000);\n" +
                "})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void apptip(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "去掉网页某些app下载提示");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 纯净苍穹\n" +
                " * @Author: 谷花泰\n" +
                " * @version: 1.0\n" +
                " * @description: 对常用网站去掉app下载提示\n" +
                " * @include: *\n" +
                " * @createTime: 2019-10-13 08:46:24\n" +
                " * @updateTime: 2019-10-13 14:29:01\n" +
                " */\n" +
                "(function () {\n" +
                "  /* 执行判断 */\n" +
                "  const key = encodeURIComponent('谷花泰:纯净苍穹:执行判断');\n" +
                "  if (window[key]) {\n" +
                "    return;\n" +
                "  };\n" +
                "  window[key] = true;\n" +
                "\n" +
                "  class FuckAD {\n" +
                "    constructor(configs) {\n" +
                "      this._configs = configs;\n" +
                "\n" +
                "      /*\n" +
                "       * config里的配置解释\n" +
                "       * {\n" +
                "       *   正则匹配的域名数组\n" +
                "       *   sites: ['zhihu.com'],\n" +
                "       * \n" +
                "       *   移除的节点数组\n" +
                "       *   remove: ['#id'],\n" +
                "       * \n" +
                "       *   display隐藏的节点数组\n" +
                "       *   displayNone: ['#id'],\n" +
                "       * \n" +
                "       *   visibility隐藏的节点数组\n" +
                "       *   visibilityHidden: ['#id'],\n" +
                "       * \n" +
                "       *   额外的css\n" +
                "       *   style: `\n" +
                "       *   body {\n" +
                "       *     background-color: #000;\n" +
                "       *   }\n" +
                "       *   `,\n" +
                "       *  \n" +
                "       *   额外的函数执行\n" +
                "       *   others() {\n" +
                "       *     console.log('others: 哈哈');\n" +
                "       *   }\n" +
                "       * }\n" +
                "       *\n" +
                "       */\n" +
                "\n" +
                "      /* 初始化 */\n" +
                "      this.init();\n" +
                "    };\n" +
                "    /*\n" +
                "     * 初始化\n" +
                "     */\n" +
                "    init() {\n" +
                "      const that = this;\n" +
                "      /* 所有要移除的节点 */\n" +
                "      let remove = [];\n" +
                "      /* 总体style */\n" +
                "      let style = '';\n" +
                "      /* 要执行的其它函数集 */\n" +
                "      let others = [];\n" +
                "\n" +
                "      /* 统计 */\n" +
                "      this._configs.forEach(config => {\n" +
                "        const canLoad = that.siteInList(config.sites);\n" +
                "        if (canLoad) {\n" +
                "          remove = remove.concat(config.remove);\n" +
                "          style += (config.style || '');\n" +
                "          style += (that.letSelectorsDisplayNone(config.displayNone));\n" +
                "          style += (that.letSelectorsVisibilityHidden(config.visibilityHidden));\n" +
                "          config.others && (others = others.concat(config.others));\n" +
                "        };\n" +
                "      });\n" +
                "\n" +
                "      /* 添加style */\n" +
                "      this.addStyle(style);\n" +
                "      that.removeNodesBySelectors(remove);\n" +
                "\n" +
                "      /* 执行others内所有函数 */\n" +
                "      try {\n" +
                "        others.forEach(func => {\n" +
                "          func();\n" +
                "        });\n" +
                "      } catch (err) {\n" +
                "        console.error('via: others function run error', err);\n" +
                "      };\n" +
                "\n" +
                "      /* 监听dom，确保节点移除 */\n" +
                "      if (remove && remove.length > 0) {\n" +
                "        this.observe({\n" +
                "          targetNode: document.documentElement,\n" +
                "          config: {\n" +
                "            attributes: false\n" +
                "          },\n" +
                "          callback(mutations, observer) {\n" +
                "            that.removeNodesBySelectors(remove);\n" +
                "          }\n" +
                "        })\n" +
                "      }\n" +
                "    };\n" +
                "    /*\n" +
                "     * 监听dom节点加载函数\n" +
                "     */\n" +
                "    observe({ targetNode, config = {}, callback = () => { } }) {\n" +
                "      if (!targetNode) {\n" +
                "        return;\n" +
                "      };\n" +
                "\n" +
                "      config = Object.assign({\n" +
                "        attributes: true,\n" +
                "        childList: true,\n" +
                "        subtree: true\n" +
                "      }, config);\n" +
                "\n" +
                "      const observer = new MutationObserver(callback);\n" +
                "      observer.observe(targetNode, config);\n" +
                "    };\n" +
                "    /*\n" +
                "     * 添加style \n" +
                "     */\n" +
                "    addStyle(style = '') {\n" +
                "      const styleElm = document.createElement('style');\n" +
                "      styleElm.innerHTML = style;\n" +
                "      document.head.appendChild(styleElm);\n" +
                "    };\n" +
                "    /*\n" +
                "     * 选择节点，返回节点数组\n" +
                "     */\n" +
                "    selectNodes(selector) {\n" +
                "      if (!selector) {\n" +
                "        return [];\n" +
                "      };\n" +
                "      const nodes = document.querySelectorAll(selector);\n" +
                "      return nodes;\n" +
                "    };\n" +
                "    /*\n" +
                "     * 判断网站是否在名单内 \n" +
                "     */\n" +
                "    siteInList(sites) {\n" +
                "      const hostname = window.location.hostname;\n" +
                "      const result = sites.some(site => {\n" +
                "        if (hostname.match(site)) {\n" +
                "          return true;\n" +
                "        }\n" +
                "        return false;\n" +
                "      });\n" +
                "      return result;\n" +
                "    };\n" +
                "    /*\n" +
                "     * 移除多个节点\n" +
                "     */\n" +
                "    removeNodes(nodes = []) {\n" +
                "      Array.from(nodes, node => {\n" +
                "        node.parentNode.removeChild(node);\n" +
                "      });\n" +
                "    };\n" +
                "    /*\n" +
                "     * 根据selector数组移除多个节点\n" +
                "     */\n" +
                "    removeNodesBySelectors(selectors = []) {\n" +
                "      let nodeArr = [];\n" +
                "      selectors.forEach(selector => {\n" +
                "        const nodes = this.selectNodes(selector);\n" +
                "        if (nodes && nodes.length > 0) {\n" +
                "          nodeArr = nodeArr.concat(Array.from(nodes));\n" +
                "        };\n" +
                "      });\n" +
                "      this.removeNodes(nodeArr);\n" +
                "    };\n" +
                "    /*\n" +
                "     * 根据css选择器生成style\n" +
                "     */\n" +
                "    generateStyleBySelectors(selectors = [], customStyle = `{}`) {\n" +
                "      if (!selectors || selectors.length === 0) {\n" +
                "        return '';\n" +
                "      };\n" +
                "      let style = '';\n" +
                "      selectors.forEach(selector => {\n" +
                "        if (selector) {\n" +
                "          style += `\n" +
                "          \n" +
                "          ${selectors} ${customStyle}\n" +
                "  \n" +
                "          `;\n" +
                "        };\n" +
                "      });\n" +
                "      return style;\n" +
                "    };\n" +
                "    /*\n" +
                "     * 让数组里的选择器全部 display: none\n" +
                "     */\n" +
                "    letSelectorsDisplayNone(selectors = []) {\n" +
                "      return this.generateStyleBySelectors(selectors, `{\n" +
                "        display: none!important;\n" +
                "      }`);\n" +
                "    };\n" +
                "    /*\n" +
                "     * 让数组里的选择器全部 visibility: hidden\n" +
                "     */\n" +
                "    letSelectorsVisibilityHidden(selectors = []) {\n" +
                "      return this.generateStyleBySelectors(selectors, `{\n" +
                "        visibility: hidden!important;\n" +
                "      }`);\n" +
                "    };\n" +
                "  };\n" +
                "\n" +
                "  new FuckAD([\n" +
                "    {\n" +
                "      sites: ['bilibili.com'],\n" +
                "      displayNone: [\n" +
                "        '.index__openAppBtn__src-commonComponent-topArea-',\n" +
                "        '.index__container__src-commonComponent-bottomOpenApp-',\n" +
                "        '.index__openApp__src-videoPage-related2Col-videoItem-',\n" +
                "        '.index__floatOpenBtn__src-videoPage-floatOpenBtn-',\n" +
                "        '.index__downLoadBtn__src-videoPage-commentArea-',\n" +
                "        '.bili-app-link-container',\n" +
                "        '.open-app-bar',\n" +
                "        '.btn-ctnr',\n" +
                "        '.bili-app',\n" +
                "        '#openAppBtn'\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      sites: ['zhihu.com'],\n" +
                "      displayNone: [\n" +
                "        '.MobileAppHeader-downloadLink',\n" +
                "        '.ContentItem-more',\n" +
                "        '.TopstoryItem--advertCard',\n" +
                "        '.DownloadGuide',\n" +
                "        '.Profile-followButton',\n" +
                "        '.OpenInAppButton',\n" +
                "        '.OpenInApp',\n" +
                "        '.ViewAllInappButton',\n" +
                "        '.HotQuestions-bottomButton',\n" +
                "        '.MHotFeedAd',\n" +
                "        '.MBannerAd',\n" +
                "        '.HotQuestions'\n" +
                "      ],\n" +
                "      style: `\n" +
                "        .MobileAppHeader-actions {\n" +
                "          display: flex;\n" +
                "          justify-content: space-between;\n" +
                "          width: 100%;\n" +
                "        }\n" +
                "      `\n" +
                "    },\n" +
                "    {\n" +
                "      sites: ['www.baidu.com', 'm.baidu.com'],\n" +
                "      remove: [\n" +
                "        '#header > div:last-child',\n" +
                "        '.ec_wise_ad'\n" +
                "      ],\n" +
                "      displayNone: [\n" +
                "        '.blank-frame',\n" +
                "        '#bottom',\n" +
                "        '.suggest-hot',\n" +
                "        '.callicon-wrap',\n" +
                "        '#navs',\n" +
                "        '#page-copyright',\n" +
                "        '[data-module=\"xcxMulti\"]',\n" +
                "      ],\n" +
                "      visibilityHidden: [\n" +
                "        '#userinfo-wrap'\n" +
                "      ],\n" +
                "      style: `\n" +
                "        body {\n" +
                "          background-color: #fff !important;\n" +
                "          height: 100vh !important;\n" +
                "        }\n" +
                "      `\n" +
                "    },\n" +
                "    {\n" +
                "      sites: ['music.163.com'],\n" +
                "      displayNone: [\n" +
                "        '.topfr',\n" +
                "        '.m-homeft',\n" +
                "        '.u-ft',\n" +
                "        '.cmt_more_applink'\n" +
                "      ],\n" +
                "      visibilityHidden: [\n" +
                "        '.m-moreLists'\n" +
                "      ],\n" +
                "      style: `\n" +
                "        .m-scroll_wrapper {\n" +
                "          bottom: 0 !important;\n" +
                "        }\n" +
                "      `\n" +
                "    },\n" +
                "    {\n" +
                "      sites: ['y.qq.com'],\n" +
                "      displayNone: [\n" +
                "        '#js_mod_dialog',\n" +
                "        '.top_box',\n" +
                "        '.bottom_bar',\n" +
                "        '.top_bar',\n" +
                "        '.top_operation_box',\n" +
                "        '.lyric_action',\n" +
                "        '.sing',\n" +
                "        '.btn_download',\n" +
                "        '.open_qqmusic__btn',\n" +
                "        '.similar_song',\n" +
                "        '.related_album',\n" +
                "        '.related_info',\n" +
                "        '.recommend_mv',\n" +
                "        '.mod_lead_flow'\n" +
                "      ]\n" +
                "    }\n" +
                "  ]);\n" +
                "})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void gpnight(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "网页高配夜间模式");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 野径云俱黑，江船火独明\n" +
                " * @Author: 谷花泰\n" +
                " * @version: 5.1\n" +
                " * @description: 让你的网页随风潜入夜\n" +
                " * @include: *\n" +
                " * @createTime: 2019-10-19 01:09:24\n" +
                " * @updateTime: 2019-10-20 02:36:48\n" +
                " */\n" +
                "(function () {\n" +
                "  /* 判断是否该执行 */\n" +
                "  /* 网址黑名单制，遇到这些域名不执行 */\n" +
                "  const blackList = ['example.com'];\n" +
                "\n" +
                "  const hostname = window.location.hostname;\n" +
                "  const key = encodeURIComponent('谷花泰:野径云俱黑，江船火独明:执行判断');\n" +
                "  const isBlack = blackList.some(keyword => {\n" +
                "    if (hostname.match(keyword)) {\n" +
                "      return true;\n" +
                "    };\n" +
                "    return false;\n" +
                "  });\n" +
                "\n" +
                "  if (isBlack || window[key]) {\n" +
                "    return;\n" +
                "  };\n" +
                "  window[key] = true;\n" +
                "\n" +
                "  /* 开始执行代码 */\n" +
                "  class ChangeBackground {\n" +
                "    constructor() {\n" +
                "      this.init();\n" +
                "    };\n" +
                "    init() {\n" +
                "      this.addStyle(`\n" +
                "        html, body {\n" +
                "          background-color: #000 !important;\n" +
                "        }\n" +
                "        * {\n" +
                "          color: #CCD1D9 !important;\n" +
                "          box-shadow: none !important;\n" +
                "        }\n" +
                "        *:after, *:before {\n" +
                "          border-color: #1e1e1e !important;\n" +
                "          color: #CCD1D9 !important;\n" +
                "          box-shadow: none !important;\n" +
                "          background-color: transparent !important;\n" +
                "        }\n" +
                "        a, a > *{\n" +
                "          color: #409B9B !important;\n" +
                "        }\n" +
                "        [data-change-border-color][data-change-border-color-important] {\n" +
                "          border-color: #1e1e1e !important;\n" +
                "        }\n" +
                "        [data-change-background-color][data-change-background-color-important] {\n" +
                "          background-color: #000 !important;\n" +
                "        }\n" +
                "      `);\n" +
                "      this.selectAllNodes(node => {\n" +
                "        if (node.nodeType !== 1) {\n" +
                "          return;\n" +
                "        };\n" +
                "        const style = window.getComputedStyle(node, null);\n" +
                "        const whiteList = ['rgba(0, 0, 0, 0)', 'transparent'];\n" +
                "        const backgroundColor = style.getPropertyValue('background-color');\n" +
                "        const borderColor = style.getPropertyValue('border-color');\n" +
                "        if (whiteList.indexOf(backgroundColor) < 0) {\n" +
                "          if (this.isWhiteToBlack(backgroundColor)) {\n" +
                "            node.dataset.changeBackgroundColor = '';\n" +
                "            node.dataset.changeBackgroundColorImportant = '';\n" +
                "          } else {\n" +
                "            delete node.dataset.changeBackgroundColor;\n" +
                "            delete node.dataset.changeBackgroundColorImportant;\n" +
                "          };\n" +
                "        };\n" +
                "        if (whiteList.indexOf(borderColor) < 0) {\n" +
                "          if (this.isWhiteToBlack(borderColor)) {\n" +
                "            node.dataset.changeBorderColor = '';\n" +
                "            node.dataset.changeBorderColorImportant = '';\n" +
                "          } else {\n" +
                "            delete node.dataset.changeBorderColor;\n" +
                "            delete node.dataset.changeBorderColorImportant;\n" +
                "          };\n" +
                "        };\n" +
                "        if (borderColor.indexOf('rgb(255, 255, 255)') >= 0) {\n" +
                "          delete node.dataset.changeBorderColor;\n" +
                "          delete node.dataset.changeBorderColorImportant;\n" +
                "          node.style.borderColor = 'transparent';\n" +
                "        };\n" +
                "      });\n" +
                "    };\n" +
                "    addStyle(style = '') {\n" +
                "      const styleElm = document.createElement('style');\n" +
                "      styleElm.innerHTML = style;\n" +
                "      document.head.appendChild(styleElm);\n" +
                "    };\n" +
                "    /* 是否为灰白黑 */\n" +
                "    isWhiteToBlack(colorStr = '') {\n" +
                "      let hasWhiteToBlack = false;\n" +
                "      const colorArr = colorStr.match(/rgb.+?\\)/g);\n" +
                "      if (!colorArr || colorArr.length === 0) {\n" +
                "        return true;\n" +
                "      };\n" +
                "      colorArr.forEach(color => {\n" +
                "        const reg = /rgb[a]*?\\(([0-9]+),.*?([0-9]+),.*?([0-9]+).*?\\)/g;\n" +
                "        const result = reg.exec(color);\n" +
                "        const red = result[1];\n" +
                "        const green = result[2];\n" +
                "        const blue = result[3];\n" +
                "        const deviation = 20;\n" +
                "        const max = Math.max(red, green, blue);\n" +
                "        const min = Math.min(red, green, blue);\n" +
                "        if (max - min <= deviation) {\n" +
                "          hasWhiteToBlack = true;\n" +
                "        };\n" +
                "      });\n" +
                "      return hasWhiteToBlack;\n" +
                "    };\n" +
                "    selectAllNodes(callback = () => { }) {\n" +
                "      const allNodes = document.querySelectorAll('*');\n" +
                "      Array.from(allNodes, node => {\n" +
                "        callback(node);\n" +
                "      });\n" +
                "      this.observe({\n" +
                "        targetNode: document.documentElement,\n" +
                "        config: {\n" +
                "          attributes: false\n" +
                "        },\n" +
                "        callback(mutations, observer) {\n" +
                "          const allNodes = document.querySelectorAll('*');\n" +
                "          Array.from(allNodes, node => {\n" +
                "            callback(node);\n" +
                "          });\n" +
                "        }\n" +
                "      });\n" +
                "    };\n" +
                "    observe({ targetNode, config = {}, callback = () => { } }) {\n" +
                "      if (!targetNode) {\n" +
                "        return;\n" +
                "      };\n" +
                "\n" +
                "      config = Object.assign({\n" +
                "        attributes: true,\n" +
                "        childList: true,\n" +
                "        subtree: true\n" +
                "      }, config);\n" +
                "\n" +
                "      const observer = new MutationObserver(callback);\n" +
                "      observer.observe(targetNode, config);\n" +
                "    };\n" +
                "  };\n" +
                "  new ChangeBackground();\n" +
                "})();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void urlclick(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "网页上的链接可以被点击");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 让链接可点击\n" +
                " * @Author: 谷花泰\n" +
                " * @version: 2.0\n" +
                " * @description: 不用再复制链接打开这么麻烦了\n" +
                " * @include: *\n" +
                " * @createTime: 2019-11-12 13:47:41\n" +
                " * @updateTime   : 2020-03-16 00:38:38\n" +
                " */\n" +
                "(function () {\n" +
                "  /* 判断是否该执行 */\n" +
                "  /* 网址黑名单制，遇到这些域名不执行 */\n" +
                "  const blackList = ['example.com'];\n" +
                "\n" +
                "  const hostname = window.location.hostname;\n" +
                "  const key = encodeURIComponent('谷花泰:让链接可点击:执行判断');\n" +
                "  const isBlack = blackList.some(keyword => {\n" +
                "    if (hostname.match(keyword)) {\n" +
                "      return true;\n" +
                "    };\n" +
                "    return false;\n" +
                "  });\n" +
                "\n" +
                "  if (isBlack || window[key]) {\n" +
                "    return;\n" +
                "  };\n" +
                "  window[key] = true;\n" +
                "\n" +
                "  class ClickLink {\n" +
                "    constructor() {\n" +
                "      this.init();\n" +
                "    };\n" +
                "    init() {\n" +
                "      console.log('嘿嘿嘿');\n" +
                "      this.url_regexp = /((https?:\\/\\/|www\\.)[\\x21-\\x7e]+[\\w\\/=]|\\w([\\w._-])+@\\w[\\w\\._-]+\\.(com|cn|org|net|info|tv|cc|gov|edu)|(\\w[\\w._-]+\\.(com|cn|org|net|info|tv|cc|gov|edu))(\\/[\\x21-\\x7e]*[\\w\\/])?|ed2k:\\/\\/[\\x21-\\x7e]+\\|\\/|thunder:\\/\\/[\\x21-\\x7e]+=)/gi;\n" +
                "      this.urlPrefixes = ['http://', 'https://', 'ftp://', 'thunder://', 'ed2k://', 'mailto://', 'file://'];\n" +
                "      document.addEventListener(\"mouseover\", this.clearLink.bind(this));\n" +
                "      this.excludedTags = \"a,svg,canvas,applet,input,button,area,pre,embed,frame,frameset,head,iframe,img,option,map,meta,noscript,object,script,style,textarea,code\".split(\",\");\n" +
                "      this.xPath = \"//text()[not(ancestor::\" + this.excludedTags.join(') and not(ancestor::') + \")]\";\n" +
                "      this.startObserve();\n" +
                "      setTimeout(this.linkMixInit.bind(this), 100);\n" +
                "    };\n" +
                "    clearLink(event) {\n" +
                "      let j, len, link, prefix, ref, ref1, url;\n" +
                "      link = (ref = event.originalTarget) != null ? ref : event.target;\n" +
                "\n" +
                "      if (!(link != null && link.localName === \"a\" && ((ref1 = link.className) != null ? ref1.indexOf(\"textToLink\") : void 0) !== -1)) {\n" +
                "        return;\n" +
                "      };\n" +
                "\n" +
                "      url = link.getAttribute(\"href\");\n" +
                "\n" +
                "      for (j = 0, len = this.urlPrefixes.length; j < len; j++) {\n" +
                "        prefix = this.urlPrefixes[j];\n" +
                "\n" +
                "        if (url.indexOf(prefix) === 0) {\n" +
                "          return;\n" +
                "        };\n" +
                "      };\n" +
                "\n" +
                "      if (url.indexOf('@') !== -1) {\n" +
                "        return link.setAttribute(\"href\", \"mailto://\" + url);\n" +
                "      } else {\n" +
                "        return link.setAttribute(\"href\", \"http://\" + url);\n" +
                "      };\n" +
                "    };\n" +
                "    setLink(candidate) {\n" +
                "      let ref, ref1, ref2, span, text;\n" +
                "\n" +
                "      if (candidate == null || ((ref = candidate.parentNode) != null ? (ref1 = ref.className) != null ? typeof ref1.indexOf === \"function\" ? ref1.indexOf(\"textToLink\") : void 0 : void 0 : void 0) !== -1 || candidate.nodeName === \"#cdata-section\") {\n" +
                "        return;\n" +
                "      };\n" +
                "\n" +
                "      text = candidate.textContent.replace(this.url_regexp, '<a href=\"$1\" target=\"_blank\" class=\"textToLink\">$1</a>');\n" +
                "\n" +
                "      if (((ref2 = candidate.textContent) != null ? ref2.length : void 0) === text.length) {\n" +
                "        return;\n" +
                "      };\n" +
                "\n" +
                "      span = document.createElement(\"span\");\n" +
                "      span.innerHTML = text;\n" +
                "      return candidate.parentNode.replaceChild(span, candidate);\n" +
                "    };\n" +
                "    linkPack(result, start) {\n" +
                "      let i, j, k, ref, ref1, ref2, ref3, startTime;\n" +
                "      startTime = Date.now();\n" +
                "\n" +
                "      while (start + 10000 < result.snapshotLength) {\n" +
                "        for (i = j = ref = start, ref1 = start + 10000; ref <= ref1 ? j <= ref1 : j >= ref1; i = ref <= ref1 ? ++j : --j) {\n" +
                "          this.setLink(result.snapshotItem(i));\n" +
                "        };\n" +
                "\n" +
                "        start += 10000;\n" +
                "\n" +
                "        if (Date.now() - startTime > 2500) {\n" +
                "          return;\n" +
                "        };\n" +
                "      };\n" +
                "\n" +
                "      for (i = k = ref2 = start, ref3 = result.snapshotLength; ref2 <= ref3 ? k <= ref3 : k >= ref3; i = ref2 <= ref3 ? ++k : --k) {\n" +
                "        this.setLink(result.snapshotItem(i));\n" +
                "      };\n" +
                "    };\n" +
                "    linkify(node) {\n" +
                "      let result;\n" +
                "      result = document.evaluate(this.xPath, node, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);\n" +
                "      return this.linkPack(result, 0);\n" +
                "    };\n" +
                "    linkFilter(node) {\n" +
                "      let j, len, tag;\n" +
                "\n" +
                "      for (j = 0, len = this.excludedTags.length; j < len; j++) {\n" +
                "        tag = this.excludedTags[j];\n" +
                "\n" +
                "        if (tag === node.parentNode.localName.toLowerCase()) {\n" +
                "          return NodeFilter.FILTER_REJECT;\n" +
                "        };\n" +
                "      };\n" +
                "\n" +
                "      return NodeFilter.FILTER_ACCEPT;\n" +
                "    };\n" +
                "    observePage(root) {\n" +
                "      const tW = document.createTreeWalker(root, NodeFilter.SHOW_TEXT, {\n" +
                "        acceptNode: this.linkFilter\n" +
                "      }, false);\n" +
                "\n" +
                "      while (tW.nextNode()) {\n" +
                "        this.setLink(tW.currentNode);\n" +
                "      };\n" +
                "    };\n" +
                "    startObserve() {\n" +
                "      this.observer = new window.MutationObserver(mutations => {\n" +
                "        let Node, j, k, len, len1, mutation, ref;\n" +
                "\n" +
                "        for (j = 0, len = mutations.length; j < len; j++) {\n" +
                "          mutation = mutations[j];\n" +
                "\n" +
                "          if (mutation.type === \"childList\") {\n" +
                "            ref = mutation.addedNodes;\n" +
                "\n" +
                "            for (k = 0, len1 = ref.length; k < len1; k++) {\n" +
                "              Node = ref[k];\n" +
                "              this.observePage(Node);\n" +
                "            };\n" +
                "          };\n" +
                "        };\n" +
                "      });\n" +
                "\n" +
                "    };\n" +
                "    linkMixInit() {\n" +
                "      if (window !== window.top || window.document.title === \"\") {\n" +
                "        return;\n" +
                "      };\n" +
                "\n" +
                "      this.linkify(document.body);\n" +
                "\n" +
                "      return this.observer.observe(document.body, {\n" +
                "        childList: true,\n" +
                "        subtree: true\n" +
                "      });\n" +
                "    };\n" +
                "  };\n" +
                "\n" +
                "  try {\n" +
                "    new ClickLink();\n" +
                "  } catch (err) {\n" +
                "    console.log('via插件：让链接可点击：加载失败', err);\n" +
                "  };\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void wzzg(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "让网页文字更有质感");
        values.put("url", "*");
        values.put("js","(function() {\n" +
                "if(document.getElementById('zgttyy')){\n" +
                "rerurn;\n" +
                "}\n" +
                "\n" +
                "var font_style = document.createElement(\"style\");\n" +
                "    font_style.type = 'text/css';\n" +
                "font_style.id=\"zgttyy\";\n" +
                "    str = \" html,body,table,tr,td,th,tbody,form,article,div,dt,ul,ol,li,dl,dd,section,footer,nav,strong,aside,header,label,address,bdo,big,blockquote,caption,em,center,cite,dialog,dir,fieldset,figcaption,figure,main,pre,small,h1,h2,h3,h4,h5,h6:not([class*='icon']):not(.fa):not(.fas):not(i) {font-family: 'PingFang SC','Heiti SC','myfont','Microsoft YaHei','Source Han Sans SC','Noto Sans CJK SC','HanHei SC', 'sans-serif' ,'icomoon','Icons' ,'brand-icons' ,'FontAwesome','Material Icons','Material Icons Extended','Glyphicons Halflings'  !important;} *{text-shadow:1px 1px 10px #d0d0d0 !important; font-weight:bold !important;font-family: 'PingFang SC','Microsoft YaHei';}\";\n" +
                "\n" +
                "font_style.appendChild(document.createTextNode(str));\n" +
                "\n" +
                "\n" +
                "var head=document.getElementsByTagName(\"head\");\n" +
                "if(head.length>0&&head[0].appendChild(font_style)){\n" +
                "\n" +
                "}else{\n" +
                "\n" +
                "document.body.appendChild(font_style);\n" +
                "}\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void zkqw(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "自动展开某些网页页面全文");
        values.put("url", "*");
        values.put("js","(function(){\n" +
                "var customUserAgent = 'Mozilla/5.0 (Linux; Android 9; M2102K1AC Build/RKQ1.201112.002; wv lite baiduboxapp) baiduboxapp/ AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.89 Mobile Safari/537.36 T7/12.16 SearchCraft/3.9.1 (Baidu; P1 11)';\n" +
                " \n" +
                "\n" +
                "            Object.defineProperty(navigator, 'userAgent', {\n" +
                "              value: customUserAgent,\n" +
                "              writable: false\n" +
                "            });\n" +
                " \n" +
                "\n" +
                " \n" +
                "console.log(navigator.userAgent);\n" +
                "var g_times = 0;\n" +
                "function myfun() {\n" +
                "\n" +
                "document.querySelector(\"div.fold-btn-arrow-black\")?.click();\n" +
                "\n" +
                "document.querySelector(\"div.expand_more\")?.click();\n" +
                "document.querySelector(\"div.cancel-btn.btn\")?.click();\n" +
                "\n" +
                "document.querySelector(\"img.icon-content-more\")?.click();\n" +
                "document.querySelector(\"button.btn.btn-cancel\")?.click();\n" +
                "\n" +
                "window.showAll?.();\n" +
                "\n" +
                "document.querySelector('[class^=\"arrow\"]')?.click();\n" +
                "document.querySelectorAll('[class^=\"guideLayerCancel\"]').forEach(a => a.click());\n" +
                "\n" +
                "document.querySelector(\"div.open-btn\")?.click();\n" +
                "document.querySelector(\"div.cancel-btn\")?.click();\n" +
                "\n" +
                "document.querySelector(\"div.read-whole\")?.click();\n" +
                "\n" +
                "document.querySelector(\"div.mask.read-mask.app-download.read-in-app\")?.click();\n" +
                "document.querySelector(\"div.read-more.read-in-wap.font-red\")?.click();\n" +
                "\n" +
                "document.querySelector(\"section#artLookAll.look-all\")?.click();\n" +
                "\n" +
                "try{document.querySelector('div.baidu-close-collapse-btn')?.click();}catch(e){}document.querySelector('button.cancel')?.click();document.querySelector(\"button.cancel\")?.click();document.querySelector('body > div.download-app-guidance > div > div > div:nth-child(3) > div.wrap-item-btn')?.click();\n" +
                "  if(g_times >= 5) {\n" +
                "    window.clearInterval(timer);\n" +
                "  } \n" +
                "  g_times ++;\n" +
                "}\n" +
                "var timer = setInterval(myfun,500);\n" +
                "myfun();\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void bdsszk(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "百度搜索展开更多结果");
        values.put("url", "*");
        values.put("js","(function(){\n" +
                "var g_times = 0;\n" +
                "function myfun() {\n" +
                "document.querySelector(\"DIV.ivk-button.ivk-button-popup[data-ivk*='展开更多搜索结果|更快更好的搜索，就用百度APP'],DIV.popup-lead-cancel\").click();\n" +
                "setTimeout(function(){ document.querySelector(\"DIV.popup-lead-cancel\").click(); }, 1);\n" +
                "if(g_times >= 3) {\n" +
                "window.clearInterval(timer);\n" +
                "}\n" +
                "g_times ++;\n" +
                "}\n" +
                "var timer = setInterval(myfun,500);\n" +
                "myfun();\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void bdssqgg(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "百度搜索去广告");
        values.put("url", "*");
        values.put("js","(function(){\n" +
                "(function(){\n" +
                "function remove(sel) {\n" +
                "  document.querySelectorAll(sel).forEach( a => a.remove());\n" +
                "}\n" +
                "var g_times = 0;\n" +
                "function myfun() {\n" +
                "function removeads() {\n" +
                "remove(\".ec_wise_ad\");\n" +
                "remove(\".se-recommend-word-list-container\");\n" +
                "remove(\"#se-recommend-word-list-container\");\n" +
                "remove('[class*=\"ball-wrapper\"]');\n" +
                "remove('[style=\"position: fixed; bottom: 0px; left: 0px; z-index: 300; width: 100%; height: 52px; background: rgb(255, 255, 255); opacity: 1; border-top: 1px solid rgb(224, 224, 224); display: flex;\"]');\n" +
                "remove('[ad_dot_url*=\"http\"]');\n" +
                "remove(\".dl-banner-without-logo\");\n" +
                "remove(\".ad_result\");\n" +
                "remove(\".ad_sc\");\n" +
                "remove('[data-text-ad=\"1\"]');\n" +
                "remove('#content_left > *:not([id]) *');\n" +
                "remove('[class=\"result c-container new-pmd\"][id=\"1\"][tpl=\"se_com_default\"][data-click=\"{\"]');\n" +
                "}\n" +
                "removeads();\n" +
                "window.setTimeout(removeads);\n" +
                " if(g_times >= 9999) {\n" +
                "   window.clearInterval(timer);\n" +
                " }\n" +
                " g_times ++;\n" +
                "}\n" +
                "var timer = setInterval(myfun,150);\n" +
                "myfun();\n" +
                "})();\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void vconsole(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "vconsole调试工具");
        values.put("url", "*");
        values.put("js","(function(){\n" +
                "const id = decodeURIComponent('132321');\n" +
                "\n" +
                "function hex2sri(hex) {\n" +
                "  let type,\n" +
                "    ar = [],\n" +
                "    i = 0;\n" +
                "  switch (hex.length) {\n" +
                "    case 64:\n" +
                "      type = 'sha256-';\n" +
                "      break;\n" +
                "    case 96:\n" +
                "      type = 'sha384-';\n" +
                "      break;\n" +
                "    case 128:\n" +
                "      type = 'sha512-';\n" +
                "      break;\n" +
                "    default:\n" +
                "      return false;\n" +
                "  }\n" +
                "  for (i = 0; i < hex.length; i = i + 2) {\n" +
                "    ar.push(parseInt(hex.slice(i, i + 2), 16));\n" +
                "  }\n" +
                "  return type + btoa(String.fromCharCode.apply(null, ar));\n" +
                "}\n" +
                "\n" +
                "function runOnce(fn, key) {\n" +
                "  const uniqId = 'BEXT_UNIQ_ID_' + id + (key ? key : '');\n" +
                "  if (window[uniqId]) {\n" +
                "    return;\n" +
                "  }\n" +
                "  window[uniqId] = true;\n" +
                "  fn && fn();\n" +
                "}\n" +
                "\n" +
                "function addElement({\n" +
                "  tag,\n" +
                "  attrs = {},\n" +
                "  to = document.body || document.documentElement,\n" +
                "}) {\n" +
                "  const el = document.createElement(tag);\n" +
                "  Object.assign(el, attrs);\n" +
                "  to.appendChild(el);\n" +
                "  return el;\n" +
                "}\n" +
                "\n" +
                "function loadScript(src, hash) {\n" +
                "  if (hash && hash.slice(0, 3) !== 'sha') hash = hex2sri(hash);\n" +
                "  return new Promise((resolve, reject) => {\n" +
                "    const el = addElement({\n" +
                "      tag: 'script',\n" +
                "      attrs: {\n" +
                "        src,\n" +
                "        type: 'text/javascript',\n" +
                "        integrity: hash ? hash : '',\n" +
                "        crossOrigin: 'anonymous',\n" +
                "        onload: () => resolve(el),\n" +
                "        onerror: reject,\n" +
                "      },\n" +
                "    });\n" +
                "  });\n" +
                "}\n" +
                "\n" +
                "runOnce(() => {\n" +
                "    loadScript('https://unpkg.com/vconsole@latest/dist/vconsole.min.js')\n" +
                "        .then(() => new window.VConsole());\n" +
                "});\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void hyms(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "网页绿色护眼模式模式");
        values.put("url", "*");
        values.put("js","! function() {\n" +
                "\tconst k = '(0, 0, 0,';\n" +
                "\tfunction hanlde(){\n" +
                "\t\tlet arr = document.getElementsByTagName(\"*\");\n" +
                "\t\tfor (var i = 0; i < arr.length; i++) {\n" +
                "\t\t\tlet item = arr[i];\n" +
                "\t\t\tlet computedStyle = document.defaultView.getComputedStyle(item, \"\");\n" +
                "\t\t\tif (item.tagName != 'IMG' && computedStyle.backgroundColor.indexOf(k) == -1 && computedStyle.background.indexOf(k) == -1) {\n" +
                "\t\t\t\titem.style.backgroundColor = '#C7EDCC';\n" +
                "\t\t\t} else {\n" +
                "\t\t\t\tconsole.log(computedStyle.backgroundColor);\n" +
                "\t\t\t}\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\thanlde();\n" +
                "\tvar clientHeight = document.body.clientHeight;\n" +
                "\tvar b = new window.MutationObserver(function(b) {\n" +
                "\t\tconsole.log(document.body.clientHeight);\n" +
                "\t\tif(document.body.clientHeight>clientHeight){\n" +
                "\t\t\thanlde()\n" +
                "\t\t}\n" +
                "\t});\n" +
                "\tb.observe(document, {\n" +
                "\t\tchildList: !0,\n" +
                "\t\tsubtree: !0\n" +
                "\t})\n" +
                "}()");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void xsyd(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "网页小说阅读模式");
        values.put("url", "*");
        values.put("js","(function(){\n" +
                "\n" +
                "function runAt(start, fn, ...args) {\n" +
                "  if (typeof fn !== 'function') return;\n" +
                "  switch (start) {\n" +
                "    case 'document-end':\n" +
                "      if (\n" +
                "        document.readyState === 'interactive' ||\n" +
                "        document.readyState === 'complete'\n" +
                "      ) {\n" +
                "        fn.call(this, ...args);\n" +
                "      } else {\n" +
                "        document.addEventListener('DOMContentLoaded', fn.bind(this, ...args));\n" +
                "      }\n" +
                "      break;\n" +
                "    case 'document-idle':\n" +
                "      if (document.readyState === 'complete') {\n" +
                "        fn.call(this, ...args);\n" +
                "      } else {\n" +
                "        window.addEventListener('load', fn.bind(this, ...args));\n" +
                "      }\n" +
                "      break;\n" +
                "    default:\n" +
                "      setTimeout(fn, start, ...args);\n" +
                "  }\n" +
                "}\n" +
                "\n" +
                "\n" +
                "runAt('document-idle',() => {\n" +
                "!function () {\n" +
                "    \n" +
                "    var zhdx = 20;\n" +
                "    var zhcz = 6;\n" +
                "    var zhbj = 4;\n" +
                "    var djkh = 1;\n" +
                "    var cszt = \"#e3edcd-#000\";\n" +
                "    var ztss = \"#FFCBE8-#C71585;#fce4ec-#880e4f;#CCE2BF-green;#e0f2f1-#004d40;#e1f5fe-#01579b;#494949-#C1C1C1;#1a1c23-#c6c7c8;#000000-#bbbbbb;#C7EDCC;#DCECD2;#f4f0e9;#fff\";\n" +
                "    var $ = function (e) { return document.querySelector(e) }, ydcss = \"display:none;text-align:center !important;font-size:20px;width:28px;height:28px;line-height:28px;text-align:center;float:right;position:fixed;right:10px;top:70%;color:#000;opacity:0.8;background:#e3edcd;cursor:pointer;position:fixed !important;z-index:9999999999 !important;box-shadow:0px 1px 1px #000;border-radius:50%;\";\n" +
                "    if (!$(\"#txtyd\")) {\n" +
                "        var ydan = document.createElement(\"span\");\n" +
                "        ydan.id = \"txtyd\";\n" +
                "        ydan.innerHTML = \"[\\u039e]\";\n" +
                "        ydan.style.cssText = ydcss;\n" +
                "        ydan.addEventListener(\"click\", function () { jryd(); });\n" +
                "        $(\"body\").appendChild(ydan);\n" +
                "    }\n" +
                "    var hdy1, hdy2; document.addEventListener(\"touchstart\", function (e) {\n" +
                "        hdy1 = e.changedTouches[0].clientY;\n" +
                "    });\n" +
                "    document.addEventListener(\"touchmove\", function (e) {\n" +
                "        hdy2 = e.changedTouches[0].clientY; $(\"#txtyd\").style.display = hdy2 - hdy1 > 0 ? \"block\" : \"none\";\n" +
                "    });\n" +
                "    function jryd() {\n" +
                "        var wybm = $(\"head\").innerHTML.match(/<meta.*charset.*?=.*?([^\"]+).*?>/i)[1], wyt = '<html><head><meta charset=\"' + wybm + '\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0\"><style type=\"text/css\">body{text-align:center;word-wrap:break-word;}#xsxs p{padding:0px ' + zhbj + 'px;text-align:justify;margin:0;text-indent:2em;}.wbt{font-weight:bold;}</style><title></title><script>var $=function(e){return document.querySelector(e)},dqurl=\"' + location.href + '\",ksqy=\"' + document.documentElement.clientHeight + '\",xsztys=\"' + ztss + '\",mrzt=\"' + cszt + '\",pbkh=\"' + djkh + '\";</script></head><body><div><iframe hidden></iframe></div><div id=\"xssj\" style=\"width:100%;height:1px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;\"></div><div id=\"xsxs\"></div><br><script>var nexturl=dqurl,nextt=\"\",xsnr,tcdzt,tztdx,tzthg,fysz,khwb;\"1\"==pbkh?khwb=\"</p><br><p>\":khwb=\"</p><p>\";function szztdx(e){$(\"body\").style.fontSize=e+\"px\";tztdx=e;tzthg=e*1+' + zhcz + ';fysz=ksqy-tzthg;$(\"body\").style.lineHeight=tzthg+\"px\"};szztdx(\"' + zhdx + '\");xstxt(dqurl);function xstxt(e){dqurl=e;var t=new XMLHttpRequest;t.open(\"get\",e,true),t.overrideMimeType(\"text/html;charset=' + wybm + '\"),t.onreadystatechange=function(){4==t.readyState&&200==t.status&&($(\"#xssj\").innerHTML=t.responseText.replace(/^\\\\s+|\\\\s+$/g,\"\").replace(/<!--.*?-->/g,\"\").replace(/>\\\\s+?</g,\"><\").replace(/<style[\\\\s\\\\S]*?<\\\\/style>/ig,\"\").replace(/<script[\\\\s\\\\S]*?<\\\\/script>/ig,\"\").replace(/<iframe[\\\\s\\\\S]*?<\\\\/iframe>/ig,\"\").match(/<body[\\\\s\\\\S]*<\\\\/body>/i),nextt=t.responseText.match(/<title>([\\\\s\\\\S]*)<\\\\/title>/i)[1],zltxt())},t.send()}function zltxt(){function e(e){e.style.display=\"none\"}function n(){for(var n=[\"div\",\"span\",\"p\"],t=0;t<n.length;t++){var r=s.querySelectorAll(n[t]);if(r.length>0)for(var o=0;o<r.length;o++)r[o].innerText.replace(/\\\\s+/g,\"\").length<8&&e(r[o])}}function t(n){var t=s.querySelectorAll(n);if(t.length>0)for(var r=0;r<t.length;r++)e(t[r])}function r(n){s.querySelector(n)&&e(s.querySelector(n))}function o(){xsnr=(\"\\\\n\\\\n\"+xsnr).replace(/\\\\r|\\\\n/g,\"\\\\n\\\\n\").replace(/\\\\n\\\\s+/g,khwb),window.screen.height>=document.documentElement.scrollHeight&&zstxt()}var s=$(\"#xssj\"),u=s.querySelectorAll(\"a\"),l=u.length,x=/\\u4e0b\\u4e00\\u9875|\\u4e0b\\u9875|\\u4e0b\\u8282|\\u4e0b\\u4e00\\u9801|\\u4e0b\\u9801|\\u4e0b\\u4e00\\u7ae0|\\u4e0b\\u7ae0/;if(l>0)for(var c=l-1;c>=0;c--){var i=u[c],a=i.innerText,f=i.href;if(x.test(a)&&-1==f.indexOf(\"#\")){nexturl=f;break}nexturl=\"\"}!function(){r(\"#foot\"),t(\"footer\"),r(\"#footer\"),r(\".footer\"),t(\"iframe\"),t(\"form\"),t(\"input\"),t(\"table\"),t(\"tbody\"),t(\"tr\"),t(\"td\"),t(\"ul\"),t(\"li\"),t(\"img\"),t(\"font\"),t(\"b\"),t(\"a\")}(),$(\"#xssj #chaptercontent\")?(r(\"#cambrian0\"),xsnr=$(\"#xssj #chaptercontent\").innerText,o()):$(\"#xssj #nr\")?(xsnr=$(\"#xssj #nr\").innerText,o()):$(\"#xssj #content\")?(xsnr=$(\"#xssj #content\").innerText,o()):$(\"#xssj .content\")?(xsnr=$(\"#xssj .content\").innerText,o()):$(\"#xssj #novelcontent\")?(xsnr=$(\"#xssj #novelcontent\").innerText,o()):(n(),n(),xsnr=s.innerText,o())}function zstxt(){dqurl!=location.href&&history.pushState(null,nextt,dqurl);document.title=nextt;var t=\"<br><br><div class=\\'wbt\\'>END</div><br>\";$(\"#xsxs\").innerHTML+=t.replace(/END/,nextt)+\"<p>\"+xsnr+\"</p>\",\"\"!=nexturl?setTimeout(\"xstxt(nexturl)\",2e3):($(\"#xsxs\").innerHTML+=t,dqurl=\"\")}document.addEventListener(\"scroll\",dddb);function dddb(){var e=document.documentElement.scrollTop||$(\"body\").scrollTop;window.screen.height<document.documentElement.scrollHeight&&e+2*window.screen.height>document.documentElement.scrollHeight&&dqurl!=nexturl&&zstxt()}</script><span id=\"txtcd\" style=\"top:80%;\">M</span><div id=\"szcsp\" style=\"color:rgb(0,0,0);font-size:24px;line-height:24px;opacity:1;background:rgb(255,255,255);cursor:pointer;position:fixed;bottom:5%;left:5%;right:5%;margin-top:auto;z-index:9999;border:1px solid rgb(197,197,197);border-radius:5px;-webkit-tap-highlight-color:rgba(0,0,0,0);display:none;\"><div id=\"szcsp2\" style=\"margin:8px;padding:8px;text-align:center;\"><p><span id=\"csztjx\">\\u3000A-\\u3000</span>\\u3000<span id=\"csztzd\">\\u3000A+\\u3000</span></p><p id=\"cszt\"></p><p><span onclick=\"location.reload();window.scrollTo(0,0);\">\\u9000\\u51fa\\u9605\\u8bfb\\u6a21\\u5f0f</span></p></div></div><style type=\"text/css\">#szcsp2 span{display:inline-block;margin:4px;padding:4px;border:1px solid #c5c5c5;border-radius:5px;}#szcsp2 p{margin:4px;}img{display:none!important;}#txtcd{' + ydcss + '}</style></body></html>'; var newy = window.open('', '_self'); newy.opener = null; newy.document.write(wyt); newy.document.close(); var cpfy = document.createElement(\"script\"); cpfy.innerHTML = '$(\"#xsxs\").addEventListener(\"click\",function(e){e.clientY<window.screen.availHeight/2?window.scrollBy(0,-fysz):(window.scrollBy(0,fysz),yctcd())});document.addEventListener(\"touchstart\",function(e){startY=e.changedTouches[0].clientY});document.addEventListener(\"touchmove\",function(e){endY=e.changedTouches[0].clientY,endY-startY>0?($(\"#txtcd\").style.display=\"block\",tcdzt=1):yctcd()});function yctcd(){\"1\"==tcdzt&&($(\"#txtcd\").style.display=\"none\",$(\"#szcsp\").style.display=\"none\",tcdzt=0)};$(\"#txtcd\").addEventListener(\"click\",function(){$(\"#szcsp\").style.display=\"block\";tcdzt=1}),$(\"#csztjx\").addEventListener(\"click\",function(){var t=tztdx-1;t>9&&szztdx(t)}),$(\"#csztzd\").addEventListener(\"click\",function(){var t=1*tztdx+1;t<41&&szztdx(t)});cjztan();szzt(mrzt);function cjztan(){for(var t=xsztys.split(\";\"),e=0;e<t.length;e++){var n=document.createElement(\"span\");n.innerHTML=\"\\u3000\",n.style.backgroundColor=t[e].split(\"-\")[0],n.setAttribute(\"ysz\",t[e]),n.onclick=function(){var t=this.getAttribute(\"ysz\");szzt(t)},$(\"#cszt\").appendChild(n)}}function szzt(o){var t=o.split(\"-\");$(\"body\").style.backgroundColor=t[0],void 0==t[1]?$(\"body\").style.color=\"#000\":$(\"body\").style.color=t[1]}';\n" +
                "        $(\"body\").appendChild(cpfy);\n" +
                "    }\n" +
                "}();\n" +
                "});\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void spbfq(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "网页视频播放器-启用之后支持手势快进倒退");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 视频播放器\n" +
                " * @Author: 就像wo\n" +
                " * @version: 4.1\n" +
                " * @description:为网页中的视频添加一个额外的播放器\n" +
                " * @include: *\n" +
                " * @createTime: 2019-12-22\n" +
                " * @updateTime: 2019-12-24\n" +
                "*/\n" +
                "(function(){\n" +
                "    var h5PlayerCode=\"KGZ1bmN0aW9uKCklN0J2YXIlMjBoNVBsYXllcktleT1lbmNvZGVVUklDb21wb25lbnQoJTIyJUU1JUIwJUIxJUU1JTgzJThGd286JUU4JUE3JTg2JUU5JUEyJTkxJUU2JTkyJUFEJUU2JTk0JUJFJUU1JTk5JUE4JTIyKTtpZih3aW5kb3cuaDVQbGF5ZXJLZXk9PXRydWUpJTdCcmV0dXJuJTIwMDslN0R3aW5kb3cuaDVQbGF5ZXJLZXk9dHJ1ZTt2YXIlMjBoNVBsYXllclNpemU9JTdCJTIycGgycGMlMjI6aW5uZXJXaWR0aC8zNjAlN0Q7dmFyJTIwaDVDb250YWluZXI9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudCglMjJESVYlMjIpO3ZhciUyMGg1Q29udHJvbHNCZ2Q9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudCglMjJESVYlMjIpO3ZhciUyMGg1Q29udHJvbHNUb3A7dmFyJTIwaDVCdWZmZXJCYXI9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudCglMjJDQU5WQVMlMjIpO3ZhciUyMGg1UHJvZ3Jlc3NCYXI7dmFyJTIwaDVWaWRlb1RpdGxlO3ZhciUyMGg1UGxheT1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkNBTlZBUyUyMik7dmFyJTIwaDVDb250cm9scz1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkRJViUyMik7dmFyJTIwaDVDdXJyZW50VGltZT1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkJVVFRPTiUyMik7dmFyJTIwaDVEdXJhdGlvbjt2YXIlMjBoNUV4dHJhVmlldzt2YXIlMjBoNUZ1bGxzY3JlZW49ZG9jdW1lbnQuY3JlYXRlRWxlbWVudCglMjJCVVRUT04lMjIpO3ZhciUyMGg1Vm9sdW1lSW5jO3ZhciUyMGg1Vm9sdW1lRGVjO2g1Q29udGFpbmVyLnN0eWxlPSUyMnBvc2l0aW9uOmFic29sdXRlO3otaW5kZXg6OTk5OTk5OTk5OTtib3gtc2l6aW5nOmJvcmRlci1ib3g7JTIyO2g1Q29udHJvbHNCZ2Quc3R5bGU9JTIycG9zaXRpb246YWJzb2x1dGU7bGVmdDowcHg7dG9wOjBweDt6LWluZGV4Ojk5OTk5OTk5OTk7Ym94LXNpemluZzppbmhlcml0O3dpZHRoOjEwMCUyNTtoZWlnaHQ6MTAwJTI1OyUyMjtoNUJ1ZmZlckJhci5zdHlsZT0lMjJwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjBweDt0b3A6MHB4O3otaW5kZXg6LTE7Ym94LXNpemluZzpib3JkZXItYm94O3dpZHRoOjEwMCUyNTtoZWlnaHQ6MTAwJTI1OyUyMjtoNUJ1ZmZlckJhci53aWR0aD0zNjA7aDVCdWZmZXJCYXIuaGVpZ2h0PTMwO2Z1bmN0aW9uJTIwaDVQbGF5ZXJHZXRCdWZmZXIob2JqKSU3QnZhciUyMGN0eD1vYmouYnVmZmVyQmFyLmdldENvbnRleHQoJTIyMmQlMjIpO2N0eC5jbGVhclJlY3QoMCwwLDM2MCwzMCk7dmFyJTIwcmF0aW89MzYwL29iai52aWRlby5kdXJhdGlvbjt2YXIlMjByYW5nZT1vYmoudmlkZW8uYnVmZmVyZWQ7Y3R4LmJlZ2luUGF0aCgpO2Zvcih2YXIlMjBpPTA7aSUzQ3JhbmdlLmxlbmd0aDtpKyspJTdCY3R4Lm1vdmVUbyhNYXRoLmZsb29yKHJhbmdlLnN0YXJ0KGkpKnJhdGlvKSwxNSk7Y3R4LmxpbmVUbyhNYXRoLmNlaWwocmFuZ2UuZW5kKGkpKnJhdGlvKSwxNSk7JTdEY3R4LnN0cm9rZSgpOyU3RGg1UHJvZ3Jlc3NCYXI9aDVCdWZmZXJCYXIuY2xvbmVOb2RlKCk7aDVQcm9ncmVzc0Jhci5zdHlsZS5iYWNrZ3JvdW5kQ29sb3I9JTIyI2ZmZmZmZjg4JTIyO2g1UHJvZ3Jlc3NCYXIuc3R5bGUuekluZGV4PSUyMjElMjI7ZnVuY3Rpb24lMjBoNVBsYXllckdldFByb2dyZXNzKG9iaix0aW1lKSU3QnZhciUyMGN0eD1vYmoucHJvZ3Jlc3NCYXIuZ2V0Q29udGV4dCglMjIyZCUyMik7Y3R4LmNsZWFyUmVjdCgwLDAsMzYwLDMwKTtjdHguYmVnaW5QYXRoKCk7Y3R4Lm1vdmVUbyh0aW1lLDApO2N0eC5saW5lVG8odGltZSwzMCk7Y3R4LnN0cm9rZSgpOyU3RGg1UGxheS5zdHlsZT0lMjJwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjQwJTI1OyUyMjtoNVBsYXkud2lkdGg9NjAwO2g1UGxheS5oZWlnaHQ9NjAwO2Z1bmN0aW9uJTIwaDVQbGF5ZXJHZXRQbGF5KGN0eCklN0JjdHguZmlsbFN0eWxlPSUyMmJsYWNrJTIyO3ZhciUyMHk9MzAwLU1hdGguc3FydCgzKSo1MDtjdHguYmVnaW5QYXRoKCk7Y3R4Lm1vdmVUbyg0MDAsMzAwKTtjdHgubGluZVRvKDI1MCx5KTtjdHgubGluZVRvKDI1MCw2MDAteSk7Y3R4LmNsb3NlUGF0aCgpO2N0eC5maWxsKCk7JTdEZnVuY3Rpb24lMjBoNVBsYXllckdldFBhdXNlKGN0eCklN0JjdHguZmlsbFN0eWxlPSUyMmJsYWNrJTIyO2N0eC5maWxsUmVjdCgyMjUsMjEwLDUwLDE4MCk7Y3R4LmZpbGxSZWN0KDMyNSwyMTAsNTAsMTgwKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyR2V0V2FpdGluZyhvYmopJTdCdmFyJTIwY3R4PW9iai5wbGF5LmdldENvbnRleHQoJTIyMmQlMjIpO2N0eC5yZXN0b3JlKCk7Y3R4LnJvdGF0ZShNYXRoLlBJLzI0KTtjdHguY2xlYXJSZWN0KC0zMDAsLTMwMCw2MDAsNjAwKTtjdHguYmVnaW5QYXRoKCk7Y3R4LmFyYygwLDAsMjUwLDAsTWF0aC5QSS8yKTtjdHguc3Ryb2tlKCk7Y3R4LnNhdmUoKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyUGxheUNsaWNrKG9iaiklN0J2YXIlMjBjdHg9b2JqLnBsYXkuZ2V0Q29udGV4dCglMjIyZCUyMik7Y3R4LmZpbGxTdHlsZT0lMjIjZmZmZmZmY2MlMjI7Y3R4LnJlc2V0VHJhbnNmb3JtKCk7Y3R4LmNsZWFyUmVjdCgwLDAsNjAwLDYwMCk7Y3R4LmJlZ2luUGF0aCgpO2N0eC5hcmMoMzAwLDMwMCwzMDAsMCwyKk1hdGguUEkpO2N0eC5maWxsKCk7aWYoIW9iai52aWRlby5wYXVzZWQpJTdCaDVQbGF5ZXJHZXRQYXVzZShjdHgpOyU3RGVsc2UlN0JoNVBsYXllckdldFBsYXkoY3R4KTslN0QlN0RoNUNvbnRyb2xzLnN0eWxlPSUyMnBvc2l0aW9uOmFic29sdXRlO2xlZnQ6MHB4O2JvdHRvbTowcHg7ei1pbmRleDo5OTk5OTk5OTk5O2JveC1zaXppbmc6aW5oZXJpdDt3aWR0aDoxMDAlMjU7cGFkZGluZzowcHglMjAyJTI1O21hcmdpbjowcHg7Y29sb3I6YmxhY2s7JTIyO2g1Q29udHJvbHNUb3A9aDVDb250cm9scy5jbG9uZU5vZGUoKTtoNUNvbnRyb2xzVG9wLnN0eWxlLnRvcD0lMjIwcHglMjI7aDVDb250cm9sc1RvcC5zdHlsZS5wYWRkaW5nPSUyMjBweCUyMjtoNUN1cnJlbnRUaW1lLnN0eWxlPSUyMnBvc2l0aW9uOnJlbGF0aXZlO2Zsb2F0OmxlZnQ7Ym94LXNpemluZzppbmhlcml0O2JveC1zaGFkb3c6MHB4JTIwMHB4JTIwMTBweCUyMCMwMDAwMDA7d2lkdGg6MTUlMjU7aGVpZ2h0OjEwMCUyNTtib3JkZXItc3R5bGU6bm9uZTttYXJnaW4tcmlnaHQ6MSUyNTtiYWNrZ3JvdW5kLWNvbG9yOiNmZmZmZmY7b3ZlcmZsb3c6YXV0bzt3aGl0ZS1zcGFjZTpub3dyYXA7dGV4dC1hbGlnbjpjZW50ZXI7Zm9udC13ZWlnaHQ6Ym9sZDslMjI7aDVWaWRlb1RpdGxlPWg1Q3VycmVudFRpbWUuY2xvbmVOb2RlKCk7aDVWaWRlb1RpdGxlLnN0eWxlLmJhY2tncm91bmRDb2xvcj0lMjIjZmZmZmZmMDAlMjI7aDVWaWRlb1RpdGxlLnN0eWxlLndpZHRoPSUyMjEwMCUyNSUyMjtoNVZpZGVvVGl0bGUuc3R5bGUuekluZGV4PSUyMjIlMjI7aDVEdXJhdGlvbj1oNUN1cnJlbnRUaW1lLmNsb25lTm9kZSgpO2g1RHVyYXRpb24uc3R5bGUuYmFja2dyb3VuZENvbG9yPSUyMiNjY2NjY2MlMjI7aDVEdXJhdGlvbi5zdHlsZS5ib3JkZXJTdHlsZT0lMjJvdXRzZXQlMjI7aDVFeHRyYVZpZXc9aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtoNUZ1bGxzY3JlZW4uc3R5bGU9JTIycG9zaXRpb246cmVsYXRpdmU7ZmxvYXQ6cmlnaHQ7Ym94LXNpemluZzppbmhlcml0O2JveC1zaGFkb3c6MHB4JTIwMHB4JTIwMTBweCUyMCMwMDAwMDA7d2lkdGg6MTUlMjU7aGVpZ2h0OjEwMCUyNTtib3JkZXItc3R5bGU6b3V0c2V0O21hcmdpbi1sZWZ0OjElMjU7YmFja2dyb3VuZC1jb2xvcjojY2NjY2NjO292ZXJmbG93OmF1dG87d2hpdGUtc3BhY2U6bm93cmFwO3RleHQtYWxpZ246Y2VudGVyO2ZvbnQtd2VpZ2h0OmJvbGQ7JTIyO2g1RnVsbHNjcmVlbi5pbm5lckhUTUw9JTIyJUU1JTg1JUE4JUU1JUIxJThGJTIyO2Z1bmN0aW9uJTIwaDVQbGF5ZXJGdWxsc2NyZWVuQ2xpY2soY29udGFpbmVyKSU3QmlmKGRvY3VtZW50LmZ1bGxzY3JlZW5FbGVtZW50PT1udWxsKSU3QmNvbnRhaW5lci5yZXF1ZXN0RnVsbHNjcmVlbigpOyU3RGVsc2UlN0Jkb2N1bWVudC5leGl0RnVsbHNjcmVlbigpOyU3RCU3RGg1Vm9sdW1lSW5jPWg1RnVsbHNjcmVlbi5jbG9uZU5vZGUoKTtoNVZvbHVtZUluYy5pbm5lckhUTUw9JTIyJUU5JTlGJUIzJUU5JTg3JThGKyUyMjtoNVZvbHVtZURlYz1oNUZ1bGxzY3JlZW4uY2xvbmVOb2RlKCk7aDVWb2x1bWVEZWMuaW5uZXJIVE1MPSUyMiVFOSU5RiVCMyVFOSU4NyU4Ri0lMjI7dmFyJTIwaDVQbGF5ZXJWaWRlb0Fycj0lNUIlNUQ7dmFyJTIwaDVQbGF5ZXJJZnJhbWVBcnI9JTVCJTVEO3ZhciUyMGg1UGxheWVyVGltZUlEPSU1QiU1RDtpZihkb2N1bWVudC5yZWFkeVN0YXRlPT0lMjJjb21wbGV0ZSUyMiklN0JoNVBsYXllcihkb2N1bWVudCwwKTslN0RlbHNlJTdCd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJTIybG9hZCUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXIoZG9jdW1lbnQsMCk7JTdEKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyQ2xhc3MoKSU3QnZhciUyMG9iaj1uZXclMjBPYmplY3Q7b2JqLmhhdmVDb250cm9scztvYmoucGFyZW50WkluZGV4O29iai5wYXJzZW50UG9zaXRpb247b2JqLmZvcndhcmRUaW1lPTA7b2JqLmZvcndhcmRWb2x1bWU9MTtvYmouZm9yd2FyZFJhdGU9MTtvYmoudmlkZW9SYXRpbz0wO29iai5zdGF0ZT0lMjJwYXVzZWQlMjI7b2JqLmhpZGRlblRpbWVJRDtvYmoud2FpdFRpbWVJRDtvYmoucGFyZW50O29iai52aWRlbztvYmouY29udGFpbmVyPWg1Q29udGFpbmVyLmNsb25lTm9kZSgpO29iai5jb250cm9sc0JnZD1oNUNvbnRyb2xzQmdkLmNsb25lTm9kZSgpO29iai5jb250cm9sc1RvcD1oNUNvbnRyb2xzVG9wLmNsb25lTm9kZSgpO29iai5idWZmZXJCYXI9aDVCdWZmZXJCYXIuY2xvbmVOb2RlKCk7b2JqLnByb2dyZXNzQmFyPWg1UHJvZ3Jlc3NCYXIuY2xvbmVOb2RlKCk7b2JqLnZpZGVvVGl0bGU9aDVWaWRlb1RpdGxlLmNsb25lTm9kZSgpO29iai5wbGF5PWg1UGxheS5jbG9uZU5vZGUoKTtvYmouY29udHJvbHM9aDVDb250cm9scy5jbG9uZU5vZGUoKTtvYmouY3VycmVudFRpbWU9aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtvYmouZHVyYXRpb249aDVEdXJhdGlvbi5jbG9uZU5vZGUoKTtvYmouZXh0cmFWaWV3PWg1RXh0cmFWaWV3LmNsb25lTm9kZSgpO29iai5mdWxsc2NyZWVuPWg1RnVsbHNjcmVlbi5jbG9uZU5vZGUodHJ1ZSk7b2JqLnZvbHVtZUluYz1oNVZvbHVtZUluYy5jbG9uZU5vZGUodHJ1ZSk7b2JqLnZvbHVtZURlYz1oNVZvbHVtZURlYy5jbG9uZU5vZGUodHJ1ZSk7b2JqLm9mZnNldFg9MDtvYmoub2Zmc2V0WT0wO29iai53aWR0aD0wO29iai5oZWlnaHQ9MTtvYmouY29udHJvbHNFbGVtZW50cz0lNUJvYmoudmlkZW9UaXRsZSxvYmouY3VycmVudFRpbWUsb2JqLmR1cmF0aW9uLG9iai5leHRyYVZpZXcsb2JqLmZ1bGxzY3JlZW4sb2JqLnZvbHVtZUluYyxvYmoudm9sdW1lRGVjJTVEO29iai5zaXplRnVuYz1mdW5jdGlvbigpJTdCb2JqLnBhcmVudFBvc2l0aW9uPXdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKG9iai5wYXJlbnQsbnVsbCkucG9zaXRpb247b2JqLnBhcmVudFpJbmRleD13aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShvYmoucGFyZW50LG51bGwpLnpJbmRleDtpZihvYmoucGFyZW50UG9zaXRpb249PSUyMnN0YXRpYyUyMiklN0JvYmoucGFyZW50LnN0eWxlLnBvc2l0aW9uPSUyMnJlbGF0aXZlJTIyOyU3RG9iai5wYXJlbnQuc3R5bGUuekluZGV4PSUyMjk5OTk5OTk5OTklMjI7aWYob2JqLnZpZGVvLnZpZGVvV2lkdGgqb2JqLnZpZGVvLnZpZGVvSGVpZ2h0PT0wKSU3Qm9iai52aWRlb1JhdGlvPW9iai52aWRlby5vZmZzZXRIZWlnaHQvb2JqLnZpZGVvLm9mZnNldFdpZHRoOyU3RGVsc2UlN0JvYmoudmlkZW9SYXRpbz1vYmoudmlkZW8udmlkZW9IZWlnaHQvb2JqLnZpZGVvLnZpZGVvV2lkdGg7JTdEaWYob2JqLnZpZGVvLm9mZnNldEhlaWdodC9vYmoudmlkZW8ub2Zmc2V0V2lkdGglM0NvYmoudmlkZW9SYXRpbyklN0JvYmouaGVpZ2h0PW9iai52aWRlby5vZmZzZXRIZWlnaHQ7b2JqLndpZHRoPW9iai5oZWlnaHQvb2JqLnZpZGVvUmF0aW87b2JqLmNvbnRhaW5lci5zdHlsZS50b3A9b2JqLnZpZGVvLm9mZnNldFRvcC9vYmoucGFyZW50LmNsaWVudEhlaWdodCoxMDArJTIyJTI1JTIyO29iai5jb250YWluZXIuc3R5bGUubGVmdD0ob2JqLnZpZGVvLm9mZnNldExlZnQrKG9iai52aWRlby5vZmZzZXRXaWR0aC1vYmoud2lkdGgpLzIpL29iai5wYXJlbnQuY2xpZW50V2lkdGgqMTAwKyUyMiUyNSUyMjslN0RlbHNlJTdCb2JqLndpZHRoPW9iai52aWRlby5vZmZzZXRXaWR0aDtvYmouaGVpZ2h0PW9iai52aWRlb1JhdGlvKm9iai53aWR0aDtvYmouY29udGFpbmVyLnN0eWxlLmxlZnQ9b2JqLnZpZGVvLm9mZnNldExlZnQvb2JqLnBhcmVudC5jbGllbnRXaWR0aCoxMDArJTIyJTI1JTIyO29iai5jb250YWluZXIuc3R5bGUudG9wPShvYmoudmlkZW8ub2Zmc2V0VG9wKyhvYmoudmlkZW8ub2Zmc2V0SGVpZ2h0LW9iai5oZWlnaHQpLzIpL29iai5wYXJlbnQuY2xpZW50SGVpZ2h0KjEwMCslMjIlMjUlMjI7JTdEb2JqLmNvbnRhaW5lci5zdHlsZS53aWR0aD1vYmoud2lkdGgvb2JqLnBhcmVudC5vZmZzZXRXaWR0aCoxMDArJTIyJTI1JTIyO29iai5jb250YWluZXIuc3R5bGUuaGVpZ2h0PW9iai5oZWlnaHQvb2JqLnBhcmVudC5vZmZzZXRIZWlnaHQqMTAwKyUyMiUyNSUyMjtmb3IodmFyJTIwZWxlPW9iai52aWRlbztlbGUhPW51bGw7ZWxlPWVsZS5vZmZzZXRQYXJlbnQpJTdCb2JqLm9mZnNldFgrPWVsZS5vZmZzZXRMZWZ0K2VsZS5jbGllbnRMZWZ0LWVsZS5zY3JvbGxMZWZ0O29iai5vZmZzZXRZKz1lbGUub2Zmc2V0VG9wK2VsZS5jbGllbnRUb3AtZWxlLnNjcm9sbFRvcDslN0RvYmoucmVzaXplRnVuYyhmYWxzZSk7JTdEO29iai5yZXNpemVGdW5jPWZ1bmN0aW9uKGJvb2wpJTdCaWYoYm9vbCklN0JvYmoud2lkdGg9b2JqLnZpZGVvLm9mZnNldFdpZHRoO29iai5oZWlnaHQ9b2JqLnZpZGVvLm9mZnNldEhlaWdodDslN0RvYmouY29udHJvbHNUb3Auc3R5bGUuaGVpZ2h0PW9iai53aWR0aC9vYmouaGVpZ2h0LzMqMjUrJTIyJTI1JTIyO29iai5idWZmZXJCYXIuc3R5bGUuYm9yZGVyUmFkaXVzPTI1LzYrJTIyJTI1LzUwJTI1JTIyO29iai5wcm9ncmVzc0Jhci5zdHlsZS5ib3JkZXJSYWRpdXM9MjUvNislMjIlMjUvNTAlMjUlMjI7b2JqLnZpZGVvVGl0bGUuc3R5bGUuYm9yZGVyUmFkaXVzPTI1LzYrJTIyJTI1LzUwJTI1JTIyO29iai5wbGF5LnN0eWxlLndpZHRoPSUyMjIwJTI1JTIyO29iai5wbGF5LnN0eWxlLmhlaWdodD1vYmoud2lkdGgvb2JqLmhlaWdodCoyMCslMjIlMjUlMjI7b2JqLnBsYXkuc3R5bGUudG9wPSg1MC1vYmoud2lkdGgvb2JqLmhlaWdodCoxMCkrJTIyJTI1JTIyO29iai5jb250cm9scy5zdHlsZS5oZWlnaHQ9b2JqLndpZHRoL29iai5oZWlnaHQvMyoyNSslMjIlMjUlMjI7aDVQbGF5ZXJTaXplLnBoMnBjPW9iai53aWR0aC8zNjA7Zm9yKHZhciUyMGk9MTtpJTNDb2JqLmNvbnRyb2xzRWxlbWVudHMubGVuZ3RoO2krKyklN0JvYmouY29udHJvbHNFbGVtZW50cyU1QmklNUQuc3R5bGUuYm9yZGVyUmFkaXVzPTI1MC85KyUyMiUyNS81MCUyNSUyMjslN0Rmb3IodmFyJTIwaT0wO2klM0NvYmouY29udHJvbHNFbGVtZW50cy5sZW5ndGg7aSsrKSU3Qm9iai5jb250cm9sc0VsZW1lbnRzJTVCaSU1RC5zdHlsZS5ib3JkZXJXaWR0aD1oNVBsYXllclNpemUucGgycGMqMyslMjJweCUyMjtvYmouY29udHJvbHNFbGVtZW50cyU1QmklNUQuc3R5bGUuZm9udFNpemU9b2JqLndpZHRoLzM2KyUyMnB4JTIyOyU3RCU3RDtvYmoudmFsdWVGdW5jPWZ1bmN0aW9uKCklN0JvYmoudmlkZW9UaXRsZS5pbm5lckhUTUw9KGRvY3VtZW50LnRpdGxlPT0lMjIlMjIlN0MlN0NoNVBsYXllclZpZGVvQXJyLmxlbmd0aCUzRTEpP29iai52aWRlby5zcmMucmVwbGFjZSgvKGh0dHAlN0NodHRwcyU3Q2Z0cCk6JTVDLyU1Qy8oKCU1QiU1Q3clNUMtJTVEKyU1Qy4pKyU1QmEteiU1RCslN0MoJTVDZCU3QjEsMyU3RCU1Qy4pJTdCMyU3RCU1Q2QlN0IxLDMlN0QpKDolNUNkJTdCMSw1JTdEKT8vLCUyMiUyMik6ZG9jdW1lbnQudGl0bGU7b2JqLmN1cnJlbnRUaW1lLmlubmVySFRNTD1oNVBsYXllckZvcm1hdFRpbWUob2JqLnZpZGVvLmN1cnJlbnRUaW1lKTtvYmouZHVyYXRpb24uaW5uZXJIVE1MPWlzRmluaXRlKG9iai52aWRlby5kdXJhdGlvbik/aDVQbGF5ZXJGb3JtYXRUaW1lKG9iai52aWRlby5kdXJhdGlvbik6JTIyTElWRSUyMjt2YXIlMjBjdHg9b2JqLmJ1ZmZlckJhci5nZXRDb250ZXh0KCUyMjJkJTIyKTtjdHguc3Ryb2tlU3R5bGU9JTIyIzAwZmZmZiUyMjtjdHgubGluZVdpZHRoPTMwO2N0eD1vYmoucHJvZ3Jlc3NCYXIuZ2V0Q29udGV4dCglMjIyZCUyMik7Y3R4LnN0cm9rZVN0eWxlPSUyMnJlZCUyMjtjdHgubGluZVdpZHRoPTE7Y3R4PW9iai5wbGF5LmdldENvbnRleHQoJTIyMmQlMjIpO2N0eC50cmFuc2xhdGUoMzAwLDMwMCk7dmFyJTIwZ3JhZGllbnQ9Y3R4LmNyZWF0ZUxpbmVhckdyYWRpZW50KDAsMCwwLDMwMCk7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDAsJTIyd2hpdGUlMjIpO2dyYWRpZW50LmFkZENvbG9yU3RvcCgwLjEsJTIycmVkJTIyKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMC4yLCUyMm9yYW5nZSUyMik7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDAuMywlMjJ5ZWxsb3clMjIpO2dyYWRpZW50LmFkZENvbG9yU3RvcCgwLjQsJTIyZ3JlZW4lMjIpO2dyYWRpZW50LmFkZENvbG9yU3RvcCgwLjYsJTIyY3lhbiUyMik7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDAuNywlMjJibHVlJTIyKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMC45LCUyMnB1cnBsZSUyMik7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDEsJTIyYmxhY2slMjIpO2N0eC5maWxsU3R5bGU9JTIyI2ZmZmZmZmNjJTIyO2N0eC5zdHJva2VTdHlsZT1ncmFkaWVudDtjdHgubGluZVdpZHRoPTEwMDtjdHgubGluZUNhcD0lMjJyb3VuZCUyMjtjdHguc2F2ZSgpOyU3RDtvYmouZml0VG9nZXRoZXI9ZnVuY3Rpb24oKSU3Qm9iai5zaXplRnVuYygpO29iai52YWx1ZUZ1bmMoKTtvYmoucGFyZW50Lmluc2VydEJlZm9yZShvYmouY29udGFpbmVyLG9iai52aWRlbyk7b2JqLmNvbnRhaW5lci5hcHBlbmRDaGlsZChvYmoudmlkZW8pO29iai5jb250cm9sc1RvcC5hcHBlbmRDaGlsZChvYmouYnVmZmVyQmFyKTtvYmouY29udHJvbHNUb3AuYXBwZW5kQ2hpbGQob2JqLnByb2dyZXNzQmFyKTtvYmouY29udHJvbHNUb3AuYXBwZW5kQ2hpbGQob2JqLnZpZGVvVGl0bGUpO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmouY3VycmVudFRpbWUpO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmouZHVyYXRpb24pO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmouZXh0cmFWaWV3KTtvYmouY29udHJvbHMuYXBwZW5kQ2hpbGQob2JqLmZ1bGxzY3JlZW4pO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmoudm9sdW1lSW5jKTtvYmouY29udHJvbHMuYXBwZW5kQ2hpbGQob2JqLnZvbHVtZURlYyk7b2JqLmNvbnRyb2xzQmdkLmFwcGVuZENoaWxkKG9iai5jb250cm9sc1RvcCk7b2JqLmNvbnRyb2xzQmdkLmFwcGVuZENoaWxkKG9iai5wbGF5KTtvYmouY29udHJvbHNCZ2QuYXBwZW5kQ2hpbGQob2JqLmNvbnRyb2xzKTtvYmouY29udGFpbmVyLmFwcGVuZENoaWxkKG9iai5jb250cm9sc0JnZCk7aDVQbGF5ZXJQbGF5Q2xpY2sob2JqKTslN0Q7cmV0dXJuJTIwb2JqOyU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJSZW1vdmVDb250cm9scyhvYmosYm9vbCklN0JjbGVhclRpbWVvdXQob2JqLmhpZGRlblRpbWVJRCk7aWYoYm9vbCklN0JvYmouY29udHJvbHNCZ2QucmVtb3ZlQ2hpbGQob2JqLmNvbnRyb2xzVG9wKTtvYmouY29udHJvbHNCZ2QucmVtb3ZlQ2hpbGQob2JqLnBsYXkpO29iai5jb250cm9sc0JnZC5yZW1vdmVDaGlsZChvYmouY29udHJvbHMpOyU3RGVsc2UlN0JvYmouY29udHJvbHNCZ2QuYXBwZW5kQ2hpbGQob2JqLmNvbnRyb2xzVG9wKTtvYmouY29udHJvbHNCZ2QuYXBwZW5kQ2hpbGQob2JqLnBsYXkpO29iai5jb250cm9sc0JnZC5hcHBlbmRDaGlsZChvYmouY29udHJvbHMpOyU3RCU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJIaWRkZW4ob2JqKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2g1UGxheWVyUmVtb3ZlQ29udHJvbHMob2JqLGZhbHNlKTtvYmouaGlkZGVuVGltZUlEPXNldFRpbWVvdXQoZnVuY3Rpb24oKSU3Qmg1UGxheWVyUmVtb3ZlQ29udHJvbHMob2JqLHRydWUpOyU3RCwxMDAwMCk7JTdEdmFyJTIwaDVQbGF5ZXJTaGFrZT0lN0IlMjJjYW5hYmxlJTIyOnRydWUsJTIyZnVuYyUyMjpmdW5jdGlvbigpJTdCaWYoaDVQbGF5ZXJTaGFrZS5jYW5hYmxlKSU3QmlmKGV2ZW50LmFjY2VsZXJhdGlvbi54JTNFMzAlN0MlN0NldmVudC5hY2NlbGVyYXRpb24ueSUzRTMwJTdDJTdDZXZlbnQuYWNjZWxlcmF0aW9uLnolM0UzMCklN0JoNVBsYXllclNoYWtlLmNhbmFibGU9ZmFsc2U7c2V0VGltZW91dChmdW5jdGlvbigpJTdCaDVQbGF5ZXJTaGFrZS5jYW5hYmxlPXRydWU7JTdELDEwMDApO2lmKGg1UGxheWVyVmlkZW9BcnIubGVuZ3RoJTNFMCU3QyU3Q2g1UGxheWVyVGltZUlELmxlbmd0aCUzRTApJTdCaDVQbGF5ZXJSZW1vdmUoKTslN0RlbHNlJTdCaDVQbGF5ZXIoZG9jdW1lbnQsMCk7JTdEJTdEJTdEJTdEJTdEO2Z1bmN0aW9uJTIwaDVQbGF5ZXJSZW1vdmUoKSU3QmZvcih2YXIlMjBpPTA7aSUzQ2g1UGxheWVyVGltZUlELmxlbmd0aDtpKyspJTdCY2xlYXJUaW1lb3V0KGg1UGxheWVyVGltZUlEJTVCaSU1RCk7JTdEZm9yKHZhciUyMGk9MDtpJTNDaDVQbGF5ZXJWaWRlb0Fyci5sZW5ndGg7aSsrKSU3QnZhciUyMG9iaj1oNVBsYXllclZpZGVvQXJyJTVCaSU1RDtvYmoucGFyZW50Lmluc2VydEJlZm9yZShvYmoudmlkZW8sb2JqLmNvbnRhaW5lcik7b2JqLnBhcmVudC5yZW1vdmVDaGlsZChvYmouY29udGFpbmVyKTtvYmoucGFyZW50LnN0eWxlLnBvc2l0aW9uPW9iai5wYXJlbnRQb3NpdGlvbjtvYmoucGFyZW50LnN0eWxlLnpJbmRleD1vYmoucGFyZW50WkluZGV4O29iai52aWRlby5jb250cm9scz1vYmouaGF2ZUNvbnRyb2xzOyU3RGg1UGxheWVyVmlkZW9BcnI9JTVCJTVEO2g1UGxheWVySWZyYW1lQXJyPSU1QiU1RDtoNVBsYXllclRpbWVJRD0lNUIlNUQ7JTdEdmFyJTIwaDVQbGF5ZXJSZXNpemU9JTdCJTIydGltZXIlMjI6MCwlMjJmdW5jJTIyOmZ1bmN0aW9uKCklN0JjbGVhclRpbWVvdXQodGhpcy50aW1lcik7dGhpcy50aW1lcj1zZXRUaW1lb3V0KGZ1bmN0aW9uKCklN0Jmb3IodmFyJTIwaT0wO2klM0NoNVBsYXllclZpZGVvQXJyLmxlbmd0aDtpKyspJTdCaDVQbGF5ZXJWaWRlb0FyciU1QmklNUQucmVzaXplRnVuYyh0cnVlKTslN0QlN0QsMzAwKTslN0QlN0Q7d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJTIyZGV2aWNlbW90aW9uJTIyLGg1UGxheWVyU2hha2UuZnVuYyk7d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJTIycmVzaXplJTIyLGg1UGxheWVyUmVzaXplLmZ1bmMpO2Z1bmN0aW9uJTIwaDVQbGF5ZXIoZG9jLGRlcHRoKSU3QnZhciUyMHZpZGVvcz1kb2MuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJTIyVklERU8lMjIpO3ZhciUyMGlmcmFzPWRvYy5nZXRFbGVtZW50c0J5VGFnTmFtZSglMjJJRlJBTUUlMjIpO3ZhciUyMGhhdmVIaWRkZW49dmlkZW9zLmxlbmd0aCUzRTA/ZmFsc2U6dHJ1ZTtmb3IodmFyJTIwaT0wO2klM0N2aWRlb3MubGVuZ3RoO2krKyklN0J2YXIlMjBib29sPXRydWU7Zm9yKHZhciUyMGo9MDtqJTNDaDVQbGF5ZXJWaWRlb0Fyci5sZW5ndGg7aisrKSU3QmlmKHZpZGVvcyU1QmklNUQuaXNTYW1lTm9kZShoNVBsYXllclZpZGVvQXJyJTVCaiU1RC52aWRlbykpJTdCYm9vbD1mYWxzZTticmVhazslN0QlN0RpZihib29sKSU3QmlmKCFpc0g1UGxheWVySGlkZGVuKHZpZGVvcyU1QmklNUQpJiZ2aWRlb3MlNUJpJTVELnJlYWR5U3RhdGUlM0UwKSU3QmFkZEg1UGxheWVyKHZpZGVvcyU1QmklNUQpOyU3RGVsc2UlN0JoYXZlSGlkZGVuPXRydWU7JTdEJTdEJTdEZm9yKHZhciUyMGk9MDtpJTNDaWZyYXMubGVuZ3RoO2krKyklN0J2YXIlMjBib29sPXRydWU7Zm9yKHZhciUyMGo9MDtqJTNDaDVQbGF5ZXJJZnJhbWVBcnIubGVuZ3RoO2orKyklN0JpZihpZnJhcyU1QmklNUQuaXNTYW1lTm9kZShoNVBsYXllcklmcmFtZUFyciU1QmolNUQpKSU3QmJvb2w9ZmFsc2U7YnJlYWs7JTdEJTdEaWYoYm9vbCklN0JpZighaXNINVBsYXllckhpZGRlbihpZnJhcyU1QmklNUQpKSU3QmlmcmFEb2M9aWZyYXMlNUJpJTVELmNvbnRlbnREb2N1bWVudDtpZihpZnJhRG9jIT1udWxsKSU3Qmg1UGxheWVySWZyYW1lQXJyJTVCaDVQbGF5ZXJJZnJhbWVBcnIubGVuZ3RoJTVEPWlmcmFzJTVCaSU1RDtpZihpZnJhRG9jLnJlYWR5U3RhdGU9PSUyMmNvbXBsZXRlJTIyKSU3Qmg1UGxheWVyKGlmcmFEb2MsZGVwdGgrMSk7JTdEZWxzZSU3QmlmcmFzJTVCaSU1RC5hZGRFdmVudExpc3RlbmVyKCUyMmxvYWQlMjIsZnVuY3Rpb24oKSU3Qmg1UGxheWVyKGlmcmFEb2MsZGVwdGgrMSk7JTdEKTslN0QlN0QlN0RlbHNlJTdCaGF2ZUhpZGRlbj10cnVlOyU3RCU3RCU3RGlmKGhhdmVIaWRkZW4pJTdCaDVQbGF5ZXJUaW1lSUQlNUJkZXB0aCU1RD1zZXRUaW1lb3V0KGg1UGxheWVyLDEwMDAsZG9jLGRlcHRoKTslN0QlN0RmdW5jdGlvbiUyMGFkZEg1UGxheWVyKHZpZGVvKSU3QnZhciUyMG9iaj1oNVBsYXllckNsYXNzKCk7b2JqLmhhdmVDb250cm9scz12aWRlby5jb250cm9sczt2aWRlby5jb250cm9scz1mYWxzZTtvYmoudmlkZW89dmlkZW87b2JqLnBhcmVudD12aWRlby5wYXJlbnROb2RlO29iai5maXRUb2dldGhlcigpO2g1UGxheWVyVmlkZW9BcnIlNUJoNVBsYXllclZpZGVvQXJyLmxlbmd0aCU1RD1vYmo7b2JqLmNvbnRyb2xzQmdkLmFkZEV2ZW50TGlzdGVuZXIoJTIyY2xpY2slMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2lmKG9iai52aWRlby5wYXVzZWQpJTdCb2JqLnZpZGVvLnBsYXkoKTslN0RlbHNlJTdCaWYob2JqLmNvbnRyb2xzQmdkLmNvbXBhcmVEb2N1bWVudFBvc2l0aW9uKG9iai5wbGF5KSUyNTI9PTApJTdCaDVQbGF5ZXJSZW1vdmVDb250cm9scyhvYmosdHJ1ZSk7JTdEZWxzZSU3Qmg1UGxheWVySGlkZGVuKG9iaik7JTdEJTdEJTdEKTtvYmouY29udHJvbHNCZ2QuYWRkRXZlbnRMaXN0ZW5lciglMjJkYmxjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7aDVQbGF5ZXJIaWRkZW4ob2JqKTtoNVBsYXllckZ1bGxzY3JlZW5DbGljayhvYmouY29udGFpbmVyKTslN0QpO29iai5jb250cm9sc0JnZC5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoc3RhcnQlMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2g1UGxheWVyU2xpZGVTdGFydCgpOyU3RCk7b2JqLmNvbnRyb2xzQmdkLmFkZEV2ZW50TGlzdGVuZXIoJTIydG91Y2htb3ZlJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9sc0JnZC5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoZW5kJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTtoNVBsYXllclNsaWRlRW5kKG9iaik7JTdEKTtvYmouY29udHJvbHNUb3AuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7JTdEKTtvYmouY29udHJvbHNUb3AuYWRkRXZlbnRMaXN0ZW5lciglMjJkYmxjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7JTdEKTtvYmouY29udHJvbHNUb3AuYWRkRXZlbnRMaXN0ZW5lciglMjJ0b3VjaHN0YXJ0JTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTtjbGVhclRpbWVvdXQob2JqLmhpZGRlblRpbWVJRCk7JTdEKTtvYmouY29udHJvbHNUb3AuYWRkRXZlbnRMaXN0ZW5lciglMjJ0b3VjaGVuZCUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXJIaWRkZW4ob2JqKTslN0QpO29iai52aWRlb1RpdGxlLmFkZEV2ZW50TGlzdGVuZXIoJTIyY2xpY2slMjIsZnVuY3Rpb24oKSU3QmlmKGlzRmluaXRlKG9iai52aWRlby5kdXJhdGlvbikpJTdCb2JqLnZpZGVvLmN1cnJlbnRUaW1lPWV2ZW50Lm9mZnNldFgvdGhpcy5vZmZzZXRXaWR0aCpvYmoudmlkZW8uZHVyYXRpb247JTdEJTdEKTtvYmoucGxheS5hZGRFdmVudExpc3RlbmVyKCUyMmNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTtpZihvYmoudmlkZW8ucGF1c2VkKSU3Qm9iai52aWRlby5wbGF5KCk7JTdEZWxzZSU3Qm9iai52aWRlby5wYXVzZSgpOyU3RCU3RCk7b2JqLnBsYXkuYWRkRXZlbnRMaXN0ZW5lciglMjJkYmxjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7JTdEKTtvYmouY29udHJvbHMuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7JTdEKTtvYmouY29udHJvbHMuYWRkRXZlbnRMaXN0ZW5lciglMjJkYmxjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7JTdEKTtvYmouY29udHJvbHMuYWRkRXZlbnRMaXN0ZW5lciglMjJ0b3VjaHN0YXJ0JTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTtjbGVhclRpbWVvdXQob2JqLmhpZGRlblRpbWVJRCk7aWYoaXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKSklN0JoNVBsYXllclNsaWRlU3RhcnQoKTslN0QlN0QpO29iai5jb250cm9scy5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNobW92ZSUyMixmdW5jdGlvbigpJTdCaWYoaXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKSklN0JvYmoudmlkZW8ucGF1c2UoKTtoNVBsYXllclNsaWRlQ29udHJvbHNNb3ZlKG9iaik7JTdEJTdEKTtvYmouY29udHJvbHMuYWRkRXZlbnRMaXN0ZW5lciglMjJ0b3VjaGVuZCUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXJIaWRkZW4ob2JqKTtpZihpc0Zpbml0ZShvYmoudmlkZW8uZHVyYXRpb24pKSU3Qmg1UGxheWVyU2xpZGVDb250cm9sc0VuZChvYmopOyU3RCU3RCk7b2JqLmR1cmF0aW9uLmFkZEV2ZW50TGlzdGVuZXIoJTIyY2xpY2slMjIsZnVuY3Rpb24oKSU3QnZhciUyMGFkZFRpbWU9b2JqLnZpZGVvLmN1cnJlbnRUaW1lKyhvYmoudmlkZW8uZHVyYXRpb24lM0M2MT8xOm9iai52aWRlby5kdXJhdGlvbi8xMCk7b2JqLnZpZGVvLmN1cnJlbnRUaW1lPShhZGRUaW1lJTNDb2JqLnZpZGVvLmR1cmF0aW9uP2FkZFRpbWU6b2JqLnZpZGVvLmR1cmF0aW9uLTEpOyU3RCk7b2JqLmZ1bGxzY3JlZW4uYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXJGdWxsc2NyZWVuQ2xpY2sob2JqLmNvbnRhaW5lcik7JTdEKTtkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCUyMmZ1bGxzY3JlZW5jaGFuZ2UlMjIsZnVuY3Rpb24oKSU3QmlmKGRvY3VtZW50LmZ1bGxzY3JlZW5FbGVtZW50PT1udWxsKSU3Qm9iai5mdWxsc2NyZWVuLmlubmVySFRNTD0lMjIlRTUlODUlQTglRTUlQjElOEYlMjI7JTdEZWxzZSUyMGlmKG9iai5jb250YWluZXIuaXNTYW1lTm9kZShkb2N1bWVudC5mdWxsc2NyZWVuRWxlbWVudCkpJTdCb2JqLmZ1bGxzY3JlZW4uaW5uZXJIVE1MPSUyMiVFOSU4MCU4MCVFNSU4NyVCQSUyMjslN0QlN0QpO29iai52b2x1bWVJbmMuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCb2JqLnZpZGVvLnZvbHVtZT0ob2JqLnZpZGVvLnZvbHVtZSUzQzE/cGFyc2VJbnQob2JqLnZpZGVvLnZvbHVtZSoxMCsxKS8xMDoxKTslN0QpO29iai52b2x1bWVEZWMuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCb2JqLnZpZGVvLnZvbHVtZT0ob2JqLnZpZGVvLnZvbHVtZSUzRTA/TWF0aC5jZWlsKG9iai52aWRlby52b2x1bWUqMTAtMSkvMTA6MCk7JTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJjYW5wbGF5JTIyLGZ1bmN0aW9uKCklN0JpZih0aGlzLnBhdXNlZCYmKG9iai5zdGF0ZT09JTIyc2Vla2luZyUyMiU3QyU3Q29iai5zdGF0ZT09JTIyd2FpdGluZyUyMikpJTdCdGhpcy5wbGF5KCk7JTdEJTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJkdXJhdGlvbmNoYW5nZSUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXJIaWRkZW4ob2JqKTtvYmouZHVyYXRpb24uaW5uZXJIVE1MPWlzRmluaXRlKHRoaXMuZHVyYXRpb24pP2g1UGxheWVyRm9ybWF0VGltZSh0aGlzLmR1cmF0aW9uKTolMjJMSVZFJTIyOyU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIydGltZXVwZGF0ZSUyMixmdW5jdGlvbigpJTdCb2JqLmZvcndhcmRUaW1lPXRoaXMuY3VycmVudFRpbWU7b2JqLmN1cnJlbnRUaW1lLmlubmVySFRNTD1oNVBsYXllckZvcm1hdFRpbWUodGhpcy5jdXJyZW50VGltZSk7aWYoaXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKSklN0JoNVBsYXllckdldFByb2dyZXNzKG9iaixNYXRoLmZsb29yKDM2MCpvYmoudmlkZW8uY3VycmVudFRpbWUvb2JqLnZpZGVvLmR1cmF0aW9uKSswLjUpOyU3RCU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIydm9sdW1lY2hhbmdlJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopO29iai5mb3J3YXJkVm9sdW1lPW9iai52aWRlby52b2x1bWU7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU5JTlGJUIzJUU5JTg3JThGOiUyMitwYXJzZUludCh0aGlzLnZvbHVtZSoxMDApKyUyMiUyNSUyMjslN0QpO29iai52aWRlby5hZGRFdmVudExpc3RlbmVyKCUyMnJhdGVjaGFuZ2UlMjIsZnVuY3Rpb24oKSU3Qmg1UGxheWVySGlkZGVuKG9iaik7b2JqLmZvcndhcmRSYXRlPW9iai52aWRlby5wbGF5YmFja1JhdGU7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU1JTgwJThEJUU5JTgwJTlGOiUyMitwYXJzZUludCh0aGlzLnBsYXliYWNrUmF0ZSoxMDApKyUyMiUyNSUyMjslN0QpO29iai52aWRlby5hZGRFdmVudExpc3RlbmVyKCUyMnByb2dyZXNzJTIyLGZ1bmN0aW9uKCklN0JpZihpc0Zpbml0ZShvYmoudmlkZW8uZHVyYXRpb24pKSU3Qmg1UGxheWVyR2V0QnVmZmVyKG9iaik7JTdEJTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJzZWVraW5nJTIyLGZ1bmN0aW9uKCklN0JvYmouc3RhdGU9JTIyc2Vla2luZyUyMjt2YXIlMjBzdWJUaW1lPW9iai52aWRlby5jdXJyZW50VGltZS1vYmouZm9yd2FyZFRpbWU7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9KHN1YlRpbWUlM0UwPyUyMiVFNSVCRiVBQiVFOCVCRiU5QjolMjI6JTIyJUU1JUJGJUFCJUU5JTgwJTgwOiUyMikrTWF0aC5hYnMocGFyc2VJbnQoc3ViVGltZSkpKyUyMnMlMjI7JTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJ3YWl0aW5nJTIyLGZ1bmN0aW9uKCklN0JpZihvYmouc3RhdGUhPSUyMnNlZWtpbmclMjIpJTdCb2JqLnN0YXRlPSUyMndhaXRpbmclMjI7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU1JThBJUEwJUU4JUJEJUJEJUU0JUI4JUFELi4uJTIyOyU3RG9iai52aWRlby5wYXVzZSgpOyU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIyZW5kZWQlMjIsZnVuY3Rpb24oKSU3Qm9iai52aWRlby5wYXVzZSgpOyU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIycGF1c2UlMjIsZnVuY3Rpb24oKSU3Qmg1UGxheWVyUmVtb3ZlQ29udHJvbHMob2JqLGZhbHNlKTtpZihvYmouc3RhdGUhPSUyMndhaXRpbmclMjImJm9iai5zdGF0ZSE9JTIyc2Vla2luZyUyMiklN0JvYmouc3RhdGU9JTIycGF1c2VkJTIyO2g1UGxheWVyUGxheUNsaWNrKG9iaik7JTdEZWxzZSU3QmNsZWFyVGltZW91dChvYmoud2FpdFRpbWVJRCk7b2JqLndhaXRUaW1lSUQ9c2V0SW50ZXJ2YWwoaDVQbGF5ZXJHZXRXYWl0aW5nLDQwLG9iaik7JTdEJTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJwbGF5aW5nJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopO2lmKG9iai5zdGF0ZT09JTIyd2FpdGluZyUyMiU3QyU3Q29iai5zdGF0ZT09JTIyc2Vla2luZyUyMiklN0JpZihvYmouc3RhdGU9PSUyMndhaXRpbmclMjIpJTdCb2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJTIyOyU3RGNsZWFyVGltZW91dChvYmoud2FpdFRpbWVJRCk7JTdEb2JqLnN0YXRlPSUyMnBsYXlpbmclMjI7aDVQbGF5ZXJQbGF5Q2xpY2sob2JqKTslN0QpOyU3RHZhciUyMGg1UGxheWVyU2xpZGVTdGFydFg7dmFyJTIwaDVQbGF5ZXJTbGlkZVN0YXJ0WTtmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVTdGFydCgpJTdCaDVQbGF5ZXJTbGlkZVN0YXJ0WD1ldmVudC50YXJnZXRUb3VjaGVzJTVCMCU1RC5jbGllbnRYO2g1UGxheWVyU2xpZGVTdGFydFk9ZXZlbnQudGFyZ2V0VG91Y2hlcyU1QjAlNUQuY2xpZW50WTslN0RmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVDb250cm9sc01vdmUob2JqKSU3QnZhciUyMHNsaWRlTW92ZVg9ZXZlbnQudGFyZ2V0VG91Y2hlcyU1QjAlNUQuY2xpZW50WDt2YXIlMjBkaXN0YW5jZT1zbGlkZU1vdmVYLWg1UGxheWVyU2xpZGVTdGFydFg7dmFyJTIwYWRkVGltZT1kaXN0YW5jZS9vYmouY29udHJvbHMub2Zmc2V0V2lkdGgrb2JqLnZpZGVvLmN1cnJlbnRUaW1lL29iai52aWRlby5kdXJhdGlvbjthZGRUaW1lPWFkZFRpbWU9KGFkZFRpbWUlM0MwJTdDJTdDYWRkVGltZSUzRTEpPyhhZGRUaW1lJTNDMD8wOjAuOTk4KTphZGRUaW1lO2g1UGxheWVyR2V0UHJvZ3Jlc3Mob2JqLE1hdGguZmxvb3IoMzYwKihhZGRUaW1lKSkrMC41KTslN0RmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVDb250cm9sc0VuZChvYmopJTdCdmFyJTIwc2xpZGVFbmRYPWV2ZW50LmNoYW5nZWRUb3VjaGVzJTVCMCU1RC5jbGllbnRYO3ZhciUyMGRpc3RhbmNlPXNsaWRlRW5kWC1oNVBsYXllclNsaWRlU3RhcnRYO2lmKCFkaXN0YW5jZSUzRTApJTdCcmV0dXJuJTIwMDslN0R2YXIlMjBhZGRUaW1lPW9iai52aWRlby5jdXJyZW50VGltZStkaXN0YW5jZS9vYmouY29udHJvbHMub2Zmc2V0V2lkdGgqb2JqLnZpZGVvLmR1cmF0aW9uO2FkZFRpbWU9KGFkZFRpbWUlM0MwJTdDJTdDYWRkVGltZSUzRW9iai52aWRlby5kdXJhdGlvbik/KGFkZFRpbWUlM0MwPzA6b2JqLnZpZGVvLmR1cmF0aW9uLTEpOmFkZFRpbWU7b2JqLnZpZGVvLnBsYXkoKTtvYmoudmlkZW8uY3VycmVudFRpbWU9YWRkVGltZTslN0RmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVFbmQob2JqKSU3QnZhciUyMHNsaWRlRW5kWD1ldmVudC5jaGFuZ2VkVG91Y2hlcyU1QjAlNUQuY2xpZW50WDt2YXIlMjBzbGlkZUVuZFk9ZXZlbnQuY2hhbmdlZFRvdWNoZXMlNUIwJTVELmNsaWVudFk7dmFyJTIwZGlzdGFuY2VYPXNsaWRlRW5kWC1oNVBsYXllclNsaWRlU3RhcnRYO3ZhciUyMGRpc3RhbmNlWT1zbGlkZUVuZFktaDVQbGF5ZXJTbGlkZVN0YXJ0WTt2YXIlMjBwb3NYPWg1UGxheWVyU2xpZGVTdGFydFgtb2JqLm9mZnNldFg7dmFyJTIwZGlzdGFuY2U9TWF0aC5zcXJ0KE1hdGgucG93KGRpc3RhbmNlWCwyKStNYXRoLnBvdyhkaXN0YW5jZVksMikpO3ZhciUyMHBhcm09TWF0aC5hYnMoZGlzdGFuY2VZL2Rpc3RhbmNlWCk7aWYoZGlzdGFuY2U9PTApJTdCcmV0dXJuJTIwMDslN0RpZihwYXJtJTNDMC4zNiklN0J2YXIlMjBkaXN0YW5jZT1kaXN0YW5jZS9vYmoud2lkdGg7ZGlzdGFuY2U9ZGlzdGFuY2VYJTNFMD9kaXN0YW5jZTotMSpkaXN0YW5jZTtoNVBsYXllclByb2dyZXNzQ29udHJvbChvYmosZGlzdGFuY2UpOyU3RGVsc2UlMjBpZihkaXN0YW5jZVg9PTAlN0MlN0NwYXJtJTNFMi43NSklN0J2YXIlMjBkaXN0YW5jZT1kaXN0YW5jZS9vYmouaGVpZ2h0O2Rpc3RhbmNlPWRpc3RhbmNlWSUzRTA/LTEqZGlzdGFuY2U6ZGlzdGFuY2U7aWYocG9zWC9vYmoud2lkdGglM0UwLjUpJTdCaDVQbGF5ZXJWb2x1bWVDb250cm9sKG9iaixkaXN0YW5jZSk7JTdEZWxzZSU3Qmg1UGxheWVyUmF0ZUNvbnRyb2wob2JqLGRpc3RhbmNlJTNFMD8xOi0xKTslN0QlN0QlN0RmdW5jdGlvbiUyMGg1UGxheWVyUmF0ZUNvbnRyb2wob2JqLGRpcmVjdGlvbiklN0JpZihvYmoudmlkZW8ucGxheWJhY2tSYXRlIT11bmRlZmluZWQpJTdCdmFyJTIwYWRkUmF0ZT1vYmouZm9yd2FyZFJhdGUrZGlyZWN0aW9uLzQ7b2JqLnZpZGVvLnBsYXliYWNrUmF0ZT0oYWRkUmF0ZSUzQzAuMjUlN0MlN0NhZGRSYXRlJTNFNCk/KGFkZFJhdGUlM0MwLjI1PzAuMjU6NCk6YWRkUmF0ZTslN0RlbHNlJTdCb2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU4JUE3JTg2JUU5JUEyJTkxJUU0JUI4JThEJUU2JTk0JUFGJUU2JThDJTgxJUU1JTgwJThEJUU5JTgwJTlGJUU2JTkyJUFEJUU2JTk0JUJFJUVGJUJDJTgxJTIyOyU3RCU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJWb2x1bWVDb250cm9sKG9iaixkaXN0YW5jZSklN0J2YXIlMjBzdGVwPU1hdGgucm91bmQoZGlzdGFuY2UqNTApO2lmKChvYmouZm9yd2FyZFZvbHVtZStzdGVwLzEwMCklM0UxKSU3Qm9iai52aWRlby52b2x1bWU9MTslN0RlbHNlJTIwaWYoKG9iai5mb3J3YXJkVm9sdW1lK3N0ZXAvMTAwKSUzQzApJTdCb2JqLnZpZGVvLnZvbHVtZT0wOyU3RGVsc2UlN0JvYmoudmlkZW8udm9sdW1lPW9iai5mb3J3YXJkVm9sdW1lK3N0ZXAvMTAwOyU3RCU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJQcm9ncmVzc0NvbnRyb2wob2JqLGRpc3RhbmNlKSU3QnZhciUyMHN0ZXA9TWF0aC5yb3VuZChkaXN0YW5jZSo1MCk7b2JqLnZpZGVvLmN1cnJlbnRUaW1lPW9iai5mb3J3YXJkVGltZSs2MCo3KnN0ZXAvMTAwOyU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJGb3JtYXRUaW1lKHRpbWUpJTdCaWYoaXNOYU4odGltZSkpJTdCdmFyJTIwYXJyPXRpbWUuc3BsaXQoJTIyOiUyMik7dmFyJTIwbnVtPTA7Zm9yKHZhciUyMGk9MDtpJTNDYXJyLmxlbmd0aDtpKyspJTdCbnVtPW51bSo2MCtwYXJzZUludChhcnIlNUJpJTVEKTslN0RyZXR1cm4lMjBudW07JTdEZWxzZSU3QnZhciUyMHN0cj0lMjIlMjI7dmFyJTIwbnVtPXBhcnNlSW50KHRpbWUpO2Zvcih2YXIlMjBpPTA7aSUzQzIlN0MlN0NudW0lM0UwO2krKyklN0J2YXIlMjBtb2Q9bnVtJTI1NjA7bnVtPShudW0tbW9kKS82MDtzdHI9KG1vZCUzQzEwPyUyMjAlMjIrbW9kOm1vZCkrKGk9PTA/JTIyJTIyOiUyMjolMjIpK3N0cjslN0RyZXR1cm4lMjBzdHI7JTdEJTdEZnVuY3Rpb24lMjBpc0g1UGxheWVySGlkZGVuKGVsZSklN0JpZihlbGUuc2Nyb2xsV2lkdGghPTAmJmVsZS5zY3JvbGxIZWlnaHQhPTApJTdCcmV0dXJuJTIwZmFsc2U7JTdEcmV0dXJuJTIwdHJ1ZTslN0QlN0QpKCk7\";\n" +
                "    eval(decodeURI(window.atob(h5PlayerCode)));\n" +
                "})();");
        values.put("state","已启用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void xzfz(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "限制网页主动复制-你可以点红色按钮，变成绿色 ✔ 来允许自动复制");
        values.put("url", "*");
        values.put("js","启动后默认是开启限制的，\n" +
                "\n" +
                "你可以点红色按钮，变成绿色 ✔ 来允许自动复制。\n" +
                "\n" +
                "脚本带有按域名记忆功能，自动记忆域名开关状态。\n" +
                "\n" +
                "(function(){\n" +
                "const id = decodeURIComponent('98778');\n" +
                "\n" +
                "function runOnce(fn, key) {\n" +
                "  const uniqId = 'BEXT_UNIQ_ID_' + id + (key ? key : '');\n" +
                "  if (window[uniqId]) {\n" +
                "    return;\n" +
                "  }\n" +
                "  window[uniqId] = true;\n" +
                "  fn && fn();\n" +
                "}\n" +
                "\n" +
                "\n" +
                "function runAt(start, fn, ...args) {\n" +
                "  if (typeof fn !== 'function') return;\n" +
                "  switch (start) {\n" +
                "    case 'document-end':\n" +
                "      if (\n" +
                "        document.readyState === 'interactive' ||\n" +
                "        document.readyState === 'complete'\n" +
                "      ) {\n" +
                "        fn.call(this, ...args);\n" +
                "      } else {\n" +
                "        document.addEventListener('DOMContentLoaded', fn.bind(this, ...args));\n" +
                "      }\n" +
                "      break;\n" +
                "    case 'document-idle':\n" +
                "      if (document.readyState === 'complete') {\n" +
                "        fn.call(this, ...args);\n" +
                "      } else {\n" +
                "        window.addEventListener('load', fn.bind(this, ...args));\n" +
                "      }\n" +
                "      break;\n" +
                "    default:\n" +
                "      setTimeout(fn, start, ...args);\n" +
                "  }\n" +
                "}\n" +
                "\n" +
                "function bextButton(option = {}) {\n" +
                "  let opt = Object.assign(\n" +
                "      {\n" +
                "        posX: 'right',\n" +
                "        posY: 'bottom',\n" +
                "        offsetX: 0,\n" +
                "        offsetY: 0,\n" +
                "        absPosX: null,\n" +
                "        absPosY: null,\n" +
                "        btnColor: 'white',\n" +
                "        textColor: 'black',\n" +
                "        text: '按',\n" +
                "        movable: true,\n" +
                "        sticky: false,\n" +
                "        click: null,\n" +
                "        drag: null,\n" +
                "        btnStyle: null,\n" +
                "        textStyle: null,\n" +
                "      },\n" +
                "      option,\n" +
                "    ),\n" +
                "    makeStyle = (id, css) => {\n" +
                "      let style = document.createElement('style');\n" +
                "      if (typeof id === 'string') style.id = internal.button.id + '_' + id;\n" +
                "      style.textContent = css;\n" +
                "      return style;\n" +
                "    },\n" +
                "    container = document.createElement('div'),\n" +
                "    internal = {\n" +
                "      idFlag: 'bextBtn_',\n" +
                "      styleParent: null,\n" +
                "      modifiedStyle: [],\n" +
                "      \n" +
                "      movePosition: [],\n" +
                "      posX: 'right',\n" +
                "      posY: 'bottom',\n" +
                "      offsetX: 0,\n" +
                "      offsetY: 0,\n" +
                "      absPosX: null,\n" +
                "      absPosY: null,\n" +
                "      btnColor: 'white',\n" +
                "      textColor: 'black',\n" +
                "      button: null,\n" +
                "      text: null,\n" +
                "      movable: false,\n" +
                "      sticky: false,\n" +
                "      btnStyle: null,\n" +
                "      textStyle: null,\n" +
                "      event: [], \n" +
                "      genPosStyle: (posX, posY, offsetX, offsetY) => {\n" +
                "        return makeStyle(\n" +
                "          'posStyle',\n" +
                "          `#${internal.button.id} {\n" +
                "    top:    ${posY == 'top' ? offsetY + 'px' : posY == 'middle' ? 0 : 'auto'};\n" +
                "    bottom: ${\n" +
                "      posY == 'bottom' ? offsetY + 'px' : posY == 'middle' ? 0 : 'auto'\n" +
                "    };\n" +
                "    left:   ${posX == 'left' ? offsetX + 'px' : posX == 'center' ? 0 : 'auto'};\n" +
                "    right:  ${posX == 'right' ? offsetX + 'px' : posX == 'center' ? 0 : 'auto'};\n" +
                "    margin: ${posY == 'middle' ? 'auto' : '.5em'} ${\n" +
                "            posX == 'center' ? 'auto' : '.5em'\n" +
                "          };}`,\n" +
                "        );\n" +
                "      },\n" +
                "      genMovePosStyle: (offset) => {\n" +
                "        return makeStyle(\n" +
                "          'posStyle',\n" +
                "          `#${internal.button.id} {\n" +
                "    top: ${offset[0]}px;\n" +
                "    bottom: auto;\n" +
                "    left: ${offset[1]}px;\n" +
                "    right: auto;\n" +
                "    margin: .5em;\n" +
                "  }`,\n" +
                "        );\n" +
                "      },\n" +
                "      genColorStyle: (button, text) => {\n" +
                "        return makeStyle(\n" +
                "          'colorStyle',\n" +
                "          `#${internal.button.id} {\n" +
                "    background-color: ${button};\n" +
                "  }\n" +
                "  #${internal.button.id} span {\n" +
                "    color: ${text};\n" +
                "  }`,\n" +
                "        );\n" +
                "      },\n" +
                "      pushStyle: () => {\n" +
                "        if (internal.styleParent) {\n" +
                "          internal.modifiedStyle.forEach((s, i) => {\n" +
                "            if (!internal.styleParent.querySelector(`#${s.id}`)) {\n" +
                "              internal.styleParent.appendChild(s);\n" +
                "            }\n" +
                "          });\n" +
                "        }\n" +
                "      },\n" +
                "      posOfsMode: () => {\n" +
                "        if (internal.modifiedStyle[0]) {\n" +
                "          internal.modifiedStyle[0].remove();\n" +
                "        }\n" +
                "        internal.modifiedStyle[0] = internal.genPosStyle(\n" +
                "          internal.posX,\n" +
                "          internal.posY,\n" +
                "          internal.offsetX,\n" +
                "          internal.offsetY,\n" +
                "        );\n" +
                "        internal.pushStyle();\n" +
                "      },\n" +
                "      btnDragEvent: [\n" +
                "        (e) => {\n" +
                "          \n" +
                "          if (internal.modifiedStyle[3]) {\n" +
                "            internal.modifiedStyle[3].remove();\n" +
                "          }\n" +
                "          internal.modifiedStyle[3] = makeStyle(\n" +
                "            'pageLock',\n" +
                "            'body { overflow: hidden }',\n" +
                "          );\n" +
                "          internal.pushStyle();\n" +
                "          internal.movePosition = [\n" +
                "            e.touches[0].clientX - internal.button.offsetLeft,\n" +
                "            e.touches[0].clientY - internal.button.offsetTop,\n" +
                "          ];\n" +
                "        },\n" +
                "        (e) => {\n" +
                "          \n" +
                "          if (internal.modifiedStyle[0]) {\n" +
                "            internal.modifiedStyle[0].remove();\n" +
                "          }\n" +
                "          let button = internal.button,\n" +
                "            buttonSize = [\n" +
                "              button.offsetHeight +\n" +
                "                parseFloat(getComputedStyle(button).marginTop) +\n" +
                "                parseFloat(getComputedStyle(button).marginBottom),\n" +
                "              button.offsetWidth +\n" +
                "                parseFloat(getComputedStyle(button).marginLeft) +\n" +
                "                parseFloat(getComputedStyle(button).marginRight),\n" +
                "            ];\n" +
                "          internal.modifiedStyle[0] = internal.genMovePosStyle([\n" +
                "            Math.max(\n" +
                "              0,\n" +
                "              Math.min(\n" +
                "                e.touches[0].clientY - internal.movePosition[1],\n" +
                "                window.innerHeight - buttonSize[0],\n" +
                "              ),\n" +
                "            ),\n" +
                "            Math.max(\n" +
                "              0,\n" +
                "              Math.min(\n" +
                "                e.touches[0].clientX - internal.movePosition[0],\n" +
                "                window.innerWidth - buttonSize[1],\n" +
                "              ),\n" +
                "            ),\n" +
                "          ]);\n" +
                "          internal.pushStyle();\n" +
                "        },\n" +
                "        (e) => {\n" +
                "          \n" +
                "          if (internal.modifiedStyle[3]) {\n" +
                "            internal.modifiedStyle[3].remove();\n" +
                "            delete internal.modifiedStyle[3];\n" +
                "          }\n" +
                "          if (internal.sticky) {\n" +
                "            let x = ['left', 'center', 'right'],\n" +
                "              xi = window.innerWidth / 3,\n" +
                "              y = ['top', 'middle', 'bottom'],\n" +
                "              yi = window.innerHeight / 3;\n" +
                "            internal.button.posX =\n" +
                "              x[parseInt(e.changedTouches[0].clientX / xi)];\n" +
                "            internal.button.posY =\n" +
                "              y[parseInt(e.changedTouches[0].clientY / yi)];\n" +
                "            internal.absPosX = null;\n" +
                "            internal.absPosY = null;\n" +
                "          } else {\n" +
                "            internal.absPosX = parseInt(getComputedStyle(internal.button).left);\n" +
                "            internal.absPosY = parseInt(getComputedStyle(internal.button).top);\n" +
                "          }\n" +
                "        },\n" +
                "      ],\n" +
                "      genId: function () {\n" +
                "        let id = null;\n" +
                "        while (!id || window.bextBtn.id.includes(id)) {\n" +
                "          id = parseInt(Math.random() * 10 ** 9).toString(36);\n" +
                "        }\n" +
                "        window.bextBtn.id.push(id);\n" +
                "        return internal.idFlag + id;\n" +
                "      },\n" +
                "    };\n" +
                "  if (!window.bextBtn) {\n" +
                "    window.bextBtn = {\n" +
                "      btnCmnStyle: null,\n" +
                "      id: [],\n" +
                "    };\n" +
                "  }\n" +
                "  if (!window.bextBtn.btnCmnStyle) {\n" +
                "    window.bextBtn.btnCmnStyle = makeStyle(\n" +
                "      null,\n" +
                "      `.bextBtn {\n" +
                "    all: initial;\n" +
                "    position: fixed;\n" +
                "    z-index: 9999999;\n" +
                "    font-size: 15px;\n" +
                "    line-height: 1.5em;\n" +
                "    text-align: center;\n" +
                "    width: max-content;\n" +
                "    height: max-content;\n" +
                "    min-width: 1.5em;\n" +
                "    min-height: 1.5em;\n" +
                "    margin: .5em;\n" +
                "    padding: .25em;\n" +
                "    border-radius: 2em;\n" +
                "    box-shadow: 2px 0 5px gray;\n" +
                "    user-select: none;\n" +
                "}\n" +
                ".bextBtn span {\n" +
                "    all: initial;\n" +
                "    font-size: inherit;\n" +
                "}`,\n" +
                "    );\n" +
                "  }\n" +
                "  container.innerHTML = `\n" +
                "        <div id='${internal.genId()}' class='bextBtn'>\n" +
                "            <span>按</span>\n" +
                "        </div>\n" +
                "    `;\n" +
                "  internal.button = container.children[0];\n" +
                "  internal.text = internal.button.children[0];\n" +
                "  Object.defineProperties(internal.button, {\n" +
                "    text: {\n" +
                "      get() {\n" +
                "        return internal.text.innerText;\n" +
                "      },\n" +
                "      set(text) {\n" +
                "        internal.text.innerText = text.toString();\n" +
                "      },\n" +
                "    },\n" +
                "    posX: {\n" +
                "      get() {\n" +
                "        return internal.posX;\n" +
                "      },\n" +
                "      set(pos) {\n" +
                "        if (pos == 'left' || pos == 'center' || pos == 'right') {\n" +
                "          internal.posX = pos;\n" +
                "          internal.absPosX = null;\n" +
                "          internal.absPosY = null;\n" +
                "          if (internal.modifiedStyle[0]) {\n" +
                "            internal.modifiedStyle[0].remove();\n" +
                "          }\n" +
                "          internal.modifiedStyle[0] = internal.genPosStyle(\n" +
                "            pos,\n" +
                "            internal.posY,\n" +
                "            internal.offsetX,\n" +
                "            internal.offsetY,\n" +
                "          );\n" +
                "          internal.pushStyle();\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    posY: {\n" +
                "      get() {\n" +
                "        return internal.posY;\n" +
                "      },\n" +
                "      set(pos) {\n" +
                "        if (pos == 'top' || pos == 'middle' || pos == 'bottom') {\n" +
                "          internal.posY = pos;\n" +
                "          internal.absPosX = null;\n" +
                "          internal.absPosY = null;\n" +
                "          if (internal.modifiedStyle[0]) {\n" +
                "            internal.modifiedStyle[0].remove();\n" +
                "          }\n" +
                "          internal.modifiedStyle[0] = internal.genPosStyle(\n" +
                "            internal.posX,\n" +
                "            pos,\n" +
                "            internal.offsetX,\n" +
                "            internal.offsetY,\n" +
                "          );\n" +
                "          internal.pushStyle();\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    offsetX: {\n" +
                "      get() {\n" +
                "        return internal.offsetX;\n" +
                "      },\n" +
                "      set(offset) {\n" +
                "        if (typeof offset == 'number') {\n" +
                "          internal.offsetX = offset;\n" +
                "          internal.absPosX = null;\n" +
                "          internal.absPosY = null;\n" +
                "          if (internal.modifiedStyle[0]) {\n" +
                "            internal.modifiedStyle[0].remove();\n" +
                "          }\n" +
                "          internal.modifiedStyle[0] = internal.genPosStyle(\n" +
                "            internal.posX,\n" +
                "            internal.posY,\n" +
                "            offset,\n" +
                "            internal.offsetY,\n" +
                "          );\n" +
                "          internal.pushStyle();\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    offsetY: {\n" +
                "      get() {\n" +
                "        return internal.offsetY;\n" +
                "      },\n" +
                "      set(offset) {\n" +
                "        if (typeof offset == 'number') {\n" +
                "          internal.offsetY = offset;\n" +
                "          internal.absPosX = null;\n" +
                "          internal.absPosY = null;\n" +
                "          if (internal.modifiedStyle[0]) {\n" +
                "            internal.modifiedStyle[0].remove();\n" +
                "          }\n" +
                "          internal.modifiedStyle[0] = internal.genPosStyle(\n" +
                "            internal.posX,\n" +
                "            internal.posY,\n" +
                "            internal.offsetX,\n" +
                "            offset,\n" +
                "          );\n" +
                "          internal.pushStyle();\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    absPosX: {\n" +
                "      get() {\n" +
                "        return internal.absPosX;\n" +
                "      },\n" +
                "      set(offset) {\n" +
                "        if (typeof offset == 'number') {\n" +
                "          internal.absPosX = offset;\n" +
                "          if (internal.absPosX && internal.absPosY) {\n" +
                "            if (internal.modifiedStyle[0]) {\n" +
                "              internal.modifiedStyle[0].remove();\n" +
                "            }\n" +
                "            internal.modifiedStyle[0] = internal.genMovePosStyle([\n" +
                "              internal.absPosX,\n" +
                "              internal.absPosY,\n" +
                "            ]);\n" +
                "            internal.pushStyle();\n" +
                "          }\n" +
                "        } else {\n" +
                "          internal.absPosX = null;\n" +
                "          internal.posOfsMode();\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    absPosY: {\n" +
                "      get() {\n" +
                "        return internal.absPosY;\n" +
                "      },\n" +
                "      set(offset) {\n" +
                "        if (typeof offset == 'number') {\n" +
                "          internal.absPosY = offset;\n" +
                "          if (internal.absPosX && internal.absPosY) {\n" +
                "            if (internal.modifiedStyle[0]) {\n" +
                "              internal.modifiedStyle[0].remove();\n" +
                "            }\n" +
                "            internal.modifiedStyle[0] = internal.genMovePosStyle([\n" +
                "              internal.absPosX,\n" +
                "              internal.absPosY,\n" +
                "            ]);\n" +
                "            internal.pushStyle();\n" +
                "          }\n" +
                "        } else {\n" +
                "          internal.absPosY = null;\n" +
                "          internal.posOfsMode();\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    btnColor: {\n" +
                "      get() {\n" +
                "        return internal.btnColor;\n" +
                "      },\n" +
                "      set(color) {\n" +
                "        if (typeof color == 'string' && color !== '') {\n" +
                "          internal.btnColor = color;\n" +
                "          if (internal.modifiedStyle[4]) {\n" +
                "            internal.modifiedStyle[4].remove();\n" +
                "          }\n" +
                "          internal.modifiedStyle[4] = internal.genColorStyle(\n" +
                "            color,\n" +
                "            internal.textColor,\n" +
                "          );\n" +
                "          internal.pushStyle();\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    textColor: {\n" +
                "      get() {\n" +
                "        return internal.textColor;\n" +
                "      },\n" +
                "      set(color) {\n" +
                "        if (typeof color == 'string' && color !== '') {\n" +
                "          internal.textColor = color;\n" +
                "          if (internal.modifiedStyle[4]) {\n" +
                "            internal.modifiedStyle[4].remove();\n" +
                "          }\n" +
                "          internal.modifiedStyle[4] = internal.genColorStyle(\n" +
                "            internal.btnColor,\n" +
                "            color,\n" +
                "          );\n" +
                "          internal.pushStyle();\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    click: {\n" +
                "      get() {\n" +
                "        if (internal.event[0]) {\n" +
                "          return internal.event[0];\n" +
                "        } else return null;\n" +
                "      },\n" +
                "      set(func) {\n" +
                "        if (internal.event[0]) {\n" +
                "          internal.button.removeEventListener('click', internal.event[0]);\n" +
                "        }\n" +
                "        if (func && typeof func == 'function') {\n" +
                "          internal.event[0] = func.bind(window, internal.button);\n" +
                "          internal.button.addEventListener('click', internal.event[0]);\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    drag: {\n" +
                "      get() {\n" +
                "        if (internal.event[1]) {\n" +
                "          return internal.event[1];\n" +
                "        } else return null;\n" +
                "      },\n" +
                "      set(func) {\n" +
                "        if (internal.event[1]) {\n" +
                "          internal.button.removeEventListener('touchend', internal.event[1], {\n" +
                "            passive: true,\n" +
                "          });\n" +
                "        }\n" +
                "        if (func && typeof func == 'function') {\n" +
                "          internal.event[1] = func.bind(window, internal.button);\n" +
                "          internal.button.addEventListener('touchend', internal.event[1], {\n" +
                "            passive: true,\n" +
                "          });\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    movable: {\n" +
                "      get() {\n" +
                "        return internal.movable;\n" +
                "      },\n" +
                "      set(bool) {\n" +
                "        if (typeof bool == 'boolean' && bool !== internal.movable) {\n" +
                "          internal.movable = bool;\n" +
                "          if (bool) {\n" +
                "            internal.button.addEventListener(\n" +
                "              'touchstart',\n" +
                "              internal.btnDragEvent[0],\n" +
                "              { passive: true },\n" +
                "            );\n" +
                "            internal.button.addEventListener(\n" +
                "              'touchmove',\n" +
                "              internal.btnDragEvent[1],\n" +
                "              { passive: true },\n" +
                "            );\n" +
                "            internal.button.addEventListener(\n" +
                "              'touchend',\n" +
                "              internal.btnDragEvent[2],\n" +
                "              { passive: true },\n" +
                "            );\n" +
                "          } else {\n" +
                "            internal.button.removeEventListener(\n" +
                "              'touchstart',\n" +
                "              internal.btnDragEvent[0],\n" +
                "              { passive: true },\n" +
                "            );\n" +
                "            internal.button.removeEventListener(\n" +
                "              'touchmove',\n" +
                "              internal.btnDragEvent[1],\n" +
                "              { passive: true },\n" +
                "            );\n" +
                "            internal.button.removeEventListener(\n" +
                "              'touchend',\n" +
                "              internal.btnDragEvent[2],\n" +
                "              { passive: true },\n" +
                "            );\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    sticky: {\n" +
                "      get() {\n" +
                "        return internal.sticky;\n" +
                "      },\n" +
                "      set(bool) {\n" +
                "        if (typeof bool == 'boolean' && bool !== internal.sticky) {\n" +
                "          internal.sticky = bool;\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    btnStyle: {\n" +
                "      get() {\n" +
                "        return internal.btnStyle;\n" +
                "      },\n" +
                "      set(css) {\n" +
                "        if (!css) return false;\n" +
                "        if (typeof css !== 'object' && typeof css !== 'string') return false;\n" +
                "        if (internal.modifiedStyle[1]) {\n" +
                "          internal.modifiedStyle[1].remove();\n" +
                "        }\n" +
                "        if (typeof css == 'object') {\n" +
                "          Object.assign(container.style, css);\n" +
                "          css = container.getAttribute('style');\n" +
                "          container.style = '';\n" +
                "        }\n" +
                "        internal.btnStyle = css;\n" +
                "        internal.modifiedStyle[1] = makeStyle(\n" +
                "          'btnStyle',\n" +
                "          `#${internal.button.id} { ${css} }`,\n" +
                "        );\n" +
                "        internal.pushStyle();\n" +
                "      },\n" +
                "    },\n" +
                "    textStyle: {\n" +
                "      get() {\n" +
                "        return internal.textStyle;\n" +
                "      },\n" +
                "      set(css) {\n" +
                "        if (!css) return false;\n" +
                "        if (typeof css !== 'object' && typeof css !== 'string') return false;\n" +
                "        if (internal.modifiedStyle[2]) {\n" +
                "          internal.modifiedStyle[2].remove();\n" +
                "        }\n" +
                "        if (typeof css == 'object') {\n" +
                "          Object.assign(container.style, css);\n" +
                "          css = container.getAttribute('style');\n" +
                "          container.style = '';\n" +
                "        }\n" +
                "        internal.textStyle = css;\n" +
                "        internal.modifiedStyle[2] = makeStyle(\n" +
                "          'btnStyle',\n" +
                "          `#${internal.button.id} span { ${css} }`,\n" +
                "        );\n" +
                "        internal.pushStyle();\n" +
                "      },\n" +
                "    },\n" +
                "    push: {\n" +
                "      value: (btnParent = document.body, styleParent = document.head) => {\n" +
                "        function realPush() {\n" +
                "          internal.styleParent = styleParent;\n" +
                "          styleParent.appendChild(window.bextBtn.btnCmnStyle);\n" +
                "          internal.pushStyle();\n" +
                "          btnParent.appendChild(internal.button);\n" +
                "        }\n" +
                "        if (internal.styleParent) return;\n" +
                "        if (btnParent && styleParent) {\n" +
                "          realPush();\n" +
                "        } else {\n" +
                "          runAt('document-end', realPush);\n" +
                "        }\n" +
                "      },\n" +
                "    },\n" +
                "    pop: {\n" +
                "      value: () => {\n" +
                "        if (!internal.styleParent) return;\n" +
                "        internal.click = null;\n" +
                "        internal.drag = null;\n" +
                "        internal.movable = false;\n" +
                "        internal.button.remove();\n" +
                "        internal.modifiedStyle.forEach((s) => {\n" +
                "          s.remove();\n" +
                "        });\n" +
                "        internal.styleParent = null;\n" +
                "      },\n" +
                "    },\n" +
                "  });\n" +
                "  Object.assign(internal.button, opt);\n" +
                "  return internal.button;\n" +
                "}\n" +
                "\n" +
                "var config = {\"enable\":true,\"storage\":true};\n" +
                "\n" +
                "runOnce(() => {\n" +
                "    if (!navigator.clipboard) return;\n" +
                "\n" +
                "    let origExecCmd = document.execCommand,\n" +
                "        origClipWrite = navigator.clipboard.write,\n" +
                "        origClipWriteText = navigator.clipboard.writeText,\n" +
                "        copyCount = 0, copyStat = false,\n" +
                "        storKey = 'autoCopy_stat';\n" +
                "\n" +
                "    if (window.localStorage.hasOwnProperty(storKey)) {\n" +
                "        {\n" +
                "            config.enable = (window.localStorage.getItem(storKey) == 0) ? true : false;\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    function addCount() {\n" +
                "        copyCount++;\n" +
                "        button.text = copyCount;\n" +
                "    }\n" +
                "    function recordCopy(cmd, ...arg) {\n" +
                "        if (cmd.toLowerCase() == 'copy') {\n" +
                "            addCount();\n" +
                "        } else {\n" +
                "            origExecCmd(cmd, ...arg);\n" +
                "        }\n" +
                "    }    function recordCopyPromise() {\n" +
                "        return new Promise(resolve => {\n" +
                "            addCount();\n" +
                "            resolve();\n" +
                "        });\n" +
                "    }    function lockCopy(lock) {\n" +
                "        document.execCommand = lock ? recordCopy : origExecCmd;\n" +
                "        navigator.clipboard.write = lock ? recordCopyPromise : origClipWrite;\n" +
                "        navigator.clipboard.writeText = lock ? recordCopyPromise : origClipWriteText;\n" +
                "        copyStat = lock ? false : true;\n" +
                "        copyCount = 0;\n" +
                "        window.localStorage.setItem(storKey, lock ? 0 : 1);\n" +
                "    }    lockCopy(config.enable);\n" +
                "    let text = ['0', '✔'], button = new bextButton();\n" +
                "    button.offsetY = 100;\n" +
                "    button.text = (config.enable) ? text[0] : text[1];\n" +
                "    button.textColor = 'white';\n" +
                "    button.btnColor = (config.enable) ? 'crimson' : 'limegreen';\n" +
                "    button.btnStyle = { padding: '.25em' };\n" +
                "    button.click = btn => {\n" +
                "        btn.text = (copyStat) ? text[0] : text[1];\n" +
                "        btn.btnColor = (copyStat) ? 'crimson' : 'limegreen';\n" +
                "        lockCopy(copyStat);\n" +
                "    };\n" +
                "    button.push();\n" +
                "});\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void ggfy1(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "谷歌翻译网页无须vpn，启用后可以自动翻译网页，翻译最好启用一个就好");
        values.put("url", "*");
        values.put("js","(function(){\n" +
                "\n" +
                "void((function () {\n" +
                "        var script = document.createElement('script');\n" +
                "        script.src = '//cdn.jsdelivr.net/gh/damengzhu/js1@main/element.js';\n" +
                "        document.getElementsByTagName('head')[0].appendChild(script);\n" +
                "\n" +
                "        var google_translate_element = document.createElement('div');\n" +
                "        google_translate_element.id = 'google_translate_element';\n" +
                "        google_translate_element.style = 'position:fixed; bottom:10px; right:10px; cursor:pointer;';\n" +
                "        document.documentElement.appendChild(google_translate_element);\n" +
                "\n" +
                "        script = document.createElement('script');\n" +
                "        script.innerHTML = \"function googleTranslateElementInit() {\" +\n" +
                "            \"new google.translate.TranslateElement({\" +\n" +
                "            \"layout: google.translate.TranslateElement.InlineLayout.SIMPLE,\" +\n" +
                "            \"multilanguagePage: true,\" +\n" +
                "            \"pageLanguage: 'auto',\" +\n" +
                "            \"includedLanguages: 'zh-CN,zh-TW,en,ko,ja'\" +\n" +
                "            \"}, 'google_translate_element');}\";\n" +
                "        document.getElementsByTagName('head')[0].appendChild(script);\n" +
                "})());\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void ggfy2(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "谷歌翻译需要vpn，启用后可以自动翻译网页，翻译最好启用一个就好");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 谷歌原生网页翻译\n" +
                " * @Author: 谷花泰\n" +
                " * @version: 2.0\n" +
                " * @description: 调用谷歌翻译接口翻译整个网站\n" +
                " * @include: *\n" +
                " * @createTime: 2019-10-12 09:11:58\n" +
                " * @updateTime   : 2020-03-16 00:51:04\n" +
                " */\n" +
                "(function () {\n" +
                "  /* 判断是否该执行 */\n" +
                "  /* 网址黑名单制，遇到这些域名不执行 */\n" +
                "  const blackList = ['zhihu.com', 'twitter.com', 'wenku.baidu.com', 'wk.baidu.com'];\n" +
                "\n" +
                "  const hostname = window.location.hostname;\n" +
                "  const key = encodeURIComponent('谷花泰:谷歌原生网页翻译:执行判断');\n" +
                "  const isBlack = blackList.some(keyword => {\n" +
                "    if (hostname.match(keyword)) {\n" +
                "      return true;\n" +
                "    };\n" +
                "    return false;\n" +
                "  });\n" +
                "\n" +
                "  if (isBlack || window[key]) {\n" +
                "    return;\n" +
                "  };\n" +
                "  window[key] = true;\n" +
                "\n" +
                "  /* 开始执行代码 */\n" +
                "  const config = {\n" +
                "    initName: 'googleTranslate',\n" +
                "    initElm: 'google_translate_elm',\n" +
                "    /*多语言页面支持*/\n" +
                "    multilanguagePage: true,\n" +
                "    /* 哪种需要转 */\n" +
                "    pageLanguage: 'auto',\n" +
                "    /* 能转成啥 */\n" +
                "    includedLanguages: 'zh-CN,zh-TW,en',\n" +
                "    /* 隐藏工具bar */\n" +
                "    autoDisplay: false\n" +
                "  };\n" +
                "\n" +
                "  /* 注入谷歌翻译脚本 */\n" +
                "  const script = document.createElement('script');\n" +
                "  script.src = `//translate.google.cn/translate_a/element.js?&cb=${config.initName}`;\n" +
                "  script.async = true;\n" +
                "  document.head.appendChild(script);\n" +
                "\n" +
                "  /* 创建谷歌翻译右下角的语言选择框容器 */\n" +
                "  const div = document.createElement('div');\n" +
                "  div.id = config.initElm;\n" +
                "  div.setAttribute('style', `\n" +
                "    position: fixed;\n" +
                "    bottom: 5vw;\n" +
                "    right: 5vw;\n" +
                "    z-index: 999999999;\n" +
                "  `);\n" +
                "  document.documentElement.appendChild(div);\n" +
                "\n" +
                "  /* 一些样式 */\n" +
                "  const style = document.createElement('style');\n" +
                "  style.innerHTML = `\n" +
                "    body {\n" +
                "      position: relative;\n" +
                "      top: 0 !important;\n" +
                "      left: 0 !important;    \n" +
                "    }\n" +
                "    .skiptranslate iframe{\n" +
                "      width: 0px !important;\n" +
                "      height: 0px !important;\n" +
                "      overflow: hidden;\n" +
                "      display: none !important;\n" +
                "    }\n" +
                "    .goog-te-gadget-simple {\n" +
                "      border: 1px solid #ececec !important;\n" +
                "      border-radius: 4px;\n" +
                "      padding: 8px;\n" +
                "      line-height: 26px;\n" +
                "      text-align: center;\n" +
                "    }\n" +
                "    .goog-te-gadget-simple a,.goog-te-gadget-simple a:link,.goog-te-gadget-simple a:visited,.goog-te-gadget-simple a:hover,.goog-te-gadget-simple a:active{\n" +
                "      text-decoration: none;\n" +
                "      color:inherit;\n" +
                "    }\n" +
                "  `;\n" +
                "  document.head.appendChild(style);\n" +
                "\n" +
                "  /* 谷歌翻译实例化函数 */\n" +
                "  window[config.initName] = () => {\n" +
                "    new window.google.translate.TranslateElement({\n" +
                "      layout: google.translate.TranslateElement.InlineLayout.SIMPLE,\n" +
                "      multilanguagePage: config.multilanguagePage,\n" +
                "      pageLanguage: config.pageLanguage,\n" +
                "      includedLanguages: config.includedLanguages,\n" +
                "      autoDisplay: config.autoDisplay\n" +
                "    }, config.initElm);\n" +
                "  };\n" +
                "\n" +
                "  /* 监听dom函数 */\n" +
                "  function observe({ targetNode, config = {}, callback = () => { } }) {\n" +
                "    if (!targetNode) {\n" +
                "      return;\n" +
                "    };\n" +
                "    config = Object.assign({\n" +
                "      attributes: true,\n" +
                "      childList: true,\n" +
                "      subtree: true\n" +
                "    }, config);\n" +
                "    const observer = new MutationObserver(callback);\n" +
                "    observer.observe(targetNode, config);\n" +
                "  };\n" +
                "\n" +
                "  /* 监听右下角的语言选择框 */\n" +
                "  const initElm = document.querySelector(`#${config.initElm}`);\n" +
                "  observe({\n" +
                "    targetNode: initElm,\n" +
                "    config: {\n" +
                "      attributes: false\n" +
                "    },\n" +
                "    callback(mutationList, observer) {\n" +
                "      /* 遍历节点 */\n" +
                "      mutationList.forEach(mutation => {\n" +
                "        Array.from(mutation.addedNodes, node => {\n" +
                "          /* 清除 google image */\n" +
                "          const googleImages = document.querySelectorAll(`#goog-gt-tt img, #${config.initElm} img`);\n" +
                "          Array.from(googleImages, img => {\n" +
                "            img.parentNode.removeChild(img);\n" +
                "          });\n" +
                "\n" +
                "          /* 添加关闭按钮 */\n" +
                "          if (node.className === 'goog-te-menu-value') {\n" +
                "            const btnContent = document.querySelector('.goog-te-gadget-simple > span > a');\n" +
                "            const closeBtn = document.createElement('span');\n" +
                "            closeBtn.innerText = '关闭 | ';\n" +
                "            closeBtn.addEventListener('click', (e) => {\n" +
                "              initElm.parentNode.removeChild(initElm);\n" +
                "              e.stopPropagation();\n" +
                "            }, true);\n" +
                "            btnContent.parentNode.insertBefore(closeBtn, btnContent);\n" +
                "          };\n" +
                "\n" +
                "        });\n" +
                "        /* 结束监听 */\n" +
                "        observer.disconnect();\n" +
                "      });\n" +
                "    }\n" +
                "  });\n" +
                "})();\n" +
                "彩云\n" +
                "!function(){if(!document.getElementById(\"gzfy\")){var zfyan=document.createElement(\"span\");zfyan.id=\"gzfy\";zfyan.innerHTML=\"彩\";zfyan.style.cssText=\"display:none;text-align:center !important;font-size:22px;width:32px;height:32px;line-height:28px;text-align:center;float:right;position:fixed;right:15px;top:75%;color:#fff;opacity:0.6;background:#008080;cursor:pointer;position:fixed !important;z-index:9999999999 !important;box-shadow:0px 1px 1px #000;border-radius:50%\";zfyan.addEventListener(\"click\",function(){zfy()});document.body.appendChild(zfyan);}var zfyhdy1,zfyhdy2;document.addEventListener(\"touchstart\",function(e){zfyhdy1=e.changedTouches[0].clientY});document.addEventListener(\"touchmove\",function(e){zfyhdy2=e.changedTouches[0].clientY;document.getElementById(\"gzfy\").style.display=zfyhdy2-zfyhdy1>0?\"block\":\"none\"});function zfy(){var zfyfy=document.getElementById(\"gzfy\");zfyfy.parentNode.removeChild(zfyfy);var cyfy=document.createElement(\"script\");cyfy.type=\"text/javascript\";cyfy.charset=\"UTF-8\";cyfy.src=(\"https:\"==document.location.protocol?\" https://\":\"http://\")+\"caiyunapp.com/dest/trs.js\";document.body.appendChild(cyfy); }}();\n");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void caiyun(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "彩云翻译网页，启启用后可以自动翻译网页，翻译最好启用一个就好");
        values.put("url", "*");
        values.put("js","!function(){if(!document.getElementById(\"gzfy\")){var zfyan=document.createElement(\"span\");zfyan.id=\"gzfy\";zfyan.innerHTML=\"彩\";zfyan.style.cssText=\"display:none;text-align:center !important;font-size:22px;width:32px;height:32px;line-height:28px;text-align:center;float:right;position:fixed;right:15px;top:75%;color:#fff;opacity:0.6;background:#008080;cursor:pointer;position:fixed !important;z-index:9999999999 !important;box-shadow:0px 1px 1px #000;border-radius:50%\";zfyan.addEventListener(\"click\",function(){zfy()});document.body.appendChild(zfyan);}var zfyhdy1,zfyhdy2;document.addEventListener(\"touchstart\",function(e){zfyhdy1=e.changedTouches[0].clientY});document.addEventListener(\"touchmove\",function(e){zfyhdy2=e.changedTouches[0].clientY;document.getElementById(\"gzfy\").style.display=zfyhdy2-zfyhdy1>0?\"block\":\"none\"});function zfy(){var zfyfy=document.getElementById(\"gzfy\");zfyfy.parentNode.removeChild(zfyfy);var cyfy=document.createElement(\"script\");cyfy.type=\"text/javascript\";cyfy.charset=\"UTF-8\";cyfy.src=(\"https:\"==document.location.protocol?\" https://\":\"http://\")+\"caiyunapp.com/dest/trs.js\";document.body.appendChild(cyfy); }}();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void bdmh(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "百度皮肤美化");
        values.put("url", "*");
        values.put("js","(function(){\n" +
                "\n" +
                "function runAt(start, fn, ...args) {\n" +
                "  if (typeof fn !== 'function') return;\n" +
                "  switch (start) {\n" +
                "    case 'document-end':\n" +
                "      if (\n" +
                "        document.readyState === 'interactive' ||\n" +
                "        document.readyState === 'complete'\n" +
                "      ) {\n" +
                "        fn.call(this, ...args);\n" +
                "      } else {\n" +
                "        document.addEventListener('DOMContentLoaded', fn.bind(this, ...args));\n" +
                "      }\n" +
                "      break;\n" +
                "    case 'document-idle':\n" +
                "      if (document.readyState === 'complete') {\n" +
                "        fn.call(this, ...args);\n" +
                "      } else {\n" +
                "        window.addEventListener('load', fn.bind(this, ...args));\n" +
                "      }\n" +
                "      break;\n" +
                "    default:\n" +
                "      setTimeout(fn, start, ...args);\n" +
                "  }\n" +
                "}\n" +
                "\n" +
                "\n" +
                "runAt('document-idle',() => {\n" +
                "(function () {\n" +
                "  const whiteList = [\"m.baidu.com\", \"www.baidu.com\", \"wapbaike.baidu.com\", \"baike.baidu.com\", \"http://m.baidu.com/?tn=simple\", \"https://m.baidu.com/?tn=simple\", \"mbd.baidu.com\", \"pae.baidu.com\", \"baijiahao.baidu.com\", \"haokan.baidu.com\", \"mobile.baidu.com\", \"zhidao.baidu.com\", \"wk.baidu.com\", \"fanyi.baidu.com\", \"jingyan.baidu.com\"];\n" +
                "  const hostname = window.location.hostname;\n" +
                "  const key = encodeURIComponent('酷安搜CWorld:Pure百度美化:执行判断');\n" +
                "  const canLoad = whiteList.some(url => {\n" +
                "    if (url.match(hostname)) {\n" +
                "      return true;\n" +
                "    }    return false;\n" +
                "  });\n" +
                "\n" +
                "  if (!canLoad || window[key]) {\n" +
                "    return;\n" +
                "  }  window[key] = true;\n" +
                "  if ([\"m.baidu.com\", \"www.baidu.com\", \"wapbaike.baidu.com\", \"baike.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /**广告**/\n" +
                "\n" +
                "/*------以下内容为可选，选择文字后按 Ctrl+SHift+/ 即可更改------*/\n" +
                "\n" +
                "/*首页实时热点*/\n" +
                "\n" +
                "/* #hotwordlist{display: none !important;} */\n" +
                "\n" +
                "/*搜索页小视频*/\n" +
                "\n" +
                ".se-tablink-scroll-wrapper a.se-tabitem[data-log=\"xsp\"] {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*搜索页热议（集成微博）*/\n" +
                "\n" +
                ".se-tablink-scroll-wrapper a.se-tabitem[data-log=\"realtime_ugc\"] {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*搜索页采购*/\n" +
                "\n" +
                ".se-tablink-scroll-wrapper a.se-tabitem[data-log=\"b2b\"] {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*搜索页招聘*/\n" +
                "\n" +
                ".se-tablink-scroll-wrapper a.se-tabitem[data-log=\"baipin\"] {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*搜索页小程序*/\n" +
                "\n" +
                ".se-tablink-scroll-wrapper a.se-tabitem[data-log=\"mini_program\"] {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*搜索页小程序*/\n" +
                "\n" +
                ".se-tablink-scroll-wrapper a.se-tabitem[data-log=\"notes\"] {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*百度百科 猜你关注*/\n" +
                "\n" +
                ".baike-app-view .theme-container {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*百度百科 TA说*/\n" +
                "\n" +
                ".baike-app-view .tashuo-list {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*------以下内容请勿改动------*/\n" +
                "\n" +
                "#header > div:last-child {\n" +
                "  padding-bottom: 0 !important;\n" +
                "  opacity: 0 !important;\n" +
                "  height: 0 !important;\n" +
                "}\n" +
                "\n" +
                ".ns-square-point,\n" +
                ".ts-image-uploader-icon-point,\n" +
                "a.index-banner.square-banner-bgicon,\n" +
                ".news-container,\n" +
                ".banners a[href=\"https://m.baidu.com/l=1/tc?logid=2598984798&from=0&ref=index_iphone&nsrc=IlPT2AEptyoA_yixCFOxCGZb8c3JV3T5AB3ORS5K1De8mVjte4viZQRAUj0sNTrIBTLS5GOChAJ-iyubKk2t7gV1mv5wgjN6jiHwdsjqea&bdenc=1&ct=10&cst=2&logFrom=operatebanner&logInfo=1\"],\n" +
                ".square-hot-container,\n" +
                ".se-ft-promlink,\n" +
                ".hudong-btn,\n" +
                ".sfc-image-content-mediacy-slider-toolbar-download-tips,\n" +
                "._1k19YN3-qdVhoYpBLDMjYV,\n" +
                ".commentEmbed-backHomeCard,\n" +
                ".ec_wise_ad_card div,\n" +
                ".hint-fold-results-box .hint-text,\n" +
                ".banner-item a {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/**配色**/\n" +
                "\n" +
                ".menu-container .menu-item.current {\n" +
                "  color: #5892e3;\n" +
                "}\n" +
                "\n" +
                ".mui-switch.mui-switch-animbg:checked,\n" +
                ".menu-container .menu-item.current span,\n" +
                ".ivk-button.ivk-button-popup {\n" +
                "  background-color: #5892e3;\n" +
                "}\n" +
                "\n" +
                "/**动画**/\n" +
                "\n" +
                ".menu-item a:hover {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/**细节调整**/\n" +
                "\n" +
                "/*卡片*/\n" +
                "\n" +
                ".c-result.result,\n" +
                ".rec-item,\n" +
                ".user-listview .panel,\n" +
                ".c-result,\n" +
                ".sfc-image-content-waterfall-vertical .sfc-image-content-waterfall-item,\n" +
                ".sfc-realtime .c-infinite-scroll > div,\n" +
                ".new-pagenav,\n" +
                ".se-relativewords,\n" +
                ".video-cell {\n" +
                "  transition: 0.3s;\n" +
                "  overflow: hidden;\n" +
                "  width: auto;\n" +
                "  margin: 6px 5px 0;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 1px 4px #dfdfdf;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                ".icard:active {\n" +
                "  background-color: #f4f4f4;\n" +
                "  box-shadow: 0 2px 4px #0000003d;\n" +
                "}\n" +
                "\n" +
                ".c-container {\n" +
                "  margin: 0;\n" +
                "}\n" +
                "\n" +
                ".sfc-image-content-waterfall-vertical .sfc-image-content-waterfall-item {\n" +
                "  margin: 3px 0 0;\n" +
                "  border-radius: 2px;\n" +
                "}\n" +
                "\n" +
                ".new-pagenav,\n" +
                ".se-relativewords {\n" +
                "  margin: 6px 5px !important;\n" +
                "}\n" +
                "\n" +
                ".video-cell {\n" +
                "  padding: 0 0.17rem 0.1rem 0.17rem !important;\n" +
                "}\n" +
                "\n" +
                "/*首页*/\n" +
                "\n" +
                ".tab_news .tab-news-content {\n" +
                "  background-color: #00000005;\n" +
                "}\n" +
                "\n" +
                ".menu-container.showtabs {\n" +
                "  box-shadow: 0 2px 10px #0000001f;\n" +
                "}\n" +
                "\n" +
                ".sf-container .sf-header {\n" +
                "  background: #fff;\n" +
                "  border-bottom: 0;\n" +
                "}\n" +
                "\n" +
                ".searchbox-exp #index-form,\n" +
                ".his-wrap-new #index-form .index-fix,\n" +
                ".se-form,\n" +
                ".his-wrap-new .index-fix {\n" +
                "  transition: 0.3s;\n" +
                "  border-radius: 5px;\n" +
                "  -webkit-border-radius: 5px;\n" +
                "  border: none;\n" +
                "  overflow: visible;\n" +
                "  background: #fff;\n" +
                "  box-shadow: 0 2px 5px #0000001f;\n" +
                "}\n" +
                "\n" +
                ".searchbox-exp #index-form:focus,\n" +
                ".his-wrap-new #index-form .index-fix:focus,\n" +
                ".se-form:focus {\n" +
                "  box-shadow: 0 2px 5px #0000003b;\n" +
                "}\n" +
                "\n" +
                ".searchbox-exp #index-bn {\n" +
                "  transition: 0.3s;\n" +
                "  color: #fff;\n" +
                "  border-radius: 0 5px 5px 0;\n" +
                "  background: #5892e3;\n" +
                "  border: none;\n" +
                "  box-shadow: 0 2px 5px #0000001f;\n" +
                "}\n" +
                "\n" +
                ".searchbox-exp #index-bn:active {\n" +
                "  background: #4879bd !important;\n" +
                "}\n" +
                "\n" +
                ".newHisBtn {\n" +
                "  transition: 0.3s;\n" +
                "  border-radius: 0 5px 5px 0;\n" +
                "  border: none;\n" +
                "}\n" +
                "\n" +
                ".rn-three-pic-wrap {\n" +
                "  margin-right: 3px;\n" +
                "}\n" +
                "\n" +
                ".rn-three-pic-wrap img {\n" +
                "  transition: 0.3s;\n" +
                "  border-radius: 3px;\n" +
                "}\n" +
                "\n" +
                "#hotwordlist,\n" +
                "#hotwordlist .hotword-container {\n" +
                "  background: #fff0;\n" +
                "}\n" +
                "\n" +
                ".his-wrap-new #index-form .his,\n" +
                ".his-wrap-new #index-form .sug {\n" +
                "  border-bottom: 1px solid #00000005;\n" +
                "}\n" +
                "\n" +
                ".menu-container .menu-item span {\n" +
                "  width: 40px;\n" +
                "  height: 4px;\n" +
                "  border-radius: 3px 3px 0 0;\n" +
                "}\n" +
                "\n" +
                ".se-tablink-scroll-wrapper .se-tab-cur:after {\n" +
                "  border-bottom: 0;\n" +
                "  background-color: #5892e3;\n" +
                "  height: 3px;\n" +
                "  border-radius: 2px 2px 0 0;\n" +
                "}\n" +
                "\n" +
                ".news-item-div {\n" +
                "  background: #fff0;\n" +
                "  padding-top: 0;\n" +
                "}\n" +
                "\n" +
                ".rn-channelMgr#channel_mgrview .channel-content .channel-item .name,\n" +
                ".rn-channelMgr#channel_offline .channel-content .channel-item .name {\n" +
                "  border: none;\n" +
                "  border-radius: 5px;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 2px 5px #0000001f;\n" +
                "}\n" +
                "\n" +
                ".user-listview .panel li:nth-child(1),\n" +
                ".user-listview .panel li:nth-child(2),\n" +
                ".user-listview .panel li:nth-child(3),\n" +
                ".user-listview .panel h2,\n" +
                ".user-listview .panel li {\n" +
                "  border: none;\n" +
                "}\n" +
                "\n" +
                ".user-listview .panel ul {\n" +
                "  background-color: #00000005;\n" +
                "  margin-bottom: 0;\n" +
                "}\n" +
                "\n" +
                ".filter-panel-confirm {\n" +
                "  background: #5892e3;\n" +
                "  color: #fff;\n" +
                "  border-radius: 5px;\n" +
                "  box-shadow: 0 5px 10px #5892e354;\n" +
                "}\n" +
                "\n" +
                ".filter-panel-reset {\n" +
                "  background: #0000000f;\n" +
                "  border: none;\n" +
                "  border-radius: 5px;\n" +
                "  height: 0.38rem;\n" +
                "  line-height: 0.38rem;\n" +
                "  margin-right: 0.08rem;\n" +
                "}\n" +
                "\n" +
                ".se-tab-lists.c-flexbox {\n" +
                "  width: 200% !important;\n" +
                "}\n" +
                "\n" +
                ".se-head-tablink {\n" +
                "  background: #fff0;\n" +
                "}\n" +
                "\n" +
                ".his-wrap-new {\n" +
                "  background: #eee;\n" +
                "}\n" +
                "\n" +
                ".suggest-div {\n" +
                "  top: 15px !important;\n" +
                "  right: 0;\n" +
                "  left: -0.14rem;\n" +
                "  padding: 0 0.1rem;\n" +
                "  border: 0;\n" +
                "  border-top: none;\n" +
                "}\n" +
                "\n" +
                ".his-wrap-new .suggest-feedback {\n" +
                "  border-top: none;\n" +
                "  background-color: #fff0;\n" +
                "}\n" +
                "\n" +
                "/*百度搜索 展开全部内容*/\n" +
                "\n" +
                ".hint-fold-results-box {\n" +
                "  display: none;\n" +
                "}\n" +
                "\n" +
                "div#page-relative,\n" +
                "div#page-controller {\n" +
                "  display: block;\n" +
                "}\n" +
                "\n" +
                "#results,\n" +
                ".se-page-bd,\n" +
                "section.hint-fold-results-wrapper.hint-no-fold {\n" +
                "  height: 100%;\n" +
                "}\n" +
                "\n" +
                "/*设置*/\n" +
                "\n" +
                ".user-setting li {\n" +
                "  border-bottom: 0;\n" +
                "}\n" +
                "\n" +
                ".mui-switch {\n" +
                "  width: 40px;\n" +
                "  height: 15px;\n" +
                "  border: none;\n" +
                "  box-shadow: none;\n" +
                "  border-radius: 20px;\n" +
                "  right: 16px;\n" +
                "  top: 28px;\n" +
                "  background-color: rgba(0, 0, 0, 0.1);\n" +
                "}\n" +
                "\n" +
                ".mui-switch:before {\n" +
                "  content: \"\";\n" +
                "  width: 24px;\n" +
                "  height: 24px;\n" +
                "  top: -5px;\n" +
                "  left: -5px;\n" +
                "  border-radius: 20px;\n" +
                "  background-color: #f0f0f0;\n" +
                "  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.23);\n" +
                "}\n" +
                "\n" +
                "/*文章*/\n" +
                "\n" +
                ".packupButton,\n" +
                ".bottomMargin {\n" +
                "  display: none;\n" +
                "}\n" +
                "\n" +
                ".mainContent {\n" +
                "  height: 100% !important;\n" +
                "}\n" +
                "\n" +
                "/*------百度百科------*/\n" +
                "\n" +
                "/*广告*/\n" +
                "\n" +
                ".baike-app-view .declare,\n" +
                ".baike-app-view .topbar .top-icon.spark-icon,\n" +
                ".JiyxvaTlnAg0,\n" +
                "ul#VpwCdqhH5,\n" +
                ".baike-app-view .catalog-tip.show,\n" +
                ".baike-app-view .button-fixed.tashuo-button-fixed,\n" +
                ".YWgIycS0,\n" +
                "ul#yjgRTqXU9,\n" +
                "div#nOrlCV6,\n" +
                "ul#ksAabJ7 {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*颜色*/\n" +
                "\n" +
                "/*卡片*/\n" +
                "\n" +
                ".baike-app-view .qtqy-container,\n" +
                ".baike-app-view .movie-actor,\n" +
                ".baike-app-view .movie-video,\n" +
                ".baike-app-view .movie-photo,\n" +
                ".baike-app-view .movie-comment {\n" +
                "  transition: 0.3s;\n" +
                "  overflow: hidden;\n" +
                "  width: auto;\n" +
                "  margin: 6px 5px 0;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 1px 4px #dfdfdf;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                ".baike-app-view .qtqy-container {\n" +
                "  position: relative;\n" +
                "  top: 5px;\n" +
                "  left: -7px;\n" +
                "  width: 100%;\n" +
                "  padding: 7px;\n" +
                "  margin: 5px 0;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 1px 4px #dfdfdf;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                "/*细节优化*/\n" +
                "\n" +
                ".baike-app-view .button-fixed {\n" +
                "  border: none;\n" +
                "  background: #fff0;\n" +
                "  box-shadow: none;\n" +
                "  color: #000;\n" +
                "  width: 0.44rem;\n" +
                "  height: 0.44rem;\n" +
                "  line-height: 0.38rem;\n" +
                "  font-size: 0.24rem;\n" +
                "  border-radius: 50%;\n" +
                "  bottom: auto;\n" +
                "  right: 100px;\n" +
                "}\n" +
                "\n" +
                ".baike-app-view .yx-load-more-flow .yx-load-more-inner {\n" +
                "  background: #fff;\n" +
                "  border: none;\n" +
                "  border-radius: 19px;\n" +
                "  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.23);\n" +
                "}\n" +
                "\n" +
                ".baike-app-view .ui-suggestion ul li {\n" +
                "  border-bottom: 1px solid #00000008;\n" +
                "  padding: 0;\n" +
                "}\n" +
                "\n" +
                ".baike-app-view .ui-suggestion .ui-suggestion-button {\n" +
                "  border-bottom: none;\n" +
                "  background: #f8f8f8;\n" +
                "  height: 0.45rem;\n" +
                "  overflow: hidden;\n" +
                "}\n" +
                "\n" +
                ".baike-app-view .extra-info-n .extra-list-item.extra-polysemant .more-means {\n" +
                "  border: none;\n" +
                "  background: #fff;\n" +
                "  border-radius: 5px;\n" +
                "  box-shadow: 0 1px 4px #0000003b;\n" +
                "  color: #5892e3;\n" +
                "  padding: 8px 15px;\n" +
                "}\n" +
                "\n" +
                ".baike-app-view\n" +
                "    .extra-info-n\n" +
                "    .extra-list-item.extra-polysemant\n" +
                "    .more-means-icon {\n" +
                "  position: absolute;\n" +
                "  top: 15px;\n" +
                "  right: 15px;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"http://m.baidu.com/?tn=simple\", \"https://m.baidu.com/?tn=simple\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            .tab_news .tab-news-content,\n" +
                ".s-loading-frame.bottom .bottom-load-before {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "#logo {\n" +
                "  padding-bottom: 200px !important;\n" +
                "}\n" +
                "\n" +
                ".searchbox-exp #index-form {\n" +
                "  margin-bottom: 200px !important;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"mbd.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /*广告*/\n" +
                "\n" +
                ".icard .icard-float-point .icard-point-img {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*配色*/\n" +
                "\n" +
                ".ccard-nav .selected:before,\n" +
                ".follow-no {\n" +
                "  background-color: #5892e3;\n" +
                "}\n" +
                "\n" +
                "/*动画*/\n" +
                "\n" +
                ".icard-ttcont,\n" +
                ".ccard-nav-text {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/*细节调整*/\n" +
                "\n" +
                "#cardbox {\n" +
                "  background-color: #00000005;\n" +
                "}\n" +
                "\n" +
                ".icard {\n" +
                "  transition: 0.3s;\n" +
                "  margin: 6px 5px 0;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 2px 4px #dfdfdf;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                ".icard:active {\n" +
                "  background-color: #f4f4f4;\n" +
                "  box-shadow: 0 2px 4px #0000003d;\n" +
                "}\n" +
                "\n" +
                ".cardHeaderBar,\n" +
                ".ccard-hd {\n" +
                "  box-shadow: 0 2px 10px #0000001f;\n" +
                "}\n" +
                "\n" +
                ".icard-btn {\n" +
                "  transition: 0.3s;\n" +
                "  border-radius: 100px;\n" +
                "}\n" +
                "\n" +
                ".icard-btn:hover {\n" +
                "  background-color: #0000000f;\n" +
                "}\n" +
                "\n" +
                ".icard-btn:active {\n" +
                "  background-color: #00000017;\n" +
                "}\n" +
                "\n" +
                ".ccard-nav .selected {\n" +
                "  box-shadow: 0 2px 10px #5892e31f;\n" +
                "}\n" +
                "\n" +
                ".mainContent {\n" +
                "  height: 100% !important;\n" +
                "}\n" +
                "\n" +
                ".commentEmbed-backHomeCard,\n" +
                ".packupButton {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "._3JB0JD_e7aW8naKy7Bz-kE ul li {\n" +
                "  position: relative;\n" +
                "  z-index: 999;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"pae.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /*广告*/\n" +
                "\n" +
                "/*配色*/\n" +
                "\n" +
                "/*动画*/\n" +
                "\n" +
                ".icard-ttcont,\n" +
                ".ccard-nav-text {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/*细节调整*/\n" +
                "\n" +
                ".fav-head {\n" +
                "  background-color: #fff;\n" +
                "  color: #5892e3;\n" +
                "  box-shadow: 0 2px 10px #0000001f;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"baijiahao.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /*广告*/\n" +
                "\n" +
                ".contentMedia .openImg {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*配色*/\n" +
                "\n" +
                "/*动画*/\n" +
                "\n" +
                "m {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/**细节调整**/\n" +
                "\n" +
                "/*文章*/\n" +
                "\n" +
                ".mainContent {\n" +
                "  height: 100% !important;\n" +
                "}\n" +
                "\n" +
                ".packupButton.packupButton3 {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "._1k19YN3-qdVhoYpBLDMjYV {\n" +
                "  transition: 0.3s;\n" +
                "  opacity: 0.5;\n" +
                "  background-color: rgba(51, 136, 255, 0.95);\n" +
                "  box-shadow: none;\n" +
                "}\n" +
                "\n" +
                "._1k19YN3-qdVhoYpBLDMjYV {\n" +
                "  opacity: 1;\n" +
                "  background-color: rgba(51, 136, 255, 0.95);\n" +
                "  box-shadow: 0 1px 10px 1px rgba(59, 107, 174, 0.2);\n" +
                "}\n" +
                "\n" +
                "._1k19YN3-qdVhoYpBLDMjYV ._2v86xffLyWbTz5nRDFr_L_ {\n" +
                "  transition: 0.3s;\n" +
                "  width: 0;\n" +
                "  padding-right: 0;\n" +
                "}\n" +
                "\n" +
                "._1k19YN3-qdVhoYpBLDMjYV:hover ._2v86xffLyWbTz5nRDFr_L_ {\n" +
                "  transition: 0.3s;\n" +
                "  width: auto;\n" +
                "  padding-right: 8px;\n" +
                "}\n" +
                "\n" +
                "/*个人*/\n" +
                "\n" +
                ".s-avatar-lg {\n" +
                "  overflow: visible;\n" +
                "  border-radius: 100%;\n" +
                "  box-shadow: 0 2px 10px #0000001f;\n" +
                "}\n" +
                "\n" +
                ".s-tabs-wrap.s-tabs-wrap-hairline.s-tabs-wrap-scrollable.s-tabs-suctop.nav-wrap {\n" +
                "  box-shadow: 0 2px 10px #0000001f;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"haokan.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /*广告*/\n" +
                "\n" +
                ".rmb-carsousel-position-top,\n" +
                ".extraDiv,\n" +
                ".link-tpl-container,\n" +
                ".rmb-growth-common-tpl-1-wrapper,\n" +
                ".eye-catching-feeditem,\n" +
                ".commentEmbed-backHomeCard {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*配色*/\n" +
                "\n" +
                "/*动画*/\n" +
                "\n" +
                "m {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/**细节调整**/\n" +
                "\n" +
                ".mod-play1er .play1er {\n" +
                "  position: fixed;\n" +
                "  top: 0 !important;\n" +
                "  left: 0;\n" +
                "  right: 0;\n" +
                "  background: #000;\n" +
                "  z-index: 99;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"mobile.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /*广告*/\n" +
                "\n" +
                ".detail-card-app .logo-wrap,\n" +
                "button.c-btn.btn.c-btn--primary,\n" +
                ".detail-download-layer .detail-modal .modal-container .modal-footer .safe,\n" +
                "section.detail-appsearch-button,\n" +
                ".detail-card-comment .comment-link,\n" +
                "a[href=\"/item?pid=825114773&f0=home_homeHeader%400\"] {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*配色*/\n" +
                "\n" +
                "/*卡片*/\n" +
                "\n" +
                "section.card-wrap.home-card-recommend,\n" +
                "section.card-wrap.home-card-normal,\n" +
                "section.card-wrap.detail-card-comment,\n" +
                "section.card-wrap.detail-card-dev,\n" +
                "section.card-wrap.detail-card-recommend,\n" +
                ".cate-content .cate-item {\n" +
                "  transition: 0.3s;\n" +
                "  overflow: hidden;\n" +
                "  width: auto;\n" +
                "  margin: 6px 5px 0;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 1px 4px #dfdfdf;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                "/*动画*/\n" +
                "\n" +
                "a {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/**细节调整**/\n" +
                "\n" +
                "span.normal-down {\n" +
                "  display: inline-block;\n" +
                "  white-space: nowrap;\n" +
                "  cursor: pointer;\n" +
                "  text-align: center;\n" +
                "  -moz-box-sizing: border-box;\n" +
                "  box-sizing: border-box;\n" +
                "  padding: 2px 13.5px 3px;\n" +
                "  -moz-border-radius: 50px;\n" +
                "  border-radius: 50px;\n" +
                "  font-size: 13px;\n" +
                "  position: relative;\n" +
                "  margin: 0 auto;\n" +
                "  width: 289px;\n" +
                "  line-height: 18px;\n" +
                "  height: 37px;\n" +
                "  padding-top: 7px;\n" +
                "  padding-bottom: 9px;\n" +
                "  color: #fff !important;\n" +
                "  background-image: -webkit-linear-gradient(55deg, #25cdff, #467bf3);\n" +
                "  background-image: linear-gradient(35deg, #25cdff, #467bf3);\n" +
                "  background-color: #467bf3;\n" +
                "}\n" +
                "\n" +
                ".detail-download-layer .detail-modal .modal-container .modal-body {\n" +
                "  height: 1px;\n" +
                "  opacity: 0;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"zhidao.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /*广告*/\n" +
                "\n" +
                ".iknow-root-dom-element .doodle-container.show,\n" +
                ".iknow-root-dom-element #iknow-common-header .dib.msg-new-ico:after,\n" +
                ".iknow-root-dom-element\n" +
                "    #iknow-common-header\n" +
                "    .iknow-msg-list\n" +
                "    .msg-list\n" +
                "    .ico-msg.msg-new-ico:after,\n" +
                ".iknow-root-dom-element .icon-bdad,\n" +
                ".iknow-root-dom-element .feed-recommend.mm-content-box {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*配色*/\n" +
                "\n" +
                "/*卡片*/\n" +
                "\n" +
                "ul#home-list-container li,\n" +
                ".new-search-list.w-list .w-solved-list-li {\n" +
                "  transition: 0.3s;\n" +
                "  overflow: hidden;\n" +
                "  width: auto;\n" +
                "  margin: 6px 5px 0;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 1px 4px #dfdfdf;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                ".home-list ul#home-list-container li {\n" +
                "  margin: 6px 5px 0;\n" +
                "  padding: 0 10px;\n" +
                "}\n" +
                "\n" +
                ".home-list.home-mod,\n" +
                ".home-list #home-list-container {\n" +
                "  background: #00000005;\n" +
                "}\n" +
                "\n" +
                "/*动画*/\n" +
                "\n" +
                "a {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/*展开*/\n" +
                "\n" +
                ".iknow-root-dom-element .w-detail-full .w-detail-display-btn {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                ".iknow-root-dom-element .w-detail-full .w-detail-container {\n" +
                "  max-height: initial !important;\n" +
                "}\n" +
                "\n" +
                "/**细节调整**/\n" +
                "\n" +
                ".iknow-root-dom-element #iknow-common-header {\n" +
                "  box-shadow: 0 2px 5px #0000001f;\n" +
                "}\n" +
                "\n" +
                ".pannel {\n" +
                "  background-color: #00000005;\n" +
                "  padding: 0;\n" +
                "}\n" +
                "\n" +
                ".new-search-list.w-list .w-solved-list-li {\n" +
                "  padding: 10px;\n" +
                "}\n" +
                "\n" +
                ".wm-wrapper {\n" +
                "  padding-left: 20px;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"wk.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /*广告*/\n" +
                "\n" +
                "nav.ui-nav-container#ui-nav ul li a .new-item,\n" +
                ".ui-toolbar .ui-toolbar-right > button.icon-app,\n" +
                ".sf-edu-wenku .top-bar-root > .wk-container .right-panel > button.btn-vip,\n" +
                ".sf-edu-wenku .bottom-bar-container,\n" +
                ".sf-edu-wenku .top-doc-info-root .btn-view-in-app,\n" +
                ".sf-edu-wenku .gain-doc-block-root,\n" +
                ".sf-edu-wenku .guidetowkappwg-root,\n" +
                ".sf-edu-wenku .vip-guide-contanier,\n" +
                ".sf-edu-wenku .rec-text-root .rec-list li div.doc-see-in-app,\n" +
                ".sf-edu-wenku\n" +
                "    .bottombarwg-root\n" +
                "    .bottombarwg-root-bottom\n" +
                "    .handel-box\n" +
                "    .wangpan-area\n" +
                "    .vip-icon-super {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*配色*/\n" +
                "\n" +
                "/*卡片*/\n" +
                "\n" +
                ".searchList .search-li,\n" +
                ".searchList .kg-search-li,\n" +
                ".searchList .op-search-li {\n" +
                "  transition: 0.3s;\n" +
                "  overflow: hidden;\n" +
                "  width: auto;\n" +
                "  margin: 6px 5px 0;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 1px 4px #dfdfdf;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                "/*动画*/\n" +
                "\n" +
                "a {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/**细节调整**/\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"fanyi.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /*广告*/\n" +
                "\n" +
                ".fanyi-sfr-container .app-guide,\n" +
                "a.app-bar,\n" +
                ".fanyi-sfr-container .article,\n" +
                "section.bottom-intro,\n" +
                ".fanyi-sfr-container .extend-title-inner .usecase-title::after {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*配色*/\n" +
                "\n" +
                ".fanyi-sfr-container .setting-container .setting-header {\n" +
                "  background: #5892e3;\n" +
                "}\n" +
                "\n" +
                "/*卡片*/\n" +
                "\n" +
                ".fanyi-sfr-container .translang,\n" +
                ".fanyi-sfr-container .translatein,\n" +
                ".fanyi-sfr-container .trans-btn,\n" +
                ".fanyi-sfr-container .sug-wrap {\n" +
                "  transition: 0.3s;\n" +
                "  overflow: hidden;\n" +
                "  width: auto;\n" +
                "  margin: 6px 5px 0;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 1px 4px #dfdfdf;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                "/*动画*/\n" +
                "\n" +
                "a {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/**细节调整**/\n" +
                "\n" +
                ".fanyi-sfr-container .sug-box:first-child {\n" +
                "  border-top: none;\n" +
                "}\n" +
                "\n" +
                ".fanyi-sfr-container .sug-box {\n" +
                "  border-bottom: 1px solid #0000000d;\n" +
                "}\n" +
                "\n" +
                ".fanyi-sfr-container.with-middle-app-bar .translatein {\n" +
                "  margin-bottom: 6px;\n" +
                "}\n" +
                "\n" +
                ".fanyi-sfr-container .extend-style {\n" +
                "  border-bottom: 0;\n" +
                "  height: 48px;\n" +
                "}\n" +
                "\n" +
                ".fanyi-sfr-container.with-middle-app-bar .extend-output {\n" +
                "  z-index: 99;\n" +
                "  box-shadow: 0 0 10px #0000005c;\n" +
                "  border-radius: 8px 8px 0 0;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }\n" +
                "  if ([\"jingyan.baidu.com\"].some(hostname => hostname.match(location.hostname))) {\n" +
                "    document.head.innerHTML += `\n" +
                "          <style>\n" +
                "            /*广告*/\n" +
                "\n" +
                ".main-nav .lk-btn,\n" +
                ".wgt-ad-guess {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                "/*配色*/\n" +
                "\n" +
                ".wgt-sub-nav a.act,\n" +
                ".h2,\n" +
                ".expDlList em,\n" +
                ".wgt-exp-content .go-to-step-text {\n" +
                "  color: #2db68a;\n" +
                "}\n" +
                "\n" +
                "#navigation,\n" +
                ".wgt-toolbar2,\n" +
                ".origin-ico,\n" +
                ".good-ico,\n" +
                ".wgt-exp-head .article-feed-header .article-feed-btn {\n" +
                "  background: #2db68a;\n" +
                "}\n" +
                "\n" +
                ".wgt-sub-nav a.act,\n" +
                ".origin-ico,\n" +
                ".good-ico {\n" +
                "  border-color: #2db68a;\n" +
                "}\n" +
                "\n" +
                "/*卡片*/\n" +
                "\n" +
                "ul.good-mag.clr li,\n" +
                ".rec-list li,\n" +
                ".usr-nav,\n" +
                ".ui-suggestion,\n" +
                "#excellentTab .expDlList li,\n" +
                ".content-box,\n" +
                ".wgt-rel-exp-feed {\n" +
                "  transition: 0.3s;\n" +
                "  overflow: hidden;\n" +
                "  width: auto;\n" +
                "  margin: 6px 5px 0;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: 0 1px 4px #dfdfdf;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                "/*动画*/\n" +
                "\n" +
                "a {\n" +
                "  transition: 0.3s;\n" +
                "}\n" +
                "\n" +
                "/**细节调整**/\n" +
                "\n" +
                "body {\n" +
                "  background-color: #fbfbfb;\n" +
                "}\n" +
                "\n" +
                ".read-whole-mask {\n" +
                "  display: none !important;\n" +
                "}\n" +
                "\n" +
                ".exp-content-container.fold {\n" +
                "  overflow: hidden;\n" +
                "  max-height: 100% !important;\n" +
                "}\n" +
                "\n" +
                "#navigation,\n" +
                ".wgt-toolbar2 {\n" +
                "  border: none;\n" +
                "}\n" +
                "\n" +
                ".main-nav .btn {\n" +
                "  border: none;\n" +
                "  background-color: #ffffff1c;\n" +
                "  top: 6px;\n" +
                "  left: 10px;\n" +
                "}\n" +
                "\n" +
                ".wgt-sub-nav .wp {\n" +
                "  background: #fff;\n" +
                "}\n" +
                "\n" +
                "ul.good-mag.clr li {\n" +
                "  width: 47.61%;\n" +
                "  height: 167px;\n" +
                "  margin: 0 3px;\n" +
                "}\n" +
                "\n" +
                ".category-wrapper {\n" +
                "  padding: 11px 11px 0 11px;\n" +
                "  overflow: hidden;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                ".search-input,\n" +
                ".wgt-toolbar2 .input-btn-search {\n" +
                "  transition: 0.3s;\n" +
                "  border: none;\n" +
                "  -webkit-box-shadow: 0 2px 5px #0000001a;\n" +
                "  background-color: #fff;\n" +
                "  border-radius: 5px;\n" +
                "}\n" +
                "\n" +
                ".search-input:focus {\n" +
                "  -webkit-box-shadow: 0 2px 5px #00000026;\n" +
                "  background-color: #fff;\n" +
                "  border-radius: 5px;\n" +
                "  outline: none !important;\n" +
                "}\n" +
                "\n" +
                ".search-input input {\n" +
                "  outline: none !important;\n" +
                "}\n" +
                "\n" +
                ".search-button {\n" +
                "  border: none;\n" +
                "}\n" +
                "\n" +
                ".search-button input {\n" +
                "  border: none;\n" +
                "  border-radius: 0 5px 5px 0 !important;\n" +
                "  background: #2db68a;\n" +
                "  color: #fff;\n" +
                "}\n" +
                "\n" +
                ".flat-footer,\n" +
                "#footer {\n" +
                "  border: none;\n" +
                "}\n" +
                "\n" +
                ".usr-img {\n" +
                "  padding: 0;\n" +
                "  overflow: hidden;\n" +
                "  border-radius: 50%;\n" +
                "  background-color: #fff;\n" +
                "  box-shadow: none;\n" +
                "}\n" +
                "\n" +
                ".usr-nav li,\n" +
                ".usr-nav {\n" +
                "  border: none;\n" +
                "}\n" +
                "\n" +
                ".search {\n" +
                "  position: relative;\n" +
                "  padding: 5px 5px 6px;\n" +
                "  background: #fff0;\n" +
                "  border-bottom: none;\n" +
                "  -webkit-box-shadow: inset 1px 1px 1px #fff;\n" +
                "}\n" +
                "\n" +
                ".ui-suggestion,\n" +
                "#excellentTab .expDlList li {\n" +
                "  border: none;\n" +
                "  background-color: #fff;\n" +
                "  margin: 5px 0;\n" +
                "}\n" +
                "\n" +
                ".wgt-exp-copyright .wp {\n" +
                "  padding-left: 15px;\n" +
                "}\n" +
                "          </style>\n" +
                "        `;\n" +
                "  }  document.head.innerHTML += `\n" +
                "        <style>\n" +
                "          \n" +
                "        </style>\n" +
                "      `;\n" +
                "})();\n" +
                "});\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void qxfgg(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "去悬浮广告");
        values.put("url", "*");
        values.put("js","var d=document;var s=d.createElement('script');s.setAttribute('src', 'https://greasyfork.org/scripts/7410-jskillad/code/jsKillAD.user.js');d.head.appendChild(s);");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void jxjk3(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "视频解析，启用后会破解爱奇艺等vip影视（需要手动点击网页出现的图标破解）");
        values.put("url", "*");
        values.put("js","(function(){\n" +
                "var config = {\"mianColor\":\"#de473c\",\"secondColor\":\"#f3f1e7\",\"iconMarginRight\":2,\"iconMarginTop\":100,\"iconWidth\":45,\"iconHeight\":35,\"iconFilletPercent\":0.3,\"developMenuHeight\":315,\"developMenuSecond\":0,\"parseInterfaces\":[\"https://okjx.cc/?url=\",\"https://17kyun.com/api.php?url=\",\"https://jx.m3u8.tv/jiexi/?url=\",\"https://jx.m3u8.tv/jiexi/?url=\",\"https://z1.m1907.cn/?jx=\",\"https://okjx.cc/?url=\",\"https://m2090.com/?url=\",\"http://51wujin.net/?url=\",\"https://vip.2ktvb.com/player/?url=\",\"https://660e.com/?url=\",\"https://api.sigujx.com/?url=\",\"https://jiexi.janan.net/jiexi/?url=\",\"https://jx.618g.com/?url=\",\"https://jx.ergan.top/?url=\",\"https://api.147g.cc/m3u8.php?url=\",\"http://17kyun.com/api.php?url=\"]};\n" +
                "\n" +
                "\n" +
                "(function() {\n" +
                "\t\n" +
                "\t\n" +
                "\tconst mianColor = config.mianColor;\n" +
                "\t\n" +
                "\tconst secondColor = config.secondColor;\n" +
                "\t\n" +
                "\tconst iconMarginRight = config.iconMarginRight;\n" +
                "\t\n" +
                "\tconst iconMarginTop = config.iconMarginTop;\n" +
                "\t\n" +
                "\tvar iconWidth = config.iconWidth;\n" +
                "\t\n" +
                "\tconst iconHeight = config.iconHeight;\n" +
                "\t\n" +
                "\tconst iconFilletPercent = config.iconFilletPercent;\n" +
                "\t\n" +
                "\tvar developMenuHeight = config.developMenuHeight;\n" +
                "\t\n" +
                "\tvar developMenuSecond = config.developMenuSecond;\n" +
                "\t\n" +
                "\tconst parseInterfaces = config.parseInterfaces;\n" +
                "\t\n" +
                "\t\n" +
                "\tconst videoSites = [\"v.qq.com\",\"tv.sohu.com\",\"iqiyi.com\",\"youku.com\",\"mgtv.com\",\"m.le.com\",\"www.le.com\",\"1905.com\",\"pptv.com\",\"bilibili.com\"];\n" +
                "\tconst currentUrl = document.location.href;\n" +
                "\t\n" +
                "\tif (self != top) {\n" +
                "\t\treturn;\n" +
                "\t}\n" +
                "\tvar result = videoSites.some(site=>{\n" +
                "\t\tif (currentUrl.match(site)) {\n" +
                "            return true;\n" +
                "\t\t}\n" +
                "        return false;\n" +
                "\t});\n" +
                "    if(!result){\n" +
                "        return;\n" +
                "    }\n" +
                "\t\n" +
                "\tvar uaLogo=\"pc\";\n" +
                "\tif(/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {\n" +
                "\t\tuaLogo=\"mobile\";\n" +
                "\t}\n" +
                "\t\n" +
                "\tconst globalStyle = \"cursor:pointer;position:fixed;right:\"+iconMarginRight+\"px;top:\"+iconMarginTop+\"px;z-index:2147483647;\";\n" +
                "\t\n" +
                "\tconst mainIconStyle = \"height:\"+iconHeight+\"px;width:\"+iconWidth+\"px;background:\"+mianColor+\";border-radius:\"+(iconFilletPercent*iconWidth)+\"px;box-sizing:border-box;box-shadow:-4px 4px 4px 0px rgba(0,0,0,0.4);\";\n" +
                "\t\n" +
                "\tconst triangleStyle = \"border-left:\"+(iconWidth*0.3)+\"px solid \"+secondColor+\";border-top:\"+(iconHeight*0.2)+\"px solid transparent;border-bottom:\"+(iconHeight*0.2)+\"px solid transparent;position:absolute;right:31%;top:30%;\";\n" +
                "\t\n" +
                "\tconst squareStyle = \"background:\"+secondColor+\";width:\"+(iconWidth*0.26)+\"px;height:\"+(iconWidth*0.26)+\"px;position:absolute;right:37%;top:37%;\";\n" +
                "\t\n" +
                "    const inMenuBoxStyle = \"width:115%;height:100%;overflow-y:scroll;overflow-x:hidden;\";\n" +
                "\t\n" +
                "\tconst outMenuBoxStyle = \"background:\"+mianColor+\";height:0px;overflow:hidden;font-size:\"+(iconWidth*0.4)+\"px;width:\"+(iconWidth*2.4)+\"px;position:absolute;right:0px;top:\"+iconHeight+\"px;box-shadow:-4px 4px 4px 0px rgba(0,0,0,0.4);border-radius:13px 0 1px 13px;transition:height \"+developMenuSecond+\"s;-moz-transition:height \"+developMenuSecond+\"s;-webkit-transition:height \"+developMenuSecond+\"s;-o-transition:height \"+developMenuSecond+\"s;\";\n" +
                "\t\n" +
                "\tconst MenuItemsStyle = \"color:\"+secondColor+\";display: block;padding:\"+(iconWidth*0.12)+\"px \"+(iconWidth*0.12)+\"px \"+(iconWidth*0.12)+\"px \"+(iconWidth*0.2)+\"px ;width:\"+(iconWidth*3)+\"px;\";\n" +
                "\t\n" +
                "\tconst IframeStyle = \"frameborder='no' width='100%' height='100%' allowfullscreen='true' allowtransparency='true' frameborder='0' scrolling='no';\";\n" +
                "    \n" +
                "\tvar classAndIDMap\t= {\"pc\":{\"v.qq.com\":\"mod_player\",\"iqiyi.com\":\"flashbox\",\"youku.com\":\"ykPlayer\",\"mgtv.com\":\"mgtv-player-wrap\",\"sohu.com\":\"x-player\",\"le.com\":\"fla_box\",\"1905.com\":\"player\",\"pptv.com\":\"pplive-player\",\"bilibili.com\":\"bilibili-player-video-wrap|player-limit-mask\"},\"mobile\":{\"v.qq.com\":\"mod_player\",\"iqiyi.com\":\"m-box\",\"youku.com\":\"h5-detail-player\",\"mgtv.com\":\"video-area\",\"sohu.com\":\"player-view\",\"le.com\":\"playB\",\"1905.com\":\"player\",\"pptv.com\":\"pp-details-video\",\"bilibili.com\":\"bilibiliPlayer|player-wrapper\"}};\n" +
                "    \n" +
                "    createIcon();\n" +
                "\t\n" +
                "\tdocument.onreadystatechange = function(){\n" +
                "        if(document.readyState == 'complete'){\n" +
                "            if(!document.getElementById(\"mainIcon\")){\n" +
                "                createIcon();\n" +
                "            }\n" +
                "        }\n" +
                "    };\n" +
                "    function createIcon(){\n" +
                "        try{\n" +
                "            var div = document.createElement(\"div\");\n" +
                "            div.style.cssText = globalStyle;\n" +
                "            div.setAttribute(\"id\",\"mainIcon\");\n" +
                "            var html = \"<div id='mainButton' style='\"+mainIconStyle+\"'><div id='triangle' style='\"+triangleStyle+\"'></div></div><div id='dropDownBox' style='\"+outMenuBoxStyle+\"'><div style=\"+inMenuBoxStyle+\">\";\n" +
                "            for(var i in parseInterfaces){\n" +
                "                if(i==parseInterfaces.length-1){\n" +
                "                    html += \"<span class='spanStyle' style='\"+MenuItemsStyle+\"' url='\"+parseInterfaces[i]+\"'>线路接口\"+(parseInt(i)+1)+\"</span>\";\n" +
                "                }else {\n" +
                "                    html += \"<span class='spanStyle' style='\"+MenuItemsStyle+\"border-bottom-style:solid;' url='\"+parseInterfaces[i]+\"'>线路接口\"+(parseInt(i)+1)+\"</span>\";\n" +
                "                }\n" +
                "            }\n" +
                "            html += \"</div></div>\";\n" +
                "            div.innerHTML = html;\n" +
                "            document.body.insertBefore(div,document.body.firstChild);\n" +
                "            div.onclick = function() {\n" +
                "                var dropDownBox = document.getElementById(\"dropDownBox\").style.height;\n" +
                "                var mainButton = document.getElementById(\"mainButton\");\n" +
                "                var triangle = document.getElementById(\"triangle\");\n" +
                "                if(dropDownBox == \"0px\"){\n" +
                "                    mainButton.style.borderRadius = (iconFilletPercent*iconWidth)+\"px \"+(iconFilletPercent*iconWidth)+\"px 0 0\";\n" +
                "                    triangle.removeAttribute(\"style\");\n" +
                "                    triangle.setAttribute(\"style\",squareStyle);\n" +
                "                    document.getElementById(\"dropDownBox\").style.height = developMenuHeight+\"px\";\n" +
                "                }else {\n" +
                "                    document.getElementById(\"dropDownBox\").style.height = \"0px\";\n" +
                "                    triangle.removeAttribute(\"style\");\n" +
                "                    triangle.setAttribute(\"style\",triangleStyle);\n" +
                "                    mainButton.style.borderRadius = (iconFilletPercent*iconWidth)+\"px\";\n" +
                "                }\n" +
                "            };\n" +
                "            var elements = document.getElementsByClassName(\"spanStyle\");\n" +
                "            for(var j in elements){\n" +
                "                elements[j].onmouseover = function(){\n" +
                "                    this.style.background = secondColor;\n" +
                "                    this.style.color = mianColor;\n" +
                "                };\n" +
                "                elements[j].onmouseout = function(){\n" +
                "                    this.style.background = mianColor;\n" +
                "                    this.style.color = secondColor;\n" +
                "                };\n" +
                "                elements[j].onclick=function(){\n" +
                "                    var parseInterface = this.getAttribute(\"url\");\n" +
                "                    for(let key in classAndIDMap[uaLogo]){\n" +
                "                        if (document.location.href.match(key)) {\n" +
                "                            var values = classAndIDMap[uaLogo][key].split(\"|\");\n" +
                "                            var labelType = \"\";\n" +
                "                            var class_id = \"\";\n" +
                "                            for(let value in values){\n" +
                "                                if(document.getElementById(values[value])){\n" +
                "                                    class_id = values[value];\n" +
                "                                    labelType = \"id\";\n" +
                "                                    break;\n" +
                "                                }\n" +
                "                                if(document.getElementsByClassName(values[value]).length>0){\n" +
                "                                    class_id = values[value];\n" +
                "                                    labelType = \"class\";\n" +
                "                                    break;\n" +
                "                                }\n" +
                "                            }\n" +
                "                            if(labelType!=\"\"&&class_id!=\"\"){\n" +
                "                                var iframe = \"<iframe id='iframePlayBox' src='\"+parseInterface+document.location.href+\"' \"+IframeStyle+\" ></iframe>\";\n" +
                "                                if(labelType==\"id\"){\n" +
                "                                    document.getElementById(class_id).innerHTML=\"\";\n" +
                "                                    document.getElementById(class_id).innerHTML=iframe;\n" +
                "                                }else {\n" +
                "                                    document.getElementsByClassName(class_id)[0].innerHTML=\"\";\n" +
                "                                    if(uaLogo==\"mobile\"){\n" +
                "                                        document.getElementsByClassName(class_id)[0].style.height=\"225px\";\n" +
                "                                    }\n" +
                "                                    document.getElementsByClassName(class_id)[0].innerHTML=iframe;\n" +
                "                                }\n" +
                "                                return;\n" +
                "                            }\n" +
                "                        }\n" +
                "                    }\n" +
                "                    document.getElementById(\"dropDownBox\").style.display = \"none\";\n" +
                "                };\n" +
                "            }\n" +
                "        }catch(error){\n" +
                "            \n" +
                "        }\n" +
                "    }\n" +
                "})();\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void jxjk4(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "解析视频，启用后会破解爱奇艺等vip影视（需要手动点击网页出现的图标破解）");
        values.put("url", "*");
        values.put("js","/*\n" +
                " * @name: 视频解析\n" +
                " * @Author: 就像wo\n" +
                " * @version: 3.0\n" +
                " * @description: 视频解析，免费看VIP视频\n" +
                " * @include: *\n" +
                " * @createTime: 2019-12-22\n" +
                " * @updateTime: 2019-12-24\n" +
                "*/\n" +
                "(function(){\n" +
                "    var videoParseKey=encodeURIComponent(\"就像wo:视频解析\");\n" +
                "    if(window.videoParseKey==true){\n" +
                "        return 0;\n" +
                "    }\n" +
                "    window.videoParseKey=true;\n" +
                "    var videoParseUrl=[\n" +
                "        [\"https://z1.m1907.cn/?jx=\",\"云曦筛选\"],\n" +
                "        [\"https://jx.618g.com/?url=\",\"11111\"],\n" +
                "        [\"https://vip.bljiex.com/?v=\",\"BL解析\"],\n" +
                "        [\"https://jx.bwcxy.com/?v=\",\"初心视频\"],\n" +
                "        [\"http://miaomiaoai.cn/vip/play.html?url=\",\"MMA\"],\n" +
                "        [\"https://api.sigujx.com/?url=\",\"思古\"],\n" +
                "        [\"https://z1.m1907.cn/?jx=\",\"热心线路1\"],\n" +
                "        [\"http://jx.598110.com/v/1.php?url=\",\"热心线路2\"],\n" +
                "        [\"https://www.1717yun.com/jx/ty.php?url=\",\"1717云\"],\n" +
                "        [\"https://vip.jaoyun.com/index.php?url=\",\"简傲\"],\n" +
                "        [\"https://jx.618g.com/?url=\",\"百域\"],\n" +
                "        [\"https://www.myxin.top/jx/api/?url=\",\"黑米\"],\n" +
                "        [\"https://jiexi.071811.cc/jx.php?url=\",\"石云\"],\n" +
                "        [\"https://jx.wslmf.com/?url=\",\"凡凡\"],\n" +
                "        [\"https://jx.dy-jx.com/?url=\",\"高科技\"],\n" +
                "        [\"https://vip.mpos.ren/v/?url=\",\"人人\"],\n" +
                "        [\"https://jqaaa.com/jx.php?url=\",\"金桥\"],\n" +
                "        [\"https://jx.fo97.cn/?url=\",\"星空\"],\n" +
                "        [\"https://jx.ivito.cn/?url=\",\"维多\"],\n" +
                "        [\"https://api.927jx.com/vip/?url=\",\"927\"],\n" +
                "        [\"https://api.tv920.com/vip/?url=\",\"tv920\"],\n" +
                "        [\"https://www.ka61b.cn/jx.php?url=\",\"89\"],\n" +
                "        [\"https://api.lhh.la/vip/?url=\",\"豪华啦\"],\n" +
                "        [\"https://api.sumingys.com/index.php?url=\",\"宿命\"],\n" +
                "        [\"https://api.8bjx.cn/?url=\",\"8B\"],\n" +
                "        [\"https://v.qianyicp.com/v.php?url=\",\"千忆\"],\n" +
                "        [\"https://mcncn.cn/?url=\",\"梦城\"],\n" +
                "        [\"https://jx.f41.cc/?url=\",\"41\"],\n" +
                "        [\"https://www.ckmov.vip/api.php?url=\",\"ckmov\"],\n" +
                "        [\"https://jx.mw0.cc/?url=\",\"凉城\"],\n" +
                "        [\"https://www.33tn.cn/?url=\",\"33t\"],\n" +
                "        [\"https://jx.1ff1.cn/?url=\",\"爸比云\"],\n" +
                "        [\"https://jx.000180.top/jx/?url=\",\"180\"],\n" +
                "        [\"https://py.ha12.xyz/sos/index.php?url=\",\"ha12\"],\n" +
                "        [\"https://cdn.yangju.vip/k/?url=\",\"通用4\"],\n" +
                "        [\"\u202Ahttps://api.bbbbbb.me/jx/?url=\",\"通用7\"],\n" +
                "        [\"https://jx.598110.com/index.php?url=\",\"通用13\"],\n" +
                "        [\"https://www.kpezp.cn/jlexi.php?url=\",\"通用17\"],\n" +
                "        [\"\u202Ahttps://www.ckmov.vip/api.php?url=\",\"通用28\"],\n" +
                "        [\"https://cn.bjbanshan.cn/jx.php?url=\",\"通用29\"],\n" +
                "        [\"https://jx.mw0.cc/?url=\",\"通用30\"],\n" +
                "        [\"https://www.administratorw.com/video.php?url=\",\"通用35\"],\n" +
                "        [\"\u202Ahttps://jiexi.380k.com/?url=\",\"通用36\"],\n" +
                "        [\"https://okjx.cc/?url=\",\"通用38\"],\n" +
                "        [\"http://jx.ejiafarm.com/dy.php?url=\",\"通用39\"],\n" +
                "        [\"https://5.5252e.com/2/?url=\",\"通用40\"],\n" +
                "        [\"https://jx.51heixiazi.com/918jx/?url=\",\"通用41\"],\n" +
                "        [\"http://free.qtoo.net/?v=\",\"通用42\"],\n" +
                "        [\"http://api.ledboke.com/?url=\",\"通用43\"],\n" +
                "        [\"http://jx.daheiyun.com/?url=\",\"通用44\"]\n" +
                "    ];\n" +
                "    var videoParseCode=\"dmFyJTIwbWVkaWFQYXJzZURvY0JvZHk9KGRvY3VtZW50LmJvZHk9PW51bGw/ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50OmRvY3VtZW50LmJvZHkpO3ZhciUyMG1lZGlhUGFyc2VTaXplPSU3QiUyMnBoMnBjJTIyOmlubmVyV2lkdGgvMzYwLCUyMmZvbnQlMjI6MjQsJTIyYnRuJTIyOjEyJTdEO3ZhciUyMHZpZGVvUGFyc2VCdG49ZG9jdW1lbnQuY3JlYXRlRWxlbWVudCglMjJkaXYlMjIpO3ZpZGVvUGFyc2VCdG4uc3R5bGU9JTIycG9zaXRpb246Zml4ZWQ7ei1pbmRleDo5OTk5OTk5OTk5O3JpZ2h0OjIwJTI1O2JvdHRvbToxNSUyNTtib3gtc2l6aW5nOmJvcmRlci1ib3g7b3BhY2l0eToxO2JhY2tncm91bmQtc2l6ZTphdXRvJTIwMTAwJTI1O2JhY2tncm91bmQtcmVwZWF0Om5vLXJlcGVhdDtiYWNrZ3JvdW5kLXBvc2l0aW9uOmNlbnRlciUyMjt2aWRlb1BhcnNlQnRuLnN0eWxlLmJhY2tncm91bmRDb2xvcj0lMjJyZ2JhKDg4LDE0MSwxMzQsMC44KSUyMjt2aWRlb1BhcnNlQnRuLnN0eWxlLmJhY2tncm91bmRJbWFnZT0lMjJ1cmwoJTVDJTIyaHR0cHM6Ly9zczAuYmRzdGF0aWMuY29tLzcwY0Z1SFNoX1ExWW54R2twb1dLMUhGNmhoeS9pdC91PTQxMzIxMjA1OTAsMzg3NDM4MjIwNCZmbT0yNiZncD0wLmpwZyU1QyUyMiklMjI7dmlkZW9QYXJzZUJ0bi5zdHlsZS5ib3hTaGFkb3c9JTIyMHB4JTIwMHB4JTIwMTBweCUyMDVweCUyMHJnYmEoODgsMTQxLDEzNCwxKSUyMjt2aWRlb1BhcnNlQnRuLnN0eWxlLndpZHRoPW1lZGlhUGFyc2VTaXplLmJ0bislMjIlMjUlMjI7dmlkZW9QYXJzZUJ0bi5zdHlsZS5oZWlnaHQ9KGlubmVyV2lkdGgvaW5uZXJIZWlnaHQqbWVkaWFQYXJzZVNpemUuYnRuKSslMjIlMjUlMjI7dmlkZW9QYXJzZUJ0bi5zdHlsZS5ib3JkZXJSYWRpdXM9JTIyMTAwJTI1JTIyO3ZpZGVvUGFyc2VCdG4uYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMix2aWRlb1BhcnNlQ2xpY2spO3dpbmRvdy5hZGRFdmVudExpc3RlbmVyKCUyMnJlc2l6ZSUyMixmdW5jdGlvbigpJTdCdmlkZW9QYXJzZUJ0bi5zdHlsZS5oZWlnaHQ9aW5uZXJXaWR0aC9pbm5lckhlaWdodCptZWRpYVBhcnNlU2l6ZS5idG4rJTIyJTI1JTIyOyU3RCk7dmFyJTIwdmlkZW9QYXJzZUhpZFRpbWVJRDt2YXIlMjB2aWRlb1BhcnNlSGlkVGltZT0yMDt2YXIlMjB2aWRlb1BhcnNlQnRuT3BhY2l0eT0wLjE7aWYoZG9jdW1lbnQucmVhZHlTdGF0ZT09JTIyY29tcGxldGUlMjIpJTdCbWVkaWFTZWFyY2hWaWRlbygpOyU3RGVsc2UlN0J3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciglMjJsb2FkJTIyLGZ1bmN0aW9uKCklN0JtZWRpYVNlYXJjaFZpZGVvKCk7JTdEKTslN0RmdW5jdGlvbiUyMG1lZGlhU2VhcmNoVmlkZW8oKSU3QnZhciUyMHZpZGVvcz1kb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSglMjJWSURFTyUyMik7aWYodmlkZW9zLmxlbmd0aCUzRTApJTdCbWVkaWFQYXJzZURvY0JvZHkuYXBwZW5kQ2hpbGQodmlkZW9QYXJzZUJ0bik7JTdEZWxzZSU3QnNldFRpbWVvdXQobWVkaWFTZWFyY2hWaWRlbywxMDAwKTslN0QlN0RmdW5jdGlvbiUyMHZpZGVvUGFyc2VDbGljaygpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7aWYodmlkZW9QYXJzZUJ0bi5zdHlsZS5vcGFjaXR5PT12aWRlb1BhcnNlQnRuT3BhY2l0eSklN0J2aWRlb1BhcnNlQnRuLnN0eWxlLm9wYWNpdHk9MTt2aWRlb1BhcnNlSGlkVGltZUlEPXNldFRpbWVvdXQoZnVuY3Rpb24oKSU3QnZpZGVvUGFyc2VCdG4uc3R5bGUub3BhY2l0eT12aWRlb1BhcnNlQnRuT3BhY2l0eTslN0QsdmlkZW9QYXJzZUhpZFRpbWUqMTAwMCk7cmV0dXJuJTIwMDslN0RpZihtZWRpYVBhcnNlRG9jQm9keS5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbih2aWRlb1BhcnNlQmdkKSUyNTI9PTApJTdCbWVkaWFQYXJzZShmYWxzZSk7JTdEZWxzZSU3Qm1lZGlhUGFyc2UodHJ1ZSk7JTdEJTdEdmFyJTIwdmlkZW9QYXJzZUJnZD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMmRpdiUyMik7dmFyJTIwdmlkZW9QYXJzZURpdj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMmRpdiUyMik7dmFyJTIwdmlkZW9QYXJzZVVybERpdj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMmRpdiUyMik7dmlkZW9QYXJzZUJnZC5zdHlsZT0lMjJwb3NpdGlvbjpmaXhlZDt6LWluZGV4Ojk5OTk5OTk5OTt0b3A6MHB4O2xlZnQ6MHB4O2JveC1zaXppbmc6Ym9yZGVyLWJveDt3aWR0aDoxMDAlMjU7aGVpZ2h0OjEwMCUyNTtvdmVyZmxvdzphdXRvJTIyO3ZhciUyMHZpZGVvUGFyc2VTY3JvbGxUb3A9MDt2aWRlb1BhcnNlQmdkLmFkZEV2ZW50TGlzdGVuZXIoJTIyY2xpY2slMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO21lZGlhUGFyc2UoZmFsc2UpOyU3RCk7dmlkZW9QYXJzZURpdi5zdHlsZT0lMjJwb3NpdGlvbjpyZWxhdGl2ZTt0b3A6NjUlMjU7bGVmdDoxMCUyNTtib3gtc2l6aW5nOmluaGVyaXQ7d2lkdGg6ODAlMjU7JTIyO3ZpZGVvUGFyc2VEaXYuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7JTdEKTt2aWRlb1BhcnNlVXJsRGl2LnN0eWxlPSUyMmJveC1zaXppbmc6aW5oZXJpdDt3aWR0aDoxMDAlMjU7cGFkZGluZzo1JTI1O21hcmdpbjowcHg7Zm9udC13ZWlnaHQ6Ym9sZDt0ZXh0LWFsaWduOmNlbnRlcjtjb2xvcjojOWJhZmNjO2JhY2tncm91bmQtY29sb3I6IzI2MmYzZDtib3gtc2hhZG93OjBweCUyMDBweCUyMDEwcHglMjAjMDAwMDAwOyUyMjt2aWRlb1BhcnNlVXJsRGl2LnN0eWxlLmJvcmRlclJhZGl1cz1pbm5lcldpZHRoKjUvMTAwKyUyMnB4JTIyO3ZpZGVvUGFyc2VVcmxEaXYuc3R5bGUuZm9udFNpemU9bWVkaWFQYXJzZVNpemUucGgycGMqbWVkaWFQYXJzZVNpemUuZm9udCslMjJweCUyMjtmb3IobGV0JTIwaT0wO2klM0N2aWRlb1BhcnNlVXJsLmxlbmd0aDtpKyspJTdCbWFrVmlkZW9VcmxEaXYoaSk7JTdEdmlkZW9QYXJzZUJnZC5hcHBlbmRDaGlsZCh2aWRlb1BhcnNlRGl2KTtmdW5jdGlvbiUyMG1lZGlhUGFyc2UoYm9vbCklN0JpZihib29sKSU3Qm1lZGlhUGFyc2VEb2NCb2R5LmFwcGVuZENoaWxkKHZpZGVvUGFyc2VCZ2QpO3ZpZGVvUGFyc2VCZ2Quc2Nyb2xsQnkoMCx2aWRlb1BhcnNlU2Nyb2xsVG9wKTslN0RlbHNlJTdCdmlkZW9QYXJzZVNjcm9sbFRvcD12aWRlb1BhcnNlQmdkLnNjcm9sbFRvcDttZWRpYVBhcnNlRG9jQm9keS5yZW1vdmVDaGlsZCh2aWRlb1BhcnNlQmdkKTslN0QlN0RmdW5jdGlvbiUyMG1ha1ZpZGVvVXJsRGl2KGlkKSU3QmlmKHZpZGVvUGFyc2VVcmwlNUJpZCU1RC5sZW5ndGg9PTIpJTdCbGV0JTIwdmRpdj12aWRlb1BhcnNlVXJsRGl2LmNsb25lTm9kZSgpO3ZkaXYudGl0bGU9dmlkZW9QYXJzZVVybCU1QmlkJTVEJTVCMCU1RDt2ZGl2LmlubmVySFRNTD12aWRlb1BhcnNlVXJsJTVCaWQlNUQlNUIxJTVEO3ZkaXYuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCd2luZG93Lm9wZW4odGhpcy50aXRsZStsb2NhdGlvbi5ocmVmKTslN0QpO3ZpZGVvUGFyc2VEaXYuYXBwZW5kQ2hpbGQodmRpdik7JTdEJTdE\";\n" +
                "    var h5PlayerCode=\"KGZ1bmN0aW9uKCklN0J2YXIlMjBoNVBsYXllcktleT1lbmNvZGVVUklDb21wb25lbnQoJTIyJUU1JUIwJUIxJUU1JTgzJThGd286JUU4JUE3JTg2JUU5JUEyJTkxJUU2JTkyJUFEJUU2JTk0JUJFJUU1JTk5JUE4JTIyKTtpZih3aW5kb3cuaDVQbGF5ZXJLZXk9PXRydWUpJTdCcmV0dXJuJTIwMDslN0R3aW5kb3cuaDVQbGF5ZXJLZXk9dHJ1ZTt2YXIlMjBoNUNvbnRhaW5lcj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkRJViUyMik7dmFyJTIwaDVDb250cm9sc0JnZD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkRJViUyMik7dmFyJTIwaDVDb250cm9sc1RvcDt2YXIlMjBoNUJ1ZmZlckJhcj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkNBTlZBUyUyMik7dmFyJTIwaDVQcm9ncmVzc0Jhcjt2YXIlMjBoNVZpZGVvVGl0bGU7dmFyJTIwaDVQbGF5PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJTIyQ0FOVkFTJTIyKTt2YXIlMjBoNUNvbnRyb2xzPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJTIyRElWJTIyKTt2YXIlMjBoNUN1cnJlbnRUaW1lPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJTIyQlVUVE9OJTIyKTt2YXIlMjBoNUR1cmF0aW9uO3ZhciUyMGg1RXh0cmFWaWV3O3ZhciUyMGg1RnVsbHNjcmVlbj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCUyMkJVVFRPTiUyMik7dmFyJTIwaDVWb2x1bWVJbmM7dmFyJTIwaDVWb2x1bWVEZWM7aDVDb250YWluZXIuc3R5bGU9JTIycG9zaXRpb246YWJzb2x1dGU7ei1pbmRleDo5OTk5OTk5OTk5O2JveC1zaXppbmc6Ym9yZGVyLWJveDslMjI7aDVDb250cm9sc0JnZC5zdHlsZT0lMjJwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjBweDt0b3A6MHB4O3otaW5kZXg6OTk5OTk5OTk5OTtib3gtc2l6aW5nOmluaGVyaXQ7d2lkdGg6MTAwJTI1O2hlaWdodDoxMDAlMjU7JTIyO2g1QnVmZmVyQmFyLnN0eWxlPSUyMnBvc2l0aW9uOmFic29sdXRlO2xlZnQ6MHB4O3RvcDowcHg7ei1pbmRleDoxO2JveC1zaXppbmc6Ym9yZGVyLWJveDt3aWR0aDoxMDAlMjU7aGVpZ2h0OjEwJTI1OyUyMjtoNUJ1ZmZlckJhci53aWR0aD0zNjA7aDVCdWZmZXJCYXIuaGVpZ2h0PTMwO2Z1bmN0aW9uJTIwaDVQbGF5ZXJHZXRCdWZmZXIob2JqKSU3QnZhciUyMGN0eD1vYmouYnVmZmVyQmFyLmdldENvbnRleHQoJTIyMmQlMjIpO2N0eC5jbGVhclJlY3QoMCwwLDM2MCwzMCk7dmFyJTIwcmF0aW89MzYwL29iai52aWRlby5kdXJhdGlvbjt2YXIlMjByYW5nZT1vYmoudmlkZW8uYnVmZmVyZWQ7Y3R4LmJlZ2luUGF0aCgpO2Zvcih2YXIlMjBpPTA7aSUzQ3JhbmdlLmxlbmd0aDtpKyspJTdCY3R4Lm1vdmVUbyhNYXRoLmZsb29yKHJhbmdlLnN0YXJ0KGkpKnJhdGlvKSwxNSk7Y3R4LmxpbmVUbyhNYXRoLmNlaWwocmFuZ2UuZW5kKGkpKnJhdGlvKSwxNSk7JTdEY3R4LnN0cm9rZSgpOyU3RGg1UHJvZ3Jlc3NCYXI9aDVCdWZmZXJCYXIuY2xvbmVOb2RlKCk7aDVQcm9ncmVzc0Jhci5zdHlsZS56SW5kZXg9JTIyMiUyMjtmdW5jdGlvbiUyMGg1UGxheWVyR2V0UHJvZ3Jlc3Mob2JqLHRpbWUpJTdCdmFyJTIwY3R4PW9iai5wcm9ncmVzc0Jhci5nZXRDb250ZXh0KCUyMjJkJTIyKTtjdHguY2xlYXJSZWN0KDAsMCwzNjAsMzApO3ZhciUyMHJhdGlvPTM2MC9vYmoudmlkZW8uZHVyYXRpb247Y3R4LmJlZ2luUGF0aCgpO2N0eC5tb3ZlVG8oMCwxNSk7Y3R4LmxpbmVUbyhNYXRoLmZsb29yKHRpbWUqcmF0aW8pLDE1KTtjdHguc3Ryb2tlKCk7JTdEaDVQbGF5LnN0eWxlPSUyMnBvc2l0aW9uOmFic29sdXRlOyUyMjtoNVBsYXkud2lkdGg9NjAwO2g1UGxheS5oZWlnaHQ9NjAwO2Z1bmN0aW9uJTIwaDVQbGF5ZXJHZXRQbGF5KGN0eCklN0JjdHguZmlsbFN0eWxlPSUyMmJsYWNrJTIyO3ZhciUyMHk9MzAwLU1hdGguc3FydCgzKSo1MDtjdHguYmVnaW5QYXRoKCk7Y3R4Lm1vdmVUbyg0MDAsMzAwKTtjdHgubGluZVRvKDI1MCx5KTtjdHgubGluZVRvKDI1MCw2MDAteSk7Y3R4LmNsb3NlUGF0aCgpO2N0eC5maWxsKCk7JTdEZnVuY3Rpb24lMjBoNVBsYXllckdldFBhdXNlKGN0eCklN0JjdHguZmlsbFN0eWxlPSUyMmJsYWNrJTIyO2N0eC5maWxsUmVjdCgyMjUsMjEwLDUwLDE4MCk7Y3R4LmZpbGxSZWN0KDMyNSwyMTAsNTAsMTgwKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyR2V0V2FpdGluZyhvYmopJTdCdmFyJTIwY3R4PW9iai5wbGF5LmdldENvbnRleHQoJTIyMmQlMjIpO2N0eC5yZXN0b3JlKCk7Y3R4LnJvdGF0ZShNYXRoLlBJLzI0KTtjdHguY2xlYXJSZWN0KC0zMDAsLTMwMCw2MDAsNjAwKTtjdHguYmVnaW5QYXRoKCk7Y3R4LmFyYygwLDAsMjUwLDAsTWF0aC5QSS8yKTtjdHguc3Ryb2tlKCk7Y3R4LnNhdmUoKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyUGxheUNsaWNrKG9iaiklN0J2YXIlMjBjdHg9b2JqLnBsYXkuZ2V0Q29udGV4dCglMjIyZCUyMik7Y3R4LmZpbGxTdHlsZT0lMjIjZmZmZmZmY2MlMjI7Y3R4LnJlc2V0VHJhbnNmb3JtKCk7Y3R4LmNsZWFyUmVjdCgwLDAsNjAwLDYwMCk7Y3R4LmJlZ2luUGF0aCgpO2N0eC5hcmMoMzAwLDMwMCwzMDAsMCwyKk1hdGguUEkpO2N0eC5maWxsKCk7aWYoIW9iai52aWRlby5wYXVzZWQpJTdCaDVQbGF5ZXJHZXRQYXVzZShjdHgpOyU3RGVsc2UlN0JoNVBsYXllckdldFBsYXkoY3R4KTslN0QlN0RoNUNvbnRyb2xzLnN0eWxlPSUyMnBvc2l0aW9uOmFic29sdXRlO2xlZnQ6MHB4O2JvdHRvbTowcHg7ei1pbmRleDo5OTk5OTk5OTk5O2JveC1zaXppbmc6aW5oZXJpdDtib3gtc2hhZG93OjBweCUyMDBweCUyMDEwcHglMjAjMDAwMDAwO3dpZHRoOjEwMCUyNTtwYWRkaW5nOjBweCUyMDIlMjU7bWFyZ2luOjBweDtiYWNrZ3JvdW5kLWNvbG9yOmJsYWNrOyUyMjtoNUNvbnRyb2xzVG9wPWg1Q29udHJvbHMuY2xvbmVOb2RlKCk7aDVDb250cm9sc1RvcC5zdHlsZS50b3A9JTIyMHB4JTIyO2g1Q29udHJvbHNUb3Auc3R5bGUucGFkZGluZz0lMjIwcHglMjI7aDVDdXJyZW50VGltZS5zdHlsZT0lMjJwb3NpdGlvbjpyZWxhdGl2ZTt6LWluZGV4OjA7ZmxvYXQ6bGVmdDtib3gtc2l6aW5nOmluaGVyaXQ7d2lkdGg6MTUlMjU7aGVpZ2h0OjEwMCUyNTtib3JkZXItc3R5bGU6bm9uZTttYXJnaW4tcmlnaHQ6MSUyNTtiYWNrZ3JvdW5kLWNvbG9yOiMwMDAwMDAwMDtvdmVyZmxvdzphdXRvO3RleHQtYWxpZ246Y2VudGVyO3doaXRlLXNwYWNlOm5vd3JhcDtmb250LXdlaWdodDpib2xkO2NvbG9yOndoaXRlOyUyMjtoNVZpZGVvVGl0bGU9aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtoNVZpZGVvVGl0bGUuc3R5bGUud2lkdGg9JTIyMTAwJTI1JTIyO2g1VmlkZW9UaXRsZS5zdHlsZS56SW5kZXg9JTIyMiUyMjtoNUR1cmF0aW9uPWg1Q3VycmVudFRpbWUuY2xvbmVOb2RlKCk7aDVFeHRyYVZpZXc9aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtoNUZ1bGxzY3JlZW49aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtoNUZ1bGxzY3JlZW4uc3R5bGUubWFyZ2luUmlnaHQ9JTIyMHB4JTIyO2g1RnVsbHNjcmVlbi5zdHlsZS5tYXJnaW5MZWZ0PSUyMjElMjUlMjI7aDVGdWxsc2NyZWVuLnN0eWxlLmNzc0Zsb2F0PSUyMnJpZ2h0JTIyO2g1RnVsbHNjcmVlbi5pbm5lckhUTUw9JTIyJUU1JTg1JUE4JUU1JUIxJThGJTIyO2Z1bmN0aW9uJTIwaDVQbGF5ZXJGdWxsc2NyZWVuQ2xpY2soY29udGFpbmVyKSU3QmlmKGRvY3VtZW50LmZ1bGxzY3JlZW5FbGVtZW50PT1udWxsKSU3QmNvbnRhaW5lci5yZXF1ZXN0RnVsbHNjcmVlbigpOyU3RGVsc2UlN0Jkb2N1bWVudC5leGl0RnVsbHNjcmVlbigpOyU3RCU3RGg1Vm9sdW1lSW5jPWg1RnVsbHNjcmVlbi5jbG9uZU5vZGUoKTtoNVZvbHVtZUluYy5pbm5lckhUTUw9JTIyJUU5JTlGJUIzJUU5JTg3JThGKyUyMjtoNVZvbHVtZURlYz1oNUZ1bGxzY3JlZW4uY2xvbmVOb2RlKCk7aDVWb2x1bWVEZWMuaW5uZXJIVE1MPSUyMiVFOSU5RiVCMyVFOSU4NyU4Ri0lMjI7dmFyJTIwaDVQbGF5ZXJWaWRlb0Fycj0lNUIlNUQ7dmFyJTIwaDVQbGF5ZXJJZnJhbWVBcnI9JTVCJTVEO3ZhciUyMGg1UGxheWVyVGltZUlEPSU1QiU1RDtpZihkb2N1bWVudC5yZWFkeVN0YXRlPT0lMjJjb21wbGV0ZSUyMiklN0JoNVBsYXllcihkb2N1bWVudCwwKTslN0RlbHNlJTdCd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJTIybG9hZCUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXIoZG9jdW1lbnQsMCk7JTdEKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyQ2xhc3MoKSU3QnZhciUyMG9iaj1uZXclMjBPYmplY3Q7b2JqLmhhdmVDb250cm9scztvYmoucGFyZW50WkluZGV4O29iai5wYXJlbnRQb3NpdGlvbjtvYmoudmlkZW9XaWR0aDtvYmoudmlkZW9IZWlnaHQ7b2JqLmZvcndhcmRUaW1lPTA7b2JqLmZvcndhcmRWb2x1bWU9MTtvYmouZm9yd2FyZFJhdGU9MTtvYmoudmlkZW9SYXRpbz0wO29iai5zdGF0ZT0lMjJwYXVzZWQlMjI7b2JqLmhpZGRlblRpbWVJRDtvYmoud2FpdFRpbWVJRDtvYmoucGFyZW50O29iai52aWRlbztvYmouY29udGFpbmVyPWg1Q29udGFpbmVyLmNsb25lTm9kZSgpO29iai5jb250cm9sc0JnZD1oNUNvbnRyb2xzQmdkLmNsb25lTm9kZSgpO29iai5jb250cm9sc1RvcD1oNUNvbnRyb2xzVG9wLmNsb25lTm9kZSgpO29iai5idWZmZXJCYXI9aDVCdWZmZXJCYXIuY2xvbmVOb2RlKCk7b2JqLnByb2dyZXNzQmFyPWg1UHJvZ3Jlc3NCYXIuY2xvbmVOb2RlKCk7b2JqLnZpZGVvVGl0bGU9aDVWaWRlb1RpdGxlLmNsb25lTm9kZSgpO29iai5wbGF5PWg1UGxheS5jbG9uZU5vZGUoKTtvYmouY29udHJvbHM9aDVDb250cm9scy5jbG9uZU5vZGUoKTtvYmouY3VycmVudFRpbWU9aDVDdXJyZW50VGltZS5jbG9uZU5vZGUoKTtvYmouZHVyYXRpb249aDVEdXJhdGlvbi5jbG9uZU5vZGUoKTtvYmouZXh0cmFWaWV3PWg1RXh0cmFWaWV3LmNsb25lTm9kZSgpO29iai5mdWxsc2NyZWVuPWg1RnVsbHNjcmVlbi5jbG9uZU5vZGUodHJ1ZSk7b2JqLnZvbHVtZUluYz1oNVZvbHVtZUluYy5jbG9uZU5vZGUodHJ1ZSk7b2JqLnZvbHVtZURlYz1oNVZvbHVtZURlYy5jbG9uZU5vZGUodHJ1ZSk7b2JqLm9mZnNldFg9MDtvYmoub2Zmc2V0WT0wO29iai53aWR0aD0wO29iai5oZWlnaHQ9MTtvYmouc2l6ZUZ1bmM9ZnVuY3Rpb24oKSU3Qm9iai5wYXJlbnRQb3NpdGlvbj13aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShvYmoucGFyZW50LG51bGwpLnBvc2l0aW9uO29iai5wYXJlbnRaSW5kZXg9d2luZG93LmdldENvbXB1dGVkU3R5bGUob2JqLnBhcmVudCxudWxsKS56SW5kZXg7b2JqLnZpZGVvV2lkdGg9d2luZG93LmdldENvbXB1dGVkU3R5bGUob2JqLnZpZGVvLG51bGwpLndpZHRoO29iai52aWRlb0hlaWdodD13aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShvYmoucGFyZW50LG51bGwpLmhlaWdodDtpZihvYmoucGFyZW50UG9zaXRpb249PSUyMnN0YXRpYyUyMiklN0JvYmoucGFyZW50LnN0eWxlLnBvc2l0aW9uPSUyMnJlbGF0aXZlJTIyOyU3RG9iai5wYXJlbnQuc3R5bGUuekluZGV4PSUyMjk5OTk5OTk5OTklMjI7b2JqLnZpZGVvLnN0eWxlLndpZHRoPSUyMjEwMCUyNSUyMjtvYmoudmlkZW8uc3R5bGUuaGVpZ2h0PSUyMjEwMCUyNSUyMjtvYmoudmlkZW9SYXRpbz1vYmoudmlkZW8udmlkZW9IZWlnaHQvb2JqLnZpZGVvLnZpZGVvV2lkdGg7dmFyJTIwJTIwYWRkSGVpZ2h0PW9iai5yZXNpemVGdW5jKCk7dmFyJTIwaGVpZ2h0PShvYmouaGVpZ2h0K2FkZEhlaWdodCklM0VvYmoudmlkZW8ub2Zmc2V0SGVpZ2h0P29iai52aWRlby5vZmZzZXRIZWlnaHQ6b2JqLmhlaWdodCthZGRIZWlnaHQ7b2JqLmNvbnRhaW5lci5zdHlsZS5sZWZ0PShvYmoudmlkZW8ub2Zmc2V0TGVmdCsob2JqLnZpZGVvLm9mZnNldFdpZHRoLW9iai53aWR0aCkvMikvb2JqLnBhcmVudC5jbGllbnRXaWR0aCoxMDArJTIyJTI1JTIyO29iai5jb250YWluZXIuc3R5bGUudG9wPShvYmoudmlkZW8ub2Zmc2V0VG9wKyhvYmoudmlkZW8ub2Zmc2V0SGVpZ2h0LWhlaWdodCkvMikvb2JqLnBhcmVudC5jbGllbnRIZWlnaHQqMTAwKyUyMiUyNSUyMjtvYmouY29udGFpbmVyLnN0eWxlLndpZHRoPW9iai53aWR0aC9vYmoucGFyZW50LmNsaWVudFdpZHRoKjEwMCslMjIlMjUlMjI7b2JqLmNvbnRhaW5lci5zdHlsZS5oZWlnaHQ9aGVpZ2h0L29iai5wYXJlbnQuY2xpZW50SGVpZ2h0KjEwMCslMjIlMjUlMjI7b2JqLnBsYXkuc3R5bGUudG9wPShoZWlnaHQtYWRkSGVpZ2h0KS8yKyUyMnB4JTIyO29iai5wbGF5LnN0eWxlLmxlZnQ9KG9iai53aWR0aC1hZGRIZWlnaHQpLzIrJTIycHglMjI7Zm9yKHZhciUyMGVsZT1vYmoudmlkZW87ZWxlIT1udWxsO2VsZT1lbGUub2Zmc2V0UGFyZW50KSU3Qm9iai5vZmZzZXRYKz1lbGUub2Zmc2V0TGVmdCtlbGUuY2xpZW50TGVmdDtvYmoub2Zmc2V0WSs9ZWxlLm9mZnNldFRvcCtlbGUuY2xpZW50VG9wOyU3RCU3RDtvYmouZm9udFJlc2l6ZUVsZT0lNUJvYmoudmlkZW9UaXRsZSxvYmouY3VycmVudFRpbWUsb2JqLmR1cmF0aW9uLG9iai5leHRyYVZpZXcsb2JqLmZ1bGxzY3JlZW4sb2JqLnZvbHVtZUluYyxvYmoudm9sdW1lRGVjJTVEO29iai5yZXNpemVGdW5jPWZ1bmN0aW9uKCklN0JpZihvYmoudmlkZW8ub2Zmc2V0SGVpZ2h0L29iai52aWRlby5vZmZzZXRXaWR0aCUzQ29iai52aWRlb1JhdGlvKSU3Qm9iai5oZWlnaHQ9b2JqLnZpZGVvLm9mZnNldEhlaWdodDtvYmoud2lkdGg9b2JqLmhlaWdodC9vYmoudmlkZW9SYXRpbzslN0RlbHNlJTdCb2JqLndpZHRoPW9iai52aWRlby5vZmZzZXRXaWR0aDtvYmouaGVpZ2h0PW9iai52aWRlb1JhdGlvKm9iai53aWR0aDslN0R2YXIlMjBoZWlnaHQ9b2JqLnZpZGVvUmF0aW8lM0UxP29iai53aWR0aDpvYmouaGVpZ2h0O29iai5jb250cm9sc1RvcC5zdHlsZS5oZWlnaHQ9aGVpZ2h0KjMvMjArJTIycHglMjI7b2JqLnBsYXkuc3R5bGUud2lkdGg9aGVpZ2h0KjMvMTArJTIycHglMjI7b2JqLnBsYXkuc3R5bGUuaGVpZ2h0PW9iai5wbGF5LnN0eWxlLndpZHRoO29iai5wbGF5LnN0eWxlLnRvcD0ob2JqLnZpZGVvLm9mZnNldEhlaWdodC1oZWlnaHQqMy8xMCkvMislMjJweCUyMjtvYmoucGxheS5zdHlsZS5sZWZ0PShvYmoudmlkZW8ub2Zmc2V0V2lkdGgtaGVpZ2h0KjMvMTApLzIrJTIycHglMjI7b2JqLmNvbnRyb2xzLnN0eWxlLmhlaWdodD1vYmouY29udHJvbHNUb3Auc3R5bGUuaGVpZ2h0O2Zvcih2YXIlMjBpPTA7aSUzQ29iai5mb250UmVzaXplRWxlLmxlbmd0aDtpKyspJTdCb2JqLmZvbnRSZXNpemVFbGUlNUJpJTVELnN0eWxlLmZvbnRTaXplPShvYmoudmlkZW9SYXRpbyUzRTE/aGVpZ2h0LzM2OmhlaWdodC8yMCkrJTIycHglMjI7JTdEcmV0dXJuJTIwaGVpZ2h0KjMvMTA7JTdEO29iai52YWx1ZUZ1bmM9ZnVuY3Rpb24oZG9jKSU3Qm9iai52aWRlb1RpdGxlLmlubmVySFRNTD0oZG9jLnRpdGxlPT0lMjIlMjIlN0MlN0NoNVBsYXllclZpZGVvQXJyLmxlbmd0aCUzRTEpP29iai52aWRlby5jdXJyZW50U3JjOmRvYy50aXRsZTtvYmouY3VycmVudFRpbWUuaW5uZXJIVE1MPWg1UGxheWVyRm9ybWF0VGltZShvYmoudmlkZW8uY3VycmVudFRpbWUpO29iai5kdXJhdGlvbi5pbm5lckhUTUw9aXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKT9oNVBsYXllckZvcm1hdFRpbWUob2JqLnZpZGVvLmR1cmF0aW9uKTolMjJMSVZFJTIyO3ZhciUyMGN0eD1vYmouYnVmZmVyQmFyLmdldENvbnRleHQoJTIyMmQlMjIpO2N0eC5zdHJva2VTdHlsZT0lMjIjODg4ODg4JTIyO2N0eC5saW5lV2lkdGg9MzA7Y3R4PW9iai5wcm9ncmVzc0Jhci5nZXRDb250ZXh0KCUyMjJkJTIyKTtjdHguc3Ryb2tlU3R5bGU9JTIyd2hpdGUlMjI7Y3R4LmxpbmVXaWR0aD0zMDtjdHg9b2JqLnBsYXkuZ2V0Q29udGV4dCglMjIyZCUyMik7Y3R4LnRyYW5zbGF0ZSgzMDAsMzAwKTt2YXIlMjBncmFkaWVudD1jdHguY3JlYXRlTGluZWFyR3JhZGllbnQoMCwwLDAsMzAwKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMCwlMjJ3aGl0ZSUyMik7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDAuMSwlMjJyZWQlMjIpO2dyYWRpZW50LmFkZENvbG9yU3RvcCgwLjIsJTIyb3JhbmdlJTIyKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMC4zLCUyMnllbGxvdyUyMik7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDAuNCwlMjJncmVlbiUyMik7Z3JhZGllbnQuYWRkQ29sb3JTdG9wKDAuNiwlMjJjeWFuJTIyKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMC43LCUyMmJsdWUlMjIpO2dyYWRpZW50LmFkZENvbG9yU3RvcCgwLjksJTIycHVycGxlJTIyKTtncmFkaWVudC5hZGRDb2xvclN0b3AoMSwlMjJibGFjayUyMik7Y3R4LmZpbGxTdHlsZT0lMjIjZmZmZmZmY2MlMjI7Y3R4LnN0cm9rZVN0eWxlPWdyYWRpZW50O2N0eC5saW5lV2lkdGg9MTAwO2N0eC5saW5lQ2FwPSUyMnJvdW5kJTIyO2N0eC5zYXZlKCk7JTdEO29iai5maXRUb2dldGhlcj1mdW5jdGlvbihkb2MpJTdCb2JqLnNpemVGdW5jKCk7b2JqLnZhbHVlRnVuYyhkb2MpO29iai5wYXJlbnQuaW5zZXJ0QmVmb3JlKG9iai5jb250YWluZXIsb2JqLnZpZGVvKTtvYmouY29udGFpbmVyLmFwcGVuZENoaWxkKG9iai52aWRlbyk7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai5idWZmZXJCYXIpO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmoucHJvZ3Jlc3NCYXIpO29iai5jb250cm9sc1RvcC5hcHBlbmRDaGlsZChvYmoudmlkZW9UaXRsZSk7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai5jdXJyZW50VGltZSk7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai5kdXJhdGlvbik7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai5leHRyYVZpZXcpO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmouZnVsbHNjcmVlbik7b2JqLmNvbnRyb2xzLmFwcGVuZENoaWxkKG9iai52b2x1bWVJbmMpO29iai5jb250cm9scy5hcHBlbmRDaGlsZChvYmoudm9sdW1lRGVjKTtvYmouY29udHJvbHNCZ2QuYXBwZW5kQ2hpbGQob2JqLmNvbnRyb2xzVG9wKTtvYmouY29udHJvbHNCZ2QuYXBwZW5kQ2hpbGQob2JqLnBsYXkpO29iai5jb250cm9sc0JnZC5hcHBlbmRDaGlsZChvYmouY29udHJvbHMpO29iai5jb250YWluZXIuYXBwZW5kQ2hpbGQob2JqLmNvbnRyb2xzQmdkKTtoNVBsYXllclBsYXlDbGljayhvYmopOyU3RDtyZXR1cm4lMjBvYmo7JTdEZnVuY3Rpb24lMjBoNVBsYXllclJlbW92ZUNvbnRyb2xzKG9iaixib29sKSU3QmNsZWFyVGltZW91dChvYmouaGlkZGVuVGltZUlEKTtpZihib29sKSU3Qm9iai5jb250cm9sc0JnZC5yZW1vdmVDaGlsZChvYmouY29udHJvbHNUb3ApO29iai5jb250cm9sc0JnZC5yZW1vdmVDaGlsZChvYmoucGxheSk7b2JqLmNvbnRyb2xzQmdkLnJlbW92ZUNoaWxkKG9iai5jb250cm9scyk7JTdEZWxzZSU3Qm9iai5jb250cm9sc0JnZC5hcHBlbmRDaGlsZChvYmouY29udHJvbHNUb3ApO29iai5jb250cm9sc0JnZC5hcHBlbmRDaGlsZChvYmoucGxheSk7b2JqLmNvbnRyb2xzQmdkLmFwcGVuZENoaWxkKG9iai5jb250cm9scyk7JTdEJTdEZnVuY3Rpb24lMjBoNVBsYXllckhpZGRlbihvYmopJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7aDVQbGF5ZXJSZW1vdmVDb250cm9scyhvYmosZmFsc2UpO29iai5oaWRkZW5UaW1lSUQ9c2V0VGltZW91dChmdW5jdGlvbigpJTdCaDVQbGF5ZXJSZW1vdmVDb250cm9scyhvYmosdHJ1ZSk7JTdELDEwMDAwKTslN0R2YXIlMjBoNVBsYXllclNoYWtlPSU3QiUyMmNhbmFibGUlMjI6dHJ1ZSwlMjJmdW5jJTIyOmZ1bmN0aW9uKCklN0JpZihoNVBsYXllclNoYWtlLmNhbmFibGUpJTdCaWYoZXZlbnQuYWNjZWxlcmF0aW9uLnglM0UzMCU3QyU3Q2V2ZW50LmFjY2VsZXJhdGlvbi55JTNFMzAlN0MlN0NldmVudC5hY2NlbGVyYXRpb24ueiUzRTMwKSU3Qmg1UGxheWVyU2hha2UuY2FuYWJsZT1mYWxzZTtzZXRUaW1lb3V0KGZ1bmN0aW9uKCklN0JoNVBsYXllclNoYWtlLmNhbmFibGU9dHJ1ZTslN0QsMTAwMCk7aWYoaDVQbGF5ZXJWaWRlb0Fyci5sZW5ndGglM0UwJTdDJTdDaDVQbGF5ZXJUaW1lSUQubGVuZ3RoJTNFMCklN0JoNVBsYXllclJlbW92ZSgpOyU3RGVsc2UlN0JoNVBsYXllcihkb2N1bWVudCwwKTslN0QlN0QlN0QlN0QlN0Q7ZnVuY3Rpb24lMjBoNVBsYXllclJlbW92ZSgpJTdCZm9yKHZhciUyMGk9MDtpJTNDaDVQbGF5ZXJUaW1lSUQubGVuZ3RoO2krKyklN0JjbGVhclRpbWVvdXQoaDVQbGF5ZXJUaW1lSUQlNUJpJTVEKTslN0Rmb3IodmFyJTIwaT0wO2klM0NoNVBsYXllclZpZGVvQXJyLmxlbmd0aDtpKyspJTdCdmFyJTIwb2JqPWg1UGxheWVyVmlkZW9BcnIlNUJpJTVEO29iai5wYXJlbnQuaW5zZXJ0QmVmb3JlKG9iai52aWRlbyxvYmouY29udGFpbmVyKTtvYmoucGFyZW50LnJlbW92ZUNoaWxkKG9iai5jb250YWluZXIpO29iai5wYXJlbnQuc3R5bGUucG9zaXRpb249b2JqLnBhcmVudFBvc2l0aW9uO29iai5wYXJlbnQuc3R5bGUuekluZGV4PW9iai5wYXJlbnRaSW5kZXg7b2JqLnZpZGVvLnN0eWxlLndpZHRoPW9iai52aWRlb1dpZHRoO29iai52aWRlby5zdHlsZS5oZWlnaHQ9b2JqLnZpZGVvSGVpZ2h0O29iai52aWRlby5jb250cm9scz1vYmouaGF2ZUNvbnRyb2xzOyU3RGg1UGxheWVyVmlkZW9BcnI9JTVCJTVEO2g1UGxheWVySWZyYW1lQXJyPSU1QiU1RDtoNVBsYXllclRpbWVJRD0lNUIlNUQ7JTdEdmFyJTIwaDVQbGF5ZXJSZXNpemU9JTdCJTIydGltZXIlMjI6MCwlMjJmdW5jJTIyOmZ1bmN0aW9uKCklN0JjbGVhclRpbWVvdXQodGhpcy50aW1lcik7dGhpcy50aW1lcj1zZXRUaW1lb3V0KGZ1bmN0aW9uKCklN0Jmb3IodmFyJTIwaT0wO2klM0NoNVBsYXllclZpZGVvQXJyLmxlbmd0aDtpKyspJTdCaDVQbGF5ZXJWaWRlb0FyciU1QmklNUQucmVzaXplRnVuYyh0cnVlKTslN0QlN0QsMzAwKTslN0QlN0Q7d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJTIyZGV2aWNlbW90aW9uJTIyLGg1UGxheWVyU2hha2UuZnVuYyk7d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJTIycmVzaXplJTIyLGg1UGxheWVyUmVzaXplLmZ1bmMpO2Z1bmN0aW9uJTIwaDVQbGF5ZXIoZG9jLGRlcHRoKSU3QnZhciUyMHZpZGVvcz1kb2MuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJTIyVklERU8lMjIpO3ZhciUyMGlmcmFzPWRvYy5nZXRFbGVtZW50c0J5VGFnTmFtZSglMjJJRlJBTUUlMjIpO3ZhciUyMGhhdmVIaWRkZW49dmlkZW9zLmxlbmd0aCUzRTA/ZmFsc2U6dHJ1ZTtmb3IodmFyJTIwaT0wO2klM0N2aWRlb3MubGVuZ3RoO2krKyklN0J2YXIlMjBib29sPXRydWU7Zm9yKHZhciUyMGo9MDtqJTNDaDVQbGF5ZXJWaWRlb0Fyci5sZW5ndGg7aisrKSU3QmlmKHZpZGVvcyU1QmklNUQuaXNTYW1lTm9kZShoNVBsYXllclZpZGVvQXJyJTVCaiU1RC52aWRlbykpJTdCYm9vbD1mYWxzZTticmVhazslN0QlN0RpZihib29sKSU3QmlmKCFpc0g1UGxheWVySGlkZGVuKHZpZGVvcyU1QmklNUQpJiZ2aWRlb3MlNUJpJTVELnJlYWR5U3RhdGUlM0UwKSU3QmlmKHZpZGVvcyU1QmklNUQudmlkZW9XaWR0aCp2aWRlb3MlNUJpJTVELnZpZGVvSGVpZ2h0PT0wKSU3QnZpZGVvcyU1QmklNUQucHJlbG9hZD0lMjJtZXRhZGF0YSUyMjtoYXZlSGlkZGVuPXRydWU7JTdEZWxzZSU3QmFkZEg1UGxheWVyKHZpZGVvcyU1QmklNUQsZG9jKTslN0QlN0RlbHNlJTdCaGF2ZUhpZGRlbj10cnVlOyU3RCU3RCU3RGZvcih2YXIlMjBpPTA7aSUzQ2lmcmFzLmxlbmd0aDtpKyspJTdCdmFyJTIwYm9vbD10cnVlO2Zvcih2YXIlMjBqPTA7aiUzQ2g1UGxheWVySWZyYW1lQXJyLmxlbmd0aDtqKyspJTdCaWYoaWZyYXMlNUJpJTVELmlzU2FtZU5vZGUoaDVQbGF5ZXJJZnJhbWVBcnIlNUJqJTVEKSklN0Jib29sPWZhbHNlO2JyZWFrOyU3RCU3RGlmKGJvb2wpJTdCaWYoIWlzSDVQbGF5ZXJIaWRkZW4oaWZyYXMlNUJpJTVEKSklN0JpZnJhRG9jPWlmcmFzJTVCaSU1RC5jb250ZW50RG9jdW1lbnQ7aWYoaWZyYURvYyE9bnVsbCklN0JoNVBsYXllcklmcmFtZUFyciU1Qmg1UGxheWVySWZyYW1lQXJyLmxlbmd0aCU1RD1pZnJhcyU1QmklNUQ7aWYoaWZyYURvYy5yZWFkeVN0YXRlPT0lMjJjb21wbGV0ZSUyMiklN0JoNVBsYXllcihpZnJhRG9jLGRlcHRoKzEpOyU3RGVsc2UlN0JpZnJhcyU1QmklNUQuYWRkRXZlbnRMaXN0ZW5lciglMjJsb2FkJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllcihpZnJhRG9jLGRlcHRoKzEpOyU3RCk7JTdEJTdEJTdEZWxzZSU3QmhhdmVIaWRkZW49dHJ1ZTslN0QlN0QlN0RpZihoYXZlSGlkZGVuKSU3Qmg1UGxheWVyVGltZUlEJTVCZGVwdGglNUQ9c2V0VGltZW91dChoNVBsYXllciwxMDAwLGRvYyxkZXB0aCk7JTdEJTdEZnVuY3Rpb24lMjBhZGRINVBsYXllcih2aWRlbyxkb2MpJTdCdmFyJTIwb2JqPWg1UGxheWVyQ2xhc3MoKTtvYmouaGF2ZUNvbnRyb2xzPXZpZGVvLmNvbnRyb2xzO3ZpZGVvLmNvbnRyb2xzPWZhbHNlO29iai52aWRlbz12aWRlbztvYmoucGFyZW50PXZpZGVvLnBhcmVudE5vZGU7b2JqLmZpdFRvZ2V0aGVyKGRvYyk7aDVQbGF5ZXJWaWRlb0FyciU1Qmg1UGxheWVyVmlkZW9BcnIubGVuZ3RoJTVEPW9iajtvYmouY29udHJvbHNCZ2QuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7aWYob2JqLnZpZGVvLnBhdXNlZCklN0JvYmoudmlkZW8ucGxheSgpOyU3RGVsc2UlN0JpZihvYmouY29udHJvbHNCZ2QuY29tcGFyZURvY3VtZW50UG9zaXRpb24ob2JqLnBsYXkpJTI1Mj09MCklN0JoNVBsYXllclJlbW92ZUNvbnRyb2xzKG9iaix0cnVlKTslN0RlbHNlJTdCaDVQbGF5ZXJIaWRkZW4ob2JqKTslN0QlN0QlN0QpO29iai5jb250cm9sc0JnZC5hZGRFdmVudExpc3RlbmVyKCUyMmRibGNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTtoNVBsYXllckhpZGRlbihvYmopO2g1UGxheWVyRnVsbHNjcmVlbkNsaWNrKG9iai5jb250YWluZXIpOyU3RCk7b2JqLmNvbnRyb2xzQmdkLmFkZEV2ZW50TGlzdGVuZXIoJTIydG91Y2hzdGFydCUyMixmdW5jdGlvbigpJTdCZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7aDVQbGF5ZXJTbGlkZVN0YXJ0KCk7JTdEKTtvYmouY29udHJvbHNCZ2QuYWRkRXZlbnRMaXN0ZW5lciglMjJ0b3VjaG1vdmUlMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpOyU3RCk7b2JqLmNvbnRyb2xzQmdkLmFkZEV2ZW50TGlzdGVuZXIoJTIydG91Y2hlbmQlMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2g1UGxheWVyU2xpZGVFbmQob2JqKTslN0QpO29iai5jb250cm9sc1RvcC5hZGRFdmVudExpc3RlbmVyKCUyMmNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9sc1RvcC5hZGRFdmVudExpc3RlbmVyKCUyMmRibGNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9sc1RvcC5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoc3RhcnQlMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2NsZWFyVGltZW91dChvYmouaGlkZGVuVGltZUlEKTslN0QpO29iai5jb250cm9sc1RvcC5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoZW5kJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopOyU3RCk7b2JqLnZpZGVvVGl0bGUuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCaWYoaXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKSklN0JvYmoudmlkZW8uY3VycmVudFRpbWU9ZXZlbnQub2Zmc2V0WC90aGlzLm9mZnNldFdpZHRoKm9iai52aWRlby5kdXJhdGlvbjslN0QlN0QpO29iai5wbGF5LmFkZEV2ZW50TGlzdGVuZXIoJTIyY2xpY2slMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2lmKG9iai52aWRlby5wYXVzZWQpJTdCb2JqLnZpZGVvLnBsYXkoKTslN0RlbHNlJTdCb2JqLnZpZGVvLnBhdXNlKCk7JTdEJTdEKTtvYmoucGxheS5hZGRFdmVudExpc3RlbmVyKCUyMmRibGNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9scy5hZGRFdmVudExpc3RlbmVyKCUyMmNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9scy5hZGRFdmVudExpc3RlbmVyKCUyMmRibGNsaWNrJTIyLGZ1bmN0aW9uKCklN0JldmVudC5zdG9wUHJvcGFnYXRpb24oKTslN0QpO29iai5jb250cm9scy5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoc3RhcnQlMjIsZnVuY3Rpb24oKSU3QmV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO2NsZWFyVGltZW91dChvYmouaGlkZGVuVGltZUlEKTtpZihpc0Zpbml0ZShvYmoudmlkZW8uZHVyYXRpb24pKSU3Qmg1UGxheWVyU2xpZGVTdGFydCgpOyU3RCU3RCk7b2JqLmNvbnRyb2xzLmFkZEV2ZW50TGlzdGVuZXIoJTIydG91Y2htb3ZlJTIyLGZ1bmN0aW9uKCklN0JpZihpc0Zpbml0ZShvYmoudmlkZW8uZHVyYXRpb24pKSU3Qm9iai52aWRlby5wYXVzZSgpO2g1UGxheWVyU2xpZGVDb250cm9sc01vdmUob2JqKTslN0QlN0QpO29iai5jb250cm9scy5hZGRFdmVudExpc3RlbmVyKCUyMnRvdWNoZW5kJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopO2lmKGlzRmluaXRlKG9iai52aWRlby5kdXJhdGlvbikpJTdCaDVQbGF5ZXJTbGlkZUNvbnRyb2xzRW5kKG9iaik7JTdEJTdEKTtvYmoucHJvZ3Jlc3NCYXIuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCaWYoaXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKSklN0JvYmoudmlkZW8uY3VycmVudFRpbWU9ZXZlbnQub2Zmc2V0WC90aGlzLm9mZnNldFdpZHRoKm9iai52aWRlby5kdXJhdGlvbjslN0QlN0QpO29iai5jdXJyZW50VGltZS5hZGRFdmVudExpc3RlbmVyKCUyMmNsaWNrJTIyLGZ1bmN0aW9uKCklN0JvYmoudmlkZW8uY3VycmVudFRpbWU9MDslN0QpO29iai5kdXJhdGlvbi5hZGRFdmVudExpc3RlbmVyKCUyMmNsaWNrJTIyLGZ1bmN0aW9uKCklN0J2YXIlMjBhZGRUaW1lPW9iai52aWRlby5jdXJyZW50VGltZSsob2JqLnZpZGVvLmR1cmF0aW9uJTNDNjE/MTpvYmoudmlkZW8uZHVyYXRpb24vMTApO29iai52aWRlby5jdXJyZW50VGltZT0oYWRkVGltZSUzQ29iai52aWRlby5kdXJhdGlvbj9hZGRUaW1lOm9iai52aWRlby5kdXJhdGlvbi0xKTslN0QpO29iai5leHRyYVZpZXcuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCb2JqLnZpZGVvLmN1cnJlbnRUaW1lPW9iai52aWRlby5kdXJhdGlvbi0xOyU3RCk7b2JqLmZ1bGxzY3JlZW4uYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXJGdWxsc2NyZWVuQ2xpY2sob2JqLmNvbnRhaW5lcik7JTdEKTtkb2MuYWRkRXZlbnRMaXN0ZW5lciglMjJmdWxsc2NyZWVuY2hhbmdlJTIyLGZ1bmN0aW9uKCklN0JpZihkb2MuZnVsbHNjcmVlbkVsZW1lbnQ9PW51bGwpJTdCb2JqLmZ1bGxzY3JlZW4uaW5uZXJIVE1MPSUyMiVFNSU4NSVBOCVFNSVCMSU4RiUyMjslN0RlbHNlJTIwaWYob2JqLmNvbnRhaW5lci5pc1NhbWVOb2RlKGRvYy5mdWxsc2NyZWVuRWxlbWVudCkpJTdCb2JqLmZ1bGxzY3JlZW4uaW5uZXJIVE1MPSUyMiVFOSU4MCU4MCVFNSU4NyVCQSUyMjslN0QlN0QpO29iai52b2x1bWVJbmMuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCb2JqLnZpZGVvLnZvbHVtZT0ob2JqLnZpZGVvLnZvbHVtZSUzQzE/cGFyc2VJbnQob2JqLnZpZGVvLnZvbHVtZSoxMCsxKS8xMDoxKTslN0QpO29iai52b2x1bWVEZWMuYWRkRXZlbnRMaXN0ZW5lciglMjJjbGljayUyMixmdW5jdGlvbigpJTdCb2JqLnZpZGVvLnZvbHVtZT0ob2JqLnZpZGVvLnZvbHVtZSUzRTA/TWF0aC5jZWlsKG9iai52aWRlby52b2x1bWUqMTAtMSkvMTA6MCk7JTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJjYW5wbGF5JTIyLGZ1bmN0aW9uKCklN0JpZih0aGlzLnBhdXNlZCYmKG9iai5zdGF0ZT09JTIyc2Vla2luZyUyMiU3QyU3Q29iai5zdGF0ZT09JTIyd2FpdGluZyUyMikpJTdCdGhpcy5wbGF5KCk7JTdEJTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJkdXJhdGlvbmNoYW5nZSUyMixmdW5jdGlvbigpJTdCaDVQbGF5ZXJIaWRkZW4ob2JqKTtvYmouZHVyYXRpb24uaW5uZXJIVE1MPWlzRmluaXRlKHRoaXMuZHVyYXRpb24pP2g1UGxheWVyRm9ybWF0VGltZSh0aGlzLmR1cmF0aW9uKTolMjJMSVZFJTIyOyU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIydGltZXVwZGF0ZSUyMixmdW5jdGlvbigpJTdCb2JqLmZvcndhcmRUaW1lPXRoaXMuY3VycmVudFRpbWU7b2JqLmN1cnJlbnRUaW1lLmlubmVySFRNTD1oNVBsYXllckZvcm1hdFRpbWUodGhpcy5jdXJyZW50VGltZSk7aWYoaXNGaW5pdGUob2JqLnZpZGVvLmR1cmF0aW9uKSklN0JoNVBsYXllckdldFByb2dyZXNzKG9iaixvYmoudmlkZW8uY3VycmVudFRpbWUpOyU3RCU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIydm9sdW1lY2hhbmdlJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopO29iai5mb3J3YXJkVm9sdW1lPW9iai52aWRlby52b2x1bWU7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU5JTlGJUIzJUU5JTg3JThGOiUyMitwYXJzZUludCh0aGlzLnZvbHVtZSoxMDApKyUyMiUyNSUyMjslN0QpO29iai52aWRlby5hZGRFdmVudExpc3RlbmVyKCUyMnJhdGVjaGFuZ2UlMjIsZnVuY3Rpb24oKSU3Qmg1UGxheWVySGlkZGVuKG9iaik7b2JqLmZvcndhcmRSYXRlPW9iai52aWRlby5wbGF5YmFja1JhdGU7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU1JTgwJThEJUU5JTgwJTlGOiUyMitwYXJzZUludCh0aGlzLnBsYXliYWNrUmF0ZSoxMDApKyUyMiUyNSUyMjslN0QpO29iai52aWRlby5hZGRFdmVudExpc3RlbmVyKCUyMnByb2dyZXNzJTIyLGZ1bmN0aW9uKCklN0JpZihpc0Zpbml0ZShvYmoudmlkZW8uZHVyYXRpb24pKSU3Qmg1UGxheWVyR2V0QnVmZmVyKG9iaik7JTdEJTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJzZWVraW5nJTIyLGZ1bmN0aW9uKCklN0JvYmouc3RhdGU9JTIyc2Vla2luZyUyMjt2YXIlMjBzdWJUaW1lPW9iai52aWRlby5jdXJyZW50VGltZS1vYmouZm9yd2FyZFRpbWU7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9KHN1YlRpbWUlM0UwPyUyMiVFNSVCRiVBQiVFOCVCRiU5QjolMjI6JTIyJUU1JUJGJUFCJUU5JTgwJTgwOiUyMikrTWF0aC5hYnMocGFyc2VJbnQoc3ViVGltZSkpKyUyMnMlMjI7JTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJ3YWl0aW5nJTIyLGZ1bmN0aW9uKCklN0JpZihvYmouc3RhdGUhPSUyMnNlZWtpbmclMjIpJTdCb2JqLnN0YXRlPSUyMndhaXRpbmclMjI7b2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU1JThBJUEwJUU4JUJEJUJEJUU0JUI4JUFELi4uJTIyOyU3RG9iai52aWRlby5wYXVzZSgpOyU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIyZW5kZWQlMjIsZnVuY3Rpb24oKSU3Qm9iai52aWRlby5wYXVzZSgpOyU3RCk7b2JqLnZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJTIycGF1c2UlMjIsZnVuY3Rpb24oKSU3Qmg1UGxheWVyUmVtb3ZlQ29udHJvbHMob2JqLGZhbHNlKTtpZihvYmouc3RhdGUhPSUyMndhaXRpbmclMjImJm9iai5zdGF0ZSE9JTIyc2Vla2luZyUyMiklN0JvYmouc3RhdGU9JTIycGF1c2VkJTIyO2g1UGxheWVyUGxheUNsaWNrKG9iaik7JTdEZWxzZSU3QmNsZWFyVGltZW91dChvYmoud2FpdFRpbWVJRCk7b2JqLndhaXRUaW1lSUQ9c2V0SW50ZXJ2YWwoaDVQbGF5ZXJHZXRXYWl0aW5nLDQwLG9iaik7JTdEJTdEKTtvYmoudmlkZW8uYWRkRXZlbnRMaXN0ZW5lciglMjJwbGF5aW5nJTIyLGZ1bmN0aW9uKCklN0JoNVBsYXllckhpZGRlbihvYmopO2lmKG9iai5zdGF0ZT09JTIyd2FpdGluZyUyMiU3QyU3Q29iai5zdGF0ZT09JTIyc2Vla2luZyUyMiklN0JpZihvYmouc3RhdGU9PSUyMndhaXRpbmclMjIpJTdCb2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJTIyOyU3RGNsZWFyVGltZW91dChvYmoud2FpdFRpbWVJRCk7JTdEb2JqLnN0YXRlPSUyMnBsYXlpbmclMjI7aDVQbGF5ZXJQbGF5Q2xpY2sob2JqKTslN0QpOyU3RHZhciUyMGg1UGxheWVyU2xpZGVTdGFydFg7dmFyJTIwaDVQbGF5ZXJTbGlkZVN0YXJ0WTtmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVTdGFydCgpJTdCaDVQbGF5ZXJTbGlkZVN0YXJ0WD1ldmVudC50YXJnZXRUb3VjaGVzJTVCMCU1RC5jbGllbnRYO2g1UGxheWVyU2xpZGVTdGFydFk9ZXZlbnQudGFyZ2V0VG91Y2hlcyU1QjAlNUQuY2xpZW50WTslN0RmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVDb250cm9sc01vdmUob2JqKSU3QnZhciUyMHNsaWRlTW92ZVg9ZXZlbnQudGFyZ2V0VG91Y2hlcyU1QjAlNUQuY2xpZW50WDt2YXIlMjBkaXN0YW5jZT1zbGlkZU1vdmVYLWg1UGxheWVyU2xpZGVTdGFydFg7dmFyJTIwYWRkVGltZT1kaXN0YW5jZS9vYmouY29udHJvbHMub2Zmc2V0V2lkdGgqb2JqLnZpZGVvLmR1cmF0aW9uK29iai52aWRlby5jdXJyZW50VGltZTthZGRUaW1lPShhZGRUaW1lJTNDMCU3QyU3Q2FkZFRpbWUlM0VvYmoudmlkZW8uZHVyYXRpb24pPyhhZGRUaW1lJTNDMD8wOm9iai52aWRlby5kdXJhdGlvbi0xKTphZGRUaW1lO2g1UGxheWVyR2V0UHJvZ3Jlc3Mob2JqLGFkZFRpbWUpO29iai5leHRyYVZpZXcuaW5uZXJIVE1MPWg1UGxheWVyRm9ybWF0VGltZShhZGRUaW1lKTslN0RmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVDb250cm9sc0VuZChvYmopJTdCdmFyJTIwc2xpZGVFbmRYPWV2ZW50LmNoYW5nZWRUb3VjaGVzJTVCMCU1RC5jbGllbnRYO3ZhciUyMGRpc3RhbmNlPXNsaWRlRW5kWC1oNVBsYXllclNsaWRlU3RhcnRYO2lmKCFkaXN0YW5jZSUzRTApJTdCcmV0dXJuJTIwMDslN0R2YXIlMjBhZGRUaW1lPW9iai52aWRlby5jdXJyZW50VGltZStkaXN0YW5jZS9vYmouY29udHJvbHMub2Zmc2V0V2lkdGgqb2JqLnZpZGVvLmR1cmF0aW9uO2FkZFRpbWU9KGFkZFRpbWUlM0MwJTdDJTdDYWRkVGltZSUzRW9iai52aWRlby5kdXJhdGlvbik/KGFkZFRpbWUlM0MwPzA6b2JqLnZpZGVvLmR1cmF0aW9uLTEpOmFkZFRpbWU7b2JqLnZpZGVvLnBsYXkoKTtvYmoudmlkZW8uY3VycmVudFRpbWU9YWRkVGltZTslN0RmdW5jdGlvbiUyMGg1UGxheWVyU2xpZGVFbmQob2JqKSU3QnZhciUyMHNsaWRlRW5kWD1ldmVudC5jaGFuZ2VkVG91Y2hlcyU1QjAlNUQuY2xpZW50WDt2YXIlMjBzbGlkZUVuZFk9ZXZlbnQuY2hhbmdlZFRvdWNoZXMlNUIwJTVELmNsaWVudFk7dmFyJTIwZGlzdGFuY2VYPXNsaWRlRW5kWC1oNVBsYXllclNsaWRlU3RhcnRYO3ZhciUyMGRpc3RhbmNlWT1zbGlkZUVuZFktaDVQbGF5ZXJTbGlkZVN0YXJ0WTt2YXIlMjBwb3NYPWg1UGxheWVyU2xpZGVTdGFydFgtb2JqLm9mZnNldFg7dmFyJTIwZGlzdGFuY2U9TWF0aC5zcXJ0KE1hdGgucG93KGRpc3RhbmNlWCwyKStNYXRoLnBvdyhkaXN0YW5jZVksMikpO3ZhciUyMHBhcm09TWF0aC5hYnMoZGlzdGFuY2VZL2Rpc3RhbmNlWCk7aWYoZGlzdGFuY2U9PTApJTdCcmV0dXJuJTIwMDslN0RpZihwYXJtJTNDMC4zNiklN0J2YXIlMjBkaXN0YW5jZT1kaXN0YW5jZS9vYmoud2lkdGg7ZGlzdGFuY2U9ZGlzdGFuY2VYJTNFMD9kaXN0YW5jZTotMSpkaXN0YW5jZTtoNVBsYXllclByb2dyZXNzQ29udHJvbChvYmosZGlzdGFuY2UpOyU3RGVsc2UlMjBpZihkaXN0YW5jZVg9PTAlN0MlN0NwYXJtJTNFMi43NSklN0J2YXIlMjBkaXN0YW5jZT1kaXN0YW5jZS9vYmouaGVpZ2h0O2Rpc3RhbmNlPWRpc3RhbmNlWSUzRTA/LTEqZGlzdGFuY2U6ZGlzdGFuY2U7aWYocG9zWC9vYmoud2lkdGglM0UwLjUpJTdCaDVQbGF5ZXJWb2x1bWVDb250cm9sKG9iaixkaXN0YW5jZSk7JTdEZWxzZSU3Qmg1UGxheWVyUmF0ZUNvbnRyb2wob2JqLGRpc3RhbmNlJTNFMD8xOi0xKTslN0QlN0QlN0RmdW5jdGlvbiUyMGg1UGxheWVyUmF0ZUNvbnRyb2wob2JqLGRpcmVjdGlvbiklN0JpZihvYmoudmlkZW8ucGxheWJhY2tSYXRlIT11bmRlZmluZWQpJTdCdmFyJTIwYWRkUmF0ZT1vYmouZm9yd2FyZFJhdGUrZGlyZWN0aW9uLzQ7b2JqLnZpZGVvLnBsYXliYWNrUmF0ZT0oYWRkUmF0ZSUzQzAuMjUlN0MlN0NhZGRSYXRlJTNFNCk/KGFkZFJhdGUlM0MwLjI1PzAuMjU6NCk6YWRkUmF0ZTslN0RlbHNlJTdCb2JqLmV4dHJhVmlldy5pbm5lckhUTUw9JTIyJUU4JUE3JTg2JUU5JUEyJTkxJUU0JUI4JThEJUU2JTk0JUFGJUU2JThDJTgxJUU1JTgwJThEJUU5JTgwJTlGJUU2JTkyJUFEJUU2JTk0JUJFJUVGJUJDJTgxJTIyOyU3RCU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJWb2x1bWVDb250cm9sKG9iaixkaXN0YW5jZSklN0J2YXIlMjBzdGVwPU1hdGgucm91bmQoZGlzdGFuY2UqNTApO2lmKChvYmouZm9yd2FyZFZvbHVtZStzdGVwLzEwMCklM0UxKSU3Qm9iai52aWRlby52b2x1bWU9MTslN0RlbHNlJTIwaWYoKG9iai5mb3J3YXJkVm9sdW1lK3N0ZXAvMTAwKSUzQzApJTdCb2JqLnZpZGVvLnZvbHVtZT0wOyU3RGVsc2UlN0JvYmoudmlkZW8udm9sdW1lPW9iai5mb3J3YXJkVm9sdW1lK3N0ZXAvMTAwOyU3RCU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJQcm9ncmVzc0NvbnRyb2wob2JqLGRpc3RhbmNlKSU3QnZhciUyMHN0ZXA9TWF0aC5yb3VuZChkaXN0YW5jZSo1MCk7b2JqLnZpZGVvLmN1cnJlbnRUaW1lPW9iai5mb3J3YXJkVGltZSs2MCo3KnN0ZXAvMTAwOyU3RGZ1bmN0aW9uJTIwaDVQbGF5ZXJGb3JtYXRUaW1lKHRpbWUpJTdCaWYoaXNOYU4odGltZSkpJTdCdmFyJTIwYXJyPXRpbWUuc3BsaXQoJTIyOiUyMik7dmFyJTIwbnVtPTA7Zm9yKHZhciUyMGk9MDtpJTNDYXJyLmxlbmd0aDtpKyspJTdCbnVtPW51bSo2MCtwYXJzZUludChhcnIlNUJpJTVEKTslN0RyZXR1cm4lMjBudW07JTdEZWxzZSU3QnZhciUyMHN0cj0lMjIlMjI7dmFyJTIwbnVtPXBhcnNlSW50KHRpbWUpO2Zvcih2YXIlMjBpPTA7aSUzQzIlN0MlN0NudW0lM0UwO2krKyklN0J2YXIlMjBtb2Q9bnVtJTI1NjA7bnVtPShudW0tbW9kKS82MDtzdHI9KG1vZCUzQzEwPyUyMjAlMjIrbW9kOm1vZCkrKGk9PTA/JTIyJTIyOiUyMjolMjIpK3N0cjslN0RyZXR1cm4lMjBzdHI7JTdEJTdEZnVuY3Rpb24lMjBpc0g1UGxheWVySGlkZGVuKGVsZSklN0JpZihlbGUuc2Nyb2xsV2lkdGghPTAmJmVsZS5zY3JvbGxIZWlnaHQhPTApJTdCcmV0dXJuJTIwZmFsc2U7JTdEcmV0dXJuJTIwdHJ1ZTslN0QlN0QpKCk7\";\n" +
                "    eval(decodeURI(window.atob(videoParseCode)));\n" +
                "    eval(decodeURI(window.atob(h5PlayerCode)));\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void jxjk5(){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", "去掉网页悬浮广告，可能会影响到网页不是广告的地方，可以选择关闭");
        values.put("url", "*");
        values.put("js","(function(){\n" +
                "var config = {\"mianColor\":\"#de473c\",\"secondColor\":\"#f3f1e7\",\"iconMarginRight\":2,\"iconMarginTop\":100,\"iconWidth\":45,\"iconHeight\":35,\"iconFilletPercent\":0.3,\"developMenuHeight\":315,\"developMenuSecond\":0,\"parseInterfaces\":[\"https://z1.m1907.cn/?eps=0&jx=\",\"https://vip.parwix.com:4433/player/?url=\",\"https://lecurl.cn/?url=\",\"https://jx.m3u8.tv/jiexi/?url=\",\"https://api.leduotv.com/wp-api/ifr.php?isDp=1&vid=\",\"https://okjx.cc/?url=\",\"https://m2090.com/?url=\",\"http://51wujin.net/?url=\",\"https://vip.2ktvb.com/player/?url=\",\"https://660e.com/?url=\",\"https://api.sigujx.com/?url=\",\"https://jiexi.janan.net/jiexi/?url=\",\"https://jx.618g.com/?url=\",\"https://jx.ergan.top/?url=\",\"https://api.147g.cc/m3u8.php?url=\",\"http://17kyun.com/api.php?url=\"]};\n" +
                "\n" +
                "\n" +
                "(function() {\n" +
                "\t\n" +
                "\t\n" +
                "\tconst mianColor = config.mianColor;\n" +
                "\t\n" +
                "\tconst secondColor = config.secondColor;\n" +
                "\t\n" +
                "\tconst iconMarginRight = config.iconMarginRight;\n" +
                "\t\n" +
                "\tconst iconMarginTop = config.iconMarginTop;\n" +
                "\t\n" +
                "\tvar iconWidth = config.iconWidth;\n" +
                "\t\n" +
                "\tconst iconHeight = config.iconHeight;\n" +
                "\t\n" +
                "\tconst iconFilletPercent = config.iconFilletPercent;\n" +
                "\t\n" +
                "\tvar developMenuHeight = config.developMenuHeight;\n" +
                "\t\n" +
                "\tvar developMenuSecond = config.developMenuSecond;\n" +
                "\t\n" +
                "\tconst parseInterfaces = config.parseInterfaces;\n" +
                "\t\n" +
                "\t\n" +
                "\tconst videoSites = [\"v.qq.com\",\"tv.sohu.com\",\"iqiyi.com\",\"youku.com\",\"mgtv.com\",\"m.le.com\",\"www.le.com\",\"1905.com\",\"pptv.com\",\"bilibili.com\"];\n" +
                "\tconst currentUrl = document.location.href;\n" +
                "\t\n" +
                "\tif (self != top) {\n" +
                "\t\treturn;\n" +
                "\t}\n" +
                "\tvar result = videoSites.some(site=>{\n" +
                "\t\tif (currentUrl.match(site)) {\n" +
                "            return true;\n" +
                "\t\t}\n" +
                "        return false;\n" +
                "\t});\n" +
                "    if(!result){\n" +
                "        return;\n" +
                "    }\n" +
                "\t\n" +
                "\tvar uaLogo=\"pc\";\n" +
                "\tif(/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {\n" +
                "\t\tuaLogo=\"mobile\";\n" +
                "\t}\n" +
                "\t\n" +
                "\tconst globalStyle = \"cursor:pointer;position:fixed;right:\"+iconMarginRight+\"px;top:\"+iconMarginTop+\"px;z-index:2147483647;\";\n" +
                "\t\n" +
                "\tconst mainIconStyle = \"height:\"+iconHeight+\"px;width:\"+iconWidth+\"px;background:\"+mianColor+\";border-radius:\"+(iconFilletPercent*iconWidth)+\"px;box-sizing:border-box;box-shadow:-4px 4px 4px 0px rgba(0,0,0,0.4);\";\n" +
                "\t\n" +
                "\tconst triangleStyle = \"border-left:\"+(iconWidth*0.3)+\"px solid \"+secondColor+\";border-top:\"+(iconHeight*0.2)+\"px solid transparent;border-bottom:\"+(iconHeight*0.2)+\"px solid transparent;position:absolute;right:31%;top:30%;\";\n" +
                "\t\n" +
                "\tconst squareStyle = \"background:\"+secondColor+\";width:\"+(iconWidth*0.26)+\"px;height:\"+(iconWidth*0.26)+\"px;position:absolute;right:37%;top:37%;\";\n" +
                "\t\n" +
                "    const inMenuBoxStyle = \"width:115%;height:100%;overflow-y:scroll;overflow-x:hidden;\";\n" +
                "\t\n" +
                "\tconst outMenuBoxStyle = \"background:\"+mianColor+\";height:0px;overflow:hidden;font-size:\"+(iconWidth*0.4)+\"px;width:\"+(iconWidth*2.4)+\"px;position:absolute;right:0px;top:\"+iconHeight+\"px;box-shadow:-4px 4px 4px 0px rgba(0,0,0,0.4);border-radius:13px 0 1px 13px;transition:height \"+developMenuSecond+\"s;-moz-transition:height \"+developMenuSecond+\"s;-webkit-transition:height \"+developMenuSecond+\"s;-o-transition:height \"+developMenuSecond+\"s;\";\n" +
                "\t\n" +
                "\tconst MenuItemsStyle = \"color:\"+secondColor+\";display: block;padding:\"+(iconWidth*0.12)+\"px \"+(iconWidth*0.12)+\"px \"+(iconWidth*0.12)+\"px \"+(iconWidth*0.2)+\"px ;width:\"+(iconWidth*3)+\"px;\";\n" +
                "\t\n" +
                "\tconst IframeStyle = \"frameborder='no' width='100%' height='100%' allowfullscreen='true' allowtransparency='true' frameborder='0' scrolling='no';\";\n" +
                "    \n" +
                "\tvar classAndIDMap\t= {\"pc\":{\"v.qq.com\":\"mod_player\",\"iqiyi.com\":\"flashbox\",\"youku.com\":\"ykPlayer\",\"mgtv.com\":\"mgtv-player-wrap\",\"sohu.com\":\"x-player\",\"le.com\":\"fla_box\",\"1905.com\":\"player\",\"pptv.com\":\"pplive-player\",\"bilibili.com\":\"bilibili-player-video-wrap|player-limit-mask\"},\"mobile\":{\"v.qq.com\":\"mod_player\",\"iqiyi.com\":\"m-box\",\"youku.com\":\"h5-detail-player\",\"mgtv.com\":\"video-area\",\"sohu.com\":\"player-view\",\"le.com\":\"playB\",\"1905.com\":\"player\",\"pptv.com\":\"pp-details-video\",\"bilibili.com\":\"bilibiliPlayer|player-wrapper\"}};\n" +
                "    \n" +
                "    createIcon();\n" +
                "\t\n" +
                "\tdocument.onreadystatechange = function(){\n" +
                "        if(document.readyState == 'complete'){\n" +
                "            if(!document.getElementById(\"mainIcon\")){\n" +
                "                createIcon();\n" +
                "            }\n" +
                "        }\n" +
                "    };\n" +
                "    function createIcon(){\n" +
                "        try{\n" +
                "            var div = document.createElement(\"div\");\n" +
                "            div.style.cssText = globalStyle;\n" +
                "            div.setAttribute(\"id\",\"mainIcon\");\n" +
                "            var html = \"<div id='mainButton' style='\"+mainIconStyle+\"'><div id='triangle' style='\"+triangleStyle+\"'></div></div><div id='dropDownBox' style='\"+outMenuBoxStyle+\"'><div style=\"+inMenuBoxStyle+\">\";\n" +
                "            for(var i in parseInterfaces){\n" +
                "                if(i==parseInterfaces.length-1){\n" +
                "                    html += \"<span class='spanStyle' style='\"+MenuItemsStyle+\"' url='\"+parseInterfaces[i]+\"'>线路接口\"+(parseInt(i)+1)+\"</span>\";\n" +
                "                }else {\n" +
                "                    html += \"<span class='spanStyle' style='\"+MenuItemsStyle+\"border-bottom-style:solid;' url='\"+parseInterfaces[i]+\"'>线路接口\"+(parseInt(i)+1)+\"</span>\";\n" +
                "                }\n" +
                "            }\n" +
                "            html += \"</div></div>\";\n" +
                "            div.innerHTML = html;\n" +
                "            document.body.insertBefore(div,document.body.firstChild);\n" +
                "            div.onclick = function() {\n" +
                "                var dropDownBox = document.getElementById(\"dropDownBox\").style.height;\n" +
                "                var mainButton = document.getElementById(\"mainButton\");\n" +
                "                var triangle = document.getElementById(\"triangle\");\n" +
                "                if(dropDownBox == \"0px\"){\n" +
                "                    mainButton.style.borderRadius = (iconFilletPercent*iconWidth)+\"px \"+(iconFilletPercent*iconWidth)+\"px 0 0\";\n" +
                "                    triangle.removeAttribute(\"style\");\n" +
                "                    triangle.setAttribute(\"style\",squareStyle);\n" +
                "                    document.getElementById(\"dropDownBox\").style.height = developMenuHeight+\"px\";\n" +
                "                }else {\n" +
                "                    document.getElementById(\"dropDownBox\").style.height = \"0px\";\n" +
                "                    triangle.removeAttribute(\"style\");\n" +
                "                    triangle.setAttribute(\"style\",triangleStyle);\n" +
                "                    mainButton.style.borderRadius = (iconFilletPercent*iconWidth)+\"px\";\n" +
                "                }\n" +
                "            };\n" +
                "            var elements = document.getElementsByClassName(\"spanStyle\");\n" +
                "            for(var j in elements){\n" +
                "                elements[j].onmouseover = function(){\n" +
                "                    this.style.background = secondColor;\n" +
                "                    this.style.color = mianColor;\n" +
                "                };\n" +
                "                elements[j].onmouseout = function(){\n" +
                "                    this.style.background = mianColor;\n" +
                "                    this.style.color = secondColor;\n" +
                "                };\n" +
                "                elements[j].onclick=function(){\n" +
                "                    var parseInterface = this.getAttribute(\"url\");\n" +
                "                    for(let key in classAndIDMap[uaLogo]){\n" +
                "                        if (document.location.href.match(key)) {\n" +
                "                            var values = classAndIDMap[uaLogo][key].split(\"|\");\n" +
                "                            var labelType = \"\";\n" +
                "                            var class_id = \"\";\n" +
                "                            for(let value in values){\n" +
                "                                if(document.getElementById(values[value])){\n" +
                "                                    class_id = values[value];\n" +
                "                                    labelType = \"id\";\n" +
                "                                    break;\n" +
                "                                }\n" +
                "                                if(document.getElementsByClassName(values[value]).length>0){\n" +
                "                                    class_id = values[value];\n" +
                "                                    labelType = \"class\";\n" +
                "                                    break;\n" +
                "                                }\n" +
                "                            }\n" +
                "                            if(labelType!=\"\"&&class_id!=\"\"){\n" +
                "                                var iframe = \"<iframe id='iframePlayBox' src='\"+parseInterface+document.location.href+\"' \"+IframeStyle+\" ></iframe>\";\n" +
                "                                if(labelType==\"id\"){\n" +
                "                                    document.getElementById(class_id).innerHTML=\"\";\n" +
                "                                    document.getElementById(class_id).innerHTML=iframe;\n" +
                "                                }else {\n" +
                "                                    document.getElementsByClassName(class_id)[0].innerHTML=\"\";\n" +
                "                                    if(uaLogo==\"mobile\"){\n" +
                "                                        document.getElementsByClassName(class_id)[0].style.height=\"225px\";\n" +
                "                                    }\n" +
                "                                    document.getElementsByClassName(class_id)[0].innerHTML=iframe;\n" +
                "                                }\n" +
                "                                return;\n" +
                "                            }\n" +
                "                        }\n" +
                "                    }\n" +
                "                    document.getElementById(\"dropDownBox\").style.display = \"none\";\n" +
                "                };\n" +
                "            }\n" +
                "        }catch(error){\n" +
                "            \n" +
                "        }\n" +
                "    }\n" +
                "})();\n" +
                "\n" +
                "})();");
        values.put("state","已停用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
    }
    public void res(){
        ydms();
        shuan();
        dan();
        topw();
        jcfz();
        zdpj();
        suofan();
        xlsx();
        tranlslate();
        kkmh();
        erweima();
        arltlj();
        kbn();
        spts();
        apptip();
        gpnight();
        urlclick();
        wzzg();
        zkqw();
        bdsszk();
        bdssqgg();
        vconsole();
        hyms();


        jxjk3();
        jxjk4();
        jxjk5();
        xsyd();
        spbfq();
        xzfz();
        ggfy1();
        ggfy2();
        caiyun();
        bdmh();
        qxfgg();
        SharedPreferences.Editor edit=share.edit();
        edit.putString("jstool", "早已打开");
        edit.commit();
        queryinfo();
    }

    private void onclick() {
        res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog = new AlertDialog.Builder(jstool.this)
                        .setTitle("确定还原吗？")//设置对话框的标题
                        .setMessage("这个操作会将此页面还原到首次安装时")
                        //设置对话框的按钮
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                delete_all();
                                res();
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
            }
        });
        addjs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog addwebdialog = new Dialog(jstool.this, R.style.MyDialogActivityStyle);
                View addwebview = View.inflate(jstool.this, R.layout.dialog_addjs, null);
                addwebdialog.setContentView(addwebview);
                addwebdialog.setCanceledOnTouchOutside(true);
                Window window = addwebdialog.getWindow();//得到对话框的窗口．
                WindowManager.LayoutParams wl = window.getAttributes();
                WindowManager windowManager = getWindowManager();
                Display display = windowManager.getDefaultDisplay();
//                wl.x = x;//设置对话框的位置．0为中间
//                wl.y = y;
                wl.width = (int)(display.getWidth() * 1); //设置宽度
                wl.height = (int)(display.getHeight() * 0.35);
                wl.alpha = 1f;// 设置对话框的透明度,1f不透明
                window.setGravity(Gravity.BOTTOM);//底部出现//设置显示在中间
                window.setAttributes(wl);
                text_title = (EditText) addwebview.findViewById(R.id.title);
                text_url = (EditText) addwebview.findViewById(R.id.url);
                text_js = (EditText) addwebview.findViewById(R.id.js);

                text_title.setFocusable(true);
                text_title.setFocusableInTouchMode(true);
                text_title.requestFocus();
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                                   public void run() {
                                       InputMethodManager inputManager =
                                               (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                       inputManager.showSoftInput(text_title, 0);
                                   }
                               },
                        200);

                //显示详细信息

                addwebview.findViewById(R.id.addweb).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(TextUtils.isEmpty(text_title.getText().toString())||TextUtils.isEmpty(text_url.getText().toString())||TextUtils.isEmpty(text_js.getText().toString())){
                            Toast.makeText(jstool.this, "不能存在空白", Toast.LENGTH_SHORT).show();
                        }else {
                            addweb(text_title.getText().toString(),text_url.getText().toString(),text_js.getText().toString());
                            addwebdialog.dismiss();
                        }
                        queryinfo();

                    }
                });

                addwebview.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addwebdialog.dismiss();
                    }
                });

                addwebdialog.show();

            }
        });


        //点击函数：
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (if_exist) {
                    new AlertDialog.Builder(jstool.this)
                            .setItems(new String[]{"停用", "启用","删除","修改"}, (dialog, which) -> {

                                switch (which) {
                                    case 0:
                                        state_stop(query_by_id2(position));
                                        break;
                                    case 1:
                                        state_start(query_by_id2(position));
                                        break;
                                    case 2:
                                        delete_dialog(position);
                                        break;
                                    case 3:
                                        url_infomation(position);
                                        break;
                                }
                            })
                            .show();
                }
            }
        });
        //长按选择删除
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (if_exist)
                    new AlertDialog.Builder(jstool.this)
                            .setItems(new String[]{"停用", "启用","删除","修改"}, (dialog, which) -> {

                                switch (which) {
                                    case 0:
                                        state_stop(query_by_id2(position));
                                        break;
                                    case 1:
                                        state_start(query_by_id2(position));
                                        break;
                                    case 2:
                                        delete_dialog(position);
                                        break;
                                    case 3:
                                        url_infomation(position);
                                        break;
                                }
                            })
                            .show();
                return true;
            }
        });
    }

    //    //查询数据库里的字段
//    public void select(String title,String url,String js) {
//        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";
//        //String sql = "select * from " + TABLENAME + " where name like '%"+ username + "%'";
//        String sql ="SELECT * FROM jstoolDB WHERE url='"+url+"'";
//        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
//        if(result.moveToFirst())//如果结果集的第一行有数据
//        {
//            //https://codingdict.com/questions/117721
//            //Toast.makeText(WebView.this,result.getString(1), Toast.LENGTH_SHORT).show();
//            Toast.makeText(jstool.this, "已存,名称为—"+result.getString(1), Toast.LENGTH_LONG)
//                    .show();
//            result.close();
//            db.close();
//        }
//        else {
//            addweb(title,url,js);
//            result.close();
//            db.close();
//
//        }
//    }
    //数据库添加数据(书签表)
    public void addweb(String title,String url,String js){
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("url", url);
        values.put("js",js);
        values.put("state","已启用");
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("jstoolDB",null,values);
        db.close();
        Toast.makeText(jstool.this,"添加成功",Toast.LENGTH_SHORT).show();
        queryinfo();

    }
    //jstoolDB删除函数：
    public void delete(int position){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete("jstoolDB","id=?",new String[] {String.valueOf(arrayList.get(position))});
        db.close();
        Toast.makeText(this,"删除成功",Toast.LENGTH_SHORT).show();
        //删除后清空数组，重新放入数据，刷新UI
        // arrayList.clear();
        queryinfo();


    }

    //全部删除
    public void delete_all(){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from jstoolDB", null);
        while (cursor.moveToNext()) {
            id=cursor.getInt(0);
            db.delete("jstoolDB","id=?",new String[] {String.valueOf(id)});
        }
        cursor.close();
        db.close();
        Toast.makeText(this,"删除成功",Toast.LENGTH_SHORT).show();
        //删除后清空数组，重新放入数据，刷新UI
        //arrayList.clear();
        //刷新UI
        queryinfo();
    }
    //jstoolDB全部删除对话框
    public void delete_dialog(int position){
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("确定全部删除吗？")//设置对话框的标题
                //设置对话框的按钮
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delete(position);
                        dialog.dismiss();
                    }
                }).create();
        dialog.show();

    }
    //jstoolDB全部删除对话框
    public void delete_all_dialog(){
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("确定全部删除吗？")//设置对话框的标题
                //设置对话框的按钮
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delete_all();
                        dialog.dismiss();
                    }
                }).create();
        dialog.show();

    }
    //数据库逆序查询函数：
    public void queryinfo(){
        arrayList.clear();
        final ArrayList<HashMap<String, Object>> listItem = new ArrayList <HashMap<String,Object>>();/*在数组中存放数据*/
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //Cursor cursor = db.rawQuery("select * from jstoolDB", null);
        //查询语句也可以这样写
        Cursor cursor = db.query("jstoolDB", null, null, null, null, null, "id desc");
        if (cursor != null && cursor.getCount() > 0) {
            if_exist=true;
            while(cursor.moveToNext()) {
                get_id=cursor.getInt(0);//得到int型的自增变量
                get_title = cursor.getString(1);
                get_url = cursor.getString(2);
                get_state=cursor.getString(4);
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("title", get_title);
                map.put("url", get_url);
                map.put("state", get_state);
                arrayList.add(get_id);
                listItem.add(map);
                //new String  数据来源， new int 数据到哪去
                SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem, R.layout.jstoolzujian,
                        new String[] {"title","url","state"},
                        new int[] {R.id.title, R.id.url, R.id.state});
                lv.setAdapter(mSimpleAdapter);//为ListView绑定适配器
            }
        }
        else {
            if_exist=false;
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("title", "暂无");
            map.put("url", "");
            map.put("state","");
            listItem.add(map);
            //new String  数据来源， new int 数据到哪去
            SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem, R.layout.bookmarkzujian,
                    new String[] {"title","url","state"},
                    new int[] {R.id.title, R.id.url, R.id.state});
            lv.setAdapter(mSimpleAdapter);//为ListView绑定适配器
        }

        cursor.close();
        db.close();
    }
    //按值查找：
    public String query_by_id(int position){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from jstoolDB", null);
        while (cursor.moveToNext()) {
            js=cursor.getString(3);
            url = cursor.getString(2);
            titler=cursor.getString(1);
            id=cursor.getInt(0);
            //找到id相等的就返回url
            if (arrayList.get(position).equals(id))
                break;
        }
        cursor.close();
        db.close();
        return url;
    }
    public int query_by_id2(int position){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from jstoolDB", null);
        while (cursor.moveToNext()) {

            url = cursor.getString(2);
            js=cursor.getString(3);
            titler=cursor.getString(1);
            id=cursor.getInt(0);
            //找到id相等的就返回url
            if (arrayList.get(position).equals(id))
                break;
        }
        cursor.close();
        db.close();
        return id;
    }

    //修改数据
    public void update(int id, String title, String url,String js) {
        dbHelper = new MyDatabaseHelper(jstool.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("url", url);
        values.put("js",js);
        values.put("state","已启用");
        //字符串过长是db.exexSQL就使用不了，使用db.upDate()即可 https://www.jb51.net/article/90678.htm
        db.update("jstoolDB",values,"id="+id,null);
        db.close();
        Toast.makeText(jstool.this,"修改成功",Toast.LENGTH_SHORT).show();
        queryinfo();
    }
    //启动Js
    public void state_start(int id) {
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String sql="update jstoolDB set state='已启用' where id="+ id;
        db.execSQL(sql);
        db.close();
        queryinfo();
    }
    //停止Js
    public void state_stop(int id) {
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String sql="update jstoolDB set state='已停用' where id="+ id;
        db.execSQL(sql);
        db.close();
        queryinfo();
    }
    //url详细信息对话框
    public void url_infomation(final int position){
        final Dialog dialog = new Dialog(jstool.this, R.style.MyDialogActivityStyle);
        View view = View.inflate(jstool.this, R.layout.dialog_addjs, null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();//得到对话框的窗口．
        WindowManager.LayoutParams wl = window.getAttributes();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
//                wl.x = x;//设置对话框的位置．0为中间
//                wl.y = y;
        wl.width = (int)(display.getWidth() * 1); //设置宽度
        wl.height = (int)(display.getHeight() * 0.4);
        wl.alpha = 1f;// 设置对话框的透明度,1f不透明
        window.setGravity(Gravity.BOTTOM);//底部出现//设置显示在中间
        window.setAttributes(wl);
        text_title = (EditText) view.findViewById(R.id.title);
        text_url = (EditText) view.findViewById(R.id.url);
        text_js= (EditText) view.findViewById(R.id.js);
        text_title.setFocusable(true);
        text_title.setFocusableInTouchMode(true);
        text_title.requestFocus();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
                           public void run() {
                               InputMethodManager inputManager =
                                       (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                               inputManager.showSoftInput(text_title, 0);
                           }
                       },
                200);

        //显示详细信息
        text_url.setText(query_by_id(position));
        text_title.setText(titler);
        text_js.setText(js);

        view.findViewById(R.id.addweb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                update(query_by_id2(position),text_title.getText().toString(),text_url.getText().toString(),text_js.getText().toString());

                dialog.dismiss();

            }
        });

        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }



    //数据库逆序查询函数：
    public void queryinfo2( String url ){
        final ArrayList<HashMap<String, Object>> listItem = new ArrayList <HashMap<String,Object>>();/*在数组中存放数据*/
        arrayList.clear();
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";

        String sql = "select * from jstoolDB where url like '%"+ url + "%'OR title like '%"+ url + "%'";
        Cursor cursor=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        if (cursor != null && cursor.getCount() > 0) {
            if_exist=true;
            while(cursor.moveToNext()) {
                get_id=cursor.getInt(0);//得到int型的自增变量
                get_title = cursor.getString(1);
                get_url = cursor.getString(2);
                get_state=cursor.getString(3);
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("title", get_title);
                map.put("url", get_url);
                map.put("state", get_state);
                arrayList.add(get_id);
                listItem.add(map);
                //new String  数据来源， new int 数据到哪去
                SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem, R.layout.jstoolzujian,
                        new String[] {"title","url","state"},
                        new int[] {R.id.title, R.id.url, R.id.state});
                lv.setAdapter(mSimpleAdapter);//为ListView绑定适配器
            }
        }
        else {
            if_exist=false;
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("title", "没有找到");
            map.put("url", "");
            map.put("state","");
            listItem.add(map);
            //new String  数据来源， new int 数据到哪去
            SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem, R.layout.jstoolzujian,
                    new String[] {"title","url","state"},
                    new int[] {R.id.title, R.id.url, R.id.state});
            lv.setAdapter(mSimpleAdapter);//为ListView绑定适配器
        }

        cursor.close();
        db.close();
    }


    //监听编辑框--https://www.jb51.net/article/135392.htm
    //https://blog.csdn.net/ycwol/article/details/46594275
    class EditChangedListener implements TextWatcher {
        public void afterTextChanged(Editable arg0) {
// 文字改变后出发事件
            String content = search_jstool.getText().toString();
            //若输入框内容为空按钮可点击，字体为蓝色
            if (!content.isEmpty()) {
                queryinfo2(search_jstool.getText().toString());

            } else {
                queryinfo();

            }

        }
        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
// TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
// TODO Auto-generated method stub

        }
    };

    //获取返回搜索url
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==1){
            if(resultCode==1){

                // int resultid=Integer.parseInt(data.getStringExtra("move"));
                // deleteid(resultid);


            }
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //finish();点击窗口外部区域 弹窗消失
        //点击空白处关闭键盘
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm.isActive()&&getCurrentFocus()!=null){
            if (getCurrentFocus().getWindowToken()!=null) {
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
            //刷新UI
            queryinfo();

            search_jstool.setText("");
        }
        return true;
    }



    @Override
    protected void onRestart() {
        super.onRestart();
        //刷新UI
        queryinfo();

    }


}
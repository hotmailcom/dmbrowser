package com.example.browser;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Database12 extends SQLiteOpenHelper {
    public static final String QW="Create table BBDB("+
            "id Integer primary key autoincrement,"+
            "text varchar(250),"+
            "title varchar(250))";
    private Context mContext;

    public Database12(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        mContext=context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QW);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists BBDB");
        onCreate(db);

    }
}

package com.example.browser;




import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dhs on 2018/7/27.
 */

public class history extends Activity {
    private ListView lv;
    private MyDatabaseHelper dbHelper;
    private String get_title,get_url;//暂存从数据库得到的title和url
    private int get_id; //暂存从数据库得到的id
    private String titler, url;//按值查找详细信息
    int id; //按值查找id
    private TextView text_title, text_url;//书签详细信息edittext
    private ArrayList arrayList=new ArrayList();
    private static final String TABLENAME = "historyDB";
    private boolean if_exist;
    private EditText search_historyDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);
        search_historyDB=(EditText)findViewById(R.id.search_historyDB) ;
//监听编辑框
        search_historyDB.addTextChangedListener(new history.EditChangedListener());
        lv=(ListView)findViewById(R.id.history_list);
        queryinfo();
        //点击函数：
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (if_exist) {
                    Intent intent = new Intent(history.this, webview.class);//没有任何参数（意图），只是用来传递数据
                    intent.putExtra("searchText", query_by_id(position));
                    startActivity(intent);
                    finish();
                }


            }
        });
        //长按选择删除
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (if_exist)
                    dialog_bottom(position);
                return true;
            }
        });




    }


    //删除函数：
    public void delete(int position){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete("historyDB","id=?",new String[] {String.valueOf(arrayList.get(position))});
        db.close();
        Toast.makeText(this,"删除成功",Toast.LENGTH_SHORT).show();
        //删除后清空数组，重新放入数据，刷新UI
        arrayList.clear();
        queryinfo();
    }

    //全部删除
    public void delete_all(){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from historyDB", null);
        while (cursor.moveToNext()) {
            id=cursor.getInt(0);
            db.delete("historyDB","id=?",new String[] {String.valueOf(id)});
        }
        cursor.close();
        db.close();
        Toast.makeText(this,"删除成功",Toast.LENGTH_SHORT).show();
        //删除后清空数组，重新放入数据，刷新UI
        arrayList.clear();
        queryinfo();
    }

    //全部删除对话框
    public void delete_all_dialog(){
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("确定全部删除吗？")//设置对话框的标题
                //设置对话框的按钮
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delete_all();
                        dialog.dismiss();
                    }
                }).create();
        dialog.show();

    }

    //数据库逆序查询函数：
    public void queryinfo(){
        final ArrayList<HashMap<String, Object>> listItem = new ArrayList <HashMap<String,Object>>();/*在数组中存放数据*/
        arrayList.clear();
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //Cursor cursor = db.rawQuery("select * from bookmarkDB", null);
        //查询语句也可以这样写
        Cursor cursor = db.query("historyDB", null, null, null, null, null, "id desc");
        if (cursor != null && cursor.getCount() > 0) {
            if_exist=true;
            while(cursor.moveToNext()) {
                get_id=cursor.getInt(0);//得到int型的自增变量
                get_title = cursor.getString(1);
                get_url = cursor.getString(2);
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("title", get_title);
                map.put("url", get_url);
                arrayList.add(get_id);
                listItem.add(map);
                //new String  数据来源， new int 数据到哪去
                SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem,R.layout.bookmarkzujian,
                        new String[] {"title","url"},
                        new int[] {R.id.title,R.id.url});
                lv.setAdapter(mSimpleAdapter);//为ListView绑定适配器
            }
        }
        else {
            if_exist=false;
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("title", "暂时没有浏览记录");
            map.put("url", "此处是存放您历史记录地方");
            listItem.add(map);
            //new String  数据来源， new int 数据到哪去
            SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem,R.layout.bookmarkzujian,
                    new String[] {"title","url"},
                    new int[] {R.id.title,R.id.url});
            lv.setAdapter(mSimpleAdapter);//为ListView绑定适配器
        }

        cursor.close();
        db.close();
    }

    //按值查找：
    public String query_by_id(int position){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from historyDB", null);
        while (cursor.moveToNext()) {
            url = cursor.getString(2);
            titler=cursor.getString(1);
            id=cursor.getInt(0);
            //找到id相等的就返回url
            if (arrayList.get(position).equals(id))
                break;
        }
        cursor.close();
        db.close();
        return url;
    }

    public int query_by_id2(int position){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from bookmarkDB", null);
        while (cursor.moveToNext()) {
            url = cursor.getString(2);
            titler=cursor.getString(1);
            id=cursor.getInt(0);
            //找到id相等的就返回url
            if (arrayList.get(position).equals(id))
                break;
        }
        cursor.close();
        db.close();
        return id;
    }
    //查询数据库里的字段
    public void select(String title,String url) {
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";
        //String sql = "select * from " + TABLENAME + " where name like '%"+ username + "%'";
        String sql ="SELECT * FROM bookmarkDB WHERE url='"+url+"'";

        Cursor result=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        if(result.moveToFirst())//如果结果集的第一行有数据
        {
            //https://codingdict.com/questions/117721
            //Toast.makeText(WebView.this,result.getString(1), Toast.LENGTH_SHORT).show();
            Toast.makeText(history.this, "已存在书签中,名称为———"+result.getString(1), Toast.LENGTH_LONG)
                    .show();
            result.close();
            db.close();

        }
        else {
            addweb(title,url);
            result.close();
            db.close();

        }
    }
    //数据库添加数据(书签表)
    public void addweb(String title,String url){
        //第二个参数是数据库名
        MyDatabaseHelper  dbHelper = new MyDatabaseHelper(history.this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("url", url);
        //insert（）方法中第一个参数是表名，第二个参数是表示给表中未指定数据的自动赋值为NULL。第三个参数是一个ContentValues对象
        db.insert("bookmarkDB",null,values);
        Toast.makeText(history.this,"添加成功",Toast.LENGTH_SHORT).show();
        queryinfo();

    }

    //url详细信息对话框
    public void url_infomation(final int position){
        Dialog addwebdialog = new Dialog(history.this, R.style.MyDialogActivityStyle);
        View addwebview = View.inflate(history.this, R.layout.dialogaddweb, null);
        addwebdialog.setContentView(addwebview);
        addwebdialog.setCanceledOnTouchOutside(true);
        Window window = addwebdialog.getWindow();//得到对话框的窗口．
        WindowManager.LayoutParams wl = window.getAttributes();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
//                wl.x = x;//设置对话框的位置．0为中间
//                wl.y = y;
        wl.width = (int)(display.getWidth() * 1); //设置宽度
        wl.height = (int)(display.getHeight() * 0.35);
        wl.alpha = 1f;// 设置对话框的透明度,1f不透明
        window.setGravity(Gravity.BOTTOM);//底部出现//设置显示在中间
        window.setAttributes(wl);
        text_title = (EditText) addwebview.findViewById(R.id.title);
        text_url = (EditText) addwebview.findViewById(R.id.url);
        text_title.setFocusable(true);
        text_title.setFocusableInTouchMode(true);
        text_title.requestFocus();
        text_url.setText(query_by_id(position));
        text_title.setText(titler);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
                           public void run() {
                               InputMethodManager inputManager =
                                       (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                               inputManager.showSoftInput(text_title, 0);
                           }
                       },
                200);

        //显示详细信息

        addwebview.findViewById(R.id.addweb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                select(text_title.getText().toString(),text_url.getText().toString());
                addwebdialog.dismiss();


            }
        });

        addwebview.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addwebdialog.dismiss();
            }
        });

        addwebdialog.show();

    }

    //底部对话框
    public void dialog_bottom(final int position){
        //弹出对话框

        final Dialog dialog = new Dialog(history.this, R.style.MyDialogActivityStyle);
        View view = View.inflate(history.this, R.layout.historyoption, null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();//得到对话框的窗口．
        WindowManager.LayoutParams wl = window.getAttributes();

        dialog.show();
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
//                wl.x = x;//设置对话框的位置．0为中间
//                wl.y = y;
        wl.width = (int)(display.getWidth() * 1); //设置宽度
        wl.height = (int)(display.getHeight() * 0.4);
        wl.alpha = 1f;// 设置对话框的透明度,1f不透明
        window.setGravity(Gravity.BOTTOM);//底部出现//设置显示在中间
        window.setAttributes(wl);
        //点击事件
        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //取消对话框
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.copy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //获取剪贴板管理器：
                ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                // 创建普通字符型ClipData
                ClipData mClipData = ClipData.newPlainText("Label",query_by_id(position));
                // 将ClipData内容放到系统剪贴板里。
                cm.setPrimaryClip(mClipData);
                Toast.makeText(history.this, "已复制", Toast.LENGTH_SHORT).show();
                dialog.dismiss();

            }
        });
        view.findViewById(R.id.move).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url_infomation(position);
                dialog.dismiss();

            }
        });
        view.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //删除制定position，并取消对话框
                delete(position);
                dialog.dismiss();

            }
        });

        view.findViewById(R.id.clear_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //全部删除
                dialog.dismiss();
                delete_all_dialog();

            }
        });
    }

    //数据库逆序查询函数：
    public void queryinfo2( String url ){
        final ArrayList<HashMap<String, Object>> listItem = new ArrayList <HashMap<String,Object>>();/*在数组中存放数据*/
        arrayList.clear();
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //String sql ="SELECT * FROM " + TABLENAME + " WHERE name='"+username+"'AND birthday='"+birthday+"'";

        String sql = "select * from historyDB where url like '%"+ url + "%'OR title like '%"+ url + "%'";
        Cursor cursor=db.rawQuery(sql, null);//用Cursor对象获得执行查询结果集
        if (cursor != null && cursor.getCount() > 0) {
            if_exist=true;
            while(cursor.moveToNext()) {
                get_id=cursor.getInt(0);//得到int型的自增变量
                get_title = cursor.getString(1);
                get_url = cursor.getString(2);
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("title", get_title);
                map.put("url", get_url);
                arrayList.add(get_id);
                listItem.add(map);
                //new String  数据来源， new int 数据到哪去
                SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem,R.layout.bookmarkzujian,
                        new String[] {"title","url"},
                        new int[] {R.id.title,R.id.url});
                lv.setAdapter(mSimpleAdapter);//为ListView绑定适配器
            }
        }
        else {
            if_exist=false;
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("title", "没有找到浏览记录哦");
            map.put("url", "");
            listItem.add(map);
            //new String  数据来源， new int 数据到哪去
            SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem,R.layout.bookmarkzujian,
                    new String[] {"title","url"},
                    new int[] {R.id.title,R.id.url});
            lv.setAdapter(mSimpleAdapter);//为ListView绑定适配器
        }

        cursor.close();
        db.close();
    }

    //监听编辑框--https://www.jb51.net/article/135392.htm
    //https://blog.csdn.net/ycwol/article/details/46594275
    class EditChangedListener implements TextWatcher {
        public void afterTextChanged(Editable arg0) {
// 文字改变后出发事件
            String content = search_historyDB.getText().toString();
            //若输入框内容为空按钮可点击，字体为蓝色
            if (!content.isEmpty()) {
                queryinfo2(search_historyDB.getText().toString());

            } else {
                queryinfo();
            }

        }
        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
// TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
// TODO Auto-generated method stub

        }
    };


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //finish();点击窗口外部区域 弹窗消失
        //点击空白处关闭键盘
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm.isActive()&&getCurrentFocus()!=null){
            if (getCurrentFocus().getWindowToken()!=null) {
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
            //刷新UI
            queryinfo();

            search_historyDB.setText("");
        }


        return true;
    }


}
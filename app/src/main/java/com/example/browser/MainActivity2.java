package com.example.browser;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class MainActivity2 extends Activity {

    private RelativeLayout baidu_change;
    private RelativeLayout sougou_change;
    private RelativeLayout chrome_change;
    private RelativeLayout bing_change,quak_change,custom_change;
    private File img;
    private TextView top_text,customtext;
    private String customurl,customname;
    // SharedPreferences数据存储
    private static final String FILENAME="YX";
    SharedPreferences share;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.float_view);
        getWindow().setGravity(Gravity.BOTTOM);//底部出现
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);//设置弹窗宽高
        baidu_change = (RelativeLayout) findViewById(R.id.baidu_change);
        sougou_change = (RelativeLayout) findViewById(R.id.sougou_change);
        chrome_change = (RelativeLayout) findViewById(R.id.chrome_change);
        bing_change = (RelativeLayout) findViewById(R.id.bing_change);
        quak_change = (RelativeLayout) findViewById(R.id.quak_change);
        custom_change = (RelativeLayout) findViewById(R.id.custom_change);
        top_text = (TextView) findViewById(R.id.top_text);
        customtext = (TextView) findViewById(R.id.customtext);
        share=getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
        customurl = share.getString("customurl", "");
        customname = share.getString("customname", "");
//默认百度搜索
        String searchhistory = share.getString("searchurl", "https://www.baidu.com/s?wd=");
        if(searchhistory.equals("https://www.baidu.com/s?wd=")){
            top_text.setText("当前搜索引擎为:百度");
        }else if(searchhistory.equals("https://www.sogou.com/web?query=")){
            top_text.setText("当前搜索引擎为:搜狗");
        }else if(searchhistory.equals("http://www.google.com.tw/search?q=")){
            top_text.setText("当前搜索引擎为:谷歌");
        }else if(searchhistory.equals("http://cn.bing.com/search?q=")){
            top_text.setText("当前搜索引擎为:必应");
        }else if(searchhistory.equals("https://quark.sm.cn/s?q=")){
            top_text.setText("当前搜索引擎为:夸克");
        }else {
            top_text.setText("当前搜索引擎为:自定义");
        }
        if(!customurl.isEmpty()){
            customtext.setText(customname);
        }

        initViews();
    }

    private void initViews() {

        SharedPreferences.Editor edit=share.edit();

        baidu_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                edit.putString("searchurl","https://www.baidu.com/s?wd=");
                edit.commit();
                Intent intent = new Intent();
//               intent.putExtra("burl","https://www.baidu.com/s?wd=");
                setResult(2,intent);
                MainActivity2.this.finish();

            }
        });
        sougou_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                edit.putString("searchurl","https://www.sogou.com/web?query=");
                edit.commit();
                Intent intent = new Intent();
//                intent.putExtra("burl","https://www.sogou.com/web?query=");
                setResult(2,intent);
                MainActivity2.this.finish();
            }
        });
        chrome_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                edit.putString("searchurl","http://www.google.com.tw/search?q=");
                edit.commit();
                Intent intent = new Intent();
//                intent.putExtra("burl","https://www.google.com/search?q=");
                setResult(2,intent);
                MainActivity2.this.finish();
            }
        });
        bing_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                edit.putString("searchurl","http://cn.bing.com/search?q=");
                edit.commit();
                Intent intent = new Intent();
//                intent.putExtra("burl","http://cn.bing.com/search?q=");
                setResult(2,intent);
                MainActivity2.this.finish();
            }
        });
        quak_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                edit.putString("searchurl","https://quark.sm.cn/s?q=");
                edit.commit();
                Intent intent = new Intent();
//                intent.putExtra("burl","https://quark.sm.cn/s?q=");
                setResult(2,intent);
                MainActivity2.this.finish();
            }
        });

        custom_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(customurl.startsWith("http")&&customurl.contains("=")){

                    edit.putString("searchurl",customurl);
                    edit.commit();
                    Intent intent = new Intent();
//                intent.putExtra("burl","https://quark.sm.cn/s?q=");
                    setResult(2,intent);
                    MainActivity2.this.finish();
                }else {
                    Toast.makeText(MainActivity2.this, "暂没有自定义", Toast.LENGTH_SHORT).show();
                }
            }
        });
        custom_change.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Dialog dialog = new Dialog(MainActivity2.this, R.style.MyDialogActivityStyle);
                View view = View.inflate(MainActivity2.this, R.layout.dialogaddweb, null);
                dialog.setContentView(view);
                dialog.setCanceledOnTouchOutside(true);
                Window window = dialog.getWindow();//得到对话框的窗口．
                WindowManager.LayoutParams wl = window.getAttributes();

                dialog.show();
                WindowManager windowManager = getWindowManager();
                Display display = windowManager.getDefaultDisplay();
//                wl.x = x;//设置对话框的位置．0为中间
//                wl.y = y;
                wl.width = (int)(display.getWidth() * 1); //设置宽度
                wl.height = (int)(display.getHeight() * 0.3);
                wl.alpha = 1f;// 设置对话框的透明度,1f不透明
                window.setGravity(Gravity.BOTTOM);//底部出现//设置显示在中间
                window.setAttributes(wl);
                TextView top=view.findViewById(R.id.top);
                Button cancel=view.findViewById(R.id.cancel);
                Button add=view.findViewById(R.id.addweb);
                EditText name=view.findViewById(R.id.title);
                EditText url=view.findViewById(R.id.url);

                if(!customurl.isEmpty()){
                    name.setText(customname);
                    url.setText(customurl);
                }else {
                    name.setText("写入引擎名字");
                    url.setText("引擎url:例如https://www.baidu.com/s?wd=");
                }
                top.setText("自定义搜素引擎");
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(url.getText().toString().startsWith("http")&&url.getText().toString().contains("=")){
                            edit.putString("customname",name.getText().toString());
                            edit.putString("customurl",url.getText().toString());
                            edit.putString("searchurl",url.getText().toString());
                            edit.commit();
                            Intent intent = new Intent();
//                intent.putExtra("burl","https://quark.sm.cn/s?q=");
                            setResult(2,intent);
                            MainActivity2.this.finish();
                        }else {
                            Toast.makeText(MainActivity2.this, "引擎不规范书写", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                return false;
            }
        });
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();//点击窗口外部区域 弹窗消失
        return true;
    }

}
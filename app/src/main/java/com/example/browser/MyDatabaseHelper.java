package com.example.browser;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by dhs on 2018/7/26.
 */
//https://blog.csdn.net/mountain_hua/article/details/80606461
public class MyDatabaseHelper extends SQLiteOpenHelper {
    //书签表
    public static final String CREATE_bookmarkDB = "create table bookmarkDB(" +
            "id integer primary key autoincrement," +
            "title text," +
            "url text)";

    //历史记录表
    public static final String CREATE_historyDB = "create table historyDB(" +
            "id integer primary key autoincrement," +
            "title text," +
            "url text)";
    //前缀工具
    public static final String CREATE_addprefixDB = "create table addprefixDB(" +
            "id integer primary key autoincrement," +
            "prefix text," +
            "keyword text)";

    public static final String CREATE_scyfolderDB = "create table addfolderDB(" +
            "id integer primary key autoincrement," +
            "foldername text," +
            "foldertable text)";
    public static final String CREATE_search_hisDB = "create table search_hisDB(" +
            "id integer primary key autoincrement," +
            "history text)";

    public static final String CREATE_cvDB = "create table cvDB(" +
            "id integer primary key autoincrement," +
            "copy text)";

    public static final String CREATE_adDB = "create table adDB(" +
            "id integer primary key autoincrement," +
            "adtable text," +
            "title text," +
            "url text)";


    public static final String CREATE_jstoolDB = "create table jstoolDB(" +
            "id integer primary key autoincrement," +
            "title text," +
            "url text," +
            "js LONGTEXT," +
            "state text)";
    public static final String CREATE_resourceDB = "create table resourceDB(" +
            "id integer primary key autoincrement," +
            "title text," +
            "url text)";

    private Context mContext;

    //构造方法：
    // 第一个参数Context上下文，
    // 第二个参数数据库名，
    // 第三个参数cursor允许我们在查询数据的时候返回一个自定义的光标位置，一般传入的都是null，
    // 第四个参数表示目前库的版本号（用于对库进行升级）
    public MyDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory , int version){
        super(context,name ,factory,version);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //调用SQLiteDatabase中的execSQL（）执行建表语句。
        db.execSQL(CREATE_bookmarkDB);
        db.execSQL(CREATE_historyDB);
        db.execSQL(CREATE_addprefixDB);
        db.execSQL(CREATE_scyfolderDB);
        db.execSQL(CREATE_search_hisDB);
        db.execSQL(CREATE_cvDB);
        db.execSQL(CREATE_adDB);

        db.execSQL(CREATE_jstoolDB);
        db.execSQL(CREATE_resourceDB);

        //创建成功
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //更新表
        db.execSQL("drop table if exists bookmarkDB");
        db.execSQL("drop table if exists historyDB");
        db.execSQL("drop table if exists addprefixDB");
        db.execSQL("drop table if exists scyfolderDB");
        db.execSQL("drop table if exists search_hisDB");

        db.execSQL("drop table if exists jstoolDB");
        db.execSQL("drop table if exists adDB");
        db.execSQL("drop table if exists resourceDB");

        onCreate(db);
    }

}
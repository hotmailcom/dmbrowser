package com.example.browser;

import android.app.Activity;
import android.os.Bundle;

public class BaseActivity1 extends Activity {


    private ActivityManager manager =  ActivityManager.getActivityManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager.putActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        manager.removeActivity(this);
    }

    public void exit() {
        manager.exit();
    }

}
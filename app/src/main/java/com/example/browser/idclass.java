package com.example.browser;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class idclass extends Activity {
    private ArrayList arrayList=new ArrayList();
    private ListView folder_list;
    private MyDatabaseHelper dbHelper;

    private String get_title,doMain;//暂存从数据库得到的title和url
    private int get_id; //暂存从数据库得到的id
    private boolean if_exist;
    private String titler, url;//按值查找详细信息
    int id; //按值查找id
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idclass);
        Intent it=super.getIntent();
        doMain=it.getStringExtra("idclass");
        folder_list=(ListView)findViewById(R.id.folder_list);
        queryinfo(doMain);
        //点击函数：
        folder_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (if_exist) {
                    new AlertDialog.Builder(idclass.this)
                            .setItems(new String[]{"删除该标识"}, (dialog, which) -> {
                                switch (which) {
                                    case 0:
                                        delete(position,doMain);
                                        break;
                                }
                            })
                            .show();
                }


            }
        });
        //长按选择删除
        folder_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (if_exist)
                    new AlertDialog.Builder(idclass.this)
                            .setItems(new String[]{"删除该标识","删除所有"}, (dialog, which) -> {
                                switch (which) {
                                    case 0:
                                        delete(position,doMain);
                                        break;
                                    case 1:
                                        delete_all(doMain);
                                        break;
                                }
                            })
                            .show();
                return true;
            }
        });
    }
    //删除函数：
    public void delete(int position,String doMainname){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(doMainname,"id=?",new String[] {String.valueOf(arrayList.get(position))});
        db.close();
        Toast.makeText(this,"删除成功",Toast.LENGTH_SHORT).show();
        //删除后清空数组，重新放入数据，刷新UI
        // arrayList.clear();
        queryinfo(doMainname);
    }

    //全部删除
    public void delete_all(String doMain){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from "+doMain, null);
        while (cursor.moveToNext()) {
            id=cursor.getInt(0);
            db.delete(doMain,"id=?",new String[] {String.valueOf(id)});
        }
        cursor.close();
        db.close();
        Toast.makeText(this,"删除成功",Toast.LENGTH_SHORT).show();
        //删除后清空数组，重新放入数据，刷新UI
        arrayList.clear();
        queryinfo(doMain);
    }




    //数据库逆序查询函数：
    public void queryinfo(String tablename){
        final ArrayList<HashMap<String, Object>> listItem = new ArrayList <HashMap<String,Object>>();/*在数组中存放数据*/
        arrayList.clear();
        //第二个参数是数据库名
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //Cursor cursor = db.rawQuery("select * from bookmarkDB", null);
        //查询语句也可以这样写
        Cursor cursor = db.query(tablename, null, null, null, null, null, "id desc");
        if (cursor != null && cursor.getCount() > 0) {
            if_exist=true;
            while(cursor.moveToNext()) {
                get_id=cursor.getInt(0);//得到int型的自增变量
                get_title = cursor.getString(1);
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("title", get_title);
                arrayList.add(get_id);
                listItem.add(map);
                //new String  数据来源， new int 数据到哪去
                SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem,R.layout.bookmarkzujian,
                        new String[] {"title","url"},
                        new int[] {R.id.title,R.id.url});
                folder_list.setAdapter(mSimpleAdapter);//为ListView绑定适配器
            }
        }
        else {
            if_exist=false;
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("title", "暂无标识");
            map.put("url", "");
            listItem.add(map);
            //new String  数据来源， new int 数据到哪去
            SimpleAdapter mSimpleAdapter = new SimpleAdapter(this,listItem,R.layout.bookmarkzujian,
                    new String[] {"title","url"},
                    new int[] {R.id.title,R.id.url});
            folder_list.setAdapter(mSimpleAdapter);//为ListView绑定适配器
        }

        cursor.close();
        db.close();
    }
    //按值查找：
    public String query_by_id(int position,String tablename){
        dbHelper = new MyDatabaseHelper(this,"dmbrowser.db",null,1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from "+tablename, null);
        while (cursor.moveToNext()) {
            url = cursor.getString(2);
            titler=cursor.getString(1);
            id=cursor.getInt(0);
            //找到id相等的就返回url
            if (arrayList.get(position).equals(id))
                break;
        }
        cursor.close();
        db.close();
        return url;
    }




    @Override
    protected void onRestart() {
        super.onRestart();

    }
}